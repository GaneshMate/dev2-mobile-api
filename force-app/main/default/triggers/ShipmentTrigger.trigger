trigger ShipmentTrigger on zkfedex__Shipment__c (after insert) {

    if(Trigger.isAfter && Trigger.isInsert)
    {
        ShipmentTriggerHandler objHandler = new ShipmentTriggerHandler();
        objHandler.afterInsert(Trigger.New);
    }
}