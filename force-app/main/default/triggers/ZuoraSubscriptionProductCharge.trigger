trigger ZuoraSubscriptionProductCharge on Zuora__SubscriptionProductCharge__c (before delete, after insert, after update, after delete) {

    if(Trigger.isAfter){
        SubscriptionTriggerHandler.afterChange(Trigger.isDelete ? Trigger.old : Trigger.new);
    }
    
}