trigger CaseTrigger on Case (before insert, before update, after update) {
	if (Trigger.isBefore) CaseTriggerHandler.beforeInsert(Trigger.new);
	if (Trigger.isUpdate && Trigger.isAfter) CaseTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
}