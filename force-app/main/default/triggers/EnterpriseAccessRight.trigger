trigger EnterpriseAccessRight on Enterprise_Access_Right__c (after insert,before delete) {

    if(Trigger.isAfter){
        if(trigger.isinsert){
            EnterpriseAccessRightTriggerHandler.Afterinsert(Trigger.new);  
        }
    }else if(Trigger.isBefore){
        if(Trigger.isDelete){
            EnterpriseAccessRightTriggerHandler.beforeDelete(Trigger.old);          
        }
    }
}