trigger syncContract on SBQQ__Subscription__c (after insert) {
    Map<String,Map<string,integer>> ContractSubscriptionMap = new  Map<String,Map<string,integer>>();
    for(SBQQ__Subscription__c subs : Trigger.new)
    {
        if(!ContractSubscriptionMap.containsKey(subs.SBQQ__Contract__c))
        {
            ContractSubscriptionMap.put(subs.SBQQ__Contract__c,new Map<string,integer>());
        }
        ContractSubscriptionMap.get(subs.SBQQ__Contract__c).put(subs.id,0);
    }
    
    Map<ID,Contract> allContractsMap = new Map<ID,Contract>([select id,SBQQ__Quote__r.SBQQ__LineItemCount__c,(select id from SBQQ__Subscriptions__r) from Contract where id IN: ContractSubscriptionMap.keySet()]);
    
    List<Contract> conToUpdateList = new List<Contract>();
    System.debug('------------------->>SubscriptionOut');
    for(String ConId : ContractSubscriptionMap.keySet()){
        if(allContractsMap.get(ConId).SBQQ__Quote__r.SBQQ__LineItemCount__c == allContractsMap.get(ConId).SBQQ__Subscriptions__r.size() )
        {
            allContractsMap.get(ConId).Status ='Activated';
            allContractsMap.get(ConId).ZSB__SubscribeToZuora__c = true;        
            conToUpdateList.add(allContractsMap.get(ConId));
            System.debug('------------------->>SubscriptionOut');
        }
        //SBQQ__LineItemCount__c
    }  
    update conToUpdateList;
}