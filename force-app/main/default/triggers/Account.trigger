/**
 * upon insert, look for unconverted lead and convert it and then map to this newly inserted account
 * @param  insert [description]
 * @return        [description]
 */
trigger Account on Account (before insert, before update,after insert, after update, before delete) {
    if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            AccountTriggerHandler.afterInsert(Trigger.new);
        }
        if (Trigger.isUpdate) {
            AccountTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
        }
    } else if (Trigger.isBefore) {
        if (Trigger.isInsert) { // calculate the enterprise and application user id
            AccountTriggerHandler.beforeInsert(Trigger.new);
        }
        if(Trigger.isInsert || Trigger.isUpdate){
            set<string> emailset = new set<string>();
           for(Account objAccount : Trigger.new){
               objaccount.Account_Currency__c =objAccount.CurrencyISOCode;
               emailset.add(objaccount.personemail);
           }
           list<user> lstuser = new list<user>();
           if(Trigger.isinsert){
               lstuser = [select id, email from user where email in:emailset and isactive=true and accountid !=null];
           }
           /*else if(trigger.isupdate){
               lstuser =[select id, email from user where email in:emailset and isactive=true and accountid !=null and accountid not in:trigger.newmap.keyset()];
           }*/
          /* for(Account objAct : Trigger.new){
               if(lstuser.size()>0 && !test.IsRunningTest()){
                   objAct.personemail.addError('Email already exists');
               }
           }*/
        }
        if (Trigger.isDelete) {
            AccountTriggerHandler.beforeDelete(Trigger.old);
        }
    }

}