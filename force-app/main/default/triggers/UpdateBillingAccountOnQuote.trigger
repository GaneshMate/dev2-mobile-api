trigger UpdateBillingAccountOnQuote on SBQQ__Quote__c (Before insert, before update,After update) {
//updatebilling Account
          if((trigger.isUpdate || trigger.isinsert) && trigger.isBefore){
               set<id> setAccount = new set<id>();
              
                for(SBQQ__Quote__c objQuote :trigger.new){
                               
                    if(objQuote .SBQQ__Account__c != null){
                        setAccount.add(objQuote .SBQQ__Account__c);
                        
                    }
                    
                    if(setAccount.size()>0){
                        map<id,Zuora__CustomerAccount__c> mapAccountwithBillingAccount = new map<id,Zuora__CustomerAccount__c>();
                        for(Account objAccount :[select id,CurrencyISOCode,(select id,CurrencyIsoCode from R00N40000001kyLcEAI__r where  Zuora__DefaultPaymentMethod__c='CreditCard' order by createddate DESC limit 1) from Account where id in:setAccount]){
                           if(objAccount.R00N40000001kyLcEAI__r.size()>0)
                            mapAccountwithBillingAccount.put(objAccount.id, objAccount.R00N40000001kyLcEAI__r[0] );
                        }
                        for(Account objAccount :[select id,CurrencyISOCode,(select id,CurrencyIsoCode from R00N40000001kyLcEAI__r where Zuora__DefaultPaymentMethod__c !='Other' order by createddate DESC limit 1) from Account where id in:setAccount and id not in: mapAccountwithBillingAccount.keyset()]){
                            if(objAccount.R00N40000001kyLcEAI__r.size()>0)
                            mapAccountwithBillingAccount.put(objAccount.id, objAccount.R00N40000001kyLcEAI__r[0] );
                        }
                        boolean ispromo =false;
                        for(SBQQ__Quote__c objQuotenew: trigger.new){
                            if(objQuotenew.SBQQ__Account__c != null && mapAccountwithBillingAccount.get(objQuotenew.SBQQ__Account__c) != null ){
                                if(objQuotenew.Custom_Billing_Account__c != null){
                                    if(!test.isRunningtest())
                                    objQuotenew.ZSB__BillingAccount__c = objQuotenew.Custom_Billing_Account__c;
                                }
                                else if(objQuotenew.ZSB__BillingAccount__c == null  ){
                                   if(!test.isRunningtest())
                                    objQuotenew.ZSB__BillingAccount__c = mapAccountwithBillingAccount.get(objQuotenew.SBQQ__Account__c).id;
                                    objQuotenew.Custom_Billing_Account__c = mapAccountwithBillingAccount.get(objQuotenew.SBQQ__Account__c).id;
                                }                                
                                if(objQuotenew.SBQQ__Type__c =='Amendment')
                                    objQuotenew.CurrencyISOCode = mapAccountwithBillingAccount.get(objQuotenew.SBQQ__Account__c).CurrencyISOCode;
                                    
                            }
                        }
                    }
                }
          }
            //end of update billling Account
}