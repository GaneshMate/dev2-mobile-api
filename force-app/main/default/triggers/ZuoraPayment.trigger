trigger ZuoraPayment on Zuora__Payment__c (after insert, after update, after delete, after undelete) {
	if (!Trigger.isDelete) ZuoraPaymentTriggerHandler.rollupTotalAmount(Trigger.new);
	else ZuoraPaymentTriggerHandler.rollupTotalAmount(Trigger.old);
}