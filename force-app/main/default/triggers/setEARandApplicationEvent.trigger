trigger setEARandApplicationEvent on SBQQ__Subscription__c (before insert,before update,before delete, after insert, after update, after delete) {
    // AK(160318):: Subscription__c :: Executes the logic only if trigger is ON from the custom settings.
    if (isDisableRatePlanIdTrigger()) {
        return;
    }
    // To populate rateplan Id
    if(Trigger.isbefore &&(Trigger.isinsert || trigger.isupdate)){
            
        set<id> setproductids= new set<id>();
        map<id, Pricebookentry> mapProdwithPBE = new map<id,Pricebookentry >();
        for(SBQQ__Subscription__c  objsub :trigger.new){
            if(objsub.ZSB__PRPlanId__c== null || objsub.ZSB__PRPChargeId__c== null){
                setproductids.add(objsub.SBQQ__Product__c);
            }
        }
        PriceBookIds__c stdPriceBookId = PriceBookIds__c.getValues('VectorVest Price Book 2018');
        string strPricebookId ;
        if(stdPriceBookId != null) {         
            strPricebookId = stdPriceBookId.Price_Book_Id__c;
        }
        for(Product2 objProduct:[select id, (SELECT ID,UnitPrice, ZSB__PRPChargeId__c, ZSB__PRPlanId__c FROM PricebookEntries  where Pricebook2Id = :strPricebookId ) from product2 where id in:setproductids]){
            if(objproduct.PricebookEntries.size()>0)
                mapProdwithPBE.put(objproduct.id, objproduct.PricebookEntries[0]);
        }
        for(SBQQ__Subscription__c  objsub :trigger.new){
            if(objsub.ZSB__PRPlanId__c== null || objsub.ZSB__PRPChargeId__c== null){
                if(mapProdwithPBE.get(objsub.SBQQ__Product__c) != null){
                    objsub.ZSB__PRPlanId__c= mapProdwithPBE.get(objsub.SBQQ__Product__c).ZSB__PRPlanId__c;
                    objsub.ZSB__PRPChargeId__c=mapProdwithPBE.get(objsub.SBQQ__Product__c).ZSB__PRPChargeId__c;
                }
                
            }
        }
        
      }
    
    // To populate rateplanId
    
    if(Trigger.isbefore && (trigger.isinsert || trigger.isupdate)){
        system.debug('enetrrrr 1');
        set<id> setQuotelineid= new set<id>();
        set<id> setAccountid = new set<id>();
        system.debug('Test1');
        map<string,date> mapQuotelinewithterminateddate = new map<string,date>();
        system.debug('Test2');
        map<string,string> mapEmail = new map<string,string>();
        for(SBQQ__Subscription__c objsub : trigger.new){
            system.debug('Test3');
            setQuotelineid.add(objsub.SBQQ__QuoteLine__c);
            if(objsub.email__c == null || objsub.Email__c ==''){
                system.debug('Test4');
                setAccountid.add(objsub.SBQQ__Account__c);
            }
            system.debug('Test6');
        }
        if(setAccountid.size()>0){
            system.debug('enetrrrr 2');
            for(Account objAccount : [select id,Personemail from Account where id in:setAccountid]){
                mapEmail.put(objaccount.id,objAccount.personemail);
            }
        }
        for(SBQQ__QuoteLine__c  objQuoteline:[select id,TerminatedDate__c from SBQQ__QuoteLine__c where id in:setQuotelineid]){
            system.debug('enetrrrr 3');
            mapQuotelinewithterminateddate.put(objQuoteline.id,objQuoteline.TerminatedDate__c );
        }
        //AK (19/12) :: Commented the code as it isn't allowing to create a subscription using Mobile API
        for(SBQQ__Subscription__c objsub : trigger.new){
            //populating email field from account email
            if(objsub.SBQQ__Account__c != null && (objsub.email__c == null || objsub.email__c =='') && mapemail.size()>0){
                objsub.email__c = mapemail.get(objsub.SBQQ__Account__c);
            }
           // list<SBQQ__QuoteLine__c > lstQuoteLine = new List<SBQQ__QuoteLine__c >();
            //lstQuoteLine  =[select id, TerminatedDate__c,SBQQ__ListPrice__c,SBQQ__NetPrice__c from SBQQ__QuoteLine__c where id=:objsub.SBQQ__QuoteLine__c];
            //if(lstQuoteLine.size()>0 && lstQuoteLine[0].TerminatedDate__c != null)
            if(mapQuotelinewithterminateddate.size()>0 && mapQuotelinewithterminateddate.get(objsub.SBQQ__QuoteLine__c)!= null){
                    objsub.SBQQ__TerminatedDate__c= mapQuotelinewithterminateddate.get(objsub.SBQQ__QuoteLine__c);                
            }
        }
    }
    if(Trigger.isAfter){
        system.debug('enetrrrr 4');
        //uncommented
        //if(trigger.isinsert){
        list<SBQQ__Subscription__c> lstsubtoUpdate = new list<SBQQ__Subscription__c >();
        for(SBQQ__Subscription__c objSub:trigger.new){
            if(objSub.SBQQ__Quantity__c == -1 && objSub.SBQQ__RevisedSubscription__c != null && objSub.SBQQ__TerminatedDate__c != null){
                system.debug('enterrrrrr');
                SBQQ__Subscription__c objRevisedsub = new SBQQ__Subscription__c(id=objsub.SBQQ__RevisedSubscription__c,SBQQ__TerminatedDate__c=objsub.SBQQ__TerminatedDate__c);
                lstsubtoUpdate.add(objRevisedsub);
                //update objRevisedsub;
            }
        }
        if(lstsubtoUpdate.size()>0)
        update lstsubtoUpdate;
        //}
        //uncommnted
        //SubscriptionTriggerHandlerWithEAR.afterChange(Trigger.isDelete ? Trigger.old : Trigger.new);
        if(trigger.isinsert){
            system.debug('enetrrrr 5');
            SubscriptionTriggerHandlerWithEAR.updateContractTermtype(trigger.new);
        }
        if(trigger.isinsert || trigger.isupdate){
            system.debug('enetrrrr 6');
           SubscriptionTriggerHandlerWithEAR.insertCustomSubscription(trigger.new);
        }
  }else if(Trigger.isBefore){
    if(Trigger.isDelete){
        system.debug('enetrrrr 7');
      // Earlier isObservable from account is used to check related subscriptions, Check why we have to use that and check
      //if(sub.Is_Observable__c) subs.add(sub);
      CustomerInfoChangeEventHandler.customerSubscriptionDeletedForCPQ(Trigger.old);  
            
    }
  }
  //LN: Code to populate termination date on contract when subscription get insertd with termination date
  if(Trigger.isInsert && Trigger.isAfter){
  system.debug('enetrrrr');
  Set<Id> contractIdSet = new Set<Id>();
  map<id, date> mapcontratidwithtermdate = new map<id,date>();
        
        // Getting all subscriptions having quantity -1 and not amended already
        for (SBQQ__Subscription__c subscription :Trigger.New) {
            
            if(subscription.SBQQ__Contract__c != null  && subscription.SBQQ__TerminatedDate__c != null){
                system.debug('enetrrrr 8');
                if(mapcontratidwithtermdate.get(subscription.SBQQ__Contract__c)==null)
                    mapcontratidwithtermdate.put(subscription.SBQQ__Contract__c,subscription.SBQQ__TerminatedDate__c);
                   
            }
        }
        
        if(mapcontratidwithtermdate.isEmpty()) {
            return;
        }
        
        Contract[] contractList = [SELECT Id
                                        , TerminatedDate__c
                                     FROM Contract 
                                    WHERE Id IN :mapcontratidwithtermdate.keyset()];
        
        if(contractList == null) {
            return;
        }
        
        for (Contract contract :contractList) {
            
               contract.Terminateddate__c= mapcontratidwithtermdate.get(contract.id); 
            
        }
        
        try {
            update contractList;
        } catch(Exception ex) {
            System.debug('Exception ::: ' + ex.getMessage());
            
        }
      
  }
    // AK :: 16-Mar-18 :: Send to Zuora is not getting unchecked in production hence adding the logic to make it work. 
    /*if (Trigger.isInsert && Trigger.isAfter) {
        
        Set<Id> contractIdSet = new Set<Id>();
        
        // Getting all subscriptions having quantity -1 and not amended already
        for (SBQQ__Subscription__c subscription :Trigger.New) {
            if(subscription.SBQQ__Contract__c == null || subscription.ZSB__AmendmentId__c != null || subscription.SBQQ__Quantity__c >= 0 ){
                continue;
            }
            contractIdSet.add(subscription.SBQQ__Contract__c);
        }
        
        if(contractIdSet.isEmpty()) {
            return;
        }
        
        Contract[] contractList = [SELECT Id
                                        , ZSB__SubscribeToZuora__c 
                                     FROM Contract 
                                    WHERE Id IN :contractIdSet];
        
        if(contractList == null) {
            return;
        }
        
        for (Contract contract :contractList) {
            if(contract.ZSB__SubscribeToZuora__c == TRUE) {
               contract.ZSB__SubscribeToZuora__c = FALSE; 
            }
        }
        
        try {
            update contractList;
        } catch(Exception ex) {
            System.debug('Exception ::: ' + ex.getMessage());
            for (SBQQ__Subscription__c subscription :trigger.new) {
                subscription.addError(ex.getMessage());
            }
        }
    } */
 
    //AK(160318) :: 
    // Disable_RatePlanId_Trigger__c == TRUE (Default) ::Disables the logic for the Rate Plan Id
    // Disable_RatePlanId_Trigger__c == FALSE :: Enables the logic and populates Rate Plan Id and Charge Id on Subscription.  
    private Boolean isDisableRatePlanIdTrigger() {
        Subscription__c subscriptionSetting = Subscription__c.getInstance();
        if(subscriptionSetting == null) {
            subscriptionSetting = new Subscription__c(Disable_RatePlanId_Trigger__c = false);
        }
        
        return subscriptionSetting.Disable_RatePlanId_Trigger__c;
    }
}