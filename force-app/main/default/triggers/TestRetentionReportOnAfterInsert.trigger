trigger TestRetentionReportOnAfterInsert on TestRetentionReport__c (after insert) {
    List<Integer> monthNumbers = new List<Integer> {1,3};
    
    List<TestRetentionReport__c> insertedRows = [SELECT 
                                                    Id, TestCurrentTotalSubscriber1MonthLookup__c, TestCurrentTotalSubscriber3MonthLookup__c
                                                FROM
                                                    TestRetentionReport__c 
                                                WHERE ID IN : Trigger.newMap.keySet()];
    
    for(TestRetentionReport__c retentionReportRow: insertedRows ){

            for(Integer monthNumber:monthNumbers){
                TestTotalSubscribersCurrent__c currentRow= [SELECT 
                                                                    Id,Name,StartDate__c,CurrentTotalSubscribers__c
                                                                FROM 
                                                                    TestTotalSubscribersCurrent__c 
                                                                WHERE 
                                                                    Month__c=:monthNumber 
                                                                LIMIT 1]; 
                if(monthNumber==1){
                    retentionReportRow.TestCurrentTotalSubscriber1MonthLookup__c=currentRow.Id;
                }else{
                    if(monthNumber==3){
                     retentionReportRow.TestCurrentTotalSubscriber3MonthLookup__c=currentRow.Id;   
                    }
                }
            update retentionReportRow;
        }    
        }
}