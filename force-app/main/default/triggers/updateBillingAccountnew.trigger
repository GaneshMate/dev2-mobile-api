trigger updateBillingAccountnew on Opportunity (Before insert,Before update) {
    set<id> setAccount = new set<id>();
    for(opportunity opp :trigger.new){
        if(opp.AccountId != null){
            setAccount.add(opp.Accountid);
        }
        if(setAccount.size()>0){
            map<id,Zuora__CustomerAccount__c> mapAccountwithBillingAccount = new map<id,Zuora__CustomerAccount__c>();
            for(Account objAccount :[select id,(select id,CurrencyISOCode from R00N40000001kyLcEAI__r where Zuora__PaymentMethod_Type__c ='Credit Card' order by createddate DESC limit 1) from Account where id in:setAccount]){
               if(objAccount.R00N40000001kyLcEAI__r.size()>0)
                mapAccountwithBillingAccount.put(objAccount.id, objAccount.R00N40000001kyLcEAI__r[0] );
            }
            for(opportunity oppnew:trigger.new){
                if(opp.accountid != null && mapAccountwithBillingAccount.get(oppnew.accountid) != null){
                opp.ZSB__BillingAccount__c = mapAccountwithBillingAccount.get(oppnew.accountid).id;
                if(opp.Created_By_PortalCustomer__c==true && opp.createdbyid=='00511000004ORyc')
                    opp.CurrencyISOCode = 'USD';
                else                   
                    opp.CurrencyISOCode = mapAccountwithBillingAccount.get(oppnew.accountid).CurrencyISOCode;
                }
            }
        }
    }
}