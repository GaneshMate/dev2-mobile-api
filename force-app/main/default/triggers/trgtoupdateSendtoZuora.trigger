trigger trgtoupdateSendtoZuora on Contract (after update) {
    list<Contract> lstContract = new list<Contract>();
    map<id,list<SBQQ__Subscription__c>> mapContractwithSub = new map<id,list<SBQQ__Subscription__c>>();
    for(SBQQ__Subscription__c objSub:[select id, SBQQ__Contract__C from SBQQ__Subscription__c where  ZSB__AmendmentId__c= null and SBQQ__Quantity__c < 0 and SBQQ__Contract__c in:trigger.newmap.keyset()])
    {
        if(mapContractwithSub.get(objsub.SBQQ__Contract__c)== null){
            mapContractwithSub.put(objsub.SBQQ__Contract__c, new list<SBQQ__Subscription__c>{objsub});
            
        }
        else{
            mapContractwithSub.get(objsub.SBQQ__Contract__c).add(objsub);
        }
    }
    for(Contract objContract :trigger.new){
        if(mapContractwithSub.get(objContract.id) != null && objcontract.ZSB__SubscribeToZuora__c == true){
            Contract objContract1 = new Contract(id=objContract.id,ZSB__SubscribeToZuora__c  = false);
            lstContract.add(objcontract1);
        }
    }
    if(lstContract.size()>0){
        update lstContract;
    }
}