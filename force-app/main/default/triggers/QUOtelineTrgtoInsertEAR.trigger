trigger QUOtelineTrgtoInsertEAR on SBQQ__QuoteLine__c (after insert) {    
        
       set<id> setproductid = new set<id>();
       Map<string,Enterprise_Access_Right__c> uniqueEARForAccounts = new Map<string,Enterprise_Access_Right__c>();
       if(!test.isrunningtest()){
       list<SBQQ__QuoteLine__c> lstquoteline = [select id,SBQQ__Product__r.istrial__c,SBQQ__Quote__r.Ecomm_Quote__c,SBQQ__Quote__r.SBQQ__Type__c,SBQQ__Quote__r.SBQQ__Account__c from SBQQ__QuoteLine__c where id in:trigger.newmap.keyset() and SBQQ__Product__r.istrial__c=true and SBQQ__Quote__r.Ecomm_Quote__c=true and SBQQ__Quote__r.SBQQ__Type__c!='Amendment' and SBQQ__Quote__r.Promo_code__c =null];
         for(SBQQ__Quoteline__c sfQL : lstquoteline){
            // system.debug('dateeeeee'+sfQL.SBQQ__Startdate__c);
             //if(sfQL.SBQQ__Product__r.istrial__c && sfQL.SBQQ__Quote__r.Ecomm_Quote__c && sfQl.SBQQ__Quote__r.SBQQ__Type__c!='Amendment'){
                 setproductid .add(sfql.SBQQ__Product__c);
            // }
         }
         List<Enterprise_Access_Mapping__c> eaMappinglist = [SELECT Mask__c, ClaimValue__c, Product__c, VV_Market__c, Is_Trial__c FROM Enterprise_Access_Mapping__c where Product__c IN: setproductid order by is_trial__c];
         
        
        for(Enterprise_Access_Mapping__c eamObj : eaMappinglist ){
            
            for(SBQQ__QuoteLine__c objQL : lstquoteline)
            {
                string uniqueEARForAccString = objQL.SBQQ__Quote__r.SBQQ__Account__c + eamObj.Mask__c + eamObj.ClaimValue__c;
                if(!uniqueEARForAccounts.containsKey(uniqueEARForAccString))
                {
                    uniqueEARForAccounts.put(uniqueEARForAccString , new Enterprise_Access_Right__c(Account__c = ObjQL.SBQQ__Quote__r.SBQQ__Account__c, Mask__c = eamObj.Mask__c, ClaimValue__c = eamObj.ClaimValue__c,Is_Trial__c=eamObj.Is_Trial__c));
                }
            }
        }
        }
        if (!uniqueEARForAccounts.isEmpty() && !test.isrunningtest()) {
            insert uniqueEARForAccounts.values();
        }

}