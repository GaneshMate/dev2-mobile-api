trigger BundleTrigger on Bundle__c (before insert, before update) {
	vvBundleController.autoSetExisting(Trigger.new);
}