trigger updateForEcommerce on SBQQ__Quote__c (before update,after update) {
          System.debug('Outside---->>');
          for(SBQQ__Quote__c q : Trigger.new)
          {
              if(Trigger.isBefore)
              {
                  if(!q.SBQQ__Primary__c && q.SiteUser_Primary_Check__c && !Trigger.oldMap.get(q.id).SiteUser_Primary_Check__c){
                      System.debug('Inside Before---->>');
                      q.SBQQ__Primary__c = true;                  
                  }
              }
              if(Trigger.isAfter)
              {
                  if(q.SiteUser_Primary_Check__c && !Trigger.oldMap.get(q.id).SiteUser_Primary_Check__c){
                      System.debug('Inside After---->>');
                      //ZSB.ZSBConnectorUtils.createContractFromQuote(q.id);
                      string oppId = q.SBQQ__Opportunity2__c;
                      if(!String.isEmpty(oppId)){
                        List<Opportunity> oppList = [select id,StageName,SBQQ__Contracted__c,SBQQ__CreateContractedPrices__c from opportunity where id =:oppId ];
                        if(oppList.size()>0)
                        {
                            oppList[0].StageName = 'Closed Won';
                            oppList[0].SBQQ__Contracted__c = true;
                            oppList[0].SBQQ__CreateContractedPrices__c = true;
                            update oppList[0];         
                        }
                      }
                                        
                  }
              }
          }
}