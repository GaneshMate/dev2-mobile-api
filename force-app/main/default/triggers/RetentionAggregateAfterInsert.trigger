trigger RetentionAggregateAfterInsert on RetentionAggregate__c (after insert) {
    /*
        //Get the inserted rows
        List<RetentionAggregate__c> newRetentionAggregateRows;
    
            newRetentionAggregateRows= 
            [SELECT 
                Id
                , Date__c
                , LookupOneMonth__c
                , LookupThreeMonth__c
                , LookupSixMonth__c
                , LookupTwelveMonth__c
             FROM
                RetentionAggregate__c 
             WHERE 
                Id IN : Trigger.newMap.keySet()
            ];
            
            
        List<Integer> months= new List<Integer> {1,3,6,12};
        
        for(RetentionAggregate__c newRetentionAggregateRow: newRetentionAggregateRows){
            for(Integer month: months) {
                //find the row that is x months back
                Date dateXMonthsAgo = newRetentionAggregateRow.Date__c.AddMonths(-1 * month);                
                List<RetentionAggregate__c> rowXMonthsBack = [SELECT 
                            Id
                           FROM 
                            RetentionAggregate__c 
                           WHERE 
                            Date__c <=:dateXMonthsAgo 
                           ORDER BY 
                            Date__c DESC 
                           LIMIT 1];
                
                if(rowXMonthsBack!=NULL && rowXMonthsBack.size()>0 && rowXMonthsBack[0]!=NULL){
                    //if we found a row X months back...
                    if(month==1){
                        newRetentionAggregateRow.LookupOneMonth__c = rowXMonthsBack[0].Id;
                    }else{
                        if(month==3){
                            newRetentionAggregateRow.LookupThreeMonth__c = rowXMonthsBack[0].Id;    
                        }else{
                            if(month==6){
                                newRetentionAggregateRow.LookupSixMonth__c = rowXMonthsBack[0].Id;
                            }else{
                                if(month==12){
                                    newRetentionAggregateRow.LookupTwelveMonth__c = rowXMonthsBack[0].Id;
                                }
                            }
                        }
                    }
                    update newRetentionAggregateRow;
                }
            }
        }
        */
}