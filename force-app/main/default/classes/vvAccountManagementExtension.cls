public with sharing class vvAccountManagementExtension {

    public vvAccountManagementExtension(AddBillingActOrPaymentmethodCon controller) {
        this.a = controller.a;
        populateCountries();
        refreshState();
    }


    Account a;

    public List<SelectOption> countries {get; private set;}
    Map<String,Boolean> eumap = new Map<String, Boolean>();
    Map<String,Boolean> gbpmap = new Map<String, Boolean>();

    public Boolean showGBP {get; private set;}
    public Boolean showEUR {get; private set;}

    // currency
    public Boolean showcurrency_selection {get; private set;}

    vvCSRAccountManagementController csr_ctl;

    public vvAccountManagementExtension(vvCSRSubscriptionController sub_ctl) {
        this.a = sub_ctl.a;
        populateCountries();
        refreshState();
    }
    
     public vvAccountManagementExtension(vvCSRAccountManagementControllerClone sub_ctl) {
        this.a = sub_ctl.a;
        populateCountries();
        refreshState();
    }

   /* public vvAccountManagementExtension(CommunitiesSelfRegController communities_ctl) {
        this.a = communities_ctl.a;
        populateCountries();
        refreshState();
    }*/

   /* public vvAccountManagementExtension(newCommunitiesSelfRegController communities_ctl) {
        this.a = communities_ctl.a;
        populateCountries();
        refreshState();
    }
    
     public vvAccountManagementExtension(CommunitiesSelfRegControllerUIUpdate communities_ctl) {
        this.a = communities_ctl.a;
        populateCountries();
        refreshState();
    }
     public vvAccountManagementExtension(CommunitiesSelfRegFinal communities_ctl) {
        this.a = communities_ctl.a;
        populateCountries();
        refreshState();
    }*/
     public vvAccountManagementExtension(account obj) {
         this.a = obj;
        populateCountries();
        refreshState();
    }
    
    /*public vvAccountManagementExtension(CommunitiesSelfRegFinalNewClass communities_ctl) {
        this.a = communities_ctl.a;
        populateCountries();
        refreshState();
    }
    
    public vvAccountManagementExtension(EcommHome communities_ctl) {
        this.a = communities_ctl.a;
        populateCountries();
        refreshState();
    }*/
    public vvAccountManagementExtension(Ecommnew_temp communities_ctl) {
        this.a = communities_ctl.a;
        populateCountries();
        refreshState();
    }
    /*
    public vvAccountManagementExtension(EcommHomeSync communities_ctl) {
        this.a = communities_ctl.a;
        populateCountries();
        refreshState();
    }*/
   /* public vvAccountManagementExtension(Ecommnew communities_ctl) {
        this.a = communities_ctl.a;
        populateCountries();
        refreshState();
    }
     public vvAccountManagementExtension(EcommnewOld communities_ctl) {
        this.a = communities_ctl.a;
        populateCountries();
        refreshState();
    }
     public vvAccountManagementExtension(Ecommnew_temp1 communities_ctl) {
        this.a = communities_ctl.a;
        populateCountries();
        refreshState();
    }*/
    public vvAccountManagementExtension(vvCSRAccountManagementController csr_ctl) {
        this.csr_ctl = csr_ctl;
        this.a = csr_ctl.a;
        populateCountries();
        refreshState();
    }

   /* public vvAccountManagementExtension(CommunitiesUserCentralController csr_ctl) {
        this.a = csr_ctl.a;
        populateCountries();
    }*/
     public vvAccountManagementExtension(CommunitiesUserCentralControllerNew csr_ctl) {
        this.a = csr_ctl.a;
        populateCountries();
    }
   
     /*public vvAccountManagementExtension(CommunitiesUserCentralControllerExisting csr_ctl) {
        this.a = csr_ctl.a;
        populateCountries();
    }*/

    public vvAccountManagementExtension(OneTimePurchaseController csr_ctl) {
        this.a = csr_ctl.a;
        populateCountries();
    }
    
    public void populateCountries() {
        countries = new List<SelectOption>();
        //countries.add(new SelectOption('-1','Select a country'));
        
        for (Country__c cty : [SELECT Name,VV_Country_Code__c,Is_EU__c,Is_GBP__c FROM Country__c ORDER BY Sort__c]) {
            eumap.put(cty.Name,cty.Is_EU__c);  // for displaying or hiding currency selection for europe
            gbpmap.put(cty.Name,cty.Is_GBP__c); // for GBP currencies
            countries.add(new SelectOption(cty.Name,cty.Name));
        }
    
    }

    public List<SelectOption> getStates() {
        List<SelectOption> options = new List<SelectOption>();
         options.add(new SelectOption('','Select a State')); //Added by Lalitha
        for (State__c st : [SELECT Name,VV_State_Code__c FROM State__c WHERE Country__r.Name = :a.BillingCountry ORDER BY Sort__c]) {
            options.add(new SelectOption(st.Name,st.Name));
        }

        return options;
    }
    public List<SelectOption> getShippingStates() {
        List<SelectOption> options = new List<SelectOption>();

        for (State__c st : [SELECT Name,VV_State_Code__c FROM State__c WHERE Country__r.Name = :a.ShippingCountry ORDER BY Sort__c]) {
            options.add(new SelectOption(st.Name,st.Name));
        }

        return options;
    }

    public List<SelectOption> getSalutations() {
        List<SelectOption> options = new List<SelectOption>();

        options.add(new SelectOption('',''));
        options.add(new SelectOption('Mr.','Mr.'));
        options.add(new SelectOption('Ms.','Ms.'));
        options.add(new SelectOption('Mrs.','Mrs.'));
        options.add(new SelectOption('Dr.','Dr.'));
        return options;
    } 


    /**
     * select option list for CSR page
     * @return [description]
     */
    public List<SelectOption> getMarketSelectList() {
        List<SelectOption> options = new List<SelectOption>();
        for (String label : getMarketStringList()) {
            options.add(new SelectOption(label,label));
        }
        return options;
    }

    public List<SelectOption> getBonusMarketSelectList() {
        List<SelectOption> options = new List<SelectOption>();
        for (String label : getMarketStringList()) {
            if (csr_ctl.selectedMarket != null && csr_ctl.selectedMarket != label) options.add(new SelectOption(label,label));
        }
        return options;
    }


    /**
     * string list for community page
     * @return [description]
     */
    public List<String> getMarketStringList() {
        List<Schema.PicklistEntry> fieldResult = Schema.SobjectType.Account.fields.getMap().get('VV_Market__c').getDescribe().getPicklistValues();   
        List<String> returnlist = new List<String>();
        for (Schema.PicklistEntry f : fieldResult) {
            if(f.getValue() !='SA'){
                returnlist.add(f.getValue());
            }
        }       
        return returnlist;
    }

    public List<SelectOption> getFeedTypes() { 
        List<Schema.PicklistEntry> fieldResult = Schema.SobjectType.Account.fields.getMap().get('VV_Feed__c').getDescribe().getPicklistValues();   
        List<SelectOption> options = new List<SelectOption>();
        for (Schema.PicklistEntry f : fieldResult) {
            if (csr_ctl != null && csr_ctl.selectedMarket != 'US' && csr_ctl.selectedMarket != 'CA' && f.getValue() != 'EOD') 
                continue;

            options.add(new SelectOption( f.getValue(),vvConstants.convertFeedText(f.getLabel()) ));
        }       
        return options;
    }

    public List<SelectOption> getEditionTypes() { 
        List<Schema.PicklistEntry> fieldResult = Schema.SobjectType.Account.fields.getMap().get('VV_Edition__c').getDescribe().getPicklistValues();   
        List<SelectOption> options = new List<SelectOption>();
        for (Schema.PicklistEntry f : fieldResult) {
            if (csr_ctl != null && csr_ctl.selectedMarket != 'US' && csr_ctl.selectedMarket != 'CA' && f.getValue() != 'Non-Professional') 
                continue;
                
            options.add(new SelectOption( f.getValue(),f.getLabel() ));
        }       
        return options;
    }


    public List<SelectOption> getExpirationYears() {
        List<SelectOption> options = new List<SelectOption>();

        for (Integer i=Date.today().year();i<Date.today().year() + 10;i++) {
            options.add(new SelectOption(String.valueOf(i),String.valueOf(i)));
        }
        return options;
    }


    public List<SelectOption> getHowHear() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','Please Select One...'));

        for (HowDidYouHearAboutUs__c h : [SELECT Name FROM HowDidYouHearAboutUs__c WHERE Displayon_E_comSite__c = true ORDER BY DisplayOrder__c]) {
            options.add(new SelectOption(h.Id, h.Name));
        }

        return options;
    }

    public void refreshState() {
        showEUR = false;
        showGBP = false;
        showcurrency_selection = isShowCurrencySelection(); 
        system.debug('showcurrency_selection: ' + showcurrency_selection);
    }
    public void refreshShippingState() {}

    public String getCurrencySymbol() {
        return vvCurrencyManagement.getCurrencySymbol(a);
    } 

    /** 
     * whether to show the currency selection or not
     * @return [description]
     */
    public Boolean isShowCurrencySelection() {
        showcurrency_selection = false;
        // if EU countries, show eu selection
        if (eumap.get(a.BillingCountry) != null && eumap.get(a.BillingCountry)) {
            showcurrency_selection = true;
            showEUR = true;
        } else if (gbpmap.get(a.BillingCountry) != null && gbpmap.get(a.BillingCountry)) {
            showcurrency_selection = true;
            showGBP = true;
        }

        return showcurrency_selection;

    }

}