public class EnterpriseAccessRightTriggerHandler {
 public static void afterinsert(List<Enterprise_Access_Right__c> rightlist) {
        List<Enterprise_Access_Right__c> rights = new List<Enterprise_Access_Right__c>();
        for(Enterprise_Access_Right__c right : rightlist){
            if(right.Is_Observable__c) rights.add(right);
        }
        if(!rights.isEmpty()) CustomerInfoChangeEventHandler.customerAccessRightsinserted(rights);
    }
    
    public static void beforeDelete(List<Enterprise_Access_Right__c> rightlist) {
        List<Enterprise_Access_Right__c> rights = new List<Enterprise_Access_Right__c>();
        for(Enterprise_Access_Right__c right : rightlist){
            if(right.Is_Observable__c) rights.add(right);
        }
        if(!rights.isEmpty()) CustomerInfoChangeEventHandler.customerAccessRightsDeleted(rights);
    }
}