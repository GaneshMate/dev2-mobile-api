@isTest
public class CPQIntegrationLayerHelperTest {
    static testMethod void testCPQIntegrationLayer() {
        Test.StartTest();
            Account acc = new Account();
           acc.firstname='test acc1';
            acc.lastName = 'test acc';
            acc.billingCity ='Test City';
            acc.billingStreet = 'TestStreet';
            acc.billingstate ='TestState';
            acc.billingPostalcode ='23451';
            acc.billingcountry ='United States';
            /*acc.ShippingCity ='Test City';
            acc.ShippingStreet = 'TestStreet';
            acc.Shippingstate ='TestState';
            acc.ShippingPostalcode ='23451';
            acc.Shippingcountry ='United States';*/
            Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            acc.recordtypeID=objAccrec.id;
           acc.PersonMailingCity='Test City';
            acc.PersonMailingStreet = 'TestStreet';
            acc.PersonMailingState ='TestState';
            acc.PersonMailingPostalCode ='23451';
            acc.PersonMailingCountry ='United States';
            //Account acc = new Account();
           // acc.Name = 'test acc';
            insert acc;
            Opportunity opp = new Opportunity(Name=acc.Name + ' Opportunity ' ,
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=acc.Id);
                                       
            Product2 prod = new Product2(Name = 'Laptop X200', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'code1' , Family = 'Books');
            insert prod;
            
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            
            CPQCommunity__c cpc = new CPQCommunity__c();
            cpc.Name = 'test4';
            cpc.PriceBookId__c = pricebookId ;
            
            insert cpc ;
            
            contact c = new contact();
            c.lastname = 'test';
            c.email = 'test@test.com';
            c.AccountId = acc.Id;
            //insert c;
            
            SBQQ__Quote__c quoteRec = new SBQQ__Quote__c();
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SiteUser_Primary_Check__c = true;
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SBQQ__Opportunity2__c = opp.Id;
           // quoteRec.ZSB__BillToContact__c = c.Id;
           // quoteRec.ZSB__SoldToContact__c = c.Id;
            quoteRec.ZSB__BillCycleDay__c =  '1';
            quoteRec.ZSB__BillingBatch__c = 'Batch1';
            insert quoteRec ;
            
            
            CPQIntegrationLayerHelper obj = new CPQIntegrationLayerHelper('test',acc);
            obj.sfQuoteId = 'test';
            obj.sfOpportunityId = opp.Id;
            String str = CPQIntegrationLayerHelper.cpqPriceBookId ;
             CPQIntegrationLayerHelper.cpqPriceBookId = pricebookId ;
            obj.createOpportunity(acc.Id,acc.Name);
            obj.getProductInfo(prod.Id);
            obj.createOppAndQuote(acc.Id,acc.Name);
            obj.createQuote(opp.Id);
            obj.contractOpp(opp.Id);
            List<String> productIdLists = new List<string>();
            productIdLists.add(prod.Id);
            Map<string,string> productOption =new Map<string,string>();
            productOption.put('key','value');
            obj.syncQuoteToOpportunity(quoteRec);
            obj.clonequote(quoterec.id);
            obj.createquoteandopportunity(acc.id,acc.name);
            obj.createcontract(acc.id,acc.name,quoterec.id);
            obj.getQuotemodel(quoterec.id);
            //obj.contractQuoteZuora(quoteRec.Id);
            obj.contractOppZuora(opp.Id);
            
            //CPQIntegrationLayerHelper.syncQuoteToOpportunityWithAdminUser(quoteRec.Id,opp.Id);
        Test.StopTest();
    }
    static testMethod void testCPQIntegrationLayer2() {
        Test.StartTest();
             Account acc = new Account();
            acc.firstname='test acc1';
           acc.lastName = 'test acc';
            acc.billingCity ='Test City';
            acc.billingStreet = 'TestStreet';
            acc.billingstate ='TestState';
            acc.billingPostalcode ='23451';
            acc.billingcountry ='United States';
           Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            acc.recordtypeID=objAccrec.id;
           acc.PersonMailingCity='Test City';
            acc.PersonMailingStreet = 'TestStreet';
            acc.PersonMailingState ='TestState';
            acc.PersonMailingPostalCode ='23451';
            acc.PersonMailingCountry ='United States';
            //Account acc = new Account();
           // acc.Name = 'test acc';
            insert acc;
            Opportunity opp = new Opportunity(Name=acc.Name + ' Opportunity ' ,
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=acc.Id);
                                       
            Product2 prod = new Product2(Name = 'Laptop X200', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'code1' , Family = 'Books');
            insert prod;
            
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            
            CPQCommunity__c cpc = new CPQCommunity__c();
            cpc.Name = 'test5';
            cpc.PriceBookId__c = pricebookId ;
            insert cpc ;
            
            contact c = new contact();
            c.lastname = 'test';
            c.email = 'test@test.com';
            c.AccountId = acc.Id;
            //insert c;
            
            SBQQ__Quote__c quoteRec = new SBQQ__Quote__c();
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SiteUser_Primary_Check__c = true;
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SBQQ__Opportunity2__c = opp.Id;
            quoteRec.ZSB__BillToContact__c = c.Id;
            quoteRec.ZSB__SoldToContact__c = c.Id;
            quoteRec.ZSB__BillCycleDay__c =  '1';
            quoteRec.ZSB__BillingBatch__c = 'Batch1';
            insert quoteRec ;
            
            
            CPQIntegrationLayerHelper obj = new CPQIntegrationLayerHelper('test',acc);
            obj.sfQuoteId = 'test';
            obj.sfOpportunityId = opp.Id;
            obj.contractOpp(opp.Id);
            List<string> productIdList = new List<string> ();
            productIdList.add(prod.Id);
            Map<string,string> productOption = new Map<string,string>();
            productOption.put(prod.Id,prod.Id);
            CPQIntegrationLayerHelper.updateQuoteWithProductsAndSave(quoteRec.Id,productIdList,productOption);
            //obj.updateQuoteWithProductsAndSaveold(quoteRec.Id,productIdList);
            CPQIntegrationLayerHelper.updateQuoteWithProductsAndSaveAsync( quoteRec.Id,productIdList,productOption,productOption);
            CPQAPIClassDefinitions.ProductModel pModel = new CPQAPIClassDefinitions.ProductModel();
            
            SBQQ__ProductOption__c prodOpt = new SBQQ__ProductOption__c();
            prodOpt.SBQQ__Number__c = 10;
            insert  prodOpt ;
            List<CPQAPIClassDefinitions.OptionModel>  OptionModelList = new List<CPQAPIClassDefinitions.OptionModel>();
            CPQAPIClassDefinitions.OptionModel optMod = new CPQAPIClassDefinitions.OptionModel();
            optMod.record = prodOpt ;
            OptionModelList.add(optMod);
            
            //pModel.options = OptionModelList ;
            String prodOptionId = prod.Id ;
            try{
            obj.selectProductOptionFromProductModel( pModel , prodOptionId);
            }Catch(Exception E){
                obj.createContract(acc.Id,acc.Name,productIdList,productOption,productoption);
            }
        Test.StopTest();
    }
    

}