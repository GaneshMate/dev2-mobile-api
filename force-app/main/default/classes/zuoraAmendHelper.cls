/**
 * helper class that handles subscription amendments only
 *
 * no logic in this class, only uses order builder to send in information and return info
 */
public without sharing class zuoraAmendHelper {


    /**
    * cancel subscription:
    */
    public static String cancelSubscription(String subscriptionId, Date canceldate) {
        Zuora.zObject amendment = new Zuora.zObject('Amendment');
        amendment.setValue('Name', 'Cancel Subscription');
        amendment.setValue('SubscriptionId', subscriptionId);
        amendment.setValue('EffectiveDate',Datetime.newInstance(canceldate.year(),canceldate.month(),canceldate.day()).format('YYYY-MM-dd'));
        amendment.setValue('ContractEffectiveDate', DateTime.now().format('YYYY-MM-dd'));
        amendment.setValue('Type', 'Cancellation');
        amendment.setValue('Status', 'Completed');

        Zuora.zApi.AmendResult result = ZuoraAmendHelper.amendApiCall(amendment);
        if (result.success) return 'Successfully Cancelled';
        else return getErrorStr(result.errors);
    }

    /**
     * renewal, used as the first amendment after creating subscription and added trial period
     */
    public static String renewSubscription(String subscriptionId, Date effectivedate) {
        Zuora.zObject amendment = new Zuora.zObject('Amendment');
        amendment.setValue('Name', 'Renewal');
        amendment.setValue('SubscriptionId', subscriptionId);
        amendment.setValue('EffectiveDate',Datetime.newInstance(effectivedate.year(),effectivedate.month(),effectivedate.day()).format('YYYY-MM-dd'));
        amendment.setValue('ContractEffectiveDate',Datetime.newInstance(effectivedate.year(),effectivedate.month(),effectivedate.day()).format('YYYY-MM-dd'));
        amendment.setValue('CustomerAcceptanceDate',Datetime.newInstance(effectivedate.year(),effectivedate.month(),effectivedate.day()).format('YYYY-MM-dd'));
        amendment.setValue('Type', 'Renewal');
        amendment.setValue('Status', 'Completed');

        Zuora.zApi.AmendResult result = ZuoraAmendHelper.amendApiCall(amendment);
        if (result.success) return 'Successfully Renewed';
        else return getErrorStr(result.errors);
    }   

    /**
     * add product rate plan to subscription, this is the simple version, doesn't override the charge
     */
    public static String addRatePlanToSubscription(String subscriptionId, String rateplanId, Date cedate) {
        // rateplan
        Zuora.zObject ratePlan = new Zuora.zObject('RatePlan');
        ratePlan.setValue('ProductRatePlanId', rateplanId);
        // rateplan data
        Zuora.zObject ratePlanData = new Zuora.zObject('RatePlanData'); 
        ratePlanData.setValue('RatePlan', ratePlan);

        // amendment
        Zuora.zObject amendment = new Zuora.zObject('Amendment');
        amendment.setValue('Name', 'Add New Product RatePlan to Subscription');
        amendment.setValue('SubscriptionId', subscriptionId);
        amendment.setValue('ContractEffectiveDate', Datetime.newInstance(cedate.year(),cedate.month(),cedate.day()).format('YYYY-MM-dd'));
        //amendment.setValue('ServiceActivationDate', Datetime.newInstance(cedate.year(),cedate.month(),cedate.day()).format('YYYY-MM-dd'));
        amendment.setValue('Type', 'NewProduct');
        amendment.setValue('Status', 'Completed');
        amendment.setValue('RatePlanData',ratePlanData);

        Zuora.zApi.AmendResult result = ZuoraAmendHelper.amendApiCall(amendment);
        if (result.success) return result.SubscriptionId;
        else return 'Add Rate Plan Error:' + getErrorStr(result.errors);
    }

    /**
     * add product rate plan to subscription with a trigger date, need to be more specific
     */
    public static String addRatePlanToSubscription(String subscriptionId, zqu__ProductRatePlan__c theplan, Date triggerdate) {
        // rateplan
        Zuora.zObject ratePlan = new Zuora.zObject('RatePlan');
        ratePlan.setValue('ProductRatePlanId', theplan.zqu__ZuoraId__c);
        // rateplan data
        Zuora.zObject ratePlanData = new Zuora.zObject('RatePlanData'); 
        ratePlanData.setValue('RatePlan', ratePlan);

        // need to override charge
        Zuora.zObject ratePlanCharge = new Zuora.zObject('RatePlanCharge');
        ratePlanCharge.setValue('ProductRatePlanChargeId', theplan.zqu__R00N40000001mFVKEA2__r[0].zqu__ZuoraId__c);
        ratePlanCharge.setValue('Price',theplan.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c);
        ratePlanCharge.setValue('TriggerEvent', 'SpecificDate');
        ratePlanCharge.setValue('TriggerDate', Datetime.newInstance(triggerdate.year(), triggerdate.month(), triggerdate.day()).format('YYYY-MM-dd'));
        ratePlanCharge.setValue('BillCycleType', 'ChargeTriggerDay');
        ratePlanCharge.setValue('Description', theplan.Name);

        Zuora.zObject ratePlanChargeData = new Zuora.zObject('RatePlanChargeData');
        ratePlanChargeData.setValue('RatePlanCharge', ratePlanCharge);
        ratePlanData.setValue('RatePlanChargeData', new List<Zuora.zObject> {ratePlanChargeData});

        // amendment
        Date cedate = triggerdate;
        Zuora.zObject amendment = new Zuora.zObject('Amendment');
        amendment.setValue('Name', 'Add New Product RatePlan to Subscription');
        amendment.setValue('SubscriptionId', subscriptionId);
        amendment.setValue('ContractEffectiveDate', Datetime.newInstance(cedate.year(),cedate.month(),cedate.day()).format('YYYY-MM-dd'));
        //amendment.setValue('ServiceActivationDate', Datetime.newInstance(cedate.year(),cedate.month(),cedate.day()).format('YYYY-MM-dd'));
        amendment.setValue('Type', 'NewProduct');
        amendment.setValue('Status', 'Completed');
        amendment.setValue('RatePlanData',ratePlanData);

        Zuora.zApi.AmendResult result = ZuoraAmendHelper.amendApiCall(amendment);
        if (result.success) return result.SubscriptionId;
        else return 'Add Rate Plan Error:' + getErrorStr(result.errors);
    }

    /**
     * add product rate plan to subscription with a trigger date and a fixed end date (used by bundling), need to be more specific
     */
    public static String addRatePlanToSubscription(String subscriptionId, zqu__ProductRatePlan__c theplan, Date triggerdate, Integer duration_months) {
        // rateplan
        Zuora.zObject ratePlan = new Zuora.zObject('RatePlan');
        ratePlan.setValue('ProductRatePlanId', theplan.zqu__ZuoraId__c);
        // rateplan data
        Zuora.zObject ratePlanData = new Zuora.zObject('RatePlanData'); 
        ratePlanData.setValue('RatePlan', ratePlan);

        // need to override charge
        Zuora.zObject ratePlanCharge = new Zuora.zObject('RatePlanCharge');
        ratePlanCharge.setValue('ProductRatePlanChargeId', theplan.zqu__R00N40000001mFVKEA2__r[0].zqu__ZuoraId__c);
        ratePlanCharge.setValue('Price',theplan.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c);
        ratePlanCharge.setValue('TriggerEvent', 'SpecificDate');
        ratePlanCharge.setValue('TriggerDate', Datetime.newInstance(triggerdate.year(), triggerdate.month(), triggerdate.day()).format('YYYY-MM-dd'));
        ratePlanCharge.setValue('BillCycleType', 'ChargeTriggerDay');
        ratePlanCharge.setValue('Description', theplan.Name);
        rateplanCharge.setValue('EndDateCondition', 'FixedPeriod');
        ratePlanCharge.setValue('UpToPeriodsType', 'Months'); 
        ratePlanCharge.setValue('UpToPeriods', duration_months);

        Zuora.zObject ratePlanChargeData = new Zuora.zObject('RatePlanChargeData');
        ratePlanChargeData.setValue('RatePlanCharge', ratePlanCharge);
        ratePlanData.setValue('RatePlanChargeData', new List<Zuora.zObject> {ratePlanChargeData});

        // amendment
        Date cedate = triggerdate;
        Zuora.zObject amendment = new Zuora.zObject('Amendment');
        amendment.setValue('Name', 'Add New Product RatePlan to Subscription');
        amendment.setValue('SubscriptionId', subscriptionId);
        amendment.setValue('ContractEffectiveDate', Datetime.newInstance(cedate.year(),cedate.month(),cedate.day()).format('YYYY-MM-dd'));
        //amendment.setValue('ServiceActivationDate', Datetime.newInstance(cedate.year(),cedate.month(),cedate.day()).format('YYYY-MM-dd'));
        amendment.setValue('Type', 'NewProduct');
        amendment.setValue('Status', 'Completed');
        amendment.setValue('RatePlanData',ratePlanData);

        Zuora.zApi.AmendResult result = ZuoraAmendHelper.amendApiCall(amendment);
        if (result.success) return result.SubscriptionId;
        else return 'Add Rate Plan Error:' + getErrorStr(result.errors);
    }
    /**
     * add discount product rate plan to subscription with a trigger date, need to be more specific
     */
    public static String addDiscountToSubscription(String subscriptionId, zqu__ProductRatePlan__c theplan, Date cedate, Integer numperiod, String periodtype) {
        // rateplan
        Zuora.zObject ratePlan = new Zuora.zObject('RatePlan');
        ratePlan.setValue('ProductRatePlanId', theplan.zqu__ZuoraId__c);
        // rateplan data
        Zuora.zObject ratePlanData = new Zuora.zObject('RatePlanData'); 
        ratePlanData.setValue('RatePlan', ratePlan);

        // need to override charge
        Zuora.zObject ratePlanCharge = new Zuora.zObject('RatePlanCharge');
        ratePlanCharge.setValue('ProductRatePlanChargeId', theplan.zqu__R00N40000001mFVKEA2__r[0].zqu__ZuoraId__c);

        if (theplan.zqu__R00N40000001mFVKEA2__r[0].zqu__Model__c == 'Discount-Fixed Amount') ratePlanCharge.setValue('DiscountAmount',theplan.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c);
        else ratePlanCharge.setValue('DiscountPercentage',theplan.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c);

        ratePlanCharge.setValue('BillCycleType', 'ChargeTriggerDay');
        ratePlanCharge.setValue('Description', theplan.zqu__R00N40000001mFVKEA2__r[0].zqu__Description__c);
        ratePlanCharge.setValue('TriggerDate',Datetime.newInstance(cedate.year(),cedate.month(),cedate.day()).format('YYYY-MM-dd'));
        ratePlanCharge.setValue('BillingPeriod', periodtype == null ? 'Month' : periodtype);
        //ratePlanCharge.setValue('SpecificBillingPeriod', numperiod);
        ratePlanCharge.setValue('UpToPeriodsType', 'Billing Periods');
        ratePlanCharge.setValue('UpToPeriods', numperiod);
        rateplanCharge.setValue('EndDateCondition', 'FixedPeriod');

        Zuora.zObject ratePlanChargeData = new Zuora.zObject('RatePlanChargeData');
        ratePlanChargeData.setValue('RatePlanCharge', ratePlanCharge);
        ratePlanData.setValue('RatePlanChargeData', new List<Zuora.zObject> {ratePlanChargeData});

        // amendment
        Zuora.zObject amendment = new Zuora.zObject('Amendment');
        amendment.setValue('Name', 'Add Discount to Subscription');
        amendment.setValue('SubscriptionId', subscriptionId);
        amendment.setValue('ContractEffectiveDate', Datetime.newInstance(cedate.year(),cedate.month(),cedate.day()).format('YYYY-MM-dd'));
        //amendment.setValue('ServiceActivationDate', Datetime.newInstance(cedate.year(),cedate.month(),cedate.day()).format('YYYY-MM-dd'));
        amendment.setValue('Type', 'NewProduct');
        amendment.setValue('Status', 'Completed');
        amendment.setValue('RatePlanData',ratePlanData);

        Zuora.zApi.AmendResult result = ZuoraAmendHelper.amendApiCall(amendment);
        if (result.success) return result.SubscriptionId;
        else return 'Add Rate Plan Error:' + getErrorStr(result.errors);
    }

    /**
     * remove product from subscription
     */
    public static String removeRatePlanFromSubscription(String subscriptionId, String rateplanId, Date cedate) {
        // create a rateplan data object
        Zuora.zObject ratePlan = new Zuora.zObject('RatePlan');
        ratePlan.setValue('AmendmentSubscriptionRatePlanId', rateplanId);
        Zuora.zObject ratePlanData = new Zuora.zObject('RatePlanData');
        ratePlanData.setValue('RatePlan', ratePlan);

        Zuora.zObject amendment = new Zuora.zObject('Amendment');
        amendment.setValue('Name', 'Remove Product from Subscription');
        amendment.setValue('SubscriptionId', subscriptionId);
        amendment.setValue('ContractEffectiveDate', Datetime.newInstance(cedate.year(),cedate.month(),cedate.day()).format('YYYY-MM-dd'));
        amendment.setValue('Type', 'RemoveProduct');
        amendment.setValue('Status', 'Completed');
        amendment.setValue('RatePlanData',ratePlanData);

        Zuora.zApi.AmendResult result = ZuoraAmendHelper.amendApiCall(amendment);
        if (result.success) return result.SubscriptionId;
        else return 'Remove Rate Plan Error:' + getErrorStr(result.errors);
    }

    public static String updateRatePlanPricing(String subscriptionId, String amendmentRatePlanId, String productRatePlanChargeId, Date cedate, Decimal amount) {
        // create rate plan zobject
        Zuora.zObject ratePlan = new Zuora.zObject('RatePlan');
        ratePlan.setValue('AmendmentSubscriptionRatePlanId', amendmentRatePlanId);

        // update rate plan charge price
        Zuora.zObject ratePlanCharge = new Zuora.zObject('RatePlanCharge');
        ratePlanCharge.setValue('ProductRatePlanChargeId', productRatePlanChargeId);
        ratePlanCharge.setValue('Price', amount);
        ratePlanCharge.setValue('TriggerEvent', 'SpecificDate');
        ratePlanCharge.setValue('TriggerDate',Datetime.newInstance(cedate.year(),cedate.month(),cedate.day()).format('YYYY-MM-dd'));
        ratePlanCharge.setValue('BillCycleType', 'ChargeTriggerDay');       

        // put rate plan charge in rate plan charge data
        Zuora.zObject ratePlanChargeData = new Zuora.zObject('RatePlanChargeData');
        ratePlanChargeData.setValue('RatePlanCharge', ratePlanCharge);

        // populate rateplandata with rateplan and rateplan charge data
        Zuora.zObject ratePlanData = new Zuora.zObject('RatePlanData');
        ratePlanData.setValue('RatePlan', ratePlan);
        ratePlanData.setValue('RatePlanChargeData', new List<Zuora.zObject>{ratePlanChargeData});

        Zuora.zObject amendment = new Zuora.zObject('Amendment');
        amendment.setValue('Name', 'Updating Pricing - Change Price to: ' + amount);
        amendment.setValue('SubscriptionId', subscriptionId);
        amendment.setValue('ContractEffectiveDate', Datetime.newInstance(cedate.year(),cedate.month(),cedate.day()).format('YYYY-MM-dd'));// + 'T' + DateTime.now().format('HH:mm:ss.S'));
        amendment.setValue('Type', 'UpdateProduct');
        amendment.setValue('Status', 'Completed');
        amendment.setValue('RatePlanData',ratePlanData);

        Zuora.zApi.AmendResult result = ZuoraAmendHelper.amendApiCall(amendment);

        if (result.success) return result.SubscriptionId;
        else return 'Update pricing error: ' + getErrorStr(result.errors);
    }


    /**
    * Order builder will invoke amend call with passed amendment.
    */
    public static Zuora.zApi.AmendResult amendApiCall(Zuora.zObject amendment) {
        if (Test.isRunningTest()) {
            Zuora.zApi.AmendResult sr = new Zuora.zApi.AmendResult();
            sr.SubscriptionId = '12345';
            sr.Success = true;
            return sr;
        }

        Zuora.zApi zuoraApi = new Zuora.zApi();
        zuoraApi.zlogin();

        Zuora.zApi.AmendRequest amendRequest = new Zuora.zApi.AmendRequest();
        Zuora.zObject[] amendments = new List<Zuora.zObject> {amendment};
        amendRequest.amendments = amendments;

        Zuora.zApi.AmendOptions amendmentOption = new Zuora.zApi.AmendOptions();
        amendmentOption.GenerateInvoice = false;
        amendmentOption.ProcessPayments = false;

        amendRequest.amendOptions = amendmentOption;

        Zuora.zApi.AmendResult result = zuoraApi.zamend(new List<Zuora.zApi.AmendRequest> {amendRequest});

        return result;
    }

    public static String getErrorStr(Zuora.zObject[] errors) {
        String returnstr = '';
        for (Zuora.zObject err : errors) {
            returnstr += err.toString() + ' ';
        }
        return returnstr;
    }

}