@isTest
private class batchEARTest{
    @isTest static void testMethod1(){
         Account acc = new Account();
            acc.firstname='test acc1';
            acc.lastName = 'test acc';
            acc.billingCity ='Test City';
            acc.billingStreet = 'TestStreet';
            acc.billingstate ='TestState';
            acc.billingPostalcode ='23451';
            acc.billingcountry ='United States';
            Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            acc.recordtypeID=objAccrec.id;
           acc.PersonMailingCity='Test City';
            acc.PersonMailingStreet = 'TestStreet';
            acc.PersonMailingState ='TestState';
            acc.PersonMailingPostalCode ='23451';
            acc.PersonMailingCountry ='United States';
            //acc.ispersonAccount = true;
            insert acc;
            
            Opportunity opp = new Opportunity(Name=acc.Name + ' Opportunity ' ,
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=acc.Id,Created_By_PortalCustomer__c=true);
                                       insert opp;
                                       
            Product2 prod = new Product2(Name = 'VectorVest 7 EOD US Monthly', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_MONTHLY' , Family = 'Books');
            insert prod;
            Product2 prod2 = new Product2(Name = 'VectorVest 7 EOD US Annual', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_ANNUAL' , Family = 'Books');
            insert prod2;
            Product2 prod3 = new Product2(Name = 'VectorVest 7 EOD US RealTime Monthly', Feed__c = 'RealTime',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_RT_MONTHLY' , Family = 'Books');
            insert prod3;
            Product2 prod4 = new Product2(Name = 'VectorVest 7 EOD US RealTime Trial', Feed__c = 'RealTime',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_RT_TRIAL' , Family = 'Books',isTrial__c=true);
            insert prod4;
            Enterprise_Access_Mapping__c objEM = new Enterprise_Access_Mapping__c(product__c =prod.id);
            insert objem;
            Enterprise_Access_Mapping__c objEM3 = new Enterprise_Access_Mapping__c(product__c =prod3.id);
            insert objem3;
            
            
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            
            CPQCommunity__c cpc = new CPQCommunity__c();
            cpc.Name = 'test';
            cpc.PriceBookId__c = pricebookId ;
            //insert cpc ;
            
            
            
            SBQQ__Quote__c quoteRec = new SBQQ__Quote__c();
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SiteUser_Primary_Check__c = true;
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SBQQ__Opportunity2__c = opp.Id;
            //quoteRec.ZSB__BillToContact__c = c.Id;
           // quoteRec.ZSB__SoldToContact__c = c.Id;
            quoteRec.ZSB__BillCycleDay__c =  '1';
            quoteRec.ZSB__BillingBatch__c = 'Batch1';
            quoterec.SBQQ__StartDate__C =Date.today();
            quoterec.SbQQ__Enddate__c =Date.today().Adddays(30);
            //quoterec.SBQQ__NetAmount__c =2.4;
            insert quoteRec ;
            list<SBQQ__QuoteLine__c> lstQuoteline = new list<SBQQ__QuoteLine__c>();
            SBQQ__QuoteLine__c ojQuoteline = new SBQQ__QuoteLine__c();
            ojQuoteline.SBQQ__Quote__c = quoteRec .id;
            ojQuoteline.SBQQ__Product__c = prod.id;
            ojQuoteline.SBQQ__StartDate__C =Date.today();
            ojQuoteline.SbQQ__Enddate__c =Date.today().Adddays(30);
            //insert ojQuoteline;
            lstQuoteline.add(ojQuoteline);
            SBQQ__QuoteLine__c ojQuoteline3 = new SBQQ__QuoteLine__c();
            ojQuoteline3.SBQQ__Quote__c = quoteRec .id;
            ojQuoteline3.SBQQ__Product__c = prod3.id;
            ojQuoteline3.SBQQ__StartDate__C =Date.today();
            ojQuoteline3.SbQQ__Enddate__c =Date.today().Adddays(30);
            lstQuoteline.add(ojQuoteline3);
            //insert ojQuoteline3;
            insert lstQuoteline;
             Contract objContract = new Contract();
             objContract.SBQQ__Quote__c = quoteRec.id;
             objContract.SBQQ__Opportunity__c = opp.id;
             objcontract.Accountid=acc.id;
             objcontract.ZSB__Termtype__c ='Evergreen';
             objcontract.Startdate=Date.today();
             //objContract.Enddate=Date.today().adddays(30);
             insert objContract;
             Zuora__CustomerAccount__c objZCustAccount = new Zuora__CustomerAccount__c();
             objZCustAccount .Zuora__Account__c = acc.id;
             objZCustAccount .Zuora__Status__c ='Active';
             objZCustAccount .Zuora__BillToName__c ='test';
             objZCustAccount .Zuora__Zuora_Id__c = '2c92c0f960c105580160d80905a205ea';
             //objZCustAccount .Zuora__CreditCard_Number__c = '4444333322221111';
             //objZCustAccount .Zuora__CreditCard_Expiration__c = '10/2019';
             insert objZCustAccount ;
             Zuora__PaymentMethod__c objZpaymentmethod = new Zuora__PaymentMethod__c();
             objZpaymentmethod .Zuora__CreditCardCountry__c='us';
             objZpaymentmethod .Zuora__CreditCardExpirationMonth__c='10';
             objZpaymentmethod.Zuora__CreditCardExpirationYear__c='2019';
             objZpaymentmethod.Zuora__CreditCardHolderName__c='test';
             objZpaymentmethod.Zuora__CreditCardMaskNumber__c='123';
             objZpaymentmethod.Zuora__DefaultPaymentMethod__c  = true;
             //objZpaymentmethod .
             //objZpaymentmethod .
             
             objZpaymentmethod.Zuora__BillingAccount__c = objZCustAccount .id;
             insert objZpaymentmethod;
             contact con =[select id from contact where accountid=:acc.id];
             user objuser = new user(id=userinfo.getuserid(), contactid=con.id);
             update objuser;
             user objuser1 = [select id, accountid from user where id=:objuser.id];
             system.debug('objuser###'+objuser1.accountid);
             list<SBQQ__Subscription__c> lstsub = new list<SBQQ__Subscription__c>();
             SBQQ__Subscription__c objSub = new SBQQ__Subscription__c();
             objsub.SBQQ__Contract__c = objContract.id;
             objsub.SBQQ__Product__c =prod.id;
             objsub.SBQQ__QuoteLine__c = ojQuoteline.id;
             objsub.SBQQ__Account__c = acc.id;
             objsub.SBQQ__Quantity__c = 1;
             //objsub.SBQQ__StartDate__c = Date.today();
             //insert objsub;
             lstsub.add(objsub);
             SBQQ__Subscription__c objSub1 = new SBQQ__Subscription__c();
             objsub1.SBQQ__Contract__c = objContract.id;
             objsub1.SBQQ__Product__c =prod3.id;
             objsub1.SBQQ__QuoteLine__c = ojQuoteline3.id;
             objsub1.SBQQ__Account__c = acc.id;
             objsub1.SBQQ__Quantity__c = 1;
             //objsub1.SBQQ__StartDate__c = Date.today();
             //insert objsub1;
             lstsub.add(objsub1);
              SBQQ__Subscription__c objSub2 = new SBQQ__Subscription__c();
             objsub2.SBQQ__Contract__c = objContract.id;
             objsub2.SBQQ__Product__c =prod3.id;
             objsub2.SBQQ__QuoteLine__c = ojQuoteline3.id;
             objsub2.SBQQ__Account__c = acc.id;
             objsub2.SBQQ__Quantity__c = 1;
             objsub2.SBQQ__Terminateddate__C = Date.today().addDays(-1);
             objsub2.SBQQ__SubscriptionEndDate__c= Date.today().addDays(30);
             //insert objsub2;
             lstsub.add(objsub2);
             SBQQ__Subscription__c objSub3 = new SBQQ__Subscription__c();
             objsub3.SBQQ__Contract__c = objContract.id;
             objsub3.SBQQ__Product__c =prod4.id;
             objsub3.SBQQ__QuoteLine__c = ojQuoteline3.id;
             objsub3.SBQQ__Account__c = acc.id;
             objsub3.SBQQ__Quantity__c = 1;
             objsub3.SBQQ__SubscriptionEndDate__c= Date.today().addDays(30);
             lstsub.add(objsub3);
             //insert objsub3;
             insert lstsub;
        test.starttest();
        SendnotificationTouserOnContractCheck obj = new SendnotificationTouserOnContractCheck();
        database.executebatch(new batcheAR());
        database.executebatch(new batchMarketo());
        database.executebatch(new setpasswordbatch(userinfo.getuserid(),'Testing@123'));
        test.stoptest();
    }
    }