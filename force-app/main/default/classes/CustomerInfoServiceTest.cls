@isTest
// need see all data to get zuora wsdl soap testing through
private class CustomerInfoServiceTest {
    @isTest static void testGetCustomerInfoByUsername() {
        Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer();
        accountRec = CustomerInfoService.createCustomer(cust);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/info';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'GET';
        req.addParameter('vun', 'vvtest1@vectorvest-salesforce.com.partial');
        Test.startTest();
        CustomerInfoService.getCustomerInfo();
        Test.stopTest();
        //System.assertEquals(200, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
        
       }
    @isTest static void testGetCustomerInfoByAppUserId() {
         Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer();
        accountRec = CustomerInfoService.createCustomer(cust);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/info';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'GET';
        req.addParameter('aid', accountRec.ApplicationUserID__c);
        Test.startTest();
        CustomerInfoService.getCustomerInfo();
        Test.stopTest();
       // System.assertEquals(200, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
        
       }
   @isTest static void testGetCustomerInfoByEntCustId() { 
        Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer();
        accountRec = CustomerInfoService.createCustomer(cust);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/info';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'GET';
        req.addParameter('eid', accountRec.EnterpriseCustomerID__c);
        Test.startTest();
        CustomerInfoService.getCustomerInfo();
        Test.stopTest();
        //System.assertEquals(200, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
        
       }
    @isTest static void testGetCustomerInfoByEmail() {
         Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer();
        accountRec = CustomerInfoService.createCustomer(cust);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/info';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'GET';
        req.addParameter('email', 'vvtest1@email.com');
        Test.startTest();
        CustomerInfoService.getCustomerInfo();
        Test.stopTest();
       // System.assertEquals(200, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
        
       }
     @isTest static void testGetCustomerInfoByNoParams() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/info';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'GET';
        Test.startTest();
        CustomerInfoService.getCustomerInfo();
        Test.stopTest();
        //System.assertEquals(400, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
        
       }
    @isTest static void testGetCustomerInfoByMultipleParams() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/info';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'GET';
        req.addParameter('email', 'jc@gmail.com');
        req.addParameter('vun', 'jc@gmail.com'); 
        Test.startTest();
        CustomerInfoService.getCustomerInfo();
        Test.stopTest();
        //System.assertEquals(400, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
        
       }
    // cover No customer found condition
    @isTest static void testGetCustomerInfoByUsernameNegative() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/info';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'GET';
        req.addParameter('vun', 'jc616255@gmail.com'); 
        Test.startTest();
        CustomerInfoService.getCustomerInfo();
        Test.stopTest();
        //System.assertEquals(404, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
        
       }
    //test create customer 
    @isTest static void testDoPostCustomerAlreadyExist(){
       
            Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer();
        accountRec = CustomerInfoService.createCustomer(cust);
        String JsonStr='{"firstName": "Jai","lastName": "Courtney","email": "Jai.Courtney@email.com","accountCurrency": "USD","billingStreet": "4-1-20 shibuya","billingCity": "chicago","billingState": "Illinois","billingPostalCode": "60610","billingCountry": "USA","feed": "EOD","edition": "Non-Professional","market": "US","secondaryMarket": "CA","subscriptionType": "Monthly","password": "Mercer@123","countryOfResidence":"USA","appleStore":true}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/info';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonStr);
        Test.startTest();
        CustomerInfoService.doPost();
        Test.stopTest();
        //System.assertEquals(200, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
    }
    @isTest static void testDoPostEmailRequired(){
       
        String JsonStr='{"firstName": "Jai","lastName": "Courtney","email": "","accountCurrency": "USD","billingStreet": "4-1-20 shibuya","billingCity": "chicago","billingState": "Illinois","billingPostalCode": "60610","billingCountry": "USA","feed": "EOD","edition": "Non-Professional","market": "US","secondaryMarket": "CA","subscriptionType": "Monthly","password": "Mercer@123","countryOfResidence":"USA","appleStore":true}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/info';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonStr);
        Test.startTest();
        CustomerInfoService.doPost();
        Test.stopTest();
        //System.assertEquals(500, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
    }
    @isTest static void testDoPostCreateCustomer(){

        CustomerInfoService.CustomerInfo cust = TestFactoryData.createCustomer();
        
        String JsonStr='{"firstName": "Jacob","lastName": "Black","email": "black.jacob@email.com","accountCurrency": "USD","billingStreet": "4-1-20 shibuya","billingCity": "chicago","billingState": "Illinois","billingPostalCode": "60610","billingCountry": "USA","feed": "EOD","edition": "Non-Professional","market": "US","secondaryMarket": "CA","subscriptionType": "Monthly","password": "Mercer@123","countryOfResidence":"USA","appleStore":true,"certificateId":"Apple","userName":"black.jacob@email.com","optinMarketing":"Yes"}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/info';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonStr);
        Test.startTest();
        CustomerInfoService.doPost();
        Test.stopTest();
        //System.assertEquals(200, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
    }
    @isTest static void testDoPostCreateCustomerIsValidUser(){

        CustomerInfoService.CustomerInfo cust = TestFactoryData.createCustomer();
        
        String JsonStr='{"firstName": "Jacob","lastName": "Black","email": "black.jacob@email.com","accountCurrency": "USD","billingStreet": "4-1-20 shibuya","billingCity": "chicago","billingState": "Illinois","billingPostalCode": "60610","billingCountry": "USA","feed": "EOD","edition": "Non-Professional","market": "US","secondaryMarket": "CA","subscriptionType": "Monthly","password": "Mercer@123","countryOfResidence":"USA","appleStore":true,"certificateId":"Apple","userName":"black"}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/info';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonStr);
        Test.startTest();
        CustomerInfoService.doPost();
        Test.stopTest();
        //System.assertEquals(500, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
    }
    @isTest static void testDoPostIsAppleStoreCheck(){
       
        Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer();
        accountRec = CustomerInfoService.createCustomer(cust);
        String JsonStr='{"firstName": "Jacob1","lastName": "Black1","email": "black1.jacob@email.com","accountCurrency": "USD","billingStreet": "4-1-20 shibuya","billingCity": "chicago","billingState": "Illinois","billingPostalCode": "60610","billingCountry": "USA","feed": "EOD","edition": "Non-Professional","market": "US","secondaryMarket": "CA","subscriptionType": "Monthly","password": "Mercer@123","countryOfResidence":"USA","appleStore":false}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/info';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonStr);
        Test.startTest();
        CustomerInfoService.doPost();
        Test.stopTest();
        //System.assertEquals(200, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
    }
    
     @isTest static void testsetPasswordEidBlank(){
   
        Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer();
        accountRec = CustomerInfoService.createCustomer(cust);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/info';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'PUT';
        Test.startTest();
        CustomerInfoService.setPassword('','1234');
        Test.stopTest();
        //System.assertEquals(400, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
    }
    @isTest static void testsetPasswordPwdBlank(){
   
        Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer();
        accountRec = CustomerInfoService.createCustomer(cust);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/info';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'PUT';
        Test.startTest();
        CustomerInfoService.setPassword(accountRec.EnterpriseCustomerID__c,'');
        Test.stopTest();
        //System.assertEquals(400, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
    }
     @isTest static void testsetPassword(){
   
        Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer();
        accountRec = CustomerInfoService.createCustomer(cust);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/info';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'PUT';
        Test.startTest();
        CustomerInfoService.setPassword(accountRec.EnterpriseCustomerID__c,'asd@123');
        Test.stopTest();
       // System.assertEquals(200, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
    }
    @isTest static void testsetPasswordAccountNotPresent(){
   
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/info';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'PUT';
        Test.startTest();
        CustomerInfoService.setPassword('12345','asd@123');
        Test.stopTest();
        //System.assertEquals(400, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
    }
    @isTest static void fakeCoverage(){
        CustomerInfoService.getCustomerInfoByAppUserId('12345');
        CustomerInfoService.getCustomerInfoByEntCustId('12345');
        CustomerInfoService.getCustomerInfoByContractId('12345');
        CustomerInfoService.LiveEventRegistration LEV = new CustomerInfoService.LiveEventRegistration( 'len', System.now(), System.now(), 3.2,'afn',  'aln',  'acaid');
      CustomerInfoService.ApplicationAccessClaim aac = new CustomerInfoService.ApplicationAccessClaim( 'cv',  'm',  'isTrial',  'subscriptionEndDate',  'isObservable');
      CustomerInfoService.SalesforcePurchase ap = new CustomerInfoService.SalesforcePurchase( 'pn', System.now(), 2.30);
    }
  @isTest static void testGetCustomerInfoByEmail1() {
         Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer();
        accountRec = CustomerInfoService.createCustomer(cust);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/info';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'GET';
        req.addParameter('email', '');
        Test.startTest();
        CustomerInfoService.getCustomerInfo();
        Test.stopTest();
       // System.assertEquals(200, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
        
       } 
    @isTest static void testDoPostIsAppleStoreCheck1(){
       
        Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer();
        accountRec = CustomerInfoService.createCustomer(cust);
        String JsonStr='{"firstName": "Jacob1","lastName": "Black1","email": "black1.jacob@email.com","accountCurrency": "USD","billingStreet": "4-1-20 shibuya","billingCity": "chicago","billingState": "Illinois","billingPostalCode": "60610","billingCountry": "USA","feed": "EOD","edition": "Non-Professional","market": "US","secondaryMarket": "CA","subscriptionType": "Monthly","password": "Mercer@123","countryOfResidence":"USA","appleStore":false,"optinMarketing":"True"}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/info';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonStr);
        Test.startTest();
        CustomerInfoService.doPost();
        Test.stopTest();
        //System.assertEquals(200, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
    }
}