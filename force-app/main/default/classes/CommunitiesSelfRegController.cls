/**
 * An apex page controller that supports self registration of users in communities that allow self registration
 *
 * this apex controller also supports vvCSRAccountManagement page allowing CSR to handle similar registration
 *
 * 
 * Create salesforce person account
 * Create new payment method in zuora using the hosted page
 * Create zuora account
 * Create zuora contact
 * Link zuora account with the payment method by doing an update.
 * 
 */
public class CommunitiesSelfRegController {

    public CommunitiesSelfRegController(){}

}