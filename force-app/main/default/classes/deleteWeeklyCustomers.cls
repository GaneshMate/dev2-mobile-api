global class deleteWeeklyCustomers implements Database.Batchable <sObject>, Database.Stateful {

    global final Date PriorweekStart;
    
    global deleteWeeklyCustomers (Date PriorweekStartDate) {
    
        PriorweekStart = PriorweekStartDate;
        
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC) {
        String query = 'SELECT Id FROM Test_Weekly_Customers__c WHERE Week_Start_Date__c <: PriorweekStart AND isDeleted = FALSE';
        if (Test.isRunningTest ())
            query = 'SELECT Id FROM Test_Weekly_Customers__c';
        return Database.getQueryLocator (query); 
    }
    
    global void execute (Database.BatchableContext BC, List <Test_Weekly_Customers__c> deleteRecords) {
        
        delete deleteRecords;
    
    }
    
    global void finish (Database.BatchableContext BC) {
        
    }
}