@isTest
Public Class ShowingProductListTest {
    static testMethod void testShowPrducts() {
        Test.startTest();
        
        Product2 prod = new Product2(Name = 'Laptop X200', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'code1' , Family = 'Books');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        PriceBookIds__c pb = new PriceBookIds__c();
        pb.Name = 'Standard Price Book';
        pb.Price_Book_Id__c = pricebookId ;
        insert pb ;
        
        List<Product_Filters__c> pfList = new  List<Product_Filters__c>();
        
        Product_Filters__c pf = new Product_Filters__c();
        pf.Field_API__c = 'Market__c';
        pf.Name = 'Market';
        
        Product_Filters__c pf1 = new Product_Filters__c();
        pf1.Field_API__c = 'Feed__c';
        pf1.Name = 'Feed';
        
        Product_Filters__c pf2 = new Product_Filters__c();
        pf2.Field_API__c = 'ProductCode';
        pf2.Name = 'Product Code';        
        
        pfList.add(pf);
        pfList.add(pf1);
        pfList.add(pf2);
        
        insert pfList ;
        
        ShowingProductList obj = new ShowingProductList();
        obj.objSelectedObject.put('Market__c','US');
        obj.objSelectedObject.put('Feed__c','EOD');
        obj.objSelectedObject.put('ProductCode','code1');
        obj.showProducts();
        System.assertEquals(obj.prdListwp.size(),1);
        Test.stopTest();
    }
}