@isTest
public class deleteWeeklyCustomers_Test {
    static testmethod void TestdeleteWeeklyCustomers () {
        
        
        Test_Weekly_Customers__c insertRecords = NEW Test_Weekly_Customers__c ();
            insertRecords.Sub_Id__c = 'Test';
        insert insertRecords;
        
        Test.startTest();
            deleteWeeklyCustomers t = new deleteWeeklyCustomers(Date.today().addDays(-1));
            
            deleteWeeklyCustomersScheduler del = new deleteWeeklyCustomersScheduler ();
            String sch = '0 01 * * * ?';
            system.schedule('Test', sch, del);
            Database.executeBatch(t);
        Test.stopTest();
    }
}