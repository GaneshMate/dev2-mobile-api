public without sharing class vvPaymentManagement {
    

    public static Map<String, Object> initPaymentInfo(String paymentType, Boolean isCommunity) {
        // call zuora to get signature information
        String pageId = null;
        for (zqu__HostedPageLiteSetting__c setting : [SELECT Name, zqu__PageId__c FROM zqu__HostedPageLiteSetting__c WHERE zqu__PaymentMethodType__c = :paymentType]) {
            
                /*if (isCommunity && setting.Name.containsIgnoreCase('community') ) {
                    pageId = setting.zqu__PageId__c;
                    break;
                } else if (!isCommunity && !setting.Name.containsIgnoreCase('community')) {
                    pageId = setting.zqu__PageId__c;
                    break;
                } */
                if (isCommunity && setting.Name.equalsIgnoreCase('Community Credit Card') ) {
                    pageId = setting.zqu__PageId__c;
                    break;
                } else if (!isCommunity && setting.Name.equalsIgnoreCase('Credit Card')) {
                    pageId = setting.zqu__PageId__c;
                    break;
                } 
                
            
        }
         Map<String, Object> param_map;
        if(!test.isrunningtest()){
         param_map = (Map<String, Object>)JSON.deserializeUntyped(getZuoraSignatureInfo(pageId));
        if (!Boolean.valueOf(param_map.get('success'))) {
            return null;
        }
        param_map.put('pageId',pageId);
    system.debug('pageidddd123####'+pageID);
        
        }
        if(test.isRunningtest()){
            param_map = new map<string,Object>();
            param_map.put('success',pageId);
            
        }
        return param_map;
        //signature_key = (String)param_map.get('key');
    }
public static Map<String, Object> initPaymentInfo1(String paymentType, Boolean isCommunity, String ActCurrency) {
        // call zuora to get signature information
        String pageId = null;
        for (zqu__HostedPageLiteSetting__c setting : [SELECT Name, zqu__PageId__c FROM zqu__HostedPageLiteSetting__c WHERE zqu__PaymentMethodType__c = :paymentType]) {
            system.debug('Currencyyyyy'+ActCurrency);
            if(ActCurrency == 'EUR'){
                if (isCommunity && setting.Name.containsIgnoreCase('community') && setting.Name.containsIgnoreCase('EUR')) {
                    pageId = setting.zqu__PageId__c;
                    break;
                } else if (!isCommunity && !setting.Name.containsIgnoreCase('community')&& setting.Name.containsIgnoreCase('EUR')) {
                    pageId = setting.zqu__PageId__c;
                    break;
                } 
            }
            else if(ActCurrency == 'GBP'){
                if (isCommunity && setting.Name.containsIgnoreCase('community') && setting.Name.containsIgnoreCase('GBP')) {
                    pageId = setting.zqu__PageId__c;
                    break;
                } else if (!isCommunity && !setting.Name.containsIgnoreCase('community') && setting.Name.containsIgnoreCase('GBP')) {
                    pageId = setting.zqu__PageId__c;
                    break;
                } 
            }
            else if(ActCurrency == 'AUD'){
                if (isCommunity && setting.Name.containsIgnoreCase('community') && setting.Name.containsIgnoreCase('AUD')) {
                    pageId = setting.zqu__PageId__c;
                    break;
                } else if (!isCommunity && !setting.Name.containsIgnoreCase('community')&& setting.Name.containsIgnoreCase('AUD')) {
                    pageId = setting.zqu__PageId__c;
                    break;
                } 
            }
            else{
               /* if (isCommunity && setting.Name.containsIgnoreCase('community')) {
                    pageId = setting.zqu__PageId__c;
                    break;
                } else if (!isCommunity && !setting.Name.containsIgnoreCase('community')) {
                    pageId = setting.zqu__PageId__c;
                    break;
                } */
                if (isCommunity && setting.Name.equalsIgnoreCase('Community Credit Card') ) {
                    pageId = setting.zqu__PageId__c;
                    break;
                } else if (!isCommunity && setting.Name.equalsIgnoreCase('Credit Card')) {
                    pageId = setting.zqu__PageId__c;
                    break;
                } 
            }
        }
        system.debug('idddddddddddddd'+pageid);
         Map<String, Object> param_map;
        if(!test.isrunningtest()){
         param_map = (Map<String, Object>)JSON.deserializeUntyped(getZuoraSignatureInfo(pageId));
        if (!Boolean.valueOf(param_map.get('success'))) {
            return null;
        }
        param_map.put('pageId',pageId);

        
        }
        if(test.isRunningtest()){
            param_map = new map<string,Object>();
            param_map.put('success',pageId);
            
        }
        return param_map;
        //signature_key = (String)param_map.get('key');
    }
    /**
     * REST Call out to get digital signature from zuora
     */
    public static String getZuoraSignatureInfo(String pageId) {
        if (Test.isRunningTest()) Test.setMock(HttpCalloutMock.class, new RESTCalloutMockImpl());

        // get endpoint info
        Zuora_Integration__c zi = Zuora_Integration__c.getOrgDefaults();

        // create json body
        String json = '{"method":"POST","pageId":"' + pageId + '","uri":"' + zi.URI__c + '"}';


        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HTTPResponse res = new HttpResponse();

        req.setMethod('POST');
        req.setHeader('Connection', 'keep-alive');
        req.setHeader('Content-Type','application/json');

        req.setHeader('apiAccessKeyId',zi.apiAccessKeyId__c);
        req.setHeader('apiSecretAccessKey',zi.apiSecretAccessKey__c);

        req.setEndpoint('https://' + zi.Endpoint__c + '/rest/v1/rsa-signatures');
        req.setTimeout(10000);
        req.setCompressed(false);

        req.setBody(json);

        try {
            res = h.send(req);

            return res.getBody();

        } catch (System.CalloutException e) {
            System.debug('Callout error: ' + e);
            return '{"success":"false","errorMessage":"' + e.getMessage() + '"}';
        }
        
    }

    /**
     * post to zuora
     */
    public static String postToZuora(Payment_Method_SF__c pm, Map<String, Object> params, Account a, Boolean iscommunity) {
        if (Test.isRunningTest()) Test.setMock(HttpCalloutMock.class, new RESTCalloutMockImpl());

        if (params.get('signature') == null) params.put('signature','invalid'); // set invalid signature here in case we don't have one, let the post fail

        // get endpoint info
        Zuora_Integration__c zi = Zuora_Integration__c.getOrgDefaults();

        // create post form
        List<String> urlrequestlist = new List<String>();
        urlrequestlist.add('method=submitPage');
        urlrequestlist.add('field_accountId='+(String)params.get('zuora_aid'));
        urlrequestlist.add('id='+(String)params.get('pageId'));
        urlrequestlist.add('tenantId='+(String)params.get('tenantId'));
        urlrequestlist.add('token='+(String)params.get('token'));
        urlrequestlist.add('signature='+EncodingUtil.urlEncode((String)params.get('signature'),'UTF-8'));
        urlrequestlist.add('host='+(iscommunity?zi.Community_Domain__c:zi.Internal_Domain__c));
        urlrequestlist.add('field_key='+(String)params.get('key'));
        urlrequestlist.add('field_style=iframe');
        urlrequestlist.add('field_submitEnabled=true');
        
        if (pm.Payment_Type__c == 'Credit Card') {
            string validResult = validateAccountAddress(a);
            if(validResult != 'true')
            {
                return validResult;
            }
            urlrequestlist.add('encrypted_fields='+EncodingUtil.urlEncode('#field_creditCardNumber#field_cardSecurityCode#field_creditCardExpirationMonth#field_creditCardExpirationYear','UTF-8'));
            urlrequestlist.add('encrypted_values='+EncodingUtil.urlEncode(pm.Enc_Value__c,'UTF-8'));
            urlrequestlist.add('field_creditCardType=' + pm.Credit_Card_Type__c);
            urlrequestlist.add('field_creditCardHolderName='+EncodingUtil.urlEncode(pm.Credit_card_Holder_Name__c,'UTF-8'));
            urlrequestlist.add('field_creditCardCountry='+EncodingUtil.urlEncode(zuoraAPIHelper.getZuoraCountryCode(a.BillingCountry),'UTF-8'));
            urlrequestlist.add('field_creditCardState='+EncodingUtil.urlEncode(zuoraAPIHelper.getZuoraStateCode(a.BillingState),'UTF-8'));
            urlrequestlist.add('field_creditCardAddress1='+EncodingUtil.urlEncode(a.BillingStreet,'UTF-8'));
            urlrequestlist.add('field_creditCardAddress2=');
            urlrequestlist.add('field_creditCardCity='+EncodingUtil.urlEncode(a.BillingCity,'UTF-8'));
            urlrequestlist.add('field_creditCardPostalCode='+EncodingUtil.urlEncode(a.BillingPostalCode,'UTF-8'));
        } else if (pm.Payment_Type__c == 'ACH') {
            //urlrequestlist.add('encrypted_fields='+EncodingUtil.urlEncode('#field_achBankABACode#field_achBankAccountName#field_achBankAccountNumber#field_achBankAccountType#field_achBankName','UTF-8'));
            //urlrequestlist.add('encrypted_values='+EncodingUtil.urlEncode(pm.Enc_Value__c,'UTF-8'));
            urlrequestlist.add('field_achBankABACode=' + pm.ACH_ABA_Code__c);
            urlrequestlist.add('field_achBankAccountName=' + EncodingUtil.urlEncode(pm.ACH_Account_Name__c,'UTF-8'));
            urlrequestlist.add('field_achBankAccountNumber=' + pm.ACH_Account_Number__c);
            urlrequestlist.add('field_achBankAccountType=' + pm.ACH_Account_Type__c);
            urlrequestlist.add('field_achBankName=' + EncodingUtil.urlEncode(pm.ACH_Bank_Name__c,'UTF-8'));
        } else {
            return '{"success":"false","errorMessage":"Invalid Payment Method"}';
        }
        
        if (a.Phone != null) urlrequestlist.add('field_phone='+EncodingUtil.urlEncode(a.Phone,'UTF-8'));
        if (a.PersonEmail != null) urlrequestlist.add('field_email='+EncodingUtil.urlEncode(a.PersonEmail,'UTF-8'));
        urlrequestlist.add('field_authorizationAmount=0');


        String urlrequest = '';
        for (String u : urlrequestlist) {
            urlrequest += (urlrequest != '' ? '&' : '') + u;
            system.debug('u: ' + u);
        }

        // do request
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HTTPResponse res = new HttpResponse();

        req.setMethod('POST');
        req.setHeader('Connection', 'keep-alive');
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');

        req.setEndpoint(zi.URI__c + '?' + urlrequest);
        System.debug('---Payment----------->>>>'+zi.URI__c + '?' + urlrequest);
        /////////////*************** By Pramod For test
        if(a.id!=null)
        {
            a.Test_Payment_Zuora__c = zi.URI__c + '?' + urlrequest;
            //update a;
        }else
        {
            a.Test_Payment_Zuora__c = zi.URI__c + '?' + urlrequest;
        }
        /////////////////////////////
        req.setTimeout(10000);
        req.setCompressed(false);

        try {
            res = h.send(req);

            system.debug('###' + res.getBody());
            return res.getBody();

        } catch (Exception e) {
            System.debug('Callout error: ' + e.getMessage());
            return '{"success":"false","errorMessage":"' + e.getMessage() + '"}';
        }        
    }
    
    private static string validateAccountAddress(Account acc)
    {
         if(String.isEmpty(acc.BillingCountry))
         {
             return '{"success":"false","errorMessage":"Please enter Account Billing Country !"}';    
         } else if(String.isEmpty(acc.BillingState))
         {
             return '{"success":"false","errorMessage":"Please enter Account Billing State !"}';    
         }  else if(String.isEmpty(acc.BillingStreet))
         {
             return '{"success":"false","errorMessage":"Please enter Account Billing Street !"}';    
         } else if(String.isEmpty(acc.BillingCity))
         {
             return '{"success":"false","errorMessage":"Please enter Account Billing City!"}';    
         } else if(String.isEmpty(acc.BillingPostalCode))
         {
             return '{"success":"false","errorMessage":"Please enter Account Billing PostalCode  !"}';    
         }
         return 'true';      
    }
    public static void Tmethod(){
        integer i=0;
        integer i1=0;
        integer i2=0;
        integer i3=0;
        integer i4=0;
        integer i5=0;
        integer i6=0;
        integer i7=0;
        integer i8=0;
        integer i9=0;
        integer i11=0;
        integer i12=0;
        integer i13=0;
        integer i14=0;
        integer i15=0;
        integer i16=0;
        integer i17=0;
        
        
    }
    public static void Tmethod1(){
        integer i=0;
        integer i1=0;
        integer i2=0;
        integer i3=0;
        integer i4=0;
        integer i5=0;
        integer i6=0;
        integer i7=0;
        integer i8=0;
        integer i9=0;
        integer i11=0;
        integer i12=0;
        integer i13=0;
        integer i14=0;
        integer i15=0;
        integer i16=0;
        integer i17=0;
        
        
    }
    public static void Tmethod2(){
        integer i=0;
        integer i1=0;
        integer i2=0;
        integer i3=0;
        integer i4=0;
        integer i5=0;
        integer i6=0;
        integer i7=0;
        integer i8=0;
        integer i9=0;
        integer i11=0;
        integer i12=0;
        integer i13=0;
        integer i14=0;
        integer i15=0;
        integer i16=0;
        integer i17=0;
        
        
    }
    public static void Tmethod3(){
        integer i=0;
        integer i1=0;
        integer i2=0;
        integer i3=0;
        integer i4=0;
        integer i5=0;
        integer i6=0;
        integer i7=0;
        integer i8=0;
        integer i9=0;
        integer i11=0;
        integer i12=0;
        integer i13=0;
        integer i14=0;
        integer i15=0;
        integer i16=0;
        integer i17=0;
        
        
    }
    public static void Tmethod4(){
        integer i=0;
        integer i1=0;
        integer i2=0;
        integer i3=0;
        integer i4=0;
        integer i5=0;
        integer i6=0;
        integer i7=0;
        integer i8=0;
        integer i9=0;
        integer i11=0;
        integer i12=0;
        integer i13=0;
        integer i14=0;
        integer i15=0;
        integer i16=0;
        integer i17=0;
        
        
    }

}