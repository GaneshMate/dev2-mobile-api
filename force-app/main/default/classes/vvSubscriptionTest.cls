/**
 * test both vvSubscriptionManagement and vvCSRSubscriptionController
 */
@isTest(seeAllData=true)
private class vvSubscriptionTest {
    @isTest static void testCreateMonthly() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();
        Zuora__CustomerAccount__c zca = [SELECT Zuora__Account__c,Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Name = 'test billing account'];
        Test.startTest();

        Campaign cam = new Campaign(Name = 'testtesttest');
        insert cam;
        // marketing params
        Map<String, String> customfields = new Map<String, String>{'CampaignId__c'=>cam.Id};

        // bundle
        Bundle__c bundle = [SELECT Id FROM Bundle__c LIMIT 1];
        customfields.put('BundleId',bundle.Id);

        // monthly
        vvSubscriptionManagement.createSubscription(new Account(Id=zca.Zuora__Account__c), zca.Zuora__Zuora_Id__c,'12345','12345', true, customfields);
        Test.stopTest();
    }

    @isTest static void testCreateAnnual() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();
        Zuora__CustomerAccount__c zca = [SELECT Zuora__Account__c,Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Name = 'test billing account'];
        Test.startTest();
        // annual
        Account a = new Account(Id=zca.Zuora__Account__c,Subscription_Plan__c='Annual');
        update a;
        vvSubscriptionManagement.createSubscription(new Account(Id=zca.Zuora__Account__c), zca.Zuora__Zuora_Id__c,'12345','12345', true, null);
        Test.stopTest();
    }

    @isTest static void testcreateRT() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();
        Zuora__CustomerAccount__c zca = [SELECT Zuora__Account__c,Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Name = 'test billing account'];
        Test.startTest();
        // quarterly RT
        Account a = new Account(Id=zca.Zuora__Account__c,Subscription_Plan__c='Quarterly',VV_Feed__c='RT');
        update a;
        vvSubscriptionManagement.createSubscription(new Account(Id=zca.Zuora__Account__c), zca.Zuora__Zuora_Id__c,'12345','12345', true, null);
        Test.stopTest();
    }

    @isTest static void testcreateRTPro() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();
        Zuora__CustomerAccount__c zca = [SELECT Zuora__Account__c,Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Name = 'test billing account'];
        Test.startTest();
        // switch to professional
        Account a = new Account(Id=zca.Zuora__Account__c,Subscription_Plan__c='Quarterly',VV_Edition__c='Professional',VV_Feed__c='RT');
        update a;
        vvSubscriptionManagement.createSubscription(new Account(Id=zca.Zuora__Account__c), zca.Zuora__Zuora_Id__c,'12345','12345', true, null);
        Test.stopTest();
    }

    @isTest static void testPopulateEnterpriseRight() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();
        Zuora__CustomerAccount__c zca = [SELECT Zuora__Account__c,Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Name = 'test billing account'];
        

        List<Zuora__Subscription__c> complete_zs_list = [SELECT Zuora__Account__r.VV_Market__c, Zuora__Account__r.VV_Feed__c, Zuora__ContractEffectiveDate__c, Zuora__Status__c, Zuora__CancelledDate__c, Zuora__Account__c, Zuora__Account__r.Subscription_Suspend_Start_Date__c, Zuora__Account__r.Subscription_Suspend_End_Date__c, Zuora__TermSettingType__c, Zuora__Account__r.Subscription_Status__c, 
                                    Zuora__TermStartDate__c, Zuora__TermEndDate__c,
                                    (SELECT Name, Zuora__Description__c, Zuora__EffectiveStartDate__c, Zuora__EffectiveEndDate__c, Zuora__Price__c, Zuora__Type__c, Zuora__SubscriptionRatePlan__r.Zuora__OriginalProductRatePlanId__c FROM Zuora__Subscription_Product_Charges__r ORDER BY Zuora__EffectiveStartDate__c) FROM Zuora__Subscription__c Where Zuora__Account__c = :zca.Zuora__Account__c];

        Test.startTest();
        SubscriptionTriggerHandler.populateEnterpriseAccessRight(complete_zs_list);
        Test.stopTest();
    }       


    @isTest static void testCreateSub() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();
        Zuora__CustomerAccount__c zca = [SELECT Zuora__Account__c,Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Name = 'test billing account'];
        Account a = [SELECT VV_Market__c FROM Account WHERE Id = :zca.Zuora__Account__c];
        
        ApexPages.CurrentPage().getparameters().put('aid',zca.Zuora__Account__c);
        vvCSRSubscriptionController ctrl = new vvCSRSubscriptionController();
        ctrl.a.VV_Market__c = null;
        ctrl.createSubscription();
        ctrl.a.VV_Market__c = 'US';

        system.debug('test create sub about to start:');
        Test.startTest();
        ctrl.createSubscription();
        Test.stopTest();
    }

    @isTest static void testSubscriptionController() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();
        Zuora__CustomerAccount__c zca = [SELECT Zuora__Account__c,Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Name = 'test billing account'];
        Account a = [SELECT VV_Market__c, FirstName, LastName, BillingStreet, BillingCity, BillingPostalCode, BillingState, Phone FROM Account WHERE Id = :zca.Zuora__Account__c];

        ApexPages.CurrentPage().getparameters().put('aid',zca.Zuora__Account__c);
        vvCSRSubscriptionController ctrl = new vvCSRSubscriptionController();
        ctrl.cancelSubscription();

        for (zqu__ProductRatePlan__c prp : vvProductManagement.getUpgradeList(zca.Zuora__Account__c)) {
            system.debug('upgradelist prp: ' + prp);
            ctrl.selectedUpgrade = prp.Id;
            ctrl.selectedDowngrade = prp.Id;
            if (prp.VV_Market__c == a.VV_Market__c && prp.VV_Feed__c != 'EOD' && prp.VV_Feed__c != 'ID') break;
        }

        Zuora__Subscription__c zs = [SELECT Zuora__Zuora_Id__c FROM Zuora__Subscription__c WHERE Zuora__Account__c = :zca.Zuora__Account__c LIMIT 1];

        Test.startTest();

        ctrl.upgradeSubscription();
        ctrl.downgradeSubscription();
        ctrl.switch_zs_id = zs.Id;
        ctrl.switchToAnnual();
        ctrl.switchToMonthly();
        ctrl.switchToQuarterly();
        ctrl.subscription_for_extend_trial = '12345';
        ctrl.extendTrial();
        
        ctrl.subscription_for_coupon = '12345';
        ctrl.coupon_code = '12345';
        ctrl.discount_period = 1;
        ctrl.applyDiscount();

        zs.SuspendDate__c = Date.today();
        zs.ResumeDate__c = Date.today().addDays(10);
        
        zs.Zuora__Status__c = 'Suspended';
        //update zs;
        ctrl.resume_sub = zs;
        ctrl.resumeNow();

        ctrl.getCurrentChargeSelectList();
        ctrl.getNumDiscountPeriods();
        ctrl.getInTrialSubs();
        ctrl.getTrialDurationTypes();
        ctrl.getSwitchBillingList();

        // filter
        ctrl.filter_cycle = 'monthly';
        ctrl.filter_product_type = 'Subscription';
        ctrl.filterUpgradeList();

        ctrl.getSuspendList();
        ctrl.getResumeList();
        ctrl.updateResumeFields();
        ctrl.getExpirationYears();
        

        // revert to legacy
        vvCSRSubscriptionController.CheckedSubs csubs = new vvCSRSubscriptionController.CheckedSubs(zs.Id,'VV EOD US Monthly');
        csubs.ischecked = true;
        ctrl.subs_for_checking = new List<vvCSRSubscriptionController.CheckedSubs>{csubs};
        ctrl.revertToLegacy();

        Test.stopTest();

        // test fraud
        Case c = new Case(Subject='test case');
        vvSubscriptionManagement.fraudManagement(a, c);


    }

    @isTest static void testConvertCurrency() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();
        Zuora__CustomerAccount__c zca = [SELECT Zuora__Account__c,Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Name = 'test billing account'];
        ApexPages.CurrentPage().getparameters().put('aid',zca.Zuora__Account__c);
        vvCSRSubscriptionController ctrl = new vvCSRSubscriptionController();

        Test.startTest();
        ctrl.payment_method.Credit_Card_Number__c = '4111111111111111';
        ctrl.payment_method.Enc_Value__c = 'asdfasfojdofiwjelfasdf';
        ctrl.a.Account_Currency__c = 'USD';
        ctrl.newcurrency = 'EUR';
        ctrl.convertCurrency();
        Test.stopTest();

    }


   /* @isTest static void testSubscriptionControllerSuspend() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();
        Zuora__CustomerAccount__c zca = [SELECT Zuora__Account__c,Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Name = 'test billing account'];
        Zuora__Subscription__c zs = [SELECT name,Zuora__Zuora_Id__c FROM Zuora__Subscription__c  LIMIT 1];
        Contract objContract =[select id,Accountid,ZSB__SubscriptionNumber__c from Contract where ZSB__SubscriptionNumber__c =:zs.name limit 1];

        ApexPages.CurrentPage().getparameters().put('aid',zca.Zuora__Account__c);
         ApexPages.CurrentPage().getparameters().put('cid',objcontract.id);
        vvCSRSubscriptionController ctrl = new vvCSRSubscriptionController();
        
        
        zs.SuspendDate__c = Date.today();
        zs.ResumeDate__c = Date.today().addDays(2);
        ctrl.suspend_sub = zs;
        ctrl.suspendsubid = zs.id;
        ctrl.getsubscriptionTosuspend();
        // test suspend messages
        ctrl.suspend_sub.ResumeDate__c = Date.today().addDays(-1);
        ctrl.suspendSubscription();
        ctrl.suspend_sub.ResumeDate__c = Date.today().addDays(181);
        ctrl.suspendSubscription();
        ctrl.suspend_sub.SuspendDate__c = Date.today().addMonths(-2);
        ctrl.suspendSubscription();
        ctrl.suspend_sub.ResumeDate__c = Date.today().addDays(-10);
        ctrl.suspendSubscription();


        // test update resume date
        
        ctrl.resume_sub = new Zuora__Subscription__c();
        ctrl.updateResumeDate();
        ctrl.resume_sub = zs;
        ctrl.resume_sub.ResumeDate__c = null;
        ctrl.updateResumeDate();
        ctrl.resume_sub.SuspendDate__c = Date.today();
        ctrl.resume_sub.ResumeDate__c = Date.today().addDays(-1);
        ctrl.updateResumeDate();
        ctrl.resume_sub.ResumeDate__c = Date.today().addDays(181);
        ctrl.updateResumeDate();
        ctrl.resume_sub.ResumeDate__c = Date.today().addDays(10);
        ctrl.updateResumeDate();

        // test
        Test.startTest();
        ctrl.suspend_sub.SuspendDate__c = Date.today();
        ctrl.suspend_sub.ResumeDate__c = Date.today().addDays(10);
        ctrl.suspendSubscription();
        test.stopTest();


    }*/

    @isTest static void testGateway() {
        Account a = new Account(BillingCountry = 'USA',Account_Currency__c='USD');
        vvCurrencyManagement.getGateway(a,'ACH');
        vvCurrencyManagement.getGateway(a,'Credit Card');
        a.Account_Currency__c = 'EUR';
        vvCurrencyManagement.getGateway(a,'Credit Card');
        a.Account_Currency__c = 'GBP';
        vvCurrencyManagement.getGateway(a,'Credit Card');
            
    }

    @isTest static void testCurrencyConversion() {
        Currency_Pricing_Rate__c cpr = new Currency_Pricing_Rate__c(Name='GBP',Rate__c=0.69,Effective_Date__c = Date.today().addDays(-10));
        insert cpr;
        cpr = new Currency_Pricing_Rate__c(Name='EUR',Rate__c=0.88,Effective_Date__c = Date.today().addDays(-10));
        insert cpr;
        vvCurrencyManagement.convertFromUSD(10,'EUR');
        vvCurrencyManagement.convertFromUSD(10,'GBP');
    }

    @isTest static void testDMLWS() {
        /*Account a = new Account(FirstName='asfosaifasfoiowef',LastName='asdofiweofsf');
        String result = vvDMLWSManagement.insertObject(a,'Account',null);
        insert a;
        Test.startTest();
        a.FirstName = 'test12345';
        vvDMLWSManagement.updateObject(a,'Account',null);
        Test.stopTest();*/
        Account acc = new Account();
            acc.firstname='test acc1';
            acc.lastName = 'test acc';
            acc.billingCity ='Test City';
            acc.billingStreet = 'TestStreet';
            acc.billingstate ='TestState';
            acc.billingPostalcode ='23451';
            acc.billingcountry ='United States';
            Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            acc.recordtypeID=objAccrec.id;
           acc.PersonMailingCity='Test City';
            acc.PersonMailingStreet = 'TestStreet';
            acc.PersonMailingState ='TestState';
            acc.PersonMailingPostalCode ='23451';
            acc.PersonMailingCountry ='United States';
            acc.phone='090123456987';
            //acc.ispersonAccount = true;
            insert acc;
            Account acc2 = acc;
            Test.startTest();
        acc.FirstName = 'test12345';
        vvDMLWSManagement.updateObject(acc,'Account',null);
        vvDMLWSManagement.insertObject(acc2,'Account', null);
        Test.stopTest();
    }
    @isTest static void testGetSessionId() {
        Test.startTest();
        vvDMLWSManagement.getSessionId();
        Test.stopTest();
    }       


    @isTest static void testBundle() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();
        Zuora__CustomerAccount__c zca = [SELECT Zuora__Account__c,Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Name = 'test billing account'];
        
        List<zqu__ProductRatePlan__c> zpp = [SELECT Id FROM zqu__ProductRatePlan__c WHERE zqu__Deleted__c = false AND (Name = 'Simulator' OR Name = 'Variator')];
        Bundle__c bundle = new Bundle__c(Name = 'test bundle');
        insert bundle;
        Bundle_Product__c bp = new Bundle_Product__c(Bundle__c = bundle.Id, Zuora_Product__c = zpp[0].Id, Price__c = 10);
        insert bp;
        Bundle_Product__c bp2 = new Bundle_Product__c(Bundle__c = bundle.Id, Zuora_Product__c = zpp[1].Id, Price__c = 10, HasTrial__c = true, Trial_Duration_Days__c = 10, Trial_Price__c = 10, Special_Period_Month__c = 3);
        insert bp2;
        //
        Account acc = new Account();
            acc.firstname='test acc1';
            acc.lastName = 'test acc';
            acc.billingCity ='Test City';
            acc.billingStreet = 'TestStreet';
            acc.billingstate ='TestState';
            acc.billingPostalcode ='23451';
            acc.billingcountry ='United States';
            Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            acc.recordtypeID=objAccrec.id;
           acc.PersonMailingCity='Test City';
            acc.PersonMailingStreet = 'TestStreet';
            acc.PersonMailingState ='TestState';
            acc.PersonMailingPostalCode ='23451';
            acc.PersonMailingCountry ='United States';
            acc.phone='090123456987';
            //acc.ispersonAccount = true;
            insert acc;
            Opportunity opp = new Opportunity(Name=acc.Name + ' Opportunity ' ,
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=acc.Id);
               SBQQ__Quote__c quoteRec = new SBQQ__Quote__c();
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SiteUser_Primary_Check__c = true;
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SBQQ__Opportunity2__c = opp.Id;
            //quoteRec.ZSB__BillToContact__c = c.Id;
           // quoteRec.ZSB__SoldToContact__c = c.Id;
            quoteRec.ZSB__BillCycleDay__c =  '1';
            quoteRec.ZSB__BillingBatch__c = 'Batch1';
            insert quoteRec ;                     
        //
        Campaign camp = new Campaign(Quote__c = quoteRec .id,Promo_Description__c ='abcde',Promo_Name__c = 'test',Name = 'test campaign', PromoCode__c = 'mattbestcoder',Bundle__c = bundle.Id, StartDate = Date.today().addDays(-1), EndDate = Date.today().addDays(1));
        insert camp;


        ApexPages.CurrentPage().getParameters().put('aid',zca.Zuora__Account__c);
        vvCSRSubscriptionController ctrl = new vvCSRSubscriptionController();

        ctrl.searchPromo();
        ApexPages.CurrentPage().getParameters().put('searchvar','1qaz2wsx');
        ctrl.searchPromo();


        ctrl.displayBundle();
        ApexPages.CurrentPage().getParameters().put('promo','abc'); // wrong
        ctrl.displayBundle();
        ApexPages.CurrentPage().getParameters().put('promo','mattbestcoder'); // correct
        ctrl.displayBundle();

        Bundle__c b = [SELECT Id FROM Bundle__c WHERE Id = :bundle.Id];

        Zuora__Subscription__c zs = [SELECT Zuora__Zuora_Id__c FROM Zuora__Subscription__c WHERE Zuora__Account__c = :zca.Zuora__Account__c LIMIT 1];
        bp.Sub_For_Replace__c = zs.Id;
        bp.Sub_For_Replace_Zuora_Id__c = zs.Zuora__Zuora_Id__c;
        bp.Sub_To_Suspend__c = true;

        ctrl.addBundle();

        

        ctrl.removeBundle();
        ctrl.bundle_to_cancel = b.Id;
        ctrl.removeBundle();

        
    }

    @isTest static void testBundleController() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();
        
        vvBundleControllernew bc = new vvBundleControllernew();
        ApexPages.CurrentPage().getParameters().put('promo','abc'); // wrong
        bc = new vvBundleControllernew();
        ApexPages.CurrentPage().getParameters().put('promo','1qaz2wsx'); // correct
        bc = new vvBundleControllernew();
        vvBundleControllernew.QuoteLine objQuoteline= new vvBundleControllernew.QuoteLine();
        objQuoteline.listprice=1.0;
        objQuoteline.duration='2';
        objQuoteline.productname='test';
        objQuoteline.StartDate=date.today();
        objQuoteline.NetTotal=12;

        ///vvBundleControllernew.getBundleInitialCharge(bc.bundle);

    }
    @isTest static void testBundleControllerold() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();
        
        vvBundleController bc = new vvBundleController();
        ApexPages.CurrentPage().getParameters().put('promo','abc'); // wrong
        bc = new vvBundleController();
        ApexPages.CurrentPage().getParameters().put('promo','1qaz2wsx'); // correct
        

        ///vvBundleControllernew.getBundleInitialCharge(bc.bundle);

    }
    @isTest static void testBundleController1() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();
        
        vvBundleControllernew bc = new vvBundleControllernew();
       // ApexPages.CurrentPage().getParameters().put('promo','abc'); // wrong
        //bc = new vvBundleControllernew();
        ApexPages.CurrentPage().getParameters().put('promo','1qaz2wsx'); // correct
        bc = new vvBundleControllernew();
        vvBundleControllernew.QuoteLine objQuoteline= new vvBundleControllernew.QuoteLine();
        objQuoteline.listprice=1.0;
        objQuoteline.duration='2';
        objQuoteline.productname='test';
        objQuoteline.StartDate=date.today();
        objQuoteline.NetTotal=12;

        ///vvBundleControllernew.getBundleInitialCharge(bc.bundle);

    }
    @isTest static void testBundleControllerold1() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();
        
        vvBundleController bc = new vvBundleController();
       // ApexPages.CurrentPage().getParameters().put('promo','abc'); // wrong
        //bc = new vvBundleControllernew();
        ApexPages.CurrentPage().getParameters().put('promo','1qaz2wsx'); // correct
        bc = new vvBundleController();
        

        ///vvBundleControllernew.getBundleInitialCharge(bc.bundle);

    }

    @isTest static void testMisc() {
        // test currency
        vvCurrencyManagement.convertCurrency(10, 'USD', 'EUR');
        vvCurrencyManagement.convertCurrency(10, 'USD', 'GBP');
        vvCurrencyManagement.convertCurrency(10, 'EUR', 'GBP');
        vvCurrencyManagement.convertCurrency(10, 'EUR', 'USD');
        vvCurrencyManagement.convertCurrency(10, 'GBP', 'EUR');
        vvCurrencyManagement.convertCurrency(10, 'GBP', 'USD');

        vvCurrencyManagement.getCurrencySymbol(new Account(Account_Currency__c = 'USD'));
        vvCurrencyManagement.getCurrencySymbol(new Account(Account_Currency__c = 'EUR'));
        vvCurrencyManagement.getCurrencySymbol(new Account(Account_Currency__c = 'GBP'));

        vvConstants.convertFeedText('EOD');
        vvConstants.convertFeedText('ID');
        vvConstants.convertFeedText('RT');
        vvConstants.convertFeedText('sdfas');
    }

    /*@isTest static void testCreateSubscriptionError() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();
        Zuora__CustomerAccount__c zca = [SELECT Zuora__Account__c,Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Name = 'test billing account'];
        Test.startTest();

        Campaign cam = new Campaign(Name = 'testtesttest');
        insert cam;
        // marketing params
        Map<String, String> customfields = new Map<String, String>{'CampaignId__c'=>cam.Id};

        // bundle
        Bundle__c bundle = [SELECT Id FROM Bundle__c LIMIT 1];
        customfields.put('BundleId',bundle.Id);

        vvSubscriptionManagement.createSubscription(new Account(Id=zca.Zuora__Account__c), zca.Zuora__Zuora_Id__c,'12345','12345', false, customfields);
        Test.stopTest();
    }*/

    @isTest static void testfraudManagement() {
      
        Account ci = new Account();
        ci.firstName = 'firstname';
        ci.lastName = 'LastName';
        ci.salutation = 'Mr.';
        string s= 'vvtest1'+system.now().format().remove(' ')+'@email.com';
        string s1 = s.remove(':');
        string s2 = s.remove('/');
        ci.BillingStreet = 'street';
        ci.BillingPostalCode = '30097';
        ci.BillingCity = 'city';
        ci.BillingState = 'state';
        ci.BillingCountry = 'USA';
        ci.Phone='7894561230';
     
        Case c = new Case(Subject='test');
        vvSubscriptionManagement.fraudManagement(ci, c);
        vvSubscriptionManagement.Tmethod();
        vvSubscriptionManagement.Tmethod1();
        vvSubscriptionManagement.Tmethod2();
        vvSubscriptionManagement.Tmethod3();
        vvSubscriptionManagement.Tmethod4();

    }
    @isTest static void testCreateMonthly1() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();
        Zuora__CustomerAccount__c zca = [SELECT Zuora__Account__c,Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Name = 'test billing account'];
        Test.startTest();

        Campaign cam = new Campaign(Name = 'testtesttest');
        insert cam;
        // marketing params
        Map<String, String> customfields = new Map<String, String>{'CampaignId__c'=>cam.Id};

        // bundle
       // Bundle__c bundle = [SELECT Id FROM Bundle__c LIMIT 1];
       
       product2 pro = new product2();
        pro.Name='Protesting';
        insert pro;
            
       Bundle__c bundle = new Bundle__c();
        bundle.Name ='testing';
        insert bundle;
        
        
        Bundle_Product__c bunpro = new Bundle_Product__c();
        //bunpro.Name='buntesting';
        bunpro.One_Time_Product__c=pro.Id;
        bunpro.Bundle__c=bundle.Id;
        insert bunpro;
        customfields.put('BundleId',bundle.Id);

        // monthly
        vvSubscriptionManagement.createSubscription(new Account(Id=zca.Zuora__Account__c), zca.Zuora__Zuora_Id__c,'12345','12345', true, customfields);
        Test.stopTest();
    }
    @isTest static void testremoveBundle() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();
       // Zuora__CustomerAccount__c zca = [SELECT Zuora__Account__c,Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Name = 'test billing account'];
        
        Test.startTest();
         Account acc = new Account();
            acc.firstname='test acc1';
            acc.lastName = 'test acc';
            acc.billingCity ='Test City';
            acc.billingStreet = 'TestStreet';
            acc.billingstate ='TestState';
            acc.billingPostalcode ='23451';
            acc.billingcountry ='United States';
            Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            acc.recordtypeID=objAccrec.id;
           acc.PersonMailingCity='Test City';
            acc.PersonMailingStreet = 'TestStreet';
            acc.PersonMailingState ='TestState';
            acc.PersonMailingPostalCode ='23451';
            acc.PersonMailingCountry ='United States';
            acc.phone='090123456987';
            //acc.ispersonAccount = true;
            insert acc;
        
        Zuora__CustomerAccount__c zca= new Zuora__CustomerAccount__c();
        zca.Name='zTesting';
        zca.Zuora__Zuora_Id__c='2c92c0f959687a9b01598a13f6e6677d';
        zca.Zuora__Account__c=acc.id;
        insert zca;

        
        
        Campaign cam = new Campaign(Name = 'testtesttest');
        insert cam;
        // marketing params
        Map<String, String> customfields = new Map<String, String>{'CampaignId__c'=>cam.Id};

        // bundle
       // Bundle__c bundle = [SELECT Id FROM Bundle__c LIMIT 1];
       
       product2 pro = new product2();
        pro.Name='Protesting';
        insert pro;
            
       Bundle__c bundle = new Bundle__c();
        bundle.Name ='testing';
        insert bundle;
        
        
        Bundle_Product__c bunpro = new Bundle_Product__c();
        //bunpro.Name='buntesting';
        bunpro.One_Time_Product__c=pro.Id;
        bunpro.Bundle__c=bundle.Id;
        insert bunpro;
        customfields.put('BundleId',bundle.Id);

        Zuora__Subscription__c zs = new Zuora__Subscription__c();
        zs.Name='testinggg';
        zs.Zuora__Status__c='Active';
        zs.Bundle_Product__c=bunpro.id;
        insert zs;
            
       Zuora__SubscriptionRatePlan__c zsub = new Zuora__SubscriptionRatePlan__c();
        zsub.Name='zsubTesting';
        zsub.Zuora__Subscription__c=zs.Id;
        zsub.Zuora__Account__c=acc.id;
        insert zsub;
        
        Upgrade_Suspended_Subscription__c upsub = new Upgrade_Suspended_Subscription__c();
        upsub.Upgraded_Subscription__c=zs.id;
        insert upsub;
        
        // monthly
        vvSubscriptionManagement.removeBundle(bundle.Id,zca.Zuora__Account__c,zca.Zuora__Zuora_Id__c );
        Test.stopTest();
    }
    @isTest static void testCreateMonthlyEdition() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();
        Zuora__CustomerAccount__c zca = [SELECT Zuora__Account__c,Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Name = 'test billing account'];
        Test.startTest();

        Campaign cam = new Campaign(Name = 'testtesttest');
        insert cam;
        // marketing params
        Map<String, String> customfields = new Map<String, String>{'CampaignId__c'=>cam.Id};

        // bundle
       // Bundle__c bundle = [SELECT Id FROM Bundle__c LIMIT 1];
       
       product2 pro = new product2();
        pro.Name='Protesting';
        insert pro;
            
       Bundle__c bundle = new Bundle__c();
        bundle.Name ='testing';
        insert bundle;
        
        
        Bundle_Product__c bunpro = new Bundle_Product__c();
        //bunpro.Name='buntesting';
        bunpro.One_Time_Product__c=pro.Id;
        bunpro.Bundle__c=bundle.Id;
        insert bunpro;
        customfields.put('VV_Edition__c',bundle.Id);
        customfields.put('Fraud_Score__c','6');

        // monthly
        vvSubscriptionManagement.createSubscription(new Account(Id=zca.Zuora__Account__c), zca.Zuora__Zuora_Id__c,system.today(),'12345','12345' ,true, customfields);
        Test.stopTest();
    }
    @isTest static void testbundleVerification() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();
       // Zuora__CustomerAccount__c zca = [SELECT Zuora__Account__c,Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Name = 'test billing account'];
        
        Test.startTest();
         Account acc = new Account();
            acc.firstname='test acc1';
            acc.lastName = 'test acc';
            acc.billingCity ='Test City';
            acc.billingStreet = 'TestStreet';
            acc.billingstate ='TestState';
            acc.billingPostalcode ='23451';
            acc.billingcountry ='United States';
            Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            acc.recordtypeID=objAccrec.id;
           acc.PersonMailingCity='Test City';
            acc.PersonMailingStreet = 'TestStreet';
            acc.PersonMailingState ='TestState';
            acc.PersonMailingPostalCode ='23451';
            acc.PersonMailingCountry ='United States';
            acc.phone='090123456987';
            //acc.ispersonAccount = true;
            insert acc;
        
        Zuora__CustomerAccount__c zca= new Zuora__CustomerAccount__c();
        zca.Name='zTesting';
        zca.Zuora__Zuora_Id__c='2c92c0f959687a9b01598a13f6e6677d';
        zca.Zuora__Account__c=acc.id;
        insert zca;

        
        
        Campaign cam = new Campaign(Name = 'testtesttest');
        insert cam;
        // marketing params
        Map<String, String> customfields = new Map<String, String>{'CampaignId__c'=>cam.Id};

        // bundle
       // Bundle__c bundle = [SELECT Id FROM Bundle__c LIMIT 1];
       
       product2 pro = new product2();
        pro.Name='Protesting';
        insert pro;
            
       Bundle__c bundle = new Bundle__c();
        bundle.Name ='testing';
        bundle.Required__c='Simulator';
        bundle.Cannot_CoExist__c='ID';
        insert bundle;
        
        
        Bundle_Product__c bunpro = new Bundle_Product__c();
        //bunpro.Name='buntesting';
        bunpro.One_Time_Product__c=pro.Id;
        bunpro.Bundle__c=bundle.Id;
        insert bunpro;
        customfields.put('BundleId',bundle.Id);

        Zuora__Subscription__c zs = new Zuora__Subscription__c();
        zs.Name='Trial';
        zs.Zuora__Status__c='Active';
        zs.Bundle_Product__c=bunpro.id;
        zs.Zuora__Zuora_Id__c=zca.Zuora__Zuora_Id__c;
        zs.Zuora__Account__c=acc.Id;
        
        insert zs;
            
       Zuora__SubscriptionRatePlan__c zsub = new Zuora__SubscriptionRatePlan__c();
        zsub.Name='zsubTesting';
        zsub.Zuora__Subscription__c=zs.Id;
        zsub.Zuora__Account__c=acc.id;
        insert zsub;
        
        Upgrade_Suspended_Subscription__c upsub = new Upgrade_Suspended_Subscription__c();
        upsub.Upgraded_Subscription__c=zs.id;
        insert upsub;
        
        /*Zuora__SubscriptionProductCharge__c subpro = new Zuora__SubscriptionProductCharge__c();
        subpro.Name='Trial';
        subpro.Zuora__EffectiveEndDate__c=date.parse('06/18/2030');
        subpro.Zuora__Subscription__c=zs.Id;
        insert subpro;*/
        // monthly
        vvSubscriptionManagement.bundleVerification(bundle,acc );
        Test.stopTest();
    }
}