public with sharing class AccController {
public account acc;
public AccController(ApexPages.StandardController controller) {
this.acc = (Account)controller.getRecord();
}
    public PageReference Save(){
    upsert acc;
    PageReference pNext = new PageReference('/'+acc.id);
    pNext.setRedirect(true);
    return pNext;
}
}