@isTest
private class vvUserManagementTests {
    
    @isTest static void testCreateCommunityUser() {
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

        // create user with role
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
            

        VVCommunitySettings__c setting = new VVCommunitySettings__c();
        setting.Name = 'OrgDefault';
        setting.Account_Owner_Name__c='Cloudware Team';
        setting.Customer_Profile__c='Customer Community Login User (Custom)';
        setting.Mobile_Customer_Profile__c='Customer Community Login User (Mobile)';
        //setting.Session_Uid__c='ps.vectorvest@cloudwareconnections.com.partial';
        //setting.Session_Pwd__c='pass4cwc4';
        //setting.Session_Security_Token__c='WAnfRbKiDc4plxJwuqJAMN43i';
        //setting.Org_Num__c='cs18';
        //setting.Client_Id__c='3MVG98RqVesxRgQ5VEQ7zQ0ZneBoxnO70.FZkgMcey7EzpJfT9vBaIQIYMqpH9QZ8kHHf44z8QiatCt4FkSfv';
        //setting.Client_Secret__c='1226336843678337315';
        insert setting;
        
        User portalAccountOwner1 = new User(
                UserRoleId = portalRole.Id,
                ProfileId = profile1.Id,
                Username = System.now().millisecond() + 'test4@test.com',
                Alias = 'batman',
                Email='bruce.wayne@wayneenterprises.com',
                EmailEncodingKey='UTF-8',
                Firstname='Bruce',
                Lastname='Wayne',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                TimeZoneSidKey='America/Chicago'
            );

        System.runAs ( thisUser ) {
            insert portalAccountOwner1;
        }


        // create account without password
         Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
           // acc.recordtypeID=objAccrec.id;
        Account a = new Account(Primary_Phone_Type__c='Mobile',PersonMailingCity='Test City',PersonMailingStreet = 'TestStreet',PersonMailingState ='TestState',PersonMailingPostalCode ='23451',PersonMailingCountry ='United States',OwnerId=portalAccountOwner1.Id,FirstName='testfirst',LastName='testlast',PersonEmail='u' + System.now().millisecond() + '1@vvsystemtest.com',Phone='2223334444',VV_Market__c='US',VV_Second_Market__c='CA',VV_Feed__c='EOD',VV_Edition__c='Non-Professional',recordtypeID=objAccrec.id,Subscription_Plan__c='Monthly',Is_Elite__c=false,Is_Elite_Legacy__c=false,Is_Apple_App_Store_Customer__c=false,BillingStreet='2234 test',BillingCity='Test',BillingState='GA',BillingCountry='USA',BillingPostalCode='22344');
        insert a;
       //PersonMailingCity='Test City',PersonMailingStreet = 'TestStreet',PersonMailingState ='TestState',PersonMailingPostalCode ='23451',PersonMailingCountry ='United States'

        vvUserManagement.createCommunityUser(a.Id, false);

        // create account with password
        Account a2 = new Account(Primary_Phone_Type__c='Mobile',PersonMailingCity='Test City',PersonMailingStreet = 'TestStreet',PersonMailingState ='TestState',PersonMailingPostalCode ='23451',PersonMailingCountry ='United States',Password__c='1qaz@WSX3edf',OwnerId=portalAccountOwner1.Id,FirstName='testfirst',LastName='testlast',PersonEmail='u' + System.now().millisecond() + '2@vvsystemtest.com',Phone='2223334444',VV_Market__c='US',VV_Second_Market__c='CA',VV_Feed__c='EOD',VV_Edition__c='Non-Professional',Subscription_Plan__c='Monthly',Is_Elite__c=false,Is_Elite_Legacy__c=false,Is_Apple_App_Store_Customer__c=false,BillingStreet='2234 test',BillingCity='Test',BillingState='GA',BillingCountry='USA',BillingPostalCode='22344');
        insert a2;

        vvUserManagement.createCommunityUser(a2.Id, false);
        Account a3 = new Account(Primary_Phone_Type__c='Mobile',PersonMailingCity='Test City',PersonMailingStreet = 'TestStreet',PersonMailingState ='TestState',PersonMailingPostalCode ='23451',PersonMailingCountry ='United States',Password__c='1qaz@WSX3edf',OwnerId=portalAccountOwner1.Id,FirstName='testfirst',LastName='testlast',PersonEmail='u' + System.now().millisecond() + '2@vvsystemtest.com',Phone='2223334444',VV_Market__c='US',VV_Second_Market__c='CA',VV_Feed__c='EOD',VV_Edition__c='Non-Professional',Subscription_Plan__c='Monthly',Is_Elite__c=false,Is_Elite_Legacy__c=false,Is_Apple_App_Store_Customer__c=true,BillingStreet='2234 test',BillingCity='Test',BillingState='GA',BillingCountry='USA',BillingPostalCode='22344',Vector_Vest_Customer_Id__c='dfggfgf');
        insert a3;

        vvUserManagement.createCommunityUser(a3.Id, false,true);
		vvUserManagement.activateUser(portalAccountOwner1.Id);
    }
}