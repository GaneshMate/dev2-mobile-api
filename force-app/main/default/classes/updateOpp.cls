global class updateOpp implements Database.Batchable<sObject>,Database.Stateful{
   global string quoteid;
   
   global updateOpp(string objquoteid){
       quoteid = objquoteid;
   }
   global Database.QueryLocator start(Database.BatchableContext BC){
      String query = 'SELECT id,QuoteCalFields__c,SBQQ__Primary__c,Promo_code__c,SBQQ__NetAmount__c,SiteUser_Primary_Check__c,SBQQ__Opportunity2__c,SBQQ__Opportunity2__r.Created_By_PortalCustomer__c FROM SBQQ__Quote__c WHERE id=\''+quoteid+'\'';
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC,
                       List<SBQQ__Quote__c> allQuoteObjs){
                       
       for(SBQQ__Quote__c qId : allQuoteObjs)
        {
           ZSB.ZSBConnectorUtils.createContractFromQuote(qId.id);
        }
   }

   global void finish(Database.BatchableContext BC){}
       

}