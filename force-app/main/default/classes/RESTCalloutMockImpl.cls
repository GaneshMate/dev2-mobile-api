@isTest
public class RESTCalloutMockImpl implements HttpCalloutMock {
	public RESTCalloutMockImpl() {}

	/**
	 * method to provide fake responses
	 * @param  req [description]
	 * @return     [description]
	 */
	public HTTPResponse respond(HTTPRequest req) {
		HttpResponse resp = new HttpResponse();
		resp.setStatusCode(200);
		resp.setStatus('Complete');

		if (req.getEndPoint().containsIgnoreCase('resume')) {	// resume subscription
			resp.setBody('{"success": true,"subscriptionId": "12345"}');
		} else if (req.getEndPoint().containsIgnoreCase('suspend')) {	// suspend subscription
			resp.setBody('{"success": true,"subscriptionId": "12345"}');
		} else if (req.getEndPoint().containsIgnoreCase('rsa-signatures')) {	// get rsa signature
			resp.setBody('{"success": true,"key": "12345","tenantId": "12345","token":"12345","signature":"12345"}');
		} else if (req.getEndPoint().containsIgnoreCase('submitPage')) {	// post the payment method
			resp.setBody('{"success": true,"refId":"12345"}');
		}
		


		return resp;
	}
}