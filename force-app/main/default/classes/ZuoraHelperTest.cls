@isTest(seeAllData=true)
private class ZuoraHelperTest {
    @isTest static void testAPIHelper() {

        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();

        Zuora__CustomerAccount__c zca = [SELECT Zuora__Account__c,Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Name = 'test billing account'];

        Test.startTest();
        zuoraAPIHelper.createAccount(new Account(LastName='testlast',FirstName='testfirst'), 'Credit Card');
        zuoraAPIHelper.updateAccount(zca.Zuora__Account__c,'test',zca.Zuora__Zuora_Id__c);
        zuoraAPIHelper.updateAcc(zca.Zuora__Account__c,'test',zca.Zuora__Zuora_Id__c);
        zuoraAPIHelper.setDefaultPayment(zca.Zuora__Zuora_Id__c,'12345', 'test');
        zuoraAPIHelper.createContact(new Account(LastName='testlast',FirstName='testfirst'),zca.Zuora__Zuora_Id__c);
        zuoraAPIHelper.updateContact(new Account(LastName='testlast',FirstName='testfirst'),'12345');
        zuoraAPIHelper.activateAccount(zca.Zuora__Account__c, zca.Zuora__Zuora_Id__c,'12345','12345');
        zuoraAPIHelper.deleteAccount(zca.Zuora__Zuora_Id__c);
        zuoraAPIHelper.cancelAccount(zca.Zuora__Zuora_Id__c);
       // zuoraAPIHelper.activateRateplans();
        zuoraAPIHelper.queryAPICall('select id from Subscription');
         zuoraAPIHelper.queryAPICall('select id from Account');
         zuoraAPIHelper.queryAPICall('select id from Invoice');
        zuoraAPIHelper.suspendSubscriptionWithResume('12345',System.today(),System.today()+1);
        zuoraAPIHelper.resumeSubscriptionFuture('12345',System.today(),true);
        zuoraAPIHelper.ondemandSyncFuture('12345');
        zuoraAPIHelper.testCoveragePassSubscribe();
        //zuoraAPIHelper.billingPreview(zca.Zuora__Zuora_Id__c,System.today());
        zuoraAPIHelper.TakePayment(zca.Zuora__Zuora_Id__c,50.00,'2c92c0f9528db6aa015290834d504e3e');
        zuoraAPIHelper.getCurrentCharges('12345');
        zuoraAPIHelper.createReferencedRefund('12345', 120,'test');
        zuoraAPIHelper.createNonReferencedRefund(zca.Zuora__Account__c, '12345', 100, 'test');
        zuoraAPIHelper.deleteAmendment('12345');
        zuoraAPIHelper.ondemandSync(zca.Id);
        zuoraAPIHelper.ondemandSync(zca.Zuora__Zuora_Id__c);
        zuoraAPIHelper.suspendSubscription('',date.today());
        Test.stopTest();

    }

    @isTest static void testAmendHelper() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();

        //Zuora__CustomerAccount__c zca = [SELECT Zuora__Account__c,Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Name = 'test billing account' limit 1];

        Test.startTest();
        ZuoraAmendHelper.cancelSubscription('12345',Date.today());
        ZuoraAmendHelper.renewSubscription('12345',Date.today());
        ZuoraAmendHelper.addRatePlanToSubscription('12345','12345',Date.today());
        ZuoraAmendHelper.removeRatePlanFromSubscription('12345','12345',Date.today());
        zuoraAmendHelper.updateRatePlanPricing('12345', '12345', '12345', Date.today(), 20);
        Test.stopTest();

    }
    @isTest static void voidactivateRateplans(){
        zqu__ZProduct__c prod = new zqu__ZProduct__c();
        prod.Name = 'test';
        prod.zqu__Deleted__c = False;
        insert prod;
        
        zqu__ProductRatePlan__c prodplan = new zqu__ProductRatePlan__c();
        prodplan.zqu__ZProduct__c = prod.Id;
        prodplan.VV_Type__c = 'Standard';
        prodplan.zqu__Deleted__c=False;
        insert prodplan;
        zuoraAPIHelper.activateRateplans();
        
        
        
    }
}