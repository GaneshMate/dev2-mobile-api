public without sharing class ZuoraSubscriptionTriggerHandler {
    /***
    updating related contract
    **/
    public static void updatecontract(List<Zuora__Subscription__c> zslist){
        map<string,Zuora__Subscription__c> mapzuorasub = new map<string,Zuora__Subscription__c>();
        set<id> SetAccountid = new set<id>();
        for(Zuora__Subscription__c objsub:zslist){
           if(objsub.SuspendDate__c != null){
               mapzuorasub.put(objsub.name,objsub);
               SetAccountid.add(objsub.Zuora__Account__c);
           }
        }
        list<contract> lstcontract = new list<contract>();
        for(Contract objContract : [select id,SuspendDate__c,ZSB__SubscriptionNumber__c, Resumedate__c, subscription_status__c from contract where ZSB__SubscriptionNumber__c in: mapzuorasub.keyset() and Accountid in:SetAccountid]){
            objcontract.subscription_status__c = mapzuorasub.get(objContract.ZSB__SubscriptionNumber__c).Zuora__Status__c;
            objcontract.SuspendDate__c = mapzuorasub.get(objContract.ZSB__SubscriptionNumber__c).SuspendDate__c;
            objcontract.Resumedate__c = mapzuorasub.get(objContract.ZSB__SubscriptionNumber__c).ResumeDate__c;
            lstcontract.add(objcontract);
        }
        if(lstcontract.size()>0){
            update lstcontract;
        }
    }
    

    /**
     * on subscription insert, if campaignid, bundleid, or bundleproductid fields are populated, auto populate the lookup fields
     */
    public static void updateLookupFields(List<Zuora__Subscription__c> zslist) {
        for (Zuora__Subscription__c zs : zslist) {
            if (zs.CampaignId__c != null) zs.Campaign__c = zs.CampaignId__c;
            if (zs.BundleProductId__c != null) zs.Bundle_Product__c = zs.BundleProductId__c;
            // add bundle stuff later
        }
    }

    /**
     * on subscription insert, check to see if there is a UpgradeSuspendSubscription record with this sub's Zuora__Zuora_Id__c, 
     * if there is and Upgraded_Subscription__c = null, then populate the upgraded subscription field with the actual sf id
     */
    public static void updateUSSSubId(List<Zuora__Subscription__c> zslist) {
        Map<String, Id> zuoraid_to_sfid_map = new Map<String, Id>();
        for (Zuora__Subscription__c zs : zslist) {
            zuoraid_to_sfid_map.put(zs.Zuora__Zuora_Id__c, zs.Id);
        }

        List<Upgrade_Suspended_Subscription__c> usslist = [SELECT Upgraded_Sub_Zuora_Id__c FROM Upgrade_Suspended_Subscription__c WHERE Upgraded_Sub_Zuora_Id__c IN :zuoraid_to_sfid_map.keySet() AND Upgraded_Subscription__c = null];

        for (Upgrade_Suspended_Subscription__c uss : usslist) {
            uss.Upgraded_Subscription__c = zuoraid_to_sfid_map.get(uss.Upgraded_Sub_Zuora_Id__c);
        }

        if (!usslist.isEmpty()) update usslist;
    }
}