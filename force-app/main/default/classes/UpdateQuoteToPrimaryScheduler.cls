public without sharing class UpdateQuoteToPrimaryScheduler implements ScheduledDispatcher.IScheduleDispatched {
        
    public void execute(SchedulableContext sc) {
            Id updateQuoteBatchInstanceId = Database.executeBatch(new UpdateQuoteToPrimary(),1);
    }
}