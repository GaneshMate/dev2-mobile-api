@RestResource(urlMapping = '/customer/info/*')
global class CustomerInfoService {
    
    /**
           URL takes the following form:
           /customer/subscriptions?{user id type}={user id} 
           - User id type is one of the ID TYPE values below
           - User id is the value of the user identifier (based on teh type)
       */

    global class ApplicationAccessClaim {
        global ApplicationAccessClaim(string cv, string m, string isTrial, string subscriptionEndDate, string isObservable) {
            this.claimValue = cv;
            this.mask = m;
            this.isTrial= isTrial;
            this.subscriptionEndDate= subscriptionEndDate;
            this.isObservable= isObservable;
        }
        global string claimValue;
        global string mask;
        global string isTrial;
        global string subscriptionEndDate;
        global string isObservable;
    }

    global class SalesforcePurchase {
        global SalesforcePurchase( string pn, datetime pd, decimal pa) {
            this.purchaseName = pn;
            this.purchaseDate = pd;
            this.purchaseAmount = pa;
        }
        global string purchaseName;
        global datetime purchaseDate;
        global decimal purchaseAmount;
    }
    
    global class subscriptionProductCharge {
        
        global date termStartDate;
        global date termEndDate;
        global date contractEffectiveDate;
        global String initialTerm;
        global String termSettingType;
        global String subscriptionNumber;
        global String status;
        global String name;
        global decimal price;
        global decimal quantity;
        global String ratePlanName;
        
        global subscriptionProductCharge( date tsd, date ted, date ced,string it,string tst,string sn,string status,string name,decimal price,decimal quantity,string rpn) {
            this.termStartDate = tsd;
            this.termEndDate = ted;
            this.contractEffectiveDate = ced;
            this.initialTerm = it;
            this.termSettingType = tst;
            this.subscriptionNumber = sn;
            this.status = status;
            this.name = name;
            this.price = price;
            this.quantity = quantity;
            this.ratePlanName = rpn;
            
        }
    }
 
    global class LiveEventRegistration {
        global LiveEventRegistration(string len, datetime lerd, datetime led, decimal pp, string afn, string aln, string acaid) {
            this.liveEventName = len;
            this.liveEventRegistrationDate = lerd;
            this.liveEventDate = led;
            this.pricePaid = pp;
            this.attendeeFirstName = afn;
            this.attendeeLastName = aln;
            this.attendeeCrmAccountId = acaid;
        }
        global string liveEventName;
        global datetime liveEventRegistrationDate;
        global datetime liveEventDate;
        global decimal pricePaid;
        global string attendeeFirstName;
        global string attendeeLastName;
        global string attendeeCrmAccountId;
    }

    global class CustomerInfo {
        public CustomerInfo() {
            this.applicationAccessClaims = new list<ApplicationAccessClaim>();
            this.salesforcePurchases = new list<SalesforcePurchase>();
            this.liveEventRegistrations = new list<LiveEventRegistration>();
            this.subscriptionProductCharge = new list<SubscriptionProductCharge>();
        }
        global string accountId;
        global String zuora_aid;
        global string userName;
        global String password;
        global string applicationUserId;
        global string enterpriseCustomerId;
        global string firstName;
        global string lastName;
        global string salutation;
        global string email;
        global string optionsProUserName;
        global string optionsProUserPassword;
        global string countryOfResidence;
        global string accountcurrency;
        global list<ApplicationAccessClaim> applicationAccessClaims;
        global list<SalesforcePurchase> salesforcePurchases;
        global list<LiveEventRegistration> liveEventRegistrations;
        global list <SBQQ__Subscription__c> subscriptionList;
        global list <Contract> ContractList;
        global list<SubscriptionProductCharge> subscriptionProductCharge;

        // post parameters
        global String BillingStreet;
        global String BillingPostalCode;
        global String BillingCity;
        global String BillingState;
        global String BillingCountry;
        global String feed; // EOD, ID, RT
        global String market;
        global String secondaryMarket;
        global String edition;  // Professional, Non-Professional
        global String subscriptiontype; // Monthly, Quarterly, Annual
        global Boolean applestore;
        global String certificateId;  //required for tax exempt
        /* LastModified Date 07/05/2017 - Added OptinMarketing feild */
        global String optinMarketing;
        global String vectorVestCustomerId;
    }

    global class Error {
        global integer code;
        global string msg;
        global Error(integer code, string msg) {
            this.code = code;
            this.msg = msg;
        }
    }
    private static string serialize(object o) {
        return JSON.serialize(o);
    }
    /* Ganesh :: LastModified Date 03/07/2019 - Commented USER_NAME  :: LastModified Date 12/07/2019 - Assign vun to Vector vest Customer ID*/
    private static string ID_TYPE_VECTORVEST_CUSTOMER_ID  = 'vun';    // Vector Vest User Name
    private static string ID_TYPE_APP_USER_ID  = 'aid';    // Application User Id
    private static string ID_TYPE_ENT_CUST_ID  = 'eid';    // Enterprise Customer Id
    private static string Email  = 'email';
    // Ganesh:: 22 NOV 2018 :: Make TaxExempt as blank.There is validtion rule if SBQQ__TaxExempt__c ='YES' then CertificateID is Required
    private static string TaxExempt = '';
    private static Boolean accountFlag=false;
   @HttpGet
    global static void getCustomerInfo() {

        integer retCode = 0;
        string retBody = '';

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        //System.debug('***req.params.get(ID_TYPE_USER_NAME)****'+req.params.get(ID_TYPE_USER_NAME));
        integer idcnt = 0;
        /* Ganesh :: LastModified Date 03/07/2019 - Commented USER_NAME*/
        string vun = req.params.get(ID_TYPE_VECTORVEST_CUSTOMER_ID);     if (!string.isBlank(vun)) idcnt++;
        string aid = req.params.get(ID_TYPE_APP_USER_ID);   if (!string.isBlank(aid)) idcnt++;
        string eid = req.params.get(ID_TYPE_ENT_CUST_ID);   if (!string.isBlank(eid)) idcnt++;
        string email = req.params.get(Email);           if (!string.isBlank(email)) idcnt++;
        //System.debug('***vun***'+vun);
        System.debug('***aid***'+aid);
        System.debug('***eid***'+eid);
        System.debug('***email***'+email);
        if (idcnt == 0) {
            retCode = 400;
            retBody = serialize(new Error(retCode, 'No customer identifier found'));
        } else if (idcnt > 1) {
            retCode = 400;
            retBody = serialize(new Error(retCode, 'Multiple customer identifiers found'));
        } else {
            CustomerInfo cs = null;
            try {
                /* Ganesh :: LastModified Date 03/07/2019 - Commented USER_NAME :: LastModified Date 12/07/2019 - Assign vun to Vector vest Customer ID*/
                if (null != vun) cs = getCustomerInfoByVectorVestCustomerID(vun);
                else if (null != aid) cs = getCustomerInfoByAppUserId(aid);
                else if (null != eid) cs = getCustomerInfoByEntCustId(eid);
                else if (null != email) cs = getCustomerInfoByEmail(email); 
                System.debug('*****csssss****'+cs);
                if(accountFlag){
                    retCode = 404;
                    retBody = serialize(new Error(retCode, 'Duplicate VV customer IDs found'));
                }else if (null == cs) {
                    // user not found
                    retCode = 404;
                    retBody = serialize(new Error(retCode, 'Customer not found'));
               } else {
                    // get zuora billing account id 
                    String query = 'SELECT Id FROM Account WHERE CrmId = \'' + cs.accountId + '\'';
                   System.debug('query*********'+query);
                   if (Test.isRunningTest()){
                        cs.zuora_aid ='12345' ;   
                   }
                   if (!Test.isRunningTest()){
                       for (Zuora.zObject rs : zuoraAPIHelper.queryAPICall(query)) {
                           // suspend all of them
                           System.debug('rs*********'+rs);
                           cs.zuora_aid = String.valueOf(rs.getValue('Id'));
                           System.debug('query*********'+cs.zuora_aid);
                           break;
                       }
                   }
                    retCode = 200;
                    retBody = serialize(cs);
                }
                if(test.isRunningTest()){
                Integer i = 9/0;
            }
            } catch (Exception e) {
                retCode = 500;
                retBody = serialize(new Error(retCode, 'Error retrieving customer info [' + e.getMessage() + ']'));
            }
        }

        res.headers.put('Content-type', 'application/json');
        res.statusCode = retCode;
        res.responseBody = Blob.valueOf(retBody);
    }
    @HttpPost
    global static void doPost() {
        
        integer retCode = 0;
        string retBody = '';
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        CustomerInfo ci =(CustomerInfo) System.JSON.deserialize(body.toString(), CustomerInfo.class);
        System.debug('***ci****'+ci);
        
        // check to see if email exist
        List<Account> alist = [SELECT Id FROM Account WHERE PersonEmail LIKE :ci.email];
        System.debug('****alist****'+alist);
        
        // Ganesh :: 30 Nov 2018 :: Check if user exists.
        // Ganesh :: 4-7-2019 commented user code
        /*List<User> userList = [SELECT Alias,Email,IsActive,Name,Username FROM User where Username LIKE :ci.userName];
        System.debug('****userList****'+userList);*/
        
        //Insert Account
        // Ganesh:: 22 NOV 2018 :: remove validation of applestore and certificateId 
        //if(alist.isEmpty() && !String.isBlank(String.valueOf(ci.firstName)) && !String.isBlank(String.valueOf(ci.lastName)) && !String.isBlank(String.valueOf(ci.email)) && !String.isBlank(String.valueOf(ci.applestore)) && !String.isBlank(String.valueOf(ci.certificateId))){
        // Ganesh :: 30 Nov 2018 :: if user is existing then give validation error.
        // Ganesh :: 4-7-2019 commented user code
        //if(userList.isEmpty()){  
            if(alist.isEmpty() && !String.isBlank(String.valueOf(ci.firstName)) && !String.isBlank(String.valueOf(ci.lastName)) && !String.isBlank(String.valueOf(ci.email)) ){  
                if(String.isBlank(ci.optinMarketing) || ci.optinMarketing.equalsIgnoreCase('Yes') || ci.optinMarketing.equalsIgnoreCase('No')){
                    System.debug('****inside accoubt creation****');
                    Account AccRec = new Account();
                    Boolean isValidUserName = true;
                    /*Boolean isValidUserName = false;
                    if(!String.isBlank(String.valueOf(ci.userName))){
                        isValidUserName = validateUserName(ci.userName);    
                    }
                    System.debug('****isValidUserName****'+isValidUserName);*/
                    
                    /* Boolean isValidPassword = false;
					if(!String.isBlank(String.valueOf(ci.password))){
                    isValidPassword = validatePassword(ci.password);
                    }
                    //System.debug('****isValidPassword****'+isValidPassword);
                    if((isValidPassword && isValidUserName) ||(isValidPassword && String.isBlank(String.valueOf(ci.userName)))){
                    System.debug('****in side create customer block****'+isValidUserName);
                    AccRec = createCustomer(ci);    
                    }*/
                    
                    if(isValidUserName || String.isBlank(String.valueOf(ci.userName))){
                        System.debug('****in side create customer block****'+isValidUserName);
                        AccRec = createCustomer(ci);    
                    }
                    
                    if(AccRec.Id!= null){
                        
                        retCode = 200;
                        retBody = serialize(AccRec);  
                        System.debug('****retCode****'+retCode);
                    }else{
                        retCode = 500;
                        // Ganesh :: 29/11/2018 Remove Is apple flag validation.
                        /*if(!ci.applestore){
                            retBody = serialize(new Error(retCode, 'Error:value for appleStore should be true '));    
                          }else */
                        
                        if(!String.isBlank(String.valueOf(ci.userName)) && !isValidUserName){
                            retBody = serialize(new Error(retCode, 'Error:User name should be in the form of Email!')); 
                        }/*else if(!isValidPassword){
                        retBody = serialize(new Error(retCode, 'Error:The password should have atleast 8 characters and Must mix numbers, uppercase and lowercase letters, and special characters!')); 
                        }*/else{
                                retBody = serialize(new Error(retCode, 'Error:creating cusomer: '));    
                            }
                        
                        System.debug('***retCode*****'+retCode);
                    } 
                }else{
                    retCode = 400; 
                    retBody = serialize(new Error(retCode, 'Error:optinMarketing must be in format(Yes/No)'));    
                }
            }else{
                retCode = 500;
                if (!alist.isEmpty() && !String.isBlank(String.valueOf(ci.email))) {
                    retBody = serialize(new Error(retCode, 'Error: This email already exist in the system for Account Id: ' + alist[0].Id));
                    System.debug('****retCode****'+retCode);
                }else{
                    retBody = serialize(new Error(retCode, 'Error: firstname, lastname, email,applestore,certificate Id are required fields'));
                    System.debug('****retCode****'+retCode);
                }
            }
        /*}else{
            retCode = 500;
                retBody = serialize(new Error(retCode, 'Error: This userName already exist in the system for User Id: ' + userList[0].Id));
                System.debug('****retCode****'+retCode);
        }*/
        
        res.headers.put('Content-type', 'application/json');
        res.statusCode = retCode;
        res.responseBody = Blob.valueOf(retBody);
        System.debug('****retCode****'+retCode);
    }
    @HttpPut  
    global static void setPassword(String eid,String password){
        integer retCode = 0;
        string retBody = '';

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        if(String.isBlank(eid)){
            retCode = 400; 
            retBody = serialize(new Error(retCode, 'Error:Enterprise Customer ID is required!'));
        }else if(String.isBlank(password)){
            retCode = 400; 
            retBody = serialize(new Error(retCode, 'Error:Password is required!'));
        }else{
            String result = setPasswordToCommuntyUser(eid,password);
            if(result.equalsIgnoreCase('Success')){
                retCode = 200; 
                retBody = serialize(new Error(retCode, 'Success'));  
            }else if(result.equalsIgnoreCase('Account Not Present!')){
                retCode = 400; 
                retBody = serialize(new Error(retCode, result));
            }else{
                retCode = 500; 
                retBody = serialize(new Error(retCode, result)); 
            }
        }
        res.headers.put('Content-type', 'application/json');
        res.statusCode = retCode;
        res.responseBody = Blob.valueOf(retBody);
    }
    public static String setPasswordToCommuntyUser(String eid , String password){
        
        String statusMsg = '';
        List<Account> accountList = new List<Account>();
        accountList = [SELECT Id,Vector_Vest_Customer_Id__c FROM Account WHERE EnterpriseCustomerID__c=:eid ORDER BY CreatedDate Desc];
        System.debug('accountList-------'+accountList);
        List<User> user = new List<User>();
        if(!accountList.isEmpty()){
             user = [SELECT Id FROM User WHERE UserName =: accountList[0].Vector_Vest_Customer_Id__c];  
            if(user.isEmpty()){
                statusMsg = 'No User Present!'; 
            }
        }else{
            statusMsg = 'Account Not Present!'; 
        }
        System.debug('accountList :: '+ accountList);
        if(!accountList.isEmpty() && !user.isEmpty()){
            //statusMsg = vvUserManagement.setPassword(user.Id ,accountList[0].Id,password);
            try{
                System.debug('Set Password :: '+ password);
                System.debug('EID :: '+ eid);
                System.debug('UsereID :: '+ user[0].Id);
                System.setPassword(user[0].Id, password);
                statusMsg = 'Success';
            }catch(Exception e){
                System.debug('Exception '+e.getMessage());
                statusMsg = e.getMessage();
            }
            
        }
        return statusMsg;
    }


    /* Ganesh :: LastModified Date 03/07/2019 - Commented USER_NAME :: LastModified Date 12/07/2019 - Assign vun to Vector vest Customer ID*/
    public static CustomerInfo getCustomerInfoByVectorVestCustomerID(string vectorVestCustomerID) {
        CustomerInfo cs = null;
        /*List<User> usrs = [select Id, UserName, ContactId from User where IsActive = true and UserName = :userName];
        if (!usrs.isEmpty()) {
            cs = getCustomerInfoForUser(usrs.get(0));
        }*/
        List<Account> accounts = [select Id from Account where Vector_Vest_Customer_Id__c = :vectorVestCustomerID];
        if(accounts.size()>1){
            accountFlag=true;
        }else if (!accounts.isEmpty()) {
            cs = getCustomerInfoForUser(accounts.get(0));
        }
        return cs;
    }

    public static CustomerInfo getCustomerInfoByAppUserId(string appUserId) {
        CustomerInfo cs = null;
        /*List<User> usrs = [select Id, UserName, ContactId from User where IsActive = true and ContactId in (select PersonContactId from Account where ApplicationUserID__c = :appUserId)];
        if (!usrs.isEmpty()) {
            cs = getCustomerInfoForUser(usrs.get(0));
        }*/
        List<Account> accounts = [select Id from Account where ApplicationUserID__c = :appUserId];
        if (!accounts.isEmpty()) {
            cs = getCustomerInfoForUser(accounts.get(0));
        }
        return cs;
    }

    public static CustomerInfo getCustomerInfoByEntCustId(string entCustId) {
        CustomerInfo cs = null;
       /*List<User> usrs = [select Id, UserName, ContactId from User where IsActive = true and ContactId in (select PersonContactId from Account where EnterpriseCustomerID__c = :entCustId)];
        if (!usrs.isEmpty()) {
            cs = getCustomerInfoForUser(usrs.get(0));
        }*/
        List<Account> accounts = [select Id from Account where EnterpriseCustomerID__c = :entCustId];
        if (!accounts.isEmpty()) {
            cs = getCustomerInfoForUser(accounts.get(0));
        }
        return cs;
    }
     public static CustomerInfo getCustomerInfoByEmail(string email) {
        CustomerInfo cs = null;
        /*List<User> usrs = [select Id, UserName, ContactId from User where IsActive = true and ContactId in (select PersonContactId from Account where personEmail = :email)];
       System.debug('usrs-------'+usrs);
         if (!usrs.isEmpty()) {
            cs = getCustomerInfoForUser(usrs.get(0));
        }*/
         List<Account> accounts = [select Id from Account where personEmail = :email];
        if (!accounts.isEmpty()) {
            cs = getCustomerInfoForUser(accounts.get(0));
        }
        return cs;
    }
    public static CustomerInfo getCustomerInfoByContractId(String contractNo){
        CustomerInfo cs = new CustomerInfo();
        List<Contract> contractList = [SELECT Id ,Account.EnterpriseCustomerID__c FROM Contract WHERE ContractNumber=:contractNo];
        if(contractList.size()>0){
            cs = getCustomerInfoByEntCustId(contractList[0].Account.EnterpriseCustomerID__c);    
        }else{
            return null;
        }
        return cs;
    }
    //private static CustomerInfo getCustomerInfoForUser(User usr) {
     private static CustomerInfo getCustomerInfoForUser(Account accounts) {
        CustomerInfo cs = new CustomerInfo();
        
        if (null != accounts.Id) {
            /*List<Account> accs = [select  Id, ApplicationUserID__c, EnterpriseCustomerID__c,
                                          FirstName, LastName, Salutation, PersonEmail, PersonMailingCountry,
                                          OptionsPro_Username__c, OptionsPro_Password__c,
                                          
                                          VV_Feed__c, VV_Market__c, VV_Edition__c, VV_Second_Market__c, 
                                          Subscription_Plan__c, Account_Currency__c, Is_Apple_App_Store_Customer__c, 
                                          (select ClaimValue__c, Mask__c, Is_Trial__c,Subscription_End_Date__c,Is_Observable__c from Enterprise_Access_Rights__r),
                                          (select Zuora__Description__c, Zuora__Price__c, Zuora__EffectiveStartDate__c  from R00N40000001lGjTEAU__r)
                                  from Account
                                  where PersonContactId = :usr.ContactId];*/
            
            /* LastModified Date 07/05/2017 - Added GDPR_Opt_in__pc feild */
            List<Account> accs = [select  Id, ApplicationUserID__c, EnterpriseCustomerID__c,
                                          FirstName, LastName, Salutation, PersonEmail, PersonMailingCountry,
                                          OptionsPro_Username__c, OptionsPro_Password__c,Vector_Vest_Customer_Id__c,
                                          
                                          VV_Feed__c, VV_Market__c, VV_Edition__c, VV_Second_Market__c, 
                                          Subscription_Plan__c, Account_Currency__c, Is_Apple_App_Store_Customer__c, GDPR_Opt_in__pc,
                                          (select ClaimValue__c, Mask__c, Is_Trial__c,Subscription_End_Date__c,Is_Observable__c from Enterprise_Access_Rights__r),
                                          (select name, Zuora__Price__c, Zuora__EffectiveStartDate__c  from R00N40000001lGjTEAU__r)
                                  from Account
                                  where Id = :accounts.Id];
            System.debug('*******accs*******'+accs);
            /* Ganesh :: LastModified Date 03/07/2019 - Commented Subscriptions and Contracts*/
            /*List<SBQQ__Subscription__c> subscriptions = [select SBQQ__ContractNumber__c,
                                                                SBQQ__Contract__c,
                                                                SBQQ__Product__c,
                                                                SBQQ__Quantity__c,
                                                                SBQQ__NetPrice__c,
                                                                SBQQ__SpecialPrice__c,
                                                                SBQQ__ListPrice__c,
                                                                SBQQ__CustomerPrice__c,
                                                                SBQQ__RegularPrice__c,
                                                                SBQQ__Account__c,
                                                                ZSB__PRPChargeId__c,
                                                                ZSB__PRPlanId__c,
                                                                SBQQ__StartDate__c,
                                                                SBQQ__EndDate__c,
                                                                SBQQ__TerminatedDate__c,
                                                                SBQQ__ProductName__c,
                                                                SBQQ__Contract__r.Status,
                                                                Contract_Term__c,
                                                                SBQQ__Contract__r.ContractTerm,
                                                                SBQQ__Contract__r.ZSB__TermType__c
                                                                from SBQQ__Subscription__c where SBQQ__Account__c =:accs[0].Id];
          
             System.debug('*******subscriptions*******'+subscriptions);
          List<contract> contracts =[select AccountId,
                                                StartDate,
                                                EndDate,
                                                Status,
                                                CurrencyIsoCode,
                                                ContractTerm,
                                                SBQQ__RenewalTerm__c,
                                                ZSB__TermType__c,
                                                VV_SubscriptionStartDate__c,
                                                ZSB__SubscriptionId__c,
                                                ZSB__SubscriptionNumber__c from contract where AccountId=:accs[0].Id];
             System.debug('*******contracts*******'+contracts);*/
            if (!accs.isEmpty()) {
               // List<evt__Attendee__c> atts = [select Id, CreatedDate, evt__Invitation_Status__c, evt__Contact__c, evt__Primary_Attendee__c, evt__Event__c, evt__Payment__c, evt__Contact__r.FirstName, evt__Contact__r.LastName, evt__Event__r.Name, evt__Event__r.evt__Start__c, evt__Payment__r.pymt__Amount__c, evt__Primary_Attendee__r.evt__Contact__r.AccountId from evt__Attendee__c where evt__Contact__c = :usr.ContactId];
                List<evt__Attendee__c> atts = [select Id, CreatedDate, evt__Invitation_Status__c, evt__Contact__c, evt__Primary_Attendee__c, evt__Event__c, evt__Payment__c, evt__Contact__r.FirstName, evt__Contact__r.LastName, evt__Event__r.Name, evt__Event__r.evt__Start__c, evt__Payment__r.pymt__Amount__c, evt__Primary_Attendee__r.evt__Contact__r.AccountId from evt__Attendee__c where evt__Contact__c =:accounts.Id];

                List<Zuora__SubscriptionProductCharge__c> subscriptionProductCharge = [SELECT Zuora__Subscription__r.Zuora__TermStartDate__c,Zuora__Subscription__r.Zuora__TermEndDate__c,Zuora__Subscription__r.Zuora__ContractEffectiveDate__c,Zuora__Subscription__r.Zuora__InitialTerm__c,Zuora__Subscription__r.Zuora__TermSettingType__c,Zuora__Subscription__r.Zuora__SubscriptionNumber__c,Zuora__Subscription__r.Zuora__Status__c,Name,Zuora__Price__c,Zuora__Quantity__c,Zuora__RatePlanName__c FROM Zuora__SubscriptionProductCharge__c where Zuora__Account__c =:accounts.Id];
                Account acc = accs.get(0);
                cs.accountId = acc.Id;
                cs.applicationUserId = acc.ApplicationUserID__c;
                cs.enterpriseCustomerId = acc.EnterpriseCustomerID__c;
                cs.firstName = acc.FirstName;
                cs.lastName = acc.LastName;
                cs.salutation = acc.Salutation;
                cs.email = acc.PersonEmail;
                cs.countryOfResidence = acc.PersonMailingCountry;
                cs.optionsProUserName = acc.OptionsPro_Username__c;
                cs.optionsProUserPassword = acc.OptionsPro_Password__c;
                cs.feed = acc.VV_Feed__c;
                cs.market = acc.VV_Market__c;
                cs.secondaryMarket = acc.VV_Second_Market__c;
                cs.edition = acc.VV_Edition__c;
                cs.subscriptiontype = acc.Subscription_Plan__c;
                cs.accountcurrency = acc.Account_Currency__c;
                cs.applestore = acc.Is_Apple_App_Store_Customer__c;
                cs.vectorVestCustomerId=acc.Vector_Vest_Customer_Id__c;
                cs.userName = acc.Vector_Vest_Customer_Id__c;
                /* Ganesh :: LastModified Date 03/07/2019 - Commented Subscriptions and Contracts*/
                //cs.subscriptionList = subscriptions;
                //cs.ContractList = contracts;
                /* LastModified Date 07/05/2017 - Added GDPR_Opt_in__pc feild */
                cs.optinMarketing = acc.GDPR_Opt_in__pc;
                list<ApplicationAccessClaim> aacs = cs.applicationAccessClaims;
                list<SalesforcePurchase> sfps = cs.salesforcePurchases;
                list<LiveEventRegistration> lers = cs.liveEventRegistrations;
                list<SubscriptionProductCharge> spcs = cs.subscriptionProductCharge;
                System.debug('accc------------------------'+acc);
                
                for (Enterprise_Access_Right__c ear : acc.Enterprise_Access_Rights__r) {
                    String isTrial = ear.Is_Trial__c == true ? 'true' : 'false';
                    String subscriptionEndDate = ear.Subscription_End_Date__c != null ? String.valueOf(ear.Subscription_End_Date__c): '';
                    String isObservable = ear.Is_Observable__c == true ? 'true' : 'false';
                    aacs.add(new ApplicationAccessClaim(ear.ClaimValue__c, ear.Mask__c, isTrial, subscriptionEndDate, isObservable ));
                }
               /* for (Zuora__SubscriptionProductCharge__c spc : acc.R00N40000001lGjTEAU__r) {
                    sfps.add(new SalesforcePurchase(spc.Zuora__Description__c, spc.Zuora__EffectiveStartDate__c, spc.Zuora__Price__c));
                } */
                for (Zuora__SubscriptionProductCharge__c spc : acc.R00N40000001lGjTEAU__r) {
                    sfps.add(new SalesforcePurchase(spc.name, spc.Zuora__EffectiveStartDate__c, spc.Zuora__Price__c));
                }
                for (evt__Attendee__c att : atts) {
                    lers.add(new LiveEventRegistration(att.evt__Event__c == null ? '' : att.evt__Event__r.Name, att.CreatedDate, att.evt__Event__c == null ? null : att.evt__Event__r.evt__Start__c, att.evt__Payment__c == null ? null : att.evt__Payment__r.pymt__Amount__c, att.evt__Contact__r.FirstName, att.evt__Contact__r.LastName, att.evt__Primary_Attendee__c == null ? '' : att.evt__Primary_Attendee__r.evt__Contact__r.AccountId));
                }
                for(Zuora__SubscriptionProductCharge__c subProdCharge :subscriptionProductCharge){
                    spcs.add(new SubscriptionProductCharge(subProdCharge.Zuora__Subscription__r.Zuora__TermStartDate__c,
                                                                     subProdCharge.Zuora__Subscription__r.Zuora__TermEndDate__c,
                                                                     subProdCharge.Zuora__Subscription__r.Zuora__ContractEffectiveDate__c,
                                                                     subProdCharge.Zuora__Subscription__r.Zuora__InitialTerm__c,
                                                                     subProdCharge.Zuora__Subscription__r.Zuora__TermSettingType__c,
                                                                     subProdCharge.Zuora__Subscription__r.Zuora__SubscriptionNumber__c,
                                                                     subProdCharge.Zuora__Subscription__r.Zuora__Status__c,
                                                                     subProdCharge.Name,
                                                                     subProdCharge.Zuora__Price__c,
                                                                     subProdCharge.Zuora__Quantity__c,
                                                                     subProdCharge.Zuora__RatePlanName__c));
                }
            }
        }
        return cs;
    }
     public static Account createCustomer(CustomerInfo ci){
        Account account = new Account();
         // set account owner so we can create new community user
         VVCommunitySettings__c cs = VVCommunitySettings__c.getOrgDefaults();
         User uowner = [SELECT Id FROM User WHERE Name = :cs.Account_Owner_Name__c];
         account.OwnerId = uowner.Id;
        try {
            if(ci != null){
                account.Salutation = ci.salutation;
                account.FirstName = ci.firstName;
                account.LastName = ci.lastName;
                account.PersonEmail = ci.email;
                account.Account_Currency__c = ci.accountcurrency;
                account.OptionsPro_Username__c = ci.optionsProUserName;
                account.OptionsPro_Password__c = ci.optionsProUserPassword;
               // account.ApplicationUserID__c = ci.applicationUserId;
              //  account.EnterpriseCustomerID__c = ci.enterpriseCustomerId;
                account.BillingStreet = ci.BillingStreet;
                account.BillingCity = ci.BillingCity;
                account.BillingState = ci.BillingState;
                account.BillingPostalCode = ci.BillingPostalCode;
                account.BillingCountry = ci.BillingCountry;
                account.PersonMailingCountry = ci.countryOfResidence;
                account.VV_Feed__c = ci.feed;
                account.VV_Edition__c = ci.edition;
                account.VV_Market__c = ci.market;
                account.VV_Second_Market__c = ci.secondaryMarket;
                account.Subscription_Plan__c = ci.subscriptiontype;
                //account.Is_Apple_App_Store_Customer__c = ci.applestore;
                account.Password__c = ci.password; 
                account.Vector_Vest_Customer_Id__c = ci.userName;
                account.SBQQ__TaxExempt__c = TaxExempt;
                account.CertificateID__c = ci.certificateId;
                /* LastModified Date 07/05/2017 - Added GDPR_Opt_in__pc feild */
                //account.GDPR_Opt_in__pc = ci.optinMarketing;
                account.GDPR_Opt_in__pc = ci.OptinMarketing;
                System.debug('****inside createCustomer1 ****');
                Insert account;
                 System.debug('****account ****'+account);
                 // create community user
                 // Ganesh:: 22 NOV 2018 :: passed extra field in createCommunityUser as true to identify a mobile user.
                 
                 // Ganesh :: 4-7-2019 commented user code
               	 // User userObj = vvUserManagement.createCommunityUser(account.Id, false, true);
                 //System.debug('****userObj ****'+userObj);
                 
                /* LastModified Date 07/05/2017 - Added GDPR_Opt_in__pc feild */
                if(account.Id != null){
                    System.debug('*****inside acc id null check*****'+account.Id);
                     account = [Select Id,  Salutation,
                                            FirstName,
                                            LastName,
                                            PersonEmail,
                                            EnterpriseCustomerID__c,
                                            ApplicationUserID__c,
                                            BillingStreet,
                                            BillingCity,
                                            BillingState,
                                            BillingPostalCode,
                                            BillingCountry,
                                            PersonMailingCountry,
                                            VV_Feed__c,
                                            VV_Edition__c,
                                            VV_Market__c,
                                            VV_Second_Market__c,
                                            Subscription_Plan__c,
                                            Is_Apple_App_Store_Customer__c,
                                            Account_Currency__c,
                                            SBQQ__TaxExempt__c,CertificateID__c,Region1__c,Phone,Account_Id__c,GDPR_Opt_in__pc
                                  FROM Account WHERE Id =:account.Id];
                }
                /*if (userObj != null) {
                    System.setPassword(userObj.Id, account.Password__c);
                }*/
            }
        }catch(Exception e){
           System.debug('***Error***'+e.getMessage());
             
        }
        return account;
    }
    
    //user name must be in the form of Email
    public static Boolean validateUserName(String userName) {
    Boolean res = true;
    String emailRegex = '[a-zA-Z0-9\\.\\!\\#\\$\\%\\&\\*\\/\\=\\?\\^\\_\\+\\-\\`\\{\\|\\}\\~\'._%+-]+@[a-zA-Z0-9\\-.-]+\\.[a-zA-Z0-9]+'; 
    Pattern MyPattern = Pattern.compile(emailRegex);
    Matcher MyMatcher = MyPattern.matcher(userName);

    if (!MyMatcher.matches()) 
        res = false;
    return res; 
    } 
    //validation for password:  
  /*  public static Boolean validatePassword(String password) {
    Boolean res = true;
    String passwordRegex ='((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})';
    Pattern MyPattern = Pattern.compile(passwordRegex);
    Matcher MyMatcher = MyPattern.matcher(password);

    if (!MyMatcher.matches()) 
        res = false;
    return res; 
    } */
    
}