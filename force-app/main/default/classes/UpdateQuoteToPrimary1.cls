global class UpdateQuoteToPrimary1 implements Database.Batchable<sObject>,Database.Stateful{
   global Set<Id> allQuoteObjs  = new Set<Id>();
   global map<id,SBQQ__Quote__c> mapQuote = new map<id, SBQQ__Quote__c>();
   global Database.QueryLocator start(Database.BatchableContext BC){
      String query = 'SELECT id,SBQQ__Primary__c,Promo_code__c,SBQQ__NetAmount__c,SiteUser_Primary_Check__c,SBQQ__Opportunity2__c,SBQQ__Opportunity2__r.Created_By_PortalCustomer__c FROM SBQQ__Quote__c WHERE SiteUser_Primary_Check__c = true AND SBQQ__Primary__c = false LIMIT 100 ';
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, 
                       List<SBQQ__Quote__c> scope){
                       
      for(SBQQ__Quote__c q : scope){
          q.SBQQ__Primary__c = true;
          allQuoteObjs.add(q.id);
          mapQuote.put(q.id,q);
      }   
      update scope;
   }

   global void finish(Database.BatchableContext BC){
       System.debug('-------->>>'+allQuoteObjs);
        for(String qId : allQuoteObjs)
        {
            
            if(!String.isEmpty(qId) && mapQuote.get(qid)!= null && mapQuote.get(qid).Promo_code__c == null && mapQuote.get(qid).SBQQ__NetAmount__c >0){
                ZSB.ZSBConnectorUtils.createContractFromQuote(qId);
            }
            else if(!String.isEmpty(qId) && mapQuote.get(qid)!= null && mapQuote.get(qid).Promo_code__c != null){
                 ZSB.ZSBConnectorUtils.createContractFromQuote(qId);
            }
        }
   }

}