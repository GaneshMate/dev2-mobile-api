/**
 * get upgrade downgrade product list
 *
 * get upsell cross sell product list
 *
 * get product add on lists
 */
public without sharing class vvProductManagement {


    /**
     * given a person account id, return all the rateplans that can be in the upgrade path
     *
     * get standard product and plugins
     */
    public static List<zqu__ProductRatePlan__c> getUpgradeList(Id accountId) {
        List<zqu__ProductRatePlan__c> returnlist = new List<zqu__ProductRatePlan__c>();

        // get current account information
        Account a = [SELECT BillingCountry, VV_Market__c, VV_Second_Market__c, VV_Edition__c, VV_Feed__c, Is_Elite__c, Is_Elite_Legacy__c, Account_Currency__c, IsPersonAccount FROM Account
            WHERE Id = :accountId];

        // get elite matrix
        Map<Id, Decimal> elite_matrix_map = getEliteMatrix(a);
        system.debug('elite_matrix_map: ' + elite_matrix_map);

        // get all account's current rate plans in effect
        Set<String> currentrpids = getCurrentRatePlanZuoraIds(accountId);
        Set<String> currentrpnames = getCurrentRatePlanNames(accountId);

        // get all the current standard plans this customer is enrolled in for all markets
        // create a map of market -> feed
        Map<String, String> market_feed_map = getCurrentMarketFeedMap(currentrpids);
        Map<String, String> market_edition_map = getCurrentMarketEditionMap(currentrpids);

        Set<String> allfeeds = new Set<String>(market_feed_map.values());

        // retrieve everything that isn't already selected by this customer
        for (zqu__ProductRatePlan__c prp : [SELECT Name, zqu__ZProduct__r.Name, zqu__ZuoraId__c, VV_Edition__c, VV_Market__c, VV_Feed__c, VV_Type__c, (SELECT zqu__ZuoraId__c,zqu__ListPrice__c,Additional_Country_Pricing__c,zqu__Description__c,zqu__Type__c FROM zqu__R00N40000001mFVKEA2__r) FROM zqu__ProductRatePlan__c
                WHERE (
                    (zqu__ZProduct__r.Name = :vvConstants.MAIN_PRODUCT_NAME)
                    OR 
                    (zqu__ZProduct__r.Name = :vvConstants.PLUGIN_PRODUCT_NAME)
                )
                AND zqu__Deleted__c = false
                AND (NOT zqu__ZuoraId__c IN :currentrpids)
                AND (NOT VV_Type__c = :vvConstants.TYPE_SEMINAR)
                AND (NOT VV_Type__c = :vvConstants.TYPE_EXCHANGE)
                AND (NOT VV_Type__c = :vvConstants.TYPE_EXPRESS)
                AND (NOT VV_Type__c = :vvConstants.TYPE_TRIAL)
                AND (NOT Name IN :currentrpnames)
                AND (NOT Name LIKE '%discount%')
                AND (NOT Name LIKE '%trial%')
                ORDER BY zqu__ZProduct__r.Name, Name
            ]) {
            // foreign currency support
            prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c = vvCurrencyManagement.convertFromUSD(prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c, a.Account_Currency__c);


            /*** hardcoded rules ***/
            if (!allfeeds.contains('RT') && (prp.Name.containsIgnoreCase('derby') || prp.Name.containsIgnoreCase('robotrader'))) {
                // if customer doesn't have RT plan, don't allow derby or robotrader
            } else if (!allfeeds.contains('EOD') && prp.Name.containsIgnoreCase('prographics')) {
                // don't add prographics if customer doesn't have EOD
            } else if ((allfeeds.contains('RT') || !allfeeds.contains('ID')) && prp.Name.containsIgnoreCase('watchdog')) {
                // don't add watchdog to the list if we are on RT or customer doesn't have ID, don't add watchdog
            } else if (prp.zqu__ZProduct__r.Name == vvConstants.PLUGIN_PRODUCT_NAME && (market_feed_map.keySet().contains(prp.VV_Market__c) || prp.VV_Market__c == 'All')) {
                // plugins
                prp.Name = vvConstants.PRE_PLUGIN + ' ' + prp.Name;
                returnlist.add(prp);
            } else if (prp.VV_Type__c == vvConstants.TYPE_STANDARD) {
                // additional country vs. main, if additional, set description, and also copy the pricing over
                if (prp.VV_Market__c != a.VV_Market__c && a.VV_Market__c != null) {
                    prp.Name = vvConstants.PRE_ADDITIONAL + ' ' + prp.Name;
                    prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c = prp.zqu__R00N40000001mFVKEA2__r[0].Additional_Country_Pricing__c != null ? vvCurrencyManagement.convertFromUSD(prp.zqu__R00N40000001mFVKEA2__r[0].Additional_Country_Pricing__c, a.Account_Currency__c) : prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c;
                } else {
                    prp.Name = vvConstants.PRE_MAIN + ' ' + prp.Name;
                }

                // for non us and canada markets, go ahead and add remaining standard countries
                if (prp.VV_Market__c != 'US' && prp.VV_Market__c != 'CA') {
                    returnlist.add(prp);
                } else {
                    String current_feed = market_feed_map.get(prp.VV_Market__c);
                    if (current_feed == null) returnlist.add(prp);
                    else if (current_feed == 'EOD' && (prp.VV_Feed__c == 'ID' || prp.VV_Feed__c == 'RT')) returnlist.add(prp);
                    else if (current_feed == 'ID' && prp.VV_Feed__c == 'RT') returnlist.add(prp);
                    else if (current_feed == 'RT' && (market_edition_map.get(prp.VV_Market__c) == null || market_edition_map.get(prp.VV_Market__c) == 'Non-Professional') && prp.VV_Edition__c == 'Professional') returnlist.add(prp);
                }
            }
            // set elite price for this
            if (elite_matrix_map.get(prp.Id) != null) {
                system.debug('before: prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c: ' + prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c);
                prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c -= elite_matrix_map.get(prp.Id);
                system.debug('after: prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c: ' + prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c);
                
            }
            
        }

        returnlist.sort();
        return returnlist;
    }

    /**
     * given account id, get all current subscribed items for downgrade or removal
     */
    public static List<zqu__ProductRatePlan__c> getDowngradeList(Id accountId) {
        List<zqu__ProductRatePlan__c> returnlist = new List<zqu__ProductRatePlan__c>();

        // get current account information
        Account a = [SELECT BillingCountry, VV_Market__c, VV_Second_Market__c, VV_Edition__c, VV_Feed__c, Is_Elite__c, Is_Elite_Legacy__c, Account_Currency__c, IsPersonAccount FROM Account
            WHERE Id = :accountId];

        // get elite matrix
        Map<Id, Decimal> elite_matrix_map = getEliteMatrix(a);

        // get all account's current rate plans in effect
        Set<String> currentrpids = getCurrentRatePlanZuoraIdsForDowngradeList(accountId);

        // get all the current standard plans this customer is enrolled in for all markets
        // create a map of market -> feed
        Map<String,String> market_feed_map = getCurrentMarketFeedMap(currentrpids);
        Map<String, String> market_edition_map = getCurrentMarketEditionMap(currentrpids);
        Set<String> allfeeds = new Set<String>(market_feed_map.values());

        // list composed of: for the items in the same market, downgrade from current feed OR a plugin that's already in the subscription
        for (zqu__ProductRatePlan__c prp : [SELECT Name, zqu__ZProduct__r.Name, zqu__ZuoraId__c, VV_Edition__c, VV_Market__c, VV_Feed__c, VV_Type__c, (SELECT zqu__ZuoraId__c,zqu__ListPrice__c,Additional_Country_Pricing__c,zqu__Description__c,zqu__Type__c FROM zqu__R00N40000001mFVKEA2__r) FROM zqu__ProductRatePlan__c
                WHERE (
                    (zqu__ZProduct__r.Name = :vvConstants.MAIN_PRODUCT_NAME)
                    OR (zqu__ZuoraId__c IN :currentrpids)
                    OR (zqu__ZProduct__r.Name = :vvConstants.MAIN_PRODUCT_NAME AND VV_Type__c = :vvConstants.TYPE_EXPRESS)
                )
                AND zqu__Deleted__c = false
                AND (NOT VV_Type__c = :vvConstants.TYPE_EXCHANGE)
                AND (NOT VV_Type__c = :vvConstants.TYPE_TRIAL)
                AND (NOT Name LIKE '%discount%')
                AND (NOT Name LIKE '%trial%')
                AND (Name <> 'One-Time Purchase')
                ORDER BY zqu__ZProduct__r.Name, Name
            ]) {
            //AND (VV_Edition__c = :a.VV_Edition__c OR VV_Edition__c = 'All')
            // foreign currency support
            prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c = vvCurrencyManagement.convertFromUSD(prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c, a.Account_Currency__c);
            
            // all standard and express products
            if (prp.VV_Type__c == vvConstants.TYPE_STANDARD || prp.VV_Type__c == vvConstants.TYPE_EXPRESS) {
                // additional country vs. main, if additional, set description, and also copy the pricing over
                if (prp.VV_Market__c != a.VV_Market__c && a.VV_Market__c != null) {
                    prp.Name = vvConstants.PRE_ADDITIONAL + ' ' + prp.Name;
                    prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c = prp.zqu__R00N40000001mFVKEA2__r[0].Additional_Country_Pricing__c != null ? vvCurrencyManagement.convertFromUSD(prp.zqu__R00N40000001mFVKEA2__r[0].Additional_Country_Pricing__c, a.Account_Currency__c) : prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c;
                } else {
                    prp.Name = vvConstants.PRE_MAIN + ' ' + prp.Name;
                }

                // for US and CA markets, we want to add what's on the downgrade path, RT->ID,EOD, ID->EOD
                String current_feed = market_feed_map.get(prp.VV_Market__c);
                if (current_feed != null) {
                    if ((prp.VV_Market__c == 'US' || prp.VV_Market__c == 'CA') && prp.VV_Type__c != vvConstants.TYPE_EXPRESS) {
                        
                        // add nonpro if we are on professional
                        if (current_feed == 'RT' && (market_edition_map.get(prp.VV_Market__c) != null && market_edition_map.get(prp.VV_Market__c) == 'Professional')  && prp.VV_Feed__c == 'RT' && prp.VV_Edition__c == 'Non-Professional')
                            returnlist.add(prp);
                        else if (current_feed == 'RT' && (prp.VV_Feed__c == 'ID' || prp.VV_Feed__c == 'EOD')) returnlist.add(prp);
                        else if (current_feed == 'ID' && prp.VV_Feed__c == 'EOD') returnlist.add(prp);
                    }
                    // for all countries or for other countries, add express option
                    if (prp.VV_Type__c == vvConstants.TYPE_EXPRESS) returnlist.add(prp);
                }
                // give option of remove this product altogether only if it is not primary product
                if (currentrpids.contains(prp.zqu__ZuoraId__c)) {
                    prp.Name += ' (Remove)';
                    returnlist.add(prp);
                }
            } else if (currentrpids.contains(prp.zqu__ZuoraId__c)) {
                // add existing plugins
                // don't allow downgrade of plugin watchdog, protrader, and autotimer if we are in RT
                if (allfeeds.contains('RT') && (prp.Name.containsIgnoreCase('autotimer') || prp.Name.containsIgnoreCase('protrader') || prp.Name.containsIgnoreCase('watchdog')) ) {
                    // don't add
                } else {
                    prp.Name = vvConstants.PRE_PLUGIN + ' ' + prp.Name + ' (Remove)';
                    returnlist.add(prp); 
                }
                
            }

            // set elite price for this
            if (elite_matrix_map.get(prp.Id) != null) prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c -= elite_matrix_map.get(prp.Id);


            
        }
        returnlist.sort();
        return returnlist;
    }


    /**
     * get the actual trial rate plan
     */
    public static zqu__ProductRatePlan__c getTrialPlan() {
        // remove name after we finalize on what to do with trial
        zqu__ProductRatePlan__c prp = [SELECT zqu__ZuoraId__c,(SELECT zqu__ZuoraId__c FROM zqu__R00N40000001mFVKEA2__r) FROM zqu__ProductRatePlan__c WHERE 
            Name = :vvConstants.VV_TRIAL_NAME AND zqu__Deleted__c = false LIMIT 1];   

        return prp;
    }
    /**
     * given a main product rate plan, find the complementing trial product rate plan
     * @param  main_product [description]
     * @return              [description]
     */
    public static Product_Rate_Plan_Trial_Map__c findTrialWithRatePlanId(Id main_product) {
        // first get the main rate plan details
        zqu__ProductRatePlan__c prp = [SELECT VV_Trial__c FROM zqu__ProductRatePlan__c WHERE Id = :main_product];

        // now get the trial product
        try {
            Product_Rate_Plan_Trial_Map__c trial = [SELECT Duration__c,Duration_Type__c,Amount__c FROM Product_Rate_Plan_Trial_Map__c WHERE Name = :prp.VV_Trial__c];
            return trial;
        } catch (Exception e) {
            return null;
        }
                
    }

    /**
     * find trial information given a trial name
     * @param  trialname [description]
     * @return           [description]
     */
    public static Product_Rate_Plan_Trial_Map__c findTrialWithTrialName(String trialname, String curr) {
        // first get the main rate plan details
        try {
            Product_Rate_Plan_Trial_Map__c p = [SELECT Duration__c,Duration_Type__c,Amount__c FROM Product_Rate_Plan_Trial_Map__c WHERE Name = :trialname];
            p.Amount__c = vvCurrencyManagement.convertFromUSD(p.Amount__c, curr);
            return p;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * calculate number of days for the trial, month is just used to display, so it's ok
     */
    public static Integer calcTrialDays(Product_Rate_Plan_Trial_Map__c p) {
        if (p.Duration_Type__c == 'Day') return Integer.valueOf(p.Duration__c);
        else if (p.Duration_Type__c == 'Week') return Integer.valueOf(p.Duration__c * 7);
        else if (p.Duration_Type__c == 'Month') return Integer.valueOf(p.Duration__c * 30);
        else return 0;
    }
    /**
     * find out which date does the trial end in the future
     */
    public static Date calcTrialEndDate(Product_Rate_Plan_Trial_Map__c p, Date indate) {
        if (p == null || p.Duration__c == null) return indate;

        Date trial_end_date = indate;
        if (p.Duration_Type__c == 'Day') trial_end_date = indate.addDays(Integer.valueOf(p.Duration__c));
        else if (p.Duration_Type__c == 'Week') trial_end_date = indate.addDays(Integer.valueOf(p.Duration__c * 7));
        else if (p.Duration_Type__c == 'Month') trial_end_date = indate.addMonths(Integer.valueOf(p.Duration__c));
        return trial_end_date;
    }


    /**
     * given a sets of parameters, determine the correct trial and standard product rate plan and charge to use for display or for subscription creation
     * edition
     * market
     * feed
     * country
     * plantype (Annual / Monthly)
     * elite (true/false)
     * express (true/false)
     * return main rate plan
     */
    public static Map<String,zqu__ProductRatePlan__c> getPlanToUse(Map<String,String> params) {
        Map<String,zqu__ProductRatePlan__c> returnmap = new Map<String,zqu__ProductRatePlan__c>();
        if (params.get('feed') == null || params.get('edition') == null || params.get('market') == null) return returnmap;

        // if feed is not RT, then set edition to non-professional since there isn't professional eod or id
        if (params.get('feed') != 'RT') params.put('edition','Non-Professional');
        
        // monthly or annual

        String tempsp = params.get('plantype');
        if (tempsp != null && tempsp == 'Quarterly') tempsp = 'Monthly';

        String plantype = (tempsp != null ? '%' + tempsp + '%' : '%') ; 

        // soql to retrieve the plan, standard and all trials
        for (zqu__ProductRatePlan__c zprp : [SELECT Name, zqu__ZProduct__r.Name, zqu__ZuoraId__c, VV_Edition__c, VV_Market__c, VV_Feed__c, VV_Trial__c, VV_Type__c, ExchangeFee__c, (SELECT Name,zqu__ZuoraId__c,zqu__ListPrice__c,Additional_Country_Pricing__c,zqu__Description__c,zqu__Type__c FROM zqu__R00N40000001mFVKEA2__r) FROM zqu__ProductRatePlan__c 
          WHERE zqu__Deleted__c = false 
            AND zqu__EffectiveStartDate__c <= :Date.today() 
            AND zqu__EffectiveEndDate__c >= :Date.today()
            AND zqu__ZProduct__r.Name = :vvConstants.MAIN_PRODUCT_NAME
            AND VV_Feed__c = :params.get('feed')
            AND VV_Edition__c = :params.get('edition')
            AND 
            (
                (
                    VV_Market__c = :params.get('market')
                    AND Name LIKE :plantype
                    AND VV_Type__c = :vvConstants.TYPE_STANDARD
                )
            )

        ]) {
            // foreign currency support
            zprp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c = vvCurrencyManagement.convertFromUSD(zprp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c, params.get('currency'));
            

            if (params.get('plantype') != null) {   // used to get specific monthly or annual plans
                returnmap.put('main',zprp);
                
            } else {        
                // return both monthly and annual
                if (zprp.Name.containsIgnoreCase('monthly')) returnmap.put('mainmonthly',zprp);
                else if (zprp.Name.containsIgnoreCase('annual')) returnmap.put('mainannual',zprp);
            }
            
        }
        system.debug('###' + returnmap);
        return returnmap;

    }



    /**
     * given a customer account, return an exchange fee product plan suitable for this account, if it is null, then don't add exchange fee
     */
    public static zqu__ProductRatePlan__c getExchangeFee(Account a, zqu__ProductRatePlan__c rp, String plantype) {
        plantype = '%' + (plantype == 'Quarterly' ? 'Monthly' : plantype) + '%';
        for (zqu__ProductRatePlan__c prp : [SELECT Name, zqu__ZProduct__r.Name, zqu__ZuoraId__c, zqu__Description__c, VV_Edition__c, VV_Market__c, VV_Feed__c, VV_Type__c, (SELECT zqu__ZuoraId__c,zqu__ListPrice__c,Additional_Country_Pricing__c,zqu__Description__c,zqu__Type__c FROM zqu__R00N40000001mFVKEA2__r) 
            FROM zqu__ProductRatePlan__c  WHERE 
                VV_Type__c = :vvConstants.TYPE_EXCHANGE 
                AND zqu__Deleted__c = false
                AND VV_Market__c = :rp.VV_Market__c 
                AND VV_Edition__c = :rp.VV_Edition__c 
                AND VV_Feed__c = :rp.VV_Feed__c 
                AND zqu__ZProduct__r.Name = :vvConstants.MAIN_PRODUCT_NAME
                AND Name LIKE :plantype]) 
        {
            system.debug('exchange fee: ' + prp);
            // foreign currency support
            prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c = vvCurrencyManagement.convertFromUSD(prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c, a.Account_Currency__c);
            
            // canada resident exchange fee
            String prptype = prp.Name.containsIgnoreCase('annual') ? ' Annual' : ' Monthly';
            if (prp.VV_Market__c == 'CA' && prp.Name.containsIgnoreCase('CA resident') && a.BillingCountry == 'Canada') {
                prp.Name = 'RealTime Exchange Fee ' + prp.VV_Market__c + prptype + ' (CA Resident)';
                return prp;
            } else if (prp.VV_Market__c == 'CA' && prp.Name.containsIgnoreCase('CA non-resident') && a.BillingCountry != 'Canada') {
                prp.Name = 'RealTime Exchange Fee ' + prp.VV_Market__c + prptype + ' (CA Non-Resident)';
                return prp;
            } else if (prp.VV_Market__c != 'CA') {
                prp.Name = 'RealTime Exchange Fee ' + prp.VV_Market__c + prptype;
                return prp;
            }

        }
        return null;
    }

    /**
     * get rateplan by pass in an id
     */
    public static zqu__ProductRatePlan__c getRatePlanWithId(String rateplanId, String curr) {

        for (zqu__ProductRatePlan__c prp : [SELECT Name, zqu__ZProduct__r.Name, zqu__ZuoraId__c, zqu__Description__c, VV_Edition__c, VV_Market__c, VV_Feed__c, VV_Type__c, (SELECT zqu__ZuoraId__c,zqu__ListPrice__c,Additional_Country_Pricing__c,zqu__Description__c,zqu__Type__c FROM zqu__R00N40000001mFVKEA2__r) 
            FROM zqu__ProductRatePlan__c  WHERE 
                Id = :rateplanId
                AND zqu__Deleted__c = false LIMIT 1]) 
        {
            prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c = vvCurrencyManagement.convertFromUSD(prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c, curr);
            
            return prp;

        }
        return null;
    }
    /**
     * get rateplan by pass in a zuora id
     */
    public static zqu__ProductRatePlan__c getRatePlanWithZuoraId(String zuora_id, String curr) {

        for (zqu__ProductRatePlan__c prp : [SELECT Name, zqu__ZProduct__r.Name, zqu__ZuoraId__c, zqu__Description__c, VV_Edition__c, VV_Market__c, VV_Feed__c, VV_Type__c, (SELECT zqu__ZuoraId__c,zqu__ListPrice__c,Additional_Country_Pricing__c,zqu__Description__c,zqu__Type__c FROM zqu__R00N40000001mFVKEA2__r) 
            FROM zqu__ProductRatePlan__c  WHERE 
                zqu__ZuoraId__c = :zuora_id
                AND zqu__Deleted__c = false LIMIT 1]) 
        {
            prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c = vvCurrencyManagement.convertFromUSD(prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c, curr);
            
            return prp;

        }
        return null;
    }

    /**
     * get correct plugin plans, pass in current main plan and use that to get the filter
     */
    public static zqu__ProductRatePlan__c getPluginRatePlan(zqu__ProductRatePlan__c rp, String plantype, String n, String curr) {
        
        plantype = (plantype != null && plantype == 'Quarterly') ? 'Monthly' : plantype;

        n = '%' + n + '%' + (plantype != null ? (plantype + '%') : '');

        for (zqu__ProductRatePlan__c prp : [SELECT Name, zqu__ZProduct__r.Name, zqu__ZuoraId__c, zqu__Description__c, VV_Edition__c, VV_Market__c, VV_Feed__c, VV_Type__c, (SELECT zqu__ZuoraId__c,zqu__ListPrice__c,Additional_Country_Pricing__c,zqu__Description__c,zqu__Type__c FROM zqu__R00N40000001mFVKEA2__r) 
            FROM zqu__ProductRatePlan__c  WHERE 
                Name LIKE :n
                AND zqu__Deleted__c = false
                AND (VV_Market__c = :rp.VV_Market__c OR VV_Market__c = 'All')
                AND (VV_Feed__c = :rp.VV_Feed__c OR VV_Feed__c = 'N/A')
                AND zqu__ZProduct__r.Name = :vvConstants.PLUGIN_PRODUCT_NAME LIMIT 1]) 
        {
            prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c = vvCurrencyManagement.convertFromUSD(prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c, curr);
            
            return prp;

        }
        return null;
    }

    /**
     * pass in a rate plan and find a similar one using a different type (monthly, annual)
     */
    public static zqu__ProductRatePlan__c getSamePlanDifferentType(zqu__ProductRatePlan__c prp, String plantype, String curr) {
        // remove annual, monthly wording from the name
        String n = prp.Name.replace('Monthly','');
        n = n.replace('Annual','');

        n = '%' + n + '%' + (plantype == 'Quarterly' ? 'Monthly' : plantype) + '%';
        system.debug('lookfor this name: ' + n);
        List<zqu__ProductRatePlan__c> prplist = [SELECT Name, zqu__ZProduct__r.Name, zqu__ZuoraId__c, zqu__Description__c, VV_Edition__c, VV_Market__c, VV_Feed__c, VV_Type__c, (SELECT zqu__ZuoraId__c,zqu__ListPrice__c,Additional_Country_Pricing__c,zqu__Description__c,zqu__Type__c FROM zqu__R00N40000001mFVKEA2__r) 
            FROM zqu__ProductRatePlan__c  WHERE Name LIKE :n AND VV_Market__c = :prp.VV_Market__c AND VV_Edition__c = :prp.VV_Edition__c AND VV_Feed__c = :prp.VV_Feed__c AND zqu__ZProduct__c = :prp.zqu__ZProduct__c LIMIT 1];
        if (prplist.isEmpty()) return null;

        prplist[0].zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c = vvCurrencyManagement.convertFromUSD(prplist[0].zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c, curr);
            
        return prplist[0];
    }

    /**
     * get discount rate plan by pass in discount type (defined in vvConstant)
     */
    public static zqu__ProductRatePlan__c getDiscountRatePlan(String discounttype) {
        List<zqu__ProductRatePlan__c> prplist = [SELECT Name, zqu__ZProduct__r.Name, zqu__ZuoraId__c, zqu__Description__c, (SELECT zqu__ZuoraId__c,zqu__ListPrice__c,zqu__Model__c,zqu__Description__c,zqu__Type__c FROM zqu__R00N40000001mFVKEA2__r) 
            FROM zqu__ProductRatePlan__c  WHERE Name = :discounttype AND zqu__ZProduct__r.Name = :vvConstants.MAIN_PRODUCT_NAME AND zqu__Deleted__c = false LIMIT 1];
        if (prplist.isEmpty()) return null;

        return prplist[0];
    }

    /**
     * get plan to use for one time purchase such as books etc
     */
    public static zqu__ProductRatePlan__c getOneTimePurchasePlan() {
        List<zqu__ProductRatePlan__c> prplist = [SELECT Name, zqu__ZProduct__r.Name, zqu__ZuoraId__c, zqu__Description__c, (SELECT zqu__ZuoraId__c,zqu__ListPrice__c,zqu__Model__c,zqu__Description__c,zqu__Type__c FROM zqu__R00N40000001mFVKEA2__r) 
            FROM zqu__ProductRatePlan__c  WHERE Name = 'One-Time Purchase' AND zqu__ZProduct__r.Name = :vvConstants.MAIN_PRODUCT_NAME AND zqu__Deleted__c = false LIMIT 1];
        if (prplist.isEmpty()) return null;

        return prplist[0];
    }

    /**
     * get legacy rate plans
     */
    public static List<zqu__ProductRatePlan__c> getLegacyRatePlans(List<Legacy_Plan__c> lplist) {
        Map<Id, Decimal> prp_pricing_map = new Map<Id, Decimal>();

        for (Legacy_Plan__c lp : lplist) {
            prp_pricing_map.put(lp.Product_Rate_Plan__c, lp.Pricing__c);
        }
        List<zqu__ProductRatePlan__c> prplist = [SELECT Name, zqu__ZProduct__r.Name, zqu__ZuoraId__c, zqu__Description__c, VV_Edition__c, VV_Market__c, VV_Feed__c, VV_Type__c, (SELECT zqu__ZuoraId__c,zqu__ListPrice__c,Additional_Country_Pricing__c,zqu__Description__c,zqu__Type__c FROM zqu__R00N40000001mFVKEA2__r) 
            FROM zqu__ProductRatePlan__c  WHERE Id IN :prp_pricing_map.keySet()];

        for (zqu__ProductRatePlan__c prp : prplist) {
            // don't need to convert currency as currency is the account currency already
            prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c = prp_pricing_map.get(prp.Id);
        }
        return prplist;
    }



    /**
     * utility methods
     */
    
    /**
     * get all current active subscription rate plan zuora ids
     * @param  accountId [description]
     * @return           [description]
     */
    public static Set<String> getCurrentRatePlanZuoraIds(Id accountId) {
        String portfolio = '%portfolio analysis%';  // don't have to include portfolio analysis report
        Set<String> currentrpids = new Set<String>();
        for (Zuora__SubscriptionRatePlan__c zs : [SELECT Zuora__OriginalProductRatePlanId__c FROM Zuora__SubscriptionRatePlan__c 
            WHERE Zuora__Account__c = :accountId 
            AND (Zuora__Subscription__r.Zuora__Status__c = 'Active' OR Zuora__Subscription__r.Zuora__Status__c = 'Suspended') 
            AND (NOT Name LIKE :portfolio)
        ]) {

            //  OR (Zuora__Subscription__r.Zuora__Status__c = 'Cancelled' AND Zuora__Subscription__r.Zuora__CancelledDate__c > TODAY))

            currentrpids.add(zs.Zuora__OriginalProductRatePlanId__c);
            //system.debug('currentpids: ' + zs.Zuora__OriginalProductRatePlanId__c);
        }
        return currentrpids;
    }


    public static Set<String> getCurrentRatePlanZuoraIdsForDowngradeList(Id accountId) {
        String portfolio = '%portfolio analysis%';  // don't have to include portfolio analysis report
        Set<String> currentrpids = new Set<String>();
        for (Zuora__SubscriptionRatePlan__c zs : [SELECT Zuora__OriginalProductRatePlanId__c FROM Zuora__SubscriptionRatePlan__c 
            WHERE Zuora__Account__c = :accountId 
            AND (Zuora__Subscription__r.Zuora__Status__c = 'Active' OR Zuora__Subscription__r.Zuora__Status__c = 'Suspended') 
            AND (NOT Name LIKE :portfolio)
            AND Zuora__Subscription__r.Bundle_Product__c = null
        ]) {

            //  OR (Zuora__Subscription__r.Zuora__Status__c = 'Cancelled' AND Zuora__Subscription__r.Zuora__CancelledDate__c > TODAY))

            currentrpids.add(zs.Zuora__OriginalProductRatePlanId__c);
            //system.debug('currentpids: ' + zs.Zuora__OriginalProductRatePlanId__c);
        }
        return currentrpids;
    }

    /**
     * get the current active subscription rate plan names without the monthly annual quarterly at the end
     */
    public static Set<String> getCurrentRatePlanNames(Id accountId) {
        String portfolio = '%portfolio analysis%';  // don't have to include portfolio analysis report
        String trial = '%trial%';
        String discount = '%discount%';
        Set<String> currentrpnames = new Set<String>();
        for (Zuora__SubscriptionRatePlan__c zs : [SELECT Name FROM Zuora__SubscriptionRatePlan__c 
            WHERE Zuora__Account__c = :accountId 
            AND (Zuora__Subscription__r.Zuora__Status__c = 'Active' OR Zuora__Subscription__r.Zuora__Status__c = 'Suspended') 
            AND (NOT Name LIKE :portfolio)
            AND (NOT Name LIKE :trial)
            AND (NOT Name LIKE :discount)
        ]) {
            currentrpnames.add(zs.Name);

            // now add the other type as well (if original is monthly, add annually)
            if (zs.Name.containsIgnoreCase('Monthly')) zs.Name = zs.Name.subString(0,zs.Name.indexOf('Monthly')) + 'Annual' ;
            else if (zs.Name.containsIgnoreCase('Annual')) zs.Name = zs.Name.subString(0,zs.Name.indexOf('Annual')) + 'Monthly';
            // add it to the set again
            currentrpnames.add(zs.Name);
        }
        return currentrpnames;
    }

    /**
     * get all current standard plans on account in a map of market -> feed
     * @param  currentrpids [description]
     * @return              [description]
     */
    public static Map<String,String> getCurrentMarketFeedMap(Set<String> currentrpids) {
        Map<String,String> market_feed_map = new Map<String,String>();
        for (zqu__ProductRatePlan__c prp : [SELECT VV_Feed__c, VV_Market__c FROM zqu__ProductRatePlan__c WHERE zqu__ZuoraId__c IN :currentrpids AND VV_Type__c = :vvConstants.TYPE_STANDARD]) {
            market_feed_map.put(prp.VV_Market__c,prp.VV_Feed__c);
        }
        return market_feed_map;
    }

    /**
     * get current standard plan on account in a map of market -> edition
     */
    public static Map<String,String> getCurrentMarketEditionMap(Set<String> currentrpids) {
        Map<String,String> market_edition_map = new Map<String,String>();
        for (zqu__ProductRatePlan__c prp : [SELECT VV_Feed__c, VV_Edition__c, VV_Market__c FROM zqu__ProductRatePlan__c WHERE zqu__ZuoraId__c IN :currentrpids AND VV_Type__c = :vvConstants.TYPE_STANDARD]) {
            // if statement below make sure we only update the map when map has no value or has a value of non pro, if it is pro, keep it at pro as that will be the highest edition
            if (market_edition_map.get(prp.VV_Market__c) == null || market_edition_map.get(prp.VV_Market__c) == 'Non-Professional') market_edition_map.put(prp.VV_Market__c,prp.VV_Edition__c);
        }
        return market_edition_map;
    }

    /**
     * get elite matrix as a map of product rate plan id to discount amount in account currency
     */
    public static Map<Id, Decimal> getEliteMatrix(Account a) {
        Map<Id, Decimal> returnmap = new Map<Id, Decimal>();

        if (a.Is_Elite_Legacy__c) {
            for (Elite_Matrix__c em : [SELECT Product_Rate_Plan__c, Discount_Amount__c FROM Elite_Matrix__c WHERE For_Old_Elite_Only__c = true]) {
                returnmap.put(em.Product_Rate_Plan__c, vvCurrencyManagement.convertFromUSD(em.Discount_Amount__c, a.Account_Currency__c));
            }
        }
        if (a.Is_Elite__c) {
            for (Elite_Matrix__c em : [SELECT Product_Rate_Plan__c, Discount_Amount__c FROM Elite_Matrix__c WHERE For_Old_Elite_Only__c = false]) {
                if (returnmap.get(em.Product_Rate_Plan__c) == null) returnmap.put(em.Product_Rate_Plan__c, vvCurrencyManagement.convertFromUSD(em.Discount_Amount__c, a.Account_Currency__c));
            }
            
        }

        return returnmap;
    }


}