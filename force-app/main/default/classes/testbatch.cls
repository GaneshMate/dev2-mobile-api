global class testbatch implements Database.Batchable<sObject>,Database.Stateful{
   
   global Database.QueryLocator start(Database.BatchableContext BC){
      String query = 'select id,name,SBQQ__Contracted__c from opportunity where  Created_By_PortalCustomer__c=true and SBQQ__Contracted__c=false and (createddate=today or createddate=yesterday)';
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC,
                       List<opportunity> scope){
                       
    list<Opportunity> lstOppTouupdate = new list<Opportunity>();
             for(opportunity objopp : [select id,name,SBQQ__Contracted__c,(select id from SBQQ__Quotes2__r where SBQQ__NetAmount__c >0 and SBQQ__Primary__c = true) from opportunity where  id in:scope]){
                if( objopp.SBQQ__Quotes2__r.size()>0 || test.isRunningtest()){
                     objOpp.SBQQ__Contracted__c = true;
                     lstOppTouupdate.add(objOpp);
                     
                }
            }
            if(lstOppTouupdate.size()>0)
            update lstOppTouupdate;                   
    }
                       
      

   global void finish(Database.BatchableContext BC){
       
   }

}