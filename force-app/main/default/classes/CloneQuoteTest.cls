@isTest
private class CloneQuoteTest{
    @isTest static void testMethod1(){
        Account acc = new Account();
            acc.firstname='test acc1';
            acc.lastName = 'test acc';
            acc.billingCity ='Test City';
            acc.billingStreet = 'TestStreet';
            acc.billingstate ='TestState';
            acc.billingPostalcode ='23451';
            acc.billingcountry ='United States';
            Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            acc.recordtypeID=objAccrec.id;
           acc.PersonMailingCity='Test City';
            acc.PersonMailingStreet = 'TestStreet';
            acc.PersonMailingState ='TestState';
            acc.PersonMailingPostalCode ='23451';
            acc.PersonMailingCountry ='United States';
            //acc.ispersonAccount = true;
            insert acc;
            
            Opportunity opp = new Opportunity(Name=acc.Name + ' Opportunity ' ,
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=acc.Id);
                                       
            Product2 prod = new Product2(Name = 'VectorVest 7 EOD US Monthly', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_MONTHLY' , Family = 'Books');
            insert prod;
            Product2 prod2 = new Product2(Name = 'VectorVest 7 EOD US Annual', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_ANNUAL' , Family = 'Books');
            insert prod2;
            
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            
            CPQCommunity__c cpc = new CPQCommunity__c();
            cpc.Name = 'test';
            cpc.PriceBookId__c = pricebookId ;
            //insert cpc ;
            
            
            
            SBQQ__Quote__c quoteRec = new SBQQ__Quote__c();
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SiteUser_Primary_Check__c = true;
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SBQQ__Opportunity2__c = opp.Id;
            //quoteRec.ZSB__BillToContact__c = c.Id;
           // quoteRec.ZSB__SoldToContact__c = c.Id;
            quoteRec.ZSB__BillCycleDay__c =  '1';
            quoteRec.ZSB__BillingBatch__c = 'Batch1';
            insert quoteRec ;
            SBQQ__QuoteLine__c ojQuoteline = new SBQQ__QuoteLine__c();
            ojQuoteline.SBQQ__Quote__c = quoteRec .id;
            ojQuoteline.SBQQ__Product__c = prod.id;
            insert ojQuoteline;
            test.starttest();
                CloneQuote objclonequote = new CloneQuote(quoteRec.id);
                Map<string,Object> allFieldsToUpdate = new Map<string,Object>();
                allFieldsToUpdate.put('SBQQ__Account__c',acc.id);
                  objclonequote = new CloneQuote(quoteRec.id,allFieldsToUpdate );
                  objclonequote = new CloneQuote(quoteRec.id,allFieldsToUpdate,true);
                  objclonequote.doit();
                  objclonequote.cloneQuoteLines(quoteRec.id);
                  objclonequote.cloneQuoteLines(quoteRec.id, date.today());
            test.stoptest();
    }
}