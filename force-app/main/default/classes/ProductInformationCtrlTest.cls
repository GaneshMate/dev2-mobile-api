@isTest
Public Class ProductInformationCtrlTest{
     static testMethod void testShowPrducts() {
        Test.startTest();
        
        Id p = [select id from profile where name='Customer Community Login User'].id;
       
        //Account ac = new Account(name ='test Accunt') ;
        //insert ac; 
         Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            //acc.recordtypeID=objAccrec.id;
        Account ac = new Account(Primary_Phone_Type__c='Mobile',PersonMailingCity='Test City',PersonMailingStreet = 'TestStreet',PersonMailingState ='TestState',PersonMailingPostalCode ='23451',PersonMailingCountry ='United States',FirstName='testfirst',LastName='testlast',PersonEmail='u' + System.now().millisecond() + '1@vvsystemtest.com',Phone='2223334444',VV_Market__c='US',VV_Second_Market__c='CA',VV_Feed__c='EOD',VV_Edition__c='Non-Professional',recordtypeID=objAccrec.id,Subscription_Plan__c='Monthly',Is_Elite__c=false,Is_Elite_Legacy__c=false,Is_Apple_App_Store_Customer__c=false,BillingStreet='2234 test',BillingCity='Test',BillingState='GA',BillingCountry='USA',BillingPostalCode='22344');
        insert ac;
       contact con =[select id from contact where AccountId=:ac.id];
        //Contact con = new Contact(LastName ='testCon',AccountId = ac.Id);
        //insert con;  
                  
        User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
       
        insert user;
        
        Product2 prod = new Product2(Name = 'Laptop X200', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'code1' , Family = 'Books');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        PriceBookIds__c pb = new PriceBookIds__c();
        pb.Name = 'Standard Price Book';
        pb.Price_Book_Id__c = pricebookId ;
        insert pb ;
        
        system.runAs(user) {
            ApexPages.currentpage().getparameters().put('selProdId' , prod.Id);

            ProductInformationCtrl prodInfo = new ProductInformationCtrl();
            
            prodInfo.showSelectedProdScreen();
            prodInfo.redirectToNextPage();
        } 
         
        
        Test.stopTest();
     }
}