global class SOAPUpdatePayment {

    /*global class updatePaymentRequest{
        webservice String Id;
        webservice String CreditCardType;
    }*/
    global class updatePaymentResponse{
        webservice String msg;
    }
    global class updatePaymentRequest {
        webservice String Id {get;set;}
        webservice String CreditCardType {get;set;}
        //webservice String AccountId {get;set;}
        
        public updatePaymentRequest(String Id,String CreditCardType,String AccountId) {
            this.Id = Id;
            this.CreditCardType = CreditCardType;
            //this.AccountId = AccountId;
        }
    }
    webservice static List<String> updatePayment(List<updatePaymentRequest> updatePayment) {
        
        String errors = '';
		List<String> errorsList = new List<String>();
        Zuora.zApi.SaveResult[] results;
        try{
        updatePaymentResponse response = new updatePaymentResponse();
        System.debug('****updatePayment id****'+updatePayment);
        for(updatePaymentRequest updateObj : updatePayment){
            System.debug('****updateObj****'+updateObj);
            if (updateObj.Id != null && updateObj.CreditCardType != null) {
                
                Zuora.zObject payment = new Zuora.zObject('PaymentMethod');
                Payment.setValue('Id', updateObj.Id); 
                payment.setValue('CreditCardType', updateObj.CreditCardType);
                //payment.setValue('AccountId', updateObj.AccountId);
                
                
                results = zuoraAPIHelper.updateAPICall(payment);
                system.debug('Check : Update Results: '+results);
                
                for (Zuora.zApi.SaveResult result : results) {
                    if (result.success) {
                        errors='Success';
                        errorsList.add(errors);
                        system.debug('errorsList if: '+errorsList);
                    }
                    else{ 
                        errors += (errors != '' ? ',' : '') + zuoraAPIHelper.getErrorStr(result.errors);
                        errorsList.add(errors);
                        system.debug('errorsList: '+errorsList);
                    }
                }
            } 
        }
        }catch(Exception e){
            System.debug('The following exception has occurred: ' + e.getMessage());
        }
        return errorsList;
        
    }
}