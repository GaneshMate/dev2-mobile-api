@istest
public class UserRestResourceTest {

    @isTest static void testUpdateUserNull() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/user/update';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'PUT';
        Test.startTest();
        UserRestResource.updateUser('');
        Test.stopTest();
        //System.assertEquals(400, res.statusCode);
    }
     @isTest static void testUserNameNotFound() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/user/update';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'PUT';
        Test.startTest();
        UserRestResource.updateUser('dd@email.com');
        Test.stopTest();
        //System.assertEquals(400, res.statusCode);
    }
    @isTest static void testUserName() {
        
        Profile p = [select id from profile where name='Standard User'];
        User u = new User(alias = 'DemoT', 
                          email='DemoTesting@testorg.com',
            			  emailencodingkey='UTF-8', 
                          firstname='fake', 
                          lastname='Testing', 
                          languagelocalekey='en_US',
           				  localesidkey='en_US', 
                          isActive=false,
                          profileid = p.Id,
            			  timezonesidkey='America/Los_Angeles', 
                          username='DemoTesting@testorg.com');
        insert u;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/user/update';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'PUT';
        Test.startTest();
        UserRestResource.updateUser(u.Username);
        Test.stopTest();
        //System.assertEquals(200, res.statusCode);
    }
     @isTest static void testUserNameAlreadyActive() {
        
        Profile p = [select id from profile where name='Standard User'];
        User u = new User(alias = 'DemoT', 
                          email='DemoTesting@testorg.com',
            			  emailencodingkey='UTF-8', 
                          firstname='fake', 
                          lastname='Testing', 
                          languagelocalekey='en_US',
           				  localesidkey='en_US', 
                          profileid = p.Id,
            			  timezonesidkey='America/Los_Angeles', 
                          username='DemoTesting@testorg.com');
        insert u;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/user/update';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'PUT';
        Test.startTest();
        UserRestResource.updateUser(u.Username);
        Test.stopTest();
        //System.assertEquals(400, res.statusCode);
    }
}