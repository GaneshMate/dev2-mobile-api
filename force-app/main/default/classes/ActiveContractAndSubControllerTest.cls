@isTest(seealldata=true)
private class ActiveContractAndSubControllerTest{
    private static testmethod void testmethod1(){
        Date Startdate;
         for(SBQQ__Subscription__c sub : [select Id,SBQQ__Startdate__c,SBQQ__Account__c from SBQQ__Subscription__c where SBQQ__Contract__r.Status='Activated' and SBQQ__Contract__r.ZSB__SubscriptionId__c != null and SBQQ__Contract__r.TerminatedDate__c=null limit 1]){   
                                             
                                          Apexpages.currentpage().getparameters().put('accountId',sub.SBQQ__Account__c); 
                                          Startdate = sub.SBQQ__Startdate__c; 
                                         }
        ActiveContractAndSubController obj= new ActiveContractAndSubController();
        //list<ActiveContractAndSubController.SubscriptionWrapper> lstwrap= obj.subscriptionWrapperList;
        for(ActiveContractAndSubController.SubscriptionWrapper objwrap : obj.subscriptionWrapperList){
            objwrap.checked =true;
        }
        obj.ErrorList = new list<string>{'test'};
        obj.cancelAll();
       /*obj.suspendsub = new Zuora__Subscription__c(suspenddate__c = date.today(),ResumeDate__c= date.today().adddays(2)); 
       for(ActiveContractAndSubController.SubscriptionWrapper objwrap : obj.subscriptionWrapperList){
            objwrap.checked =true;
        }
        obj.SuspendAll();*/
        obj.backMethod();
        obj.showdateselectionmethod();
        
    }
    private static testmethod void testmethod2(){
        Date Startdate;
         for(SBQQ__Subscription__c sub : [select Id,SBQQ__Startdate__c,SBQQ__Account__c from SBQQ__Subscription__c where SBQQ__Contract__r.Status='Activated' and SBQQ__Contract__r.ZSB__SubscriptionId__c != null and SBQQ__Contract__r.ZSB__SubscriptionNumber__c != null and SBQQ__Contract__r.TerminatedDate__c=null limit 1]){   
                                             
                                          Apexpages.currentpage().getparameters().put('accountId',sub.SBQQ__Account__c); 
                                          Startdate = sub.SBQQ__Startdate__c; 
                                         }
                                     
        ActiveContractAndSubController obj= new ActiveContractAndSubController();
        
       obj.suspendsub = new Zuora__Subscription__c(suspenddate__c = date.today(),ResumeDate__c= date.today().adddays(2)); 
       for(ActiveContractAndSubController.SubscriptionWrapper objwrap : obj.subscriptionWrapperList){
            objwrap.checked =true;
        }
        obj.SuspendAll();
        
        }
        
    }