/**
 * for zuora soap testing, need to enable seeAllData
 */
@isTest(seeAllData=true)
private class CaseTriggerHanderTest {
    @isTest static void testAfterUpdate() {
        // setup account
        Account a = new Account(PersonMailingCity='Test City',PersonMailingStreet = 'TestStreet',PersonMailingState='TestState',PersonMailingPostalCode ='23451',personMailingCountry ='United States',Primary_Phone_Type__c='Mobile',FirstName='testfirst',LastName='testlast',PersonEmail='testveryunique@test.vvtest.com',Phone='2223334444',VV_Market__c='US',VV_Second_Market__c='CA',VV_Feed__c='ID',VV_Edition__c='Non-Professional',Subscription_Plan__c='Annual',Is_Elite__c=false,Is_Elite_Legacy__c=false,Is_Apple_App_Store_Customer__c=false,BillingStreet='2234 test',BillingCity='Test',BillingState='GA',BillingCountry='USA',BillingPostalCode='22344');
        insert a;
        // setup billing account
        Zuora__CustomerAccount__c zca = new Zuora__CustomerAccount__c(Name='test billing account',Zuora__Account__c=a.Id,Zuora__Zuora_Id__c='12345',Zuora__Status__c='Active');
        insert zca;
        // setup products and rate plans
        zqu__ZProduct__c zp = new zqu__ZProduct__c(Name='testproduct',zqu__EffectiveStartDate__c=Date.today().addDays(-1),zqu__EffectiveEndDate__c=Date.today().addDays(1),zqu__SKU__c='skutest',zqu__ZuoraId__c='12345');
        insert zp;
        zqu__ProductRatePlan__c zpp = new zqu__ProductRatePlan__c(VV_Market__c='US',VV_Feed__c='EOD',VV_Edition__c='Non-Professional',Name='US EOD Monthly',zqu__ZProduct__c=zp.Id,zqu__ZuoraId__c='12345',zqu__EffectiveStartDate__c=Date.today().addDays(-1),zqu__EffectiveEndDate__c=Date.today().addDays(1));
        insert zpp;
        zqu__ProductRatePlanCharge__c zppc = new zqu__ProductRatePlanCharge__c(Name='US EOD Monthly',zqu__ProductRatePlan__c=zpp.Id,zqu__ListPrice__c=1000,zqu__ZuoraId__c='12345');
        insert zppc;
        // setup subscription
        Zuora__Subscription__c zs = new Zuora__Subscription__c(Zuora__Account__c=a.Id,Zuora__Zuora_Id__c='12345');
        insert zs;
        // setup sub charge
        Zuora__SubscriptionProductCharge__c zspc = new Zuora__SubscriptionProductCharge__c(Zuora__Subscription__c=zs.Id,Name='testtest1',Zuora__Price__c=12,Zuora__RatePlanId__c='12345',Zuora__RatePlanName__c='testtest1',Zuora__EffectiveStartDate__c=Date.today().addMonths(-3),Zuora__EffectiveEndDate__c=Date.today().addMonths(15),Zuora__EndDateCondition__c=null);
        insert zspc;

        // rt pro new
        Case c1 = new Case(Status='New',Type='RT Professional',AccountId=a.Id,Reason='new insert');
        insert c1;
        c1.Status='Closed';
        update c1;

        // rt pro upgrade
        Case c2 = new Case(Status='New',Type='RT Professional',AccountId=a.Id,Reason='Upgrade',RT_Pro_Rate_Plan__c=zpp.Id,RT_Pro_Trial_First__c=false,RT_Pro_Trial_Amount__c=0);
        insert c2;
        c2.Status='Closed';
        update c2;

        // options pro upgrade
        Case c3 = new Case(Status='New',Type='OptionsPro',OptionsPro_Sub__c='12345',AccountId=a.Id);
        insert c3;
        c3.Status='Closed';
        update c3;

        // elite

        // options pro upgrade
        Case c4 = new Case(Status='New',Type=vvConstants.CASE_ELITE_PACKAGE,OptionsPro_Sub__c='12345',AccountId=a.Id);
        insert c4;
        c4.Status='Closed';
        update c4;

        // fraud

        Case c5 = new Case(Status='New',Type=vvConstants.CASE_FRAUD_ALERT,AccountId=a.Id);
        insert c5;
        c5.Status='Closed';
        update c5;

        Case c6 = new Case(Status='New',Type=vvConstants.CASE_CREATE_SUB,AccountId=a.Id);
        insert c6;
        c6.Status='Closed';
        update c6;

        




    }
}