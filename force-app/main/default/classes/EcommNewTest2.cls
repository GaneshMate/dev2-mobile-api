/**
 * self reg e-commerce controller test method, need to use see all data as we are calling zuora soap
 */
@isTest(SeeAllData=true)
private class EcommNewTest2 {
    @isTest static void testEcommNew() {

        EcommNew c = new EcommNew();
        c.selectedMarket = '';
        c.chooseMarket();
        c.selectedProduct = '';
        c.selectProduct();
        c.selectedProduct = 'RT';
        c.showPickPro = false;
        c.selectProduct();

        c.lastname = 'testlast';
        c.firstname = 'testfirst';
        c.a.Phone = '2223334444';
        c.a.PersonEmail = 'test1345@12345678vvtest.com';
        c.confirmEmail = 'test@test.com';
        c.enterUserInfo();

        c.confirmEmail = c.a.PersonEmail;
        c.enterUserInfo();

        c.password = 'test';
        c.confirmPassword = 'test2';
        c.enterUserInfo();

        c.confirmPassword = c.password;
        c.enterUserInfo();

        c.a.BillingStreet = '112 test';
        c.a.BillingCity = 'Atlanta';
        c.a.BillingPostalCode = '22933';
        c.a.BillingState = 'Georgia';
        c.a.BillingCountry = 'USA';
        c.sameAsBilling = true;

        Account a1 = [SELECT PersonEmail FROM Account WHERE PersonEmail <> null LIMIT 1];
        c.referral_code = a1.PersonEmail;
        c.enterUserInfo();
        c.goBack();

        c.selectedMarket = 'US';
        c.chooseMarket();
        c.goBack();

        c.selectedProduct = 'EOD';
        c.selectProduct();
        c.goBack();

        c.showSelectMarket = false;
        c.showSelectProduct = false;
        c.showPickPro = true;
        c.goBack();
        c.showSelectProduct = false;
        c.showSelectTrial = true;
        c.goBack();

        c.payment_method.Payment_Type__c = null;
        c.chooseTrial();

        c.payment_method.Payment_Type__c = 'ACH';
        c.chooseTrial();

        c.payment_method.Payment_Type__c = 'Credit Card';
        c.chooseTrial();

        c.credit_card_num = '2222333344445555';
        c.payment_method.Credit_Card_Holder_Name__c = 'test test';
        c.payment_method.Credit_Card_Type__c = 'Visa';
        c.payment_method.Credit_Card_Expiration_Month__c = '03';
        c.payment_method.Credit_Card_Expiration_Year__c = '2016';
        c.credit_card_cvv = '222';
        c.payment_method.Enc_Value__c = '12341324125135231512';
        system.debug(c.payment_method);

        c.chooseTrial();
        c.goBack();
        c.checkA();

        c.selectedEdition = 'Professional';
        c.selectedProduct = 'RT';
        c.determineFees();

        //vvAccountManagementExtension ame = new vvAccountManagementExtension(c);


    }    
}