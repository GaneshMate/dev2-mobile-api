/**
 * runs every morning to regenerate access right
 * @param  sc [description]
 * @return    [description]
 */
global class SubscriptionEARScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		Database.executeBatch(new SubscriptionAfterUpdateBatch());
	}

	/*
	runs daily at 5:00
	
	System.schedule('Subscription EAR Regeneration Batch','0 5 0 * * ?', new SubscriptionEARScheduler());
	 */
}