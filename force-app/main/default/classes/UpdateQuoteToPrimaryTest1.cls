@isTest(SeeAllData=true)
public class UpdateQuoteToPrimaryTest1 {
    static testMethod void testUpdateQuoteToPrimary() {
        
        try{
            Account acc = new Account ();
            acc.Name = 'Test Account';
            insert acc;
            
            contact c = new contact();
            c.lastname = 'test';
            c.email = 'test@test.com';
            c.AccountId = acc.Id;
            insert c;
            
            Opportunity opp = new Opportunity(Name=acc.Name + ' Opportunity ' ,
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=acc.Id);
            
            insert opp;
            SBQQ__Quote__c quoteRec = new SBQQ__Quote__c();
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SiteUser_Primary_Check__c = true;
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SBQQ__Opportunity2__c = opp.Id;
            quoteRec.ZSB__BillToContact__c = c.Id;
            quoteRec.ZSB__SoldToContact__c = c.Id;
            quoteRec.ZSB__BillCycleDay__c =  '1';
            quoteRec.ZSB__BillingBatch__c = 'Batch1';
            insert quoteRec ;
            //Test.startTest();
            UpdateQuoteToPrimary1 obj = new UpdateQuoteToPrimary1();
        
            Database.executeBatch(obj,1);
            //Test.stopTest();
        }Catch(Exception E){
            
            System.debug('Excepption'+E.getMessage());
        }
        
    }
}