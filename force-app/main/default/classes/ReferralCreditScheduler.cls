global class ReferralCreditScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		Database.executeBatch(new ReferralCreditBatch(),1);	// run through one account at a time
	}

	/*
	System.schedule('Referral Credit Scheduler','0 30 23 * * ?', new ReferralCreditScheduler());

	 */
}