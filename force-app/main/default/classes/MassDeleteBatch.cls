global class MassDeleteBatch implements Database.Batchable<sObject> {
	
	String query;
	

	global MassDeleteBatch(String query) {
		this.query = query;
	}

	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		SubscriptionTriggerHandler.RUNBATCH = false;
		Database.delete(scope, false);
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}