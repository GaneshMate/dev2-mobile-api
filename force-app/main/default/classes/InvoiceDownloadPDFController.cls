/**
 * query and generate invoice pdf attachment
 */
public without sharing class InvoiceDownloadPDFController {
	public Id attachId {get; private set;}

	public InvoiceDownloadPDFController() {}

	public void createFindAttachment() {
		try {
			String invoiceid = ApexPages.currentPage().getParameters().get('id');
			if (invoiceid == null || invoiceid == '') {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Invoice Id'));
	            return;
			}
			List<Zuora__ZInvoice__c> zilist = [SELECT Zuora__Account__c, Zuora__InvoiceDate__c, Zuora__TotalAmount__c, Zuora__Zuora_Id__c FROM Zuora__ZInvoice__c WHERE Id = :invoiceid ORDER BY CreatedDate DESC];
			if (zilist.isEmpty()) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invoice does not exist'));
	            return;
			}

			Zuora__ZInvoice__c zi = zilist[0];


			// first try to find the attachment
			List<Attachment> attlist = [SELECT Id FROM Attachment WHERE ParentId = :invoiceid];
			if (!attlist.isEmpty()) {
				attachId = attlist[0].Id;
				setOwnerId(zi, zi.Zuora__Account__c);
				return;
			}

			// attachment not found, create a new one
			String query = 'SELECT Id,Body FROM Invoice WHERE Id = \'' + zi.Zuora__Zuora_Id__c + '\'';
			List<Zuora.zObject> results = zuoraAPIHelper.queryAPICall(query);
			if (results.isEmpty()) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invoice does not exist'));
	            return;
			}
			Zuora.zObject invoice_from_zuora = results[0];

			// attach it to invoice
			Attachment attach = new Attachment();

		    attach.Body = EncodingUtil.base64Decode(String.valueOf(invoice_from_zuora.getValue('Body')));
		    attach.Name = 'invoice.pdf';
		    attach.IsPrivate = false;
		    attach.ParentId = invoiceid;
		    insert attach;
		    attachId = attach.Id;

		    setOwnerId(zi, zi.Zuora__Account__c);


		} catch (Exception e) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to download invoice at this moment, please contact CSR.'));
		}		
	}	

	static void setOwnerId(Zuora__ZInvoice__c zi, Id accountid) {
		// owner of this invoice to the portal user
		List<User> us = [SELECT Id FROM User WHERE AccountId = :accountid AND IsActive = true];
		if (!us.isEmpty()) {
			zi.OwnerId = us[0].Id;
			update zi;
		}
	}
}