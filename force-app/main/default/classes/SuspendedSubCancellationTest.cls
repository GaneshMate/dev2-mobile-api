@isTest(SeeAllData=true)
private class SuspendedSubCancellationTest {
	@isTest static void testBatch() {
		Zuora__Subscription__c zs = new Zuora__Subscription__c(Zuora__Zuora_Id__c = '12345');
		insert zs;

		Upgrade_Suspended_Subscription__c uss = new Upgrade_Suspended_Subscription__c(Suspended_Subscription__c = zs.Id, Conversion_Date__c = Date.today(), Cancelled__c = false);
		insert uss;

		Test.startTest();
		Database.executeBatch(new SuspendedSubCancellationBatch(uss.Id));
		Test.stopTest();

		uss = [SELECT Cancelled__c FROM Upgrade_Suspended_Subscription__c WHERE Id = :uss.Id];
		System.assertEquals(true, uss.Cancelled__c, 'Suspended Cancellation Batch should cancelled the subscription');

	}
}