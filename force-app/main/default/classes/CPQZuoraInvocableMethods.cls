global class CPQZuoraInvocableMethods {
  @InvocableMethod(label='Sync ECommerce Contracts' description='Update ECommerce contracts')
  global static void updateAndSyncContract(List<ID> contractIds) {
      System.debug('Hello contractIds------->>>>>'+contractIds);
      if(contractIds!=null && contractIds.size()>0)
      {
          List<Contract> allContracts = [select id,SBQQ__Quote__c,ZSB__BillingAccount__c,status,DefaultPaymentMethod__c,ZSB__ProcessPayments__c,AccountId,Account.Is_Apple_App_Store_Customer__c,SBQQ__Opportunity__c,ZSB__SubscribeToZuora__c,ZSB__SubscriptionId__c,IsMobile__c from Contract where id IN: contractIds AND ZSB__SubscribeToZuora__c = false limit 200];
          List<Contract> allOtherContractsNotSynced = [select id,SBQQ__Opportunity__c,ZSB__SubscribeToZuora__c from Contract where id NOT IN :contractIds AND ZSB__SubscribeToZuora__c = false limit 200];
            System.debug('allContracts--------'+allContracts);
          // AK:: Updating the Billing Account on Contract
          Set<String> accountIdSet = new Set<String>();
          set<String> QuoteIDSet = new set<String>();
          map<Id, Id> mapContractToZuoraAccount = new map<Id,Id>();
          map<Id, Id> mapZuoraAccountToBillingAccount = new map<Id,Id>();
          for(Contract contract :allContracts) {
              if (contract.AccountId == null) {
                  continue;
              }
              if(Contract.SBQQ__Quote__c != null){
                QuoteIDSet.add(Contract.SBQQ__Quote__c);
              }
              if(contract.ZSB__BillingAccount__c == null){
                   mapContractToZuoraAccount.put(contract.id,contract.AccountId);
              }
             
              accountIdSet.add(contract.AccountId);
          }
        map<id, id> mapQuotewithBillingAccount = new map<id,id>();
        for(SBQQ__Quote__c objQuote:[select id,ZSB__BillingAccount__c from SBQQ__Quote__c where id in:QuoteIDSet limit 49999]){
            mapQuotewithBillingAccount.put(objQuote.id, objQuote.ZSB__BillingAccount__c);
        }
          Map<Id, Zuora__CustomerAccount__c> customerAccountMap = new Map<Id, Zuora__CustomerAccount__c>();
          // Ganesh :: Created object to seprate out latest payment method
          Zuora__CustomerAccount__c otherCustomerRecord = new Zuora__CustomerAccount__c();
          //Zuora__CustomerAccount__c notOtherCustomerRecord = new Zuora__CustomerAccount__c();
          List<Zuora__CustomerAccount__c> ZuoraCustomerAccountList = [SELECT Id, Zuora__Zuora_Id__c, Zuora__Account__c,Zuora__DefaultPaymentMethod__c FROM Zuora__CustomerAccount__c WHERE Zuora__Account__c = :accountIdSet order by createdDate desc limit 9999];
          System.debug('ZuoraCustomerAccountList------'+ZuoraCustomerAccountList);
          for(Zuora__CustomerAccount__c customerAccount :ZuoraCustomerAccountList) {
              
              // Ganesh :: Checking otherCustomerRecord is equal null and PaymentMethod equal to 'Other' if true then assign latest Zuora__Account__c to otherCustomerRecord object.
              // Same process for 'Non-Other'
              /*if(OtherCustomerRecord.Id == null && customerAccount.Zuora__DefaultPaymentMethod__c == 'Other'){
                  OtherCustomerRecord.Id=customerAccount.Id;
              }*/
              /*else if(NotOtherCustomerRecord.Zuora__Account__c == null) {
                  NotOtherCustomerRecord.Id=customerAccount.Id;
              }*/
              
              // Ganesh :: 17/01/2019 :: Checking mapZuoraAccountToBillingAccount is contains Zuora__CustomerAccount__c Id and PaymentMethod equal to 'Other' if true then assign latest Zuora__Account__c to mapZuoraAccountToBillingAccount map.
              if(!mapZuoraAccountToBillingAccount.containsKey(customerAccount.Id) && customerAccount.Zuora__DefaultPaymentMethod__c == 'Other'){
                  mapZuoraAccountToBillingAccount.put(customerAccount.Zuora__Account__c, customerAccount.id);
              }
          }
          // AK:: End - Updating the Billing Account on Contract
         
          Map<ID,COntract> contractsToUpdate = new  Map<ID,Contract>();
          if(allContracts.size()>0)
          {          
              for(Contract c : allContracts )
              {
                  c.Status = 'Activated'; 
                    if(c.Account.Is_Apple_App_Store_Customer__c == False){
                        if(c.DefaultPaymentMethod__c =='CreditCard'){
                            c.ZSB__ProcessPayments__c = true;
                        }
                        c.ZSB__GenerateInvoice__c = true;
                    }
                    else{
                        c.ZSB__ProcessPayments__c = False;
                        // AK:: 18JAN :: Set generate invoice flag for mobile
                        c.ZSB__GenerateInvoice__c = true;
                    }
                  if(Test.isRunningTest()) {
                      c.ZSB__SubscribeToZuora__c = false;  
                  } else{ 
                      c.ZSB__SubscribeToZuora__c = true;
                  }
                  if(!contractsToUpdate.containsKey(c.id))
                  {
                      contractsToUpdate.put(c.id,c);
                  }
                  
                  // AK:: Updating the Billing Account on Contract
                  if(c.ZSB__BillingAccount__c == null) {
                      // Ganesh :: Checking IsMobile is equal true then assign OtherCustomerRecord to billingAccount
                      if(c.IsMobile__c == true){
                        // c.ZSB__BillingAccount__c = OtherCustomerRecord.Id;
                        //Ganesh :: 17/01/2019 :: we are getting ContractIdVsZuoraAccount from map and assigning to ZSB__BillingAccount__c.
                        String ContractIdVsZuoraAccount = mapContractToZuoraAccount.get(c.id);
                        c.ZSB__BillingAccount__c = mapZuoraAccountToBillingAccount.get(ContractIdVsZuoraAccount);
                      }else{
                            if(c.SBQQ__Quote__c != null && mapQuotewithBillingAccount.size()>0){
                                c.ZSB__BillingAccount__c = mapQuotewithBillingAccount.get(c.SBQQ__Quote__c);
                            }
                            /*else{
                            c.ZSB__BillingAccount__c = NotOtherCustomerRecord.Id;
                            }*/
                      }
                     //c.ZSB__BillingAccount__c =  customerAccountMap.get(c.AccountId).Id;
                     //c.ZSB__SubscriptionId__c = customerAccountMap.get(c.AccountId).Zuora__Zuora_Id__c;
                  }
                  // AK:: End - Updating the Billing Account on Contract
              }                      
          }
          if(allOtherContractsNotSynced.size()>0)
          {          
              for(Contract oCon : allOtherContractsNotSynced )
              {
                  oCon.Status = 'Activated';                  
                  oCon.ZSB__ProcessPayments__c = true;
                  oCon.ZSB__SubscribeToZuora__c = true;
                  if(!contractsToUpdate.containsKey(oCon.id))
                  {
                      //contractsToUpdate.put(oCon.id,oCon );
                  }
                  
                  
              }                      
          }
          
          if(contractsToUpdate.values().size() > 0)
          {
              update contractsToUpdate.values();
          }
      }
      
  }
}