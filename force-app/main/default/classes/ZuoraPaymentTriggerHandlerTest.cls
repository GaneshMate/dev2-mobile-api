@isTest
private class ZuoraPaymentTriggerHandlerTest {
    @isTest static void testPaymentInsert() {
        // create account
        Account a = new Account(PersonMailingCity='Test City',PersonMailingStreet = 'TestStreet',PersonMailingState='TestState',PersonMailingPostalCode ='23451',personMailingCountry ='United States',Primary_Phone_Type__c='Mobile',FirstName='testfirst',LastName='testlast',PersonEmail='testveryunique@test.vvtest.com',Phone='2223334444',VV_Market__c='US',VV_Second_Market__c='CA',VV_Feed__c='ID',VV_Edition__c='Non-Professional',Subscription_Plan__c='Annual',Is_Elite__c=false,Is_Elite_Legacy__c=false,Is_Apple_App_Store_Customer__c=false,BillingStreet='2234 test',BillingCity='Test',BillingState='GA',BillingCountry='USA',BillingPostalCode='22344');
        insert a;
        Zuora__CustomerAccount__c zca = new Zuora__CustomerAccount__c(Zuora__Status__c='Active',Name='test billing account',Zuora__Account__c=a.Id,Zuora__Zuora_Id__c='12345');
        insert zca;
        Zuora__Payment__c zp = new Zuora__Payment__c(Zuora__Amount__c=100,Zuora__Account__c=a.Id,Zuora__BillingAccount__c=zca.Id);
        insert zp;

        delete zp;
    }
}