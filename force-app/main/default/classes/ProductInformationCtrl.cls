public class ProductInformationCtrl {
     Public Product2 selProductObj {get;set;}
     Public String selProdId{get;set;}   
     Public Account a {get;set;}
     Public Decimal reg_trial_fee {get;set;}
     Public String currencySymbol {get;set;}
     Public Payment_Method_SF__c payment_method {get;set;}
     public Boolean displayProdInfo {get;set;}
    
    Public PageReference showSelectedProdScreen(){
        displayProdInfo = true;
        currencySymbol = '$';
        selProdId = ApexPages.currentPage().getParameters().get('selProdId');
        selProductObj = new Product2();
        payment_method = new Payment_Method_SF__c();
        a = new Account();
        /*selProductObj = [SELECT id,Name,ProductCode,SBQQ__ProductPictureID__c,Product_Detail__c FROM product2
                        WHERE Id=:selProdId];*/
         PriceBookIds__c  stdPriceBookId = PriceBookIds__c.getValues('Standard Price Book');
        string strPricebookId = stdPriceBookId.Price_Book_Id__c;
        List<Product2> prodList = new List<Product2>();
        string strQuery = 'SELECT id,Name,ProductCode,SBQQ__ProductPictureID__c,Product_Detail__c,Market__c,Feed__C,'+ 
            '(SELECT ID,UnitPrice FROM PricebookEntries where Pricebook2Id = \'' + strPricebookId + '\''+') FROM Product2 where id=:selProdId';
        selProductObj = Database.Query(strQuery);    
        reg_trial_fee = selProductObj.PricebookEntries[0].UnitPrice;
            
        //if(Userinfo.getuserType() == 'PowerPartner'){
            
        //}
        User u = [Select id,ContactId from User where id=:UserInfo.getUserId()]; //id='00511000005Ic9N'
        if(u != null ){
            if(u.ContactId != null){
                Contact c = new Contact();
                c = [select id,AccountId from contact where id=:u.ContactId ];
                if(c != null && c.AccountId != null){
                   a = [select id, Salutation,FirstName,LastName,BillingStreet,BillingCity,BillingState,
                            BillingPostalCode,BillingCountry,Phone,PersonEmail,Shipping_FirstName__c,
                            Shipping_LastName__c,ShippingStreet,ShippingCity,ShippingState,Account_Currency__c,
                            ShippingPostalCode,ShippingCountry from Account 
                        WHERE id=:c.AccountId];
                   	if (a.Account_Currency__c == 'EUR') {
                   	    currencySymbol = '€';
                   	}

            		else if (a.Account_Currency__c == 'GBP') {
            		    currencySymbol = '£';
            		}
            		else{
            		    currencySymbol = '$';
            		}
            	
            		if(a != null && a.Id != null){
            		    try{
            		    payment_method = [SELECT Id,Account__c,Credit_Card_Number__c,Credit_Card_Expiration_Month__c,
            		                    Credit_Card_Expiration_Year__c,Payment_Type__c,ACH_Bank_Name__c,ACH_Account_Number__c
            		                    FROM Payment_Method_SF__c WHERE Account__c=:a.Id limit 1 ]; // WHERE Account__c=:a.Id
            		    }Catch(Exception E){
            		        System.debug('Exception');
            		    }
            		}
                }
            }
        }
        return null;
    }
    Public PageReference redirectToNextPage(){
        displayProdInfo = false;
        return null;
    }
}