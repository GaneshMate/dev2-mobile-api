@isTest(seeAllData=true)
public class PaymentUpdateBatchTest {
    
    @isTest static void testMagalog(){
        test.startTest();
        PaymentMethod__c pmt = new PaymentMethod__c();
        pmt.PaymentId__c = '1234';
        pmt.CreditCardType__c='visa';
            insert pmt;
        //List <PaymentMethod__c> PaymentMethodList = [SELECT PaymentId__c,CreditCardType__c FROM PaymentMethod__c];
        
        PaymentUpdateBatch pub= new PaymentUpdateBatch();
        Database.executeBatch(pub,1500);
        test.stopTest();
        
    }
}