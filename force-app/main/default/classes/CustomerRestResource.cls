@RestResource(urlMapping='/customer/update/*')
global class CustomerRestResource {
    
    global class Error {
        global integer code;
        global string msg;
        global Error(integer code, string msg) {
            this.code = code;
            this.msg = msg;
        }
    }
    /* LastModified Date 07/05/2017 - Added OptinMarketing feild */
    @HttpPost
    global static void updateCustomer (String eid,String certificateId,String optinMarketing){
        //Search Customer on the basis of Eid
        integer retCode = 0;
        string retBody = '';
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        // Ganesh:: 22 NOV 2018 :: remove validation of certificateId 
        //if(!String.isBlank(eid) && !String.isBlank(certificateId)){
        if(!String.isBlank(eid)){
            if(String.isBlank(optinMarketing) || optinMarketing.equalsIgnoreCase('Yes') || optinMarketing.equalsIgnoreCase('No')){
                System.debug('*****eid*****'+eid);
                System.debug('*****certificateId*****'+certificateId);
                System.debug('*****optinMarketing*****'+optinMarketing);
                List<Account> accountList = getCustomerInfoByEntCustId(eid);
                List<Account> mobileCustomerList = new List<Account> ();
                System.debug('*****accountList*****'+accountList);
                List<Account> accountListToUpdate = new List<Account>();
                if(accountList != null && accountList.size() != 0 && !accountList.isEmpty()){
                    //check is there active subscription present on account
                    Map<String,Account> accMap = new Map<String,Account>();
                    
					// Ganesh:: 22 NOV 2018 :: checking OptinMarketing should not blank and updating OptinMarketing for GDPR.
                    for(Account ac:accountList){
                        
                        mobileCustomerList.add(ac);  
                        
                        if(String.isNotBlank(optinMarketing)) {
                            ac.GDPR_Opt_in__pc = OptinMarketing ;
                        }
                        accountListToUpdate.add(ac);  
                        System.debug('*****accountListToUpdate*****'+accountListToUpdate);
                    }
                    
                    System.debug('*****accountListToUpdate*****'+accountListToUpdate);
                    Map<String,Account> accountMap = new Map<String,Account>();
                    if(accountListToUpdate.size() != 0 && !accountListToUpdate.isEmpty()){
                        //update customer account
                        for(Account acc:accountListToUpdate){
                            //accountMap.put(acc.personEmail,acc);  
                            accountMap.put(acc.id,acc);        
                        }
                        System.debug('***accountMap ****'+accountMap );
                        Database.SaveResult[] results  = Database.update(accountListToUpdate);
                        System.debug('***results ****'+results );
                        List<Database.SaveResult> failedAccList = new List<Database.SaveResult>();
                        for(Database.SaveResult ds :results){
                            if (ds.isSuccess() == false)
                                failedAccList.add(ds);    
                        }
                        System.debug('***failedAccList ****'+failedAccList);
                        if(failedAccList== null || failedAccList.size()== 0 || failedAccList.isEmpty()){
                            //check for community user
                            System.debug('***inside success rec block ****' );
                            //List<User> userList = [SELECT userName,email,isActive FROM  user WHERE email =: accountMap.keySet()];
                            List<User> userList = [SELECT id,userName,email,isActive FROM  user WHERE accountid =: accountMap.keySet()];
                            System.debug('***userList ****'+userList );
                            if(userList.isEmpty() && userList.size()==0){
                                // set account owner so we can create new community user
                                VVCommunitySettings__c vcs = VVCommunitySettings__c.getOrgDefaults();
                                User uowner = [SELECT Id FROM User WHERE Name = :vcs.Account_Owner_Name__c];
                                if(uowner !=null){
                                    accountListToUpdate[0].OwnerId = uowner.Id;
                                    update  accountListToUpdate[0];    
                                }
                                // Ganesh:: 22 NOV 2018 :: passed extra field in createCommunityUser as true to identify it's a mobile user.
                                User user = vvUserManagement.createCommunityUser(accountListToUpdate[0].Id, false,true); 
                                System.debug('***user ****'+user );
                                if(user != null){
                                    retCode = 200;
                                    retBody = serialize(new Error(retCode, 'Success!'));    
                                }else{
                                    retCode = 400;
                                    retBody = serialize(new Error(retCode, 'Failed to create community User!'));    
                                }
                            }else{
                                List<User> userListToUpdate = new List<User>();
                                for(User user:userList){
                                    if(user.IsActive == false){
                                        user.isActive = true;
                                        userListToUpdate.add(user);    
                                    }
                                }
                                if(!userListToUpdate.isEmpty() && userListToUpdate.size() !=0 ){
                                    retCode = 400;
                                    retBody = serialize(new Error(retCode, userList[0].UserName+'  is inActive User!'));    
                                }else{
                                    retCode = 200;
                                    retBody = serialize(new Error(retCode, 'Success!'));     
                                }
                            }
                        }else{
                            retCode = 400;
                            retBody = serialize(new Error(retCode, 'Failed to update customer!'));
                        }
                    }else{
                        // Customer already have a active subscription and its mobile customer
                        if(mobileCustomerList.size()!=0 && !mobileCustomerList.isEmpty()){
                            retCode = 200;
                            retBody = serialize(new Error(retCode, 'Success!'));    
                        }else{// Customer already have a active subscription and its non mobile customer
                            retCode = 400;
                            retBody = serialize(new Error(retCode, 'Can not update customer because of active subscriptions!'));    
                        } 
                        
                    }
                }else{
                    // Customer not found
                    retCode = 400;
                    retBody = serialize(new Error(retCode, 'Customer not found!'));    
                }
            }else{
                retCode = 400; 
                retBody = serialize(new Error(retCode, 'Error:optinMarketing must be in format(Yes/No)'));
            }
        }else{
            retCode = 400; 
            retBody = serialize(new Error(retCode, 'Error:Enterprise Customer ID and certificateId are required!'));
        }
        res.headers.put('Content-type', 'application/json');
        res.statusCode = retCode;
        res.responseBody = Blob.valueOf(retBody);
        System.debug('****retCode****'+retCode);
    }
    
    private static string serialize(object o) {
        return JSON.serialize(o);
    }
    
    private static List<Account> getCustomerInfoByEntCustId(String eid){
        List<Account> accountList = new List<Account>();
        if(String.isBlank(eid))
            return new List<Account>();
        accountList = [select  Id, ApplicationUserID__c, EnterpriseCustomerID__c,
                       FirstName, LastName, Salutation,PersonMailingCountry,
                       OptionsPro_Username__c, OptionsPro_Password__c,PersonEmail, IsCustomerPortal, Vector_Vest_Customer_Id__c,
                       
                       VV_Feed__c, VV_Market__c, VV_Edition__c, VV_Second_Market__c, 
                       Subscription_Plan__c, Account_Currency__c, Is_Apple_App_Store_Customer__c, 
                       (select ClaimValue__c, Mask__c, Is_Trial__c,Subscription_End_Date__c,
                        Is_Observable__c,Non_Converting_Trial__c from Enterprise_Access_Rights__r )
                       from Account
                       where EnterpriseCustomerID__c =:eid  ORDER BY CreatedDate Desc];
        return accountList != null ? accountList : new List<Account>();
    }
    /* private static List<SBQQ__Subscription__c> getSubscriptionByAccountId(String AccountId){
        
        List<SBQQ__Subscription__c> subscriptionList = new List<SBQQ__Subscription__c> ();
        if(String.isBlank(AccountId))
            return new List<SBQQ__Subscription__c> ();
        
        String AccId = AccountId.substring(0,15);
        String endDate = String.valueOf(system.today());
        String query ='select SBQQ__ContractNumber__c,SBQQ__Contract__c,SBQQ__Product__c,SBQQ__Quantity__c,SBQQ__NetPrice__c,SBQQ__SpecialPrice__c,SBQQ__ListPrice__c,SBQQ__CustomerPrice__c,SBQQ__RegularPrice__c,SBQQ__Account__c,ZSB__PRPChargeId__c,ZSB__PRPlanId__c,SBQQ__StartDate__c,SBQQ__EndDate__c,SBQQ__TerminatedDate__c,SBQQ__ProductName__c,SBQQ__Contract__r.Status,Contract_Term__c,SBQQ__Contract__r.ContractTerm,SBQQ__Contract__r.ZSB__TermType__c from SBQQ__Subscription__c where SBQQ__Account__c =\''+AccId+'\''+'And SBQQ__EndDate__c <='+ endDate+' '+'ORDER BY CreatedDate Desc';
        System.debug('+++++query++++'+query);
        subscriptionList = Database.query(query);
        return subscriptionList != null ? subscriptionList : new List<SBQQ__Subscription__c>();
    }*/
    
}