@isTest(seeAllData=true)
private class OneTimePurchaseTest {
    @isTest static void testOneTimeController() {

        // setup product
        Product2 p = new Product2(Family='Book',Name='thebook',IsActive=true);
        insert p;
        Id pricebookId = Test.getStandardPricebookId();

        PriceBookEntry pe = new PriceBookEntry(Pricebook2Id=pricebookId,Product2Id=p.Id,UnitPrice=10,IsActive=true);
        insert pe;

        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();

        Zuora__CustomerAccount__c zca = [SELECT Zuora__Account__c,Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Name = 'test billing account'];
        ApexPages.CurrentPage().getparameters().put('aid',zca.Zuora__Account__c);

        Test.startTest();
        OneTimePurchaseController opc = new OneTimePurchaseController();
        opc.selected_family_id = 'Book';
        opc.selected_product_id = pe.Id;
        opc.selectItem();
        ApexPages.CurrentPage().getparameters().put('i','0');
        opc.removeProduct();
        opc.selectItem();
        opc.getProducts();
        opc.getPaymentMethods();
        OneTimePurchaseController.currency('2.01');

        
        opc.doPurchase();
        Test.stopTest();

    }

    @isTest static void testOneTimeNoZCA() {
        // setup product
        Product2 p = new Product2(Family='Book',Name='thebook',IsActive=true);
        insert p;
        Id pricebookId = Test.getStandardPricebookId();

        PriceBookEntry pe = new PriceBookEntry(Pricebook2Id=pricebookId,Product2Id=p.Id,UnitPrice=10,IsActive=true);
        insert pe;

        Account a = new Account(Primary_Phone_Type__c='Mobile',PersonMailingCity='Test City',PersonMailingStreet = 'TestStreet',PersonMailingState ='TestState',PersonMailingPostalCode ='23451',PersonMailingCountry ='United States',FirstName='testfirst',LastName='testlast',PersonEmail='test@test.com',Phone='2223334444',VV_Market__c='US',VV_Second_Market__c='CA',VV_Feed__c='EOD',VV_Edition__c='Non-Professional',Subscription_Plan__c='Monthly',Is_Elite__c=false,Is_Elite_Legacy__c=false,Is_Apple_App_Store_Customer__c=false,BillingStreet='2234 test',BillingCity='Test',BillingState='GA',BillingCountry='USA',BillingPostalCode='22344');
        insert a;

        ApexPages.CurrentPage().getparameters().put('aid',a.Id);

        Test.startTest();

        OneTimePurchaseController ctrl = new OneTimePurchaseController();
        
        ctrl.selected_family_id = 'Book';
        ctrl.selected_product_id = pe.Id;

        ctrl.selectItem();
        
        ctrl.payment_method.Payment_Type__c = 'Credit Card';
        ctrl.payment_method.Credit_Card_Number__c = '2222333344445555';
        ctrl.payment_method.Credit_Card_Holder_Name__c = 'test test';
        ctrl.payment_method.Credit_Card_Type__c = 'Visa';
        ctrl.payment_method.Credit_Card_Expiration_Month__c = '03';
        ctrl.payment_method.Credit_Card_Expiration_Year__c = '2016';
        ctrl.payment_method.Credit_Card_CVV_Code__c = '222';
        ctrl.payment_method.Enc_Value__c = '12341324125135231512';

        
        ctrl.doPurchase();
        Test.stopTest();
    }
}