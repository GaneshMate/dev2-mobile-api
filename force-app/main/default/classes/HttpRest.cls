public with sharing class HttpRest {
 
    //  Class to handle HTTP Rest calls to other SFDC instances
    //
    //  [HTTP-00]   - Reached limit on callouts
    //  [HTTP-01]   - Unable to get OAuth 2.0 token from remote SFDC
    //  [HTTP-02]   - Error in SOQL REST query
 
    public String  accessToken;                    // OAuth 2.0 access token
    public String  sfdcInstanceUrl;                // Endpoint URL for SFDC instance
                         
    private HttpResponse send(String uri,String httpMethod) {
        return send(uri,httpMethod,null);
    }
         
    private HttpResponse send(String uri, String httpMethod, String body) {
        
        if (Limits.getCallouts() == Limits.getLimitCallouts())
            throw new MyException('[HTTP-00] Callout limit: ' + Limits.getCallouts() + ' reached. No more callouts permitted.');
        Http        h       = new Http();
        HttpRequest hRqst   = new HttpRequest();
        hRqst.setEndpoint(uri);                     // caller provides, this will be a REST resource
        hRqst.setMethod(httpMethod);                // caller provides
        hRqst.setTimeout(6000); 
        if (body != null) 
            hRqst.setBody(body);                    // caller provides
        if (this.accessToken != null)               // REST requires using the token, once obtained for each request
            hRqst.setHeader('Authorization','Bearer ' + this.accessToken);
        return h.send(hRqst);                   // make the callout
    }   
    
    private HttpResponse send(String uri, String httpMethod,String type,String body) {
        
        if (Limits.getCallouts() == Limits.getLimitCallouts())
            throw new MyException('[HTTP-00] Callout limit: ' + Limits.getCallouts() + ' reached. No more callouts permitted.');
        Http        h       = new Http();
        HttpRequest hRqst   = new HttpRequest();
        hRqst.setEndpoint(uri);                     // caller provides, this will be a REST resource
        hRqst.setHeader('Content-Type', type); // Request Type
        hRqst.setMethod(httpMethod);                // caller provides
        hRqst.setTimeout(6000); 
        if (body != null) 
            hRqst.setBody(body);                    // caller provides
        if (this.accessToken != null)               // REST requires using the token, once obtained for each request
            hRqst.setHeader('Authorization','OAuth ' + this.accessToken);
        return h.send(hRqst);                   // make the callout
    } 
    //  ----------------------------------------------------------------------
    //  authenticateByUserNamePassword      : Returns a map of <String,String> of the OAuth 2.0 access token; required before REST calls on SFDC instances can be made 
    //  ----------------------------------------------------------------------
    public void authenticateByUserNamePassword(String consumerKey, String consumerSecret, String uName, String uPassword, Boolean isSandbox) {
        // Reference documentation can be found in the REST API Guide, section: 'Understanding the Username-Password OAuth Authentication Flow'
        // OAuth 2.0 token is obtained from endpoints:
        //  PROD orgs   : https://login.salesforce.com/services/oauth2/token
        //  SANDBOX orgs: https://test.salesforce.com/services/oauth2/token
         
        //  OAuth 2.0 access token contains these name/values:
        //      access_token        : used in subsequent REST calls
        //      instance_url        : to form the REST URI
        //      id                  : identifies end user
        //      issued_at           : When signature was created
        //      signature           : HMAC-SHA256 signature signed with private key - can be used to verify the instance_url     
 
        String uri          = 'https://' + (isSandbox ? 'test' : 'login') + '.salesforce.com/services/oauth2/token';
        String clientId     = EncodingUtil.urlEncode(consumerKey,'UTF-8');
        String clientSecret = EncodingUtil.urlEncode(consumerSecret,'UTF-8');
        String username     = EncodingUtil.urlEncode(uName,'UTF-8');
        String password     = EncodingUtil.urlEncode(uPassword,'UTF-8');
 
        String body =   'grant_type=password&client_id=' + clientId + 
                        '&client_secret=' + clientSecret +
                        '&username=' + username + 
                        '&password=' + password; 
 
        HttpResponse hRes = this.send(uri,'POST',body);
        System.debug('Body------->'+hRes );
        if (hRes.getStatusCode() != 200) 
            throw new MyException('[HTTP-01] OAuth 2.0 access token request error. Verify username, password, consumer key, consumer secret, isSandbox?  StatusCode=' +
                                                 hRes.getStatusCode() + ' statusMsg=' + hRes.getStatus());
             
        Map<String,String> res = (Map<String,String>) JSON.deserialize(hRes.getBody(),Map<String,String>.class);
        this.accessToken        = res.get('access_token');      // remember these for subsequent calls
        this.sfdcInstanceUrl    = res.get('instance_url');
         
         
    }
 
 
    //  -----------------------------------------------------------------------
    //  doSoqlQuery: Executes a REST query on a remote SFDC and returns a list of SObjects
    //  -----------------------------------------------------------------------
    public List<SObject> doSoqlQuery(String query) {
        List<Sobject> res;        
        PageReference   urlPg   = new PageReference(this.sfdcInstanceUrl + '/services/data/v29.0/query');
        urlPg.getParameters().put('q',query); 
 
        String uri              = urlPg.getUrl();               // let APEX do the URL encoding of the parms as necessary
        HttpResponse hRes = this.send(uri,'GET');
        if (hRes.getStatusCode() != 200) 
            throw new MyException('[HTTP-02] Error in query ' + uri + ' StatusCode=' +
                                                 hRes.getStatusCode() + ' statusMsg=' + hRes.getStatus());
         
        // Response body comes back as:
        // {"totalSize":10,
        //  "done":true,
        //  "records":[
        //              {"attributes":{
        //                  "type"  : "the sobject",
        //                  "url"   : "/services/data/v29.0/sobjects/the sobject/the id"
        //              },
        //              "field0 in query"   : "value of field 0",
        //              "field1 in query"   : "value of field1",
        //              ...},
        //              next record ...
        //              ]
        //  }
        JSONParser jp = JSON.createParser(hRes.getBody());
        do{
            jp.nextToken();
        } while(jp.hasCurrentToken() && !'records'.equals(jp.getCurrentName()));
        jp.nextToken();  // token is 'records'
        res = (List<SObject>) jp.readValueAs(List<SObject>.class);      // Let caller cast to specific SObject
        return res;
    }
      
    public HttpResponse doRecordUpdate(String APIName,String recId,String fieldsToUpdateJSON) {
        List<Sobject> res;        
        string uri = this.sfdcInstanceUrl + '/services/data/v39.0/sobjects/'+APIName+'/'+recId+'?_HttpMethod=PATCH';
        HttpResponse hRes = this.send(uri,'POST','application/json',fieldsToUpdateJSON);  // e.g. : { "SBQQ__Primary__c" : "true" }
        if (hRes.getStatusCode() != 204 && hRes.getStatusCode() != 200) 
            throw new MyException('[HTTP-02] Error in request ' + uri + ' StatusCode=' +
                                                 hRes.getStatusCode() + ' statusMsg=' + hRes.getStatus());
         
        return hRes;
    }
    public class MyException extends Exception {} 
}