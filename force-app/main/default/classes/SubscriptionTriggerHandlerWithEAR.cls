/**
 * when subscription charge is inserted, updated, or delete, recalculate all the account fields and enterprise access rights
 */
public without sharing class SubscriptionTriggerHandlerWithEAR {
    public static void insertCustomSubscription(List<SBQQ__Subscription__c> sfSubscriptionList){
        list<Subscription_Custom__c> lstCustomsub = new list<Subscription_Custom__c>();
        map<id,id> mapCustomSubwithCPQSub = new map<id,id>();
       for(Subscription_Custom__c objCustom:[select id,Subscription__c  from Subscription_Custom__c where Subscription__c  in:sfSubscriptionList]){
           mapCustomSubwithCPQSub.put(objCustom.Subscription__c,objCustom.id);
       }
        Subscription_Custom__c objCustomsub;
        for(SBQQ__Subscription__c objSub:sfSubscriptionList){
            if(objsub.Enterprise_Subscription_ID__c != null && objsub.SBQQ__TerminatedDate__c == null){}
                else{
                if(mapCustomSubwithCPQSub.size()>0 && mapCustomSubwithCPQSub.get(objsub.id)!= null){
                    objCustomsub = new Subscription_Custom__c(id=mapCustomSubwithCPQSub.get(objsub.id),Subscription__c =objsub.id,Terminated_Date__c=objsub.SBQQ__TerminatedDate__c);
                    lstCustomsub .add(objCustomsub);                    
                }
                else{
                    objCustomsub = new Subscription_Custom__c(Subscription__c =objsub.id,Terminated_Date__c=objsub.SBQQ__TerminatedDate__c);
                    lstCustomsub.add(objCustomsub);
                }
            }
        }//for
        if(lstCustomsub.size()>0){
            upsert lstCustomsub;
        }
    }
    public static Boolean RUNBATCH = true;
    public static void updateContractTermtype(List<SBQQ__Subscription__c> sfSubscriptionList){
        list<COntract> lstContract =new list<Contract>();
        set<id> setContractids = new set<id>();
        for(SBQQ__Subscription__c objsub : [select id,SBQQ__Contract__c,Enterprise_Subscription_ID__c,SBQQ__Contract__r.ZSB__TermType__c,SBQQ__Product__r.SBQQ__BillingFrequency__c,SBQQ__Product__r.SBQQ__SubscriptionTerm__c from  SBQQ__Subscription__c where id in:sfSubscriptionList]){
            if((objsub.SBQQ__Product__r.SBQQ__BillingFrequency__c == 'Monthly' || objsub.SBQQ__Product__r.SBQQ__BillingFrequency__c == 'Annual' || objsub.SBQQ__Product__r.SBQQ__SubscriptionTerm__c==null) && objsub.SBQQ__Contract__r.ZSB__TermType__c != 'Evergreen' && !setContractids.contains(objsub.SBQQ__Contract__c)&& objsub.Enterprise_Subscription_ID__c ==null){
                    Contract objcontract = new Contract(id=objsub.SBQQ__Contract__c,ZSB__TermType__c ='Evergreen');
                    lstContract.add(objContract);
                    setContractids.add(objsub.SBQQ__Contract__c);
            }
        }
        if(lstContract.size()>0)
        update lstContract;
    }
        
    public static void afterChange(List<SBQQ__Subscription__c> sfSubscriptionList) {
        System.debug('sfSubscriptionList------'+sfSubscriptionList);
        Set<Id> accIds = new Set<Id>();
        Set<Id> accidsTodeleteEARS = new set<id>();
        for (SBQQ__Subscription__c sfSub : sfSubscriptionList) {
            if(sfsub.SBQQ__TerminatedDate__c == null ){ //|| sfsub.SBQQ__TerminatedDate__c > date.Today()){
                accIds.add(sfSub.SBQQ__Account__c);
            }   
            
        }
        for(SBQQ__Subscription__c sfSub : sfSubscriptionList){
            
                if(sfsub.SBQQ__TerminatedDate__c != null && sfsub.SBQQ__TerminatedDate__c <= date.Today()){//!accIds.contains(sfSub.SBQQ__Account__c)){
                    accidsTodeleteEARS.add(sfSub.SBQQ__Account__c);
                     
                }
            
        }
        // To delete EARs for cancelled subscriptions 
            if(accidsTodeleteEARS.size()>0){
            Database.DeleteResult[] DR_Dels = Database.delete([SELECT Id FROM Enterprise_Access_Right__c WHERE Account__c IN :accidsTodeleteEARS], false);
            //if(accids == null || accids.size()==0){
                accids.addAll(accidsTodeleteEARS);
            //}
            }//accidsTodeleteEARS size check
        // End to delete EARs
        if(accids.size()>0){
        List<SBQQ__Subscription__c> allSubscriptionWithAccount = [select id,IsMobileAutoConvert__c,App_Store__c,SuspendDate__c,ResumeDate__c,Subscription_Status__c,SBQQ__Account__c,SBQQ__StartDate__c,
        SBQQ__EndDate__c,SBQQ__Contract__r.ZSB__TermType__c,SBQQ__Product__r.id,SBQQ__Product__r.Name,SBQQ__Product__r.ismobile__c,SBQQ__Product__r.ProductCode,SBQQ__Product__r.AddOn_Type__c,SBQQ__Product__r.Feed__c , SBQQ__Product__c ,
        SBQQ__Product__r.Market__c,SBQQ__Product__r.IsTrial__c,SBQQ__Product__r.Is_Secondary_Market__c,ZSB__RatePlanChargeId__c,
        SBQQ__Product__r.Professional__c,SBQQ__Product__r.SBQQ__BillingFrequency__c,SBQQ__TerminatedDate__c from SBQQ__Subscription__c where 
        SBQQ__Account__c =: accIds AND
        (SBQQ__TerminatedDate__c = null or SBQQ__TerminatedDate__c > Today) AND // Terminated date should not be populated
        ((SBQQ__Contract__r.ZSB__TermType__c = 'Evergreen' AND SBQQ__StartDate__c <= TODAY) // if Contract is EverGreen check Start Date it should be less that today
        OR (SBQQ__Contract__r.ZSB__TermType__c != 'Evergreen' AND SBQQ__StartDate__c <= TODAY AND SBQQ__EndDate__c >= TODAY)) ORDER BY lastmodifieddate//if Contract is not EverGreen check Start Date and End Date ,Today should be in that Date range
        ];
        if(allSubscriptionWithAccount !=null && allSubscriptionWithAccount.size()>0){
            populateEnterpriseAccessRight(allSubscriptionWithAccount);

            updateMarketoFields(allSubscriptionWithAccount);
        }
        }//accids size check
    }
    
    /**
     * following method is called by trigger and batch to populate the access right object Enterprise_Access_Right__c
     * lookup to account
     * get all current active subscriptions
     * from subscription plan, vv_market, and trial to determine the following two items
     * 1. claimvalue
     * 2. mask
     */
    public static void populateEnterpriseAccessRight(List<SBQQ__Subscription__c> sfSubscriptionList) {
        Set<Id> accIds = new Set<Id>();
        Map<string,List<SBQQ__Subscription__c>> productSubscriptionMap = new Map<string,List<SBQQ__Subscription__c>>();
        
        for(SBQQ__Subscription__c sfSub : sfSubscriptionList)
        {
            system.debug('Account###'+sfSub.SBQQ__Account__c);
            accIds.add(sfSub.SBQQ__Account__c);
            //1. Subscription is synced with Zuora
            //2. Terminated date should not be populated
            //3. if Contract is EverGreen check Start Date it should be less that today
            //4. if Contract is not EverGreen check Start Date and End Date ,Today should be in that Date range
            //Only consider Subscription with above cases
            
            if((sfSub.SBQQ__TerminatedDate__c == null || sfSub.SBQQ__TerminatedDate__c > Date.Today()) && ((sfSub.SBQQ__Contract__r.ZSB__TermType__c == 'Evergreen' && sfSub.SBQQ__StartDate__c <= Date.TODAY())
            || (sfSub.SBQQ__Contract__r.ZSB__TermType__c != 'Evergreen' && sfSub.SBQQ__StartDate__c <= Date.TODAY() && sfSub.SBQQ__EndDate__c >= Date.TODAY())))
            {
                if(sfSub.SuspendDate__c != null && sfSub.ResumeDate__c != null &&!(sfSub.SuspendDate__c <= Date.TODAY() && sfSub.ResumeDate__c > Date.TODAY())){
                    if(!productSubscriptionMap.containsKey(sfSub.SBQQ__Product__c)){
                        productSubscriptionMap.put(sfSub.SBQQ__Product__c,new List<SBQQ__Subscription__c>());
                    }            
                    productSubscriptionMap.get(sfSub.SBQQ__Product__c).add(sfSub); 
                }
                else if(sfSub.SuspendDate__c != null && sfSub.ResumeDate__c == null && !(sfSub.SuspendDate__c <= Date.TODAY())){
                    if(!productSubscriptionMap.containsKey(sfSub.SBQQ__Product__c)){
                        productSubscriptionMap.put(sfSub.SBQQ__Product__c,new List<SBQQ__Subscription__c>());
                    }            
                    productSubscriptionMap.get(sfSub.SBQQ__Product__c).add(sfSub); 
                }
                else if(sfSub.SuspendDate__c == null && sfSub.ResumeDate__c == null){
                    if(!productSubscriptionMap.containsKey(sfSub.SBQQ__Product__c)){
                        productSubscriptionMap.put(sfSub.SBQQ__Product__c,new List<SBQQ__Subscription__c>());
                    }            
                    productSubscriptionMap.get(sfSub.SBQQ__Product__c).add(sfSub);
                }
            }
        }
        
        // delete first
        Database.DeleteResult[] DR_Dels = Database.delete([SELECT Id FROM Enterprise_Access_Right__c WHERE Account__c IN :accIds], false);
        system.debug('delete result: ' + DR_Dels);
        
        // get the Enterprise Access Mapping records
        List<Enterprise_Access_Mapping__c> eaMappinglist = [SELECT Mask__c, ClaimValue__c, Product__c, VV_Market__c, Is_Trial__c FROM Enterprise_Access_Mapping__c where Product__c IN: productSubscriptionMap.keySet() order by is_trial__c];
               
        Map<string,Enterprise_Access_Right__c> uniqueEARForAccounts = new Map<string,Enterprise_Access_Right__c>();
        for(Enterprise_Access_Mapping__c eamObj : eaMappinglist ){
            
            for(SBQQ__Subscription__c sub : productSubscriptionMap.get(eamObj.Product__c))
            {
                system.debug('Account$$$$'+Sub.SBQQ__Account__c);
                string uniqueEARForAccString = sub.SBQQ__Account__c + eamObj.Mask__c + eamObj.ClaimValue__c;
                if(!uniqueEARForAccounts.containsKey(uniqueEARForAccString))
                {
                    uniqueEARForAccounts.put(uniqueEARForAccString , new Enterprise_Access_Right__c(Subscription__c=sub.id,Account__c = sub.SBQQ__Account__c, Mask__c = eamObj.Mask__c, ClaimValue__c = eamObj.ClaimValue__c,Is_Trial__c=eamObj.Is_Trial__c,Subscription_End_Date__c = sub.SBQQ__EndDate__c));
                }
            }
        }
        
        if (!uniqueEARForAccounts.isEmpty()) {
            insert uniqueEARForAccounts.values();
        }
            
    }
    
    public static void updateMarketoFields(List<SBQQ__Subscription__c> sfSubscriptionList) {
    //adding
    map<id, list<SBQQ__Subscription__c>> mapActwithSubs = new map<id,list<SBQQ__Subscription__c>>();
    set<id> setAccountid = new set<id>();
        for (SBQQ__Subscription__c sfSubtemp : sfSubscriptionList) {
            setAccountid.add(sfSubtemp.SBQQ__Account__c);
            
        }
        for(SBQQ__Subscription__c sub : [SELECT SBQQ__Account__c,App_Store__c,SBQQ__Contract__r.IsMobile__c,SBQQ__StartDate__c,SBQQ__TerminatedDate__c FROM SBQQ__Subscription__c where SBQQ__Account__c in:setAccountid AND SBQQ__StartDate__c <= today AND (SBQQ__TerminatedDate__c = null OR SBQQ__TerminatedDate__c > today) AND App_Store__c != NULL]){
            if(mapActwithSubs.get(sub.SBQQ__Account__c) == null)
                mapActwithSubs.put(sub.SBQQ__Account__c,new list<SBQQ__Subscription__c>{sub});
                else
                mapActwithSubs.get(sub.SBQQ__Account__c).add(sub);
        }
        
    //adding
        System.debug('sfSubscriptionList-------'+sfSubscriptionList);
        Map<Id,Account> account_update_map = new Map<Id,Account>();
        
        for (SBQQ__Subscription__c sfSub : sfSubscriptionList) {
          // create new account to update or get it from the map
          Account a = new Account(Id = sfSub.SBQQ__Account__c, Has_RT__c = false, Has_RT_Trial__c = false, Weeks_Since_Last_Sub__c = 1000, Weeks_Since_Last_Trial__c = 1000);
          System.debug('a     updateMarketoFields-------'+a);
          if (account_update_map.get(sfSub.SBQQ__Account__c) != null){ 
                  a = account_update_map.get(sfSub.SBQQ__Account__c);
              }
          else{
                   a.Has_Subscription__c = false;
                  account_update_map.put(sfSub.SBQQ__Account__c, a);
              }
        System.debug('a after     updateMarketoFields-------'+a);
          
          if (sfSub.SBQQ__TerminatedDate__c <= Date.today() || (sfSub.SBQQ__Contract__r.ZSB__TermType__c != 'Evergreen' && sfsub.SBQQ__Enddate__c < date.today())) {
              // already finished charge plans, use had
              system.debug('hadsub###'+sfsub.id);
              updateHadFields(a, sfSub);
          }else // one time items
            if ((sfSub.SBQQ__Product__r.Feed__c == null || sfSub.SBQQ__Product__r.Feed__c == '' || sfSub.SBQQ__Product__r.IsTrial__c )) {
              system.debug('hassub###'+sfsub.id);
              updateHasFields(a, sfSub, true,mapActwithSubs.get(a.id));  
            } else if (sfSub.SBQQ__Product__r.Feed__c != null && !sfSub.SBQQ__Product__r.IsTrial__c) {
                system.debug('subbbbb####'+sfsub.id);
              a.Has_Subscription__c = true;
              a.Had_Subscription__c = false;
                    // update account market if it doesn't exist, set the market to the highest feed possible, always update to prevent issues with account
                        if(sfSub.SBQQ__Contract__r.ZSB__TermType__c == 'Evergreen'){
                            if((sfSub.SBQQ__Product__r.Feed__c == 'Express' && a.VV_Feed__c== null) || a.VV_Feed__c== 'Express' )
                            {
                                a.VV_Feed__c =  sfSub.SBQQ__Product__r.Feed__c;
                                a.VV_Market__c = sfSub.SBQQ__Product__r.Market__c;
                            }else{
                                if (a.VV_Market__c == null) a.VV_Market__c = sfSub.SBQQ__Product__r.Market__c;
                                else if (a.VV_Market__c != null && a.VV_Feed__c != null && a.VV_Feed__c == 'EOD' && (sfSub.SBQQ__Product__r.Feed__c == 'IntraDay' || sfSub.SBQQ__Product__r.Feed__c == 'RealTime')) {
                                    a.VV_Market__c = sfSub.SBQQ__Product__r.Market__c;
                                } else if (a.VV_Market__c != null && a.VV_Feed__c != null && a.VV_Feed__c == 'IntraDay' && sfSub.SBQQ__Product__r.Feed__c == 'RealTime') {
                                    a.VV_Market__c = sfSub.SBQQ__Product__r.Market__c;
                                }
    
                                if (a.VV_Feed__c == null) a.VV_Feed__c = sfSub.SBQQ__Product__r.Feed__c == 'EOD' ? 'EOD' : (sfSub.SBQQ__Product__r.Feed__c == 'IntraDay' ? 'IntraDay' : (sfSub.SBQQ__Product__r.Feed__c == 'RealTime' ? 'RealTime' : null));
                                else if (a.VV_Feed__c != null && a.VV_Feed__c == 'EOD' && (sfSub.SBQQ__Product__r.Feed__c == 'IntraDay' || sfSub.SBQQ__Product__r.Feed__c == 'RealTime')) {
                                    a.VV_Feed__c = sfSub.SBQQ__Product__r.Feed__c == 'IntraDay' ? 'IntraDay' : 'RealTime';
                                } else if (a.VV_Feed__c != null && a.VV_Feed__c == 'IntraDay' && sfSub.SBQQ__Product__r.Feed__c == 'RealTime') {
                                    a.VV_Feed__c = 'RealTime';
                                }
                            }
                        }
                    // recurring items that are current
                    system.debug('hassub###'+sfsub.id);
              updateHasFields(a, sfSub, true,mapActwithSubs.get(a.id));
            }
        
        
        
        }
         if (!account_update_map.isEmpty()) {
          List<Account> accounts_to_update = new List<Account>(account_update_map.values());
          system.debug('##update: ' + accounts_to_update);
          update accounts_to_update;
        }
    
    }
    
    static boolean checkProductOptions(string prodOption,SBQQ__Subscription__c sfSub){
            boolean isPresent = false;
            if(sfSub!=null && prodOption!=null && ((sfSub.SBQQ__Product__r.productCode != null &&sfSub.SBQQ__Product__r.productCode.containsIgnoreCase(prodOption) )|| 
            ( sfSub.SBQQ__Product__r.AddOn_Type__c != null && 
            sfSub.SBQQ__Product__r.AddOn_Type__c.equalsIgnoreCase(prodOption))))
            {
                isPresent = true;   
            }
        return isPresent;
    }
    
    static boolean checkProduct(string prodType,SBQQ__Subscription__c sfSub){
            boolean isPresent = false;
            if(sfSub!=null && prodType!=null && sfSub.SBQQ__Product__c != null && ((sfSub.SBQQ__Product__r.productCode != null && sfSub.SBQQ__Product__r.productCode.containsIgnoreCase(prodType)) || 
            (sfSub.SBQQ__Product__r.Feed__c!=null && sfSub.SBQQ__Product__r.Feed__c.equalsIgnoreCase(prodType))))
            {
                isPresent = true;   
            }
        return isPresent;
    }
    
    public static void updateHasFields(Account a, SBQQ__Subscription__c sfSubscription, Boolean bool,list<SBQQ__Subscription__c> sublist) {
      
        System.debug('a--------'+a);
        System.debug('sfSubscription--------'+sfSubscription);
        System.debug('bool--------'+bool);
        //commented
        /*List<SBQQ__Subscription__c> subList = [SELECT SBQQ__Account__c,App_Store__c,SBQQ__Contract__r.IsMobile__c,SBQQ__StartDate__c,SBQQ__TerminatedDate__c FROM SBQQ__Subscription__c where SBQQ__Account__c =:a.Id AND SBQQ__StartDate__c <= today AND (SBQQ__TerminatedDate__c = null OR SBQQ__TerminatedDate__c > today) AND App_Store__c != NULL];*/
        //commented end
        System.debug('subList--------'+subList);
        a.Had_Subscription__c = false;
        
        if(sfSubscription.IsMobileAutoConvert__c){
            a.IsMobileAutoConvert__c =true;
        }
        //populate App store field from Subscription to Account
        if(sfSubscription.App_Store__c != null){
            a.App_Store__c = sfSubscription.App_Store__c;
        }else{
            System.debug('else ----------');

            if(subList == null || subList.size()==0){
                a.App_Store__c = sfSubscription.App_Store__c;
            }
               
                        System.debug('a ----------'+a);
        }
      
        if (checkProductOptions('AutoTimer',sfSubscription)) a.Has_AutoTester__c = bool;
        else if (checkProductOptions('OptionAnalyzer',sfSubscription) || checkProductOptions('Option Analyzer',sfSubscription)) a.Has_Options_Analyzer__c = bool;
        else if (checkProductOptions('Simulator',sfSubscription)) a.Has_Simulator__c = bool; // Not being used
        else if (checkProductOptions('Variator',sfSubscription)) a.Has_Variator__c = bool; // Not being used
        else if (checkProductOptions('Pro Trader',sfSubscription) || checkProductOptions('ProTrader',sfSubscription)) a.Has_ProTrader__c = bool;
        else if (checkProductOptions('Derby',sfSubscription)) a.Has_Derby__c = bool;
        else if (checkProductOptions('OptionsPro',sfSubscription)) a.Has_OptionsPro__c= bool;
        
        if (checkProduct('RealTime',sfSubscription)) {
          a.Has_RT__c = bool;
          if (a.Has_RT__c) a.Had_RT__c = false;
        }
        if(!sfSubscription.SBQQ__Product__r.IsTrial__c && sfSubscription.SBQQ__Product__r.SBQQ__BillingFrequency__c=='Monthly'){
            a.isMonthlySubscription__c =true;
        }
        if (checkProduct('RealTime',sfSubscription) && sfSubscription.SBQQ__Product__r.IsTrial__c) {
          a.Has_RT_Trial__c = bool;
          if(sfSubscription.SBQQ__EndDate__c != null)
          a.Days_To_RT_Trial_End__c = Date.today().daysBetween(sfSubscription.SBQQ__EndDate__c);
          a.RT_Trial_Day_Num__c = sfSubscription.SBQQ__StartDate__c.daysBetween(Date.today());
          a.RT_Trial_Start__c = sfSubscription.SBQQ__StartDate__c;
          a.RT_Trial_End__c = sfSubscription.SBQQ__EndDate__c;
          if (a.Has_RT_Trial__c) {
            a.Weeks_Since_Last_RT_Trial__c = 0;
            a.Had_RT_Trial__c = false;
          }
        }
        
        if (sfSubscription.SBQQ__Product__r.IsTrial__c) {
            if(sfSubscription.SBQQ__Product__r.isMobile__c){
                a.VV_MobileTrialAccessStart__c =sfSubscription.SBQQ__StartDate__c;
                a.VV_MobileTrialAccessEnd__c =sfSubscription.SBQQ__EndDate__c;
                a.VV_MobileTrialProduct__c = sfSubscription.SBQQ__Product__r.id;
            }
          a.Has_Trial__c = bool;
          a.Trial_Day_Num__c = sfSubscription.SBQQ__StartDate__c.daysBetween(Date.today());
          a.Trial_Start__c = sfSubscription.SBQQ__StartDate__c;
          a.Trial_End__c = sfSubscription.SBQQ__EndDate__c;
          if (a.Has_Trial__c) {
            a.Weeks_Since_Last_Trial__c = 0;
            a.Had_Trial__c = false;
          }
           if(sfSubscription.SBQQ__EndDate__c != null)
          a.Days_To_Trial_End__c = Date.today().daysBetween(sfSubscription.SBQQ__EndDate__c);   
        }
        //Check for subscribed Main products
        if(sfSubscription.SBQQ__Product__r.ismobile__c && !sfSubscription.SBQQ__Product__r.IsTrial__c){
            //VV_MobileSubscriptionAccessEnd__c
            //VV_MobileSubscriptionAccessStart__c = 
            a.VV_MobileSubscriptionAccessStart__c = a.VV_MobileSubscriptionAccessStart__c == null ? sfSubscription.SBQQ__StartDate__c : (a.VV_MobileSubscriptionAccessStart__c < sfSubscription.SBQQ__StartDate__c ? sfSubscription.SBQQ__StartDate__c : a.VV_MobileSubscriptionAccessStart__c );
            a.VV_MobileSubscriptionProducts__c = sfSubscription.SBQQ__Product__r.Name;
            a.VV_MobileSubscriptionAccessEnd__c = null;
        }
        if (sfSubscription.SBQQ__Product__r.Feed__c != null && !sfSubscription.SBQQ__Product__r.IsTrial__c) {
            // last subscription start date, if it is null, use this plan's start, if it is not and before this start, use this one
            a.Last_Sub_Start__c = a.Last_Sub_Start__c == null ? sfSubscription.SBQQ__StartDate__c : (a.Last_Sub_Start__c < sfSubscription.SBQQ__StartDate__c ? sfSubscription.SBQQ__StartDate__c : a.Last_Sub_Start__c);
            
            // has effective subscription, clear out some fields
            a.Weeks_Since_Last_Sub__c = 0;
            a.Last_Sub_End__c = null;
        }

        // count all the trials
        if (sfSubscription.SBQQ__Product__r.IsTrial__c) {
            a.Past_Year_Trial_Count__c = a.Past_Year_Trial_Count__c == null ? a.Past_Year_Trial_Count__c = 1 : (a.Past_Year_Trial_Count__c + 1);
        }
    }
    
    public static void updateHadFields(Account a, SBQQ__Subscription__c sfSubscription) {
        Date enddate = sfSubscription.SBQQ__TerminatedDate__c ;//!= null ? sfSubscription.Zuora__EffectiveEndDate__c : zs.Zuora__TermEndDate__c;
        if(enddate == null){
            enddate = sfSubscription.SBQQ__Enddate__c;
        }
        //if (sfSubscription.SBQQ__TerminatedDate__c == null) return;

      //String productname = sfSubscription.Zuora__Description__c == null ? '' : sfSubscription.Zuora__Description__c;

      if (!a.Has_Subscription__c) a.Had_Subscription__c = true;
      if(!sfSubscription.SBQQ__Product__r.IsTrial__c && sfSubscription.SBQQ__Product__r.SBQQ__BillingFrequency__c=='Monthly'){
            a.isMonthlySubscription__c =false;
        }

      if (checkProduct('RealTime',sfSubscription)) {
      a.Had_RT__c = true;
      }
      if (checkProductOptions('Derby',sfSubscription)) a.Has_Derby__c = false;
      if (checkProductOptions('OptionsPro',sfSubscription)) a.Has_OptionsPro__c = false;
      if(sfSubscription.SBQQ__Product__r.IsTrial__c){
          system.debug('hadddd'+sfSubscription.id);
          a.had_trial__c=true;
          a.has_trial__c=false;
      }
      
        if (checkProduct('RealTime',sfSubscription) && sfSubscription.SBQQ__Product__r.IsTrial__c) {
        a.Had_RT_Trial__c = true;
        if (!a.Has_RT_Trial__c && enddate != null) a.Weeks_Since_Last_RT_Trial__c = Math.roundToLong(enddate.daysBetween(Date.today()) / 7);
      }
      
        //Check for subscribed Main products
        if (sfSubscription.SBQQ__Product__r.Feed__c != null && !sfSubscription.SBQQ__Product__r.IsTrial__c) {
        if (a.Weeks_Since_Last_Sub__c != 0 && enddate != null) { // only calculate this if subscription is not current
          Long weeks = Math.roundToLong(enddate.daysBetween(Date.today()) / 7);
          // only assign if this is more current because we could be upgrading or downgrading multiple times, we only want the most current last sub
          if (weeks < a.Weeks_Since_Last_Sub__c) a.Weeks_Since_Last_Sub__c = weeks;  
        }
      }
      
        if (sfSubscription.SBQQ__Product__r.IsTrial__c && !a.Has_Trial__c) {
        if (a.Weeks_Since_Last_Trial__c != 0 && enddate != null) {
          Long weeks = Math.roundToLong(enddate.daysBetween(Date.today()) / 7);
                
          if (weeks < a.Weeks_Since_Last_Trial__c) a.Weeks_Since_Last_Trial__c = Math.roundToLong(enddate.daysBetween(Date.today()) / 7);
        }
      }
        if (sfSubscription.SBQQ__Product__r.ismobile__c && !sfSubscription.SBQQ__Product__r.IsTrial__c ) {
            // last subscription end date, if it is null, then use the effective end date, if it is not, and it is before this sub's effective end date, use this one
            a.VV_MobileSubscriptionAccessEnd__c= a.VV_MobileSubscriptionAccessEnd__c== null ? enddate : (a.VV_MobileSubscriptionAccessEnd__c < enddate ? enddate : a.VV_MobileSubscriptionAccessEnd__c);
        }
        // no more primary subscription
        if (sfSubscription.SBQQ__Product__r.Feed__c != null && !sfSubscription.SBQQ__Product__r.IsTrial__c && a.Weeks_Since_Last_Sub__c != 0) {
            // last subscription end date, if it is null, then use the effective end date, if it is not, and it is before this sub's effective end date, use this one
            a.Last_Sub_End__c = a.Last_Sub_End__c == null ? enddate : (a.Last_Sub_End__c < enddate ? enddate : a.Last_Sub_End__c);
        }

        // count all the trials
        if (sfSubscription.SBQQ__Product__r.IsTrial__c && sfSubscription.SBQQ__StartDate__c.daysBetween(Date.today()) < 365) {
            a.Past_Year_Trial_Count__c = a.Past_Year_Trial_Count__c == null ? a.Past_Year_Trial_Count__c = 1 : (a.Past_Year_Trial_Count__c + 1);
        }

    }   
 }