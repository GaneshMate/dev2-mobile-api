public Class ShipmentTriggerHandler
{
    
    public void afterInsert(List<zkfedex__Shipment__c> lstShipment)
    {
        Map<Id, Id> mapAccountToShipment = new Map<Id, Id>();
        
        for(zkfedex__Shipment__c objShipment : lstShipment)
        {
            mapAccountToShipment.put(objShipment.Account__c, objShipment.id);
        }
    
        List<Account_Product__c> lstProduct = [Select id, Account__c from Account_Product__c where Status__c = 'Purchased' AND Account__c in : mapAccountToShipment.keySet()];
        for(Account_Product__c objProduct : lstProduct){
            objProduct.FedEx_Shipment__c = mapAccountToShipment.get(objProduct.Account__c);
            objProduct.Status__c = 'Shipped';
        }
        if(!lstProduct.isEmpty())
        {
         update lstProduct;
        }
    }
}