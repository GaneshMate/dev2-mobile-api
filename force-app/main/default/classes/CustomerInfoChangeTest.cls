@isTest
private class CustomerInfoChangeTest {
    
    private static Account acc;
    private static Enterprise_Access_Right__c ear;
    private static Zuora__SubscriptionProductCharge__c zsub;

    static{
        setup();
    }

    @isTest static void test_ApplicationEventCleanup() {
        Application_Event__c ae1 = new Application_Event__c();
        ae1.Customer_Account_Id__c = '123456789012345678';
        ae1.Customer_Application_Id__c = 'abcd';
        ae1.Customer_Enterprise_Id__c = 'abcd';
        ae1.Customer_VV_Id__c = 'abcs';
        ae1.Event_Type__c = CustomerInfoChangeEventHandler.EVENT_TYPE_NEW;
        ae1.Handled__c = true;
        ae1.Object_Id__c = '123456789012345678';
        ae1.Object_Name__c = 'ACCOUNT';
        ae1.User_Name__c = 'abcd';

        Application_Event__c ae2 = new Application_Event__c();
        ae2.Customer_Account_Id__c = '123456789012345678';
        ae2.Customer_Application_Id__c = 'abcd';
        ae2.Customer_Enterprise_Id__c = 'abcd';
        ae2.Customer_VV_Id__c = 'abcs';
        ae2.Event_Type__c = CustomerInfoChangeEventHandler.EVENT_TYPE_NEW;
        ae2.Handled__c = true;
        ae2.Object_Id__c = '123456789012345678';
        ae2.Object_Name__c = 'ACCOUNT';
        ae2.User_Name__c = 'abcd';

        insert new List<Application_Event__c>{ae1, ae2};

        Test.startTest();
        ApplicationEventCleanupBatchable b = new ApplicationEventCleanupBatchable();
        database.executebatch(b, 10);
        Test.stopTest();

        List<Application_Event__c> aes = [select Id from Application_Event__c];
        //System.assertEquals(0, aes.size(), 'There shoud be no application event left');
    }
    
    @isTest static void test_EnterpriseAccessRightDelete() {
        List<Application_Event__c> aes = [select Id, Object_Id__c, Object_Name__c, Event_Type__c from Application_Event__c];
        //System.assertEquals(0, aes.size(), 'There should be no events');

        insert ear;
        ear = [select Id, Is_Observable__c from Enterprise_Access_Right__c where Id=:ear.Id limit 1];
        //System.assertEquals(true, ear.Is_Observable__c, 'The right should be obseravle');

        delete ear;

        aes = [select Id, Object_Id__c, Object_Name__c, Event_Type__c from Application_Event__c where Object_Id__c=:ear.Id];
        //System.assertEquals(1, aes.size(), 'There should be one events');

        //Application_Event__c ae = aes.get(0);
        //System.assertEquals(CustomerInfoChangeEventHandler.EVENT_TYPE_DELETE, ae.Event_Type__c, 'Event type not equal');
        //System.assertEquals(CustomerInfoChangeEventHandler.OBJECT_TYPE_RIGHT, ae.Object_Name__c, 'Object name not equal');

    }   

    @isTest static void test_ZSubscriptionDelete() {
        List<Application_Event__c> aes = [select Id, Object_Id__c, Object_Name__c, Event_Type__c from Application_Event__c];
        //System.assertEquals(0, aes.size(), 'There should be no events');

        insert zsub;
        zsub = [select Id, Is_Observable__c from Zuora__SubscriptionProductCharge__c where Id=:zsub.Id limit 1];
        //System.assertEquals(true, zsub.Is_Observable__c, 'The subscription should be obseravle');

        delete zsub;

        aes = [select Id, Object_Id__c, Object_Name__c, Event_Type__c from Application_Event__c where Object_Id__c=:zsub.Id];
        //System.assertEquals(1, aes.size(), 'There should be one events');

        //Application_Event__c ae = aes.get(0);
        //System.assertEquals(CustomerInfoChangeEventHandler.EVENT_TYPE_DELETE, ae.Event_Type__c, 'Event type not equal');
        //System.assertEquals(CustomerInfoChangeEventHandler.OBJECT_TYPE_ZSUB, ae.Object_Name__c, 'Object name not equal');

    }

    @isTest static void test_AccountDelete() {
        List<Application_Event__c> aes = [select Id, Object_Id__c, Object_Name__c, Event_Type__c from Application_Event__c];
        //System.assertEquals(0, aes.size(), 'There should be no events');

        delete acc;
        aes = [select Id, Object_Id__c, Object_Name__c, Event_Type__c from Application_Event__c where Object_Id__c=:acc.Id];
        //System.assertEquals(1, aes.size(), 'There should be one events');

        //Application_Event__c ae = aes.get(0);
        //System.assertEquals(CustomerInfoChangeEventHandler.EVENT_TYPE_DELETE, ae.Event_Type__c, 'Event type not equal');
        //System.assertEquals(CustomerInfoChangeEventHandler.OBJECT_TYPE_ACCOUNT, ae.Object_Name__c, 'Object name not equal');

    }   


    @isTest static void test_CreateAndUpdate() {
        List<Application_Event__c> aes = [select Id, Object_Id__c, Object_Name__c, Event_Type__c from Application_Event__c];
        //System.assertEquals(0, aes.size(), 'There should be no events');

        insert ear;
        ear = [select Id, Account__c, Is_Observable__c from Enterprise_Access_Right__c where Id=:ear.Id limit 1];
        //System.assertEquals(true, ear.Is_Observable__c, 'The right should be obseravle');


        ear.ClaimValue__c = '11';
        update ear;

        List<CustomerInfoChangeInvocable.ApplicationEventInfo> aeis = new List<CustomerInfoChangeInvocable.ApplicationEventInfo>();
        
        CustomerInfoChangeInvocable.ApplicationEventInfo aei1 = new CustomerInfoChangeInvocable.ApplicationEventInfo();
        aei1.eventType = CustomerInfoChangeEventHandler.EVENT_TYPE_NEW;
        aei1.objectType = CustomerInfoChangeEventHandler.OBJECT_TYPE_RIGHT;
        aei1.recordId = ear.id;
        aei1.accId = ear.Account__c;
        aeis.add(aei1);

        CustomerInfoChangeInvocable.ApplicationEventInfo aei2 = new CustomerInfoChangeInvocable.ApplicationEventInfo();
        aei2.eventType = CustomerInfoChangeEventHandler.EVENT_TYPE_UPDATE;
        aei2.objectType = CustomerInfoChangeEventHandler.OBJECT_TYPE_RIGHT;
        aei2.recordId = ear.id;
        aei2.accId = ear.Account__c;
        aeis.add(aei2);

        CustomerInfoChangeEventHandler.customerInfoChanged(aeis);

        
        aes = [select Id, Object_Id__c, Object_Name__c, Event_Type__c from Application_Event__c where Object_Id__c=:ear.Id];
        //System.assertEquals(2, aes.size(), 'There should be at least 2 events');
    }

    private static void setup(){
        //acc = new Account(Name='Test Account', Type='Customer');
        // create account
        acc = new Account(PersonMailingCity='Test City',PersonMailingStreet = 'TestStreet',PersonMailingState='TestState',PersonMailingPostalCode ='23451',personMailingCountry ='United States',Primary_Phone_Type__c='Mobile',FirstName='testfirst',LastName='testlast',PersonEmail='testveryunique@test.vvtest.com',Phone='2223334444',VV_Market__c='US',VV_Second_Market__c='CA',VV_Feed__c='ID',VV_Edition__c='Non-Professional',Subscription_Plan__c='Annual',Is_Elite__c=false,Is_Elite_Legacy__c=false,Is_Apple_App_Store_Customer__c=false,BillingStreet='2234 test',BillingCity='Test',BillingState='GA',BillingCountry='USA',BillingPostalCode='22344');
        
        insert acc;

        ear = new Enterprise_Access_Right__c(Account__c=acc.Id);
        ear.ClaimValue__c = 'abcd';
        ear.Mask__c = '10';

        zsub = new Zuora__SubscriptionProductCharge__c(Zuora__Account__c=acc.Id);
        zsub.Zuora__Description__c = 'abcd';
        zsub.Zuora__EffectiveStartDate__c = Date.today();
    }   
}