global class MagalogFinderEmailHandler implements Messaging.InboundEmailHandler {

	global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email,
	        Messaging.InboundEnvelope env) {

		Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
		try {
			
			String myPlainText = '';
			myPlainText = email.plainTextBody;
 			
            Messaging.InboundEmail.textAttachment[] tAttachments = email.textAttachments;
            String csvbody='';
			// only works with one zip file and inside of zipfile, one csv
			if (tAttachments != null) {
             	for (Messaging.InboundEmail.textAttachment ttt : tAttachments) {
					csvbody = ttt.Body;
					break;
				}
			}
            System.debug('csvbody---------'+csvbody);
			if (csvbody != null) {
				List<String> lines = safeSplit(csvbody,(csvbody.indexOf('\r\n') > 0 ? '\r\n' : '\n'));
              	Database.executeBatch(new MagalogFinderCSVParserBatch(lines));
			}
		} catch (Exception e) {
			system.debug('error with mail handler: ' + e.getMessage() + e.getStackTraceString());
			result.success = false;
			return result;
		}
		result.success = true;
		return result;
	}

	public static List<String> safeSplit(String inStr, String delim) {
		Integer regexFindLimit = 100;
		Integer regexFindCount = 0;

		List<String> output = new List<String>();

		Matcher m = Pattern.compile(delim).matcher(inStr);

		Integer lastEnd = 0;

		while (!m.hitEnd()) {
			while (regexFindCount < regexFindLimit && !m.hitEnd()) {
				if (m.find()) {
					output.add(inStr.substring(lastEnd, m.start()));
					lastEnd = m.end();
				} else {
					output.add(inStr.substring(lastEnd));
					lastEnd = inStr.length();
				}

				regexFindCount++;
			}

			// Note: Using region() to advance instead of substring() saves
			// drastically on heap size. Nonetheless, we still must reset the
			// (unmodified) input sequence to avoid a 'Regex too complicated'
			// error.
			m.reset(inStr);
			m.region(lastEnd, m.regionEnd());

			regexFindCount = 0;
		}

		return output;
	}


}