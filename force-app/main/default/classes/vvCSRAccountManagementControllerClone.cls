/**
 * creating account and subscription from the CSR console
 */
public with sharing class vvCSRAccountManagementControllerClone {
    public Account a {get; set;}
    String zuora_aid;   // zuora account id
    String zuora_contactid; // zuora contact id
    public Zuora__CustomerAccount__c zca {get; private set;}    // zuora billing account in salesforce

    public User u {get; private set;}   // portal user for the account, might not be there

    public String salutation {get; set;}
    public String firstname {get; set;}
    public String lastname {get; set;}
    public String confirmEmail {get; set;}
    public boolean PaymentMsg{get;set;}
    public Boolean sameAsBilling {get;set;}

    public String selectedMarket {get; set;}    // primary market
    public String selectedMarket2 {get; set;}   // secondary market
    public String selectedProduct {get; set;}
    public String selectedEdition {get; set;}   // Non-Professional, Professional

    // if we jumped straight to here, payment is 3, otherwise 4 or 5 depend on the professional, nonprofessional
    public String subscriptionType {get; set;}  // monthly or annual
    public String paymentType {get; set;}       // card or ach
    public Double monthly {get; private set;}   // monthly fee for the chosen product
    public Double annual {get; private set;}    // annual fee for the chosen product
    public Double savings {get; private set;}   // savings for annual fee
    public Double trialamount {get; private set;}   // amount to charge for trial period
    public Integer trialdays {get; private set;}    // number of days for the trial

    // payment method
    public Payment_Method_SF__c payment_method {get; set;}

    // zuora signature key
    Map<String, Object> param_map;  // map of zuora signature items
    public String signature_key {get; private set;}
    
    // coupon
    public String coupon_code {get; set;}   // actual coupon code entered by customer
    public Coupon_Matrix__c effective_coupon {get; private set;}    // coupon in effect after validating customer's coupon code
    
    // referral
    public String referral_code {get; set;} // referral

    // selected tab for the account management page
    public String selTab {get; set;}

    public Boolean showtab1 {get; private set;}
    public Boolean showtab2 {get; private set;}
    public Boolean showtab3 {get; private set;}
    public Boolean showtab4 {get; private set;}
    Public Boolean isBillingAccountExists {get;set;}

    public vvCSRAccountManagementControllerClone() {
        selTab = 'section1';

        // already has account
        String aid = ApexPages.currentPage().getParameters().get('id');
        isBillingAccountExists = false;
        if (aid != null) {
            List<Account> acclist = [SELECT FirstName,CurrencyIsoCode,Vector_Vest_Customer_Id__c , LastName,SBQQ__TaxExempt__c,CertificateID__c,Region1__c,Shipping_FirstName__c, Shipping_LastName__c, Salutation, Phone, PersonEmail, 
                BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode,
                ShippingStreet, ShippingCity, ShippingState, ShippingCountry, ShippingPostalCode,
                VV_Market__c, VV_Second_Market__c, VV_Edition__c, VV_Feed__c, Subscription_Plan__c,IsPersonAccount, Is_Elite__c, Is_Elite_Legacy__c, Account_Currency__c, Account_Currency_Symbol__c,
                (select id from R00N40000001kyLcEAI__r)
                FROM Account WHERE Id = :aid];
            if (!acclist.isEmpty()) a = acclist[0];
            if(acclist[0].R00N40000001kyLcEAI__r != null && acclist[0].R00N40000001kyLcEAI__r.size() > 0){
                isBillingAccountExists = true ;
            }
            firstname = a.FirstName;
            lastname = a.LastName;
            salutation = a.Salutation;
            confirmEmail = a.PersonEmail;
            sameAsBilling = false;
            selectedMarket = a.VV_Market__c;
            selectedEdition = a.VV_Edition__c;
            selectedProduct = a.VV_Feed__c;
            subscriptionType = a.Subscription_Plan__c;
            paymentType = 'Credit Card';

            showtab1 = true;

            // check to see if we already have a billing account, if yes, that means this is a true update

            List<Zuora__CustomerAccount__c> zcalist = [SELECT 
                    Zuora__BillToName__c,
                    Zuora__CreditCard_Number__c,
                    Zuora__CreditCard_Expiration__c,
                    Zuora__CreditCardType__c,
                    Zuora__BillToCity__c,
                    Zuora__BillToCountry__c,
                    Zuora__BillToAddress1__c,
                    Zuora__BillToAddress2__c,
                    Zuora__BillToState__c,
                    Zuora__BillToPostalCode__c,
                    Zuora__BillToWorkEmail__c,
                    Zuora__BillToWorkPhone__c,
                    Zuora__BillToId__c,
                    Zuora__Zuora_Id__c,Zuora__DefaultPaymentMethod__c

                FROM Zuora__CustomerAccount__c 
                WHERE 
                    Zuora__Account__c = :a.Id
                    AND Zuora__Status__c = 'Active' and Zuora__DefaultPaymentMethod__c='CreditCard' order by createddate desc];
                 if(zcalist== null || zcalist.isEmpty()){
                      zcalist = [SELECT 
                    Zuora__BillToName__c,
                    Zuora__CreditCard_Number__c,
                    Zuora__CreditCard_Expiration__c,
                    Zuora__CreditCardType__c,
                    Zuora__BillToCity__c,
                    Zuora__BillToCountry__c,
                    Zuora__BillToAddress1__c,
                    Zuora__BillToAddress2__c,
                    Zuora__BillToState__c,
                    Zuora__BillToPostalCode__c,
                    Zuora__BillToWorkEmail__c,
                    Zuora__BillToWorkPhone__c,
                    Zuora__BillToId__c,
                    Zuora__Zuora_Id__c,Zuora__DefaultPaymentMethod__c

                FROM Zuora__CustomerAccount__c 
                WHERE 
                    Zuora__Account__c = :a.Id
                    AND Zuora__Status__c = 'Active'  and (Zuora__DefaultPaymentMethod__c = '' or Zuora__DefaultPaymentMethod__c = null )order by createddate desc];
                 }
                 if(zcalist== null || zcalist.isEmpty()){
                      zcalist = [SELECT 
                    Zuora__BillToName__c,
                    Zuora__CreditCard_Number__c,
                    Zuora__CreditCard_Expiration__c,
                    Zuora__CreditCardType__c,
                    Zuora__BillToCity__c,
                    Zuora__BillToCountry__c,
                    Zuora__BillToAddress1__c,
                    Zuora__BillToAddress2__c,
                    Zuora__BillToState__c,
                    Zuora__BillToPostalCode__c,
                    Zuora__BillToWorkEmail__c,
                    Zuora__BillToWorkPhone__c,
                    Zuora__BillToId__c,
                    Zuora__Zuora_Id__c,Zuora__DefaultPaymentMethod__c

                FROM Zuora__CustomerAccount__c 
                WHERE 
                    Zuora__Account__c = :a.Id
                    AND Zuora__Status__c = 'Active' order by createddate desc];
                 }
            if (!zcalist.isEmpty()) {
                zca = zcalist[0];
                zuora_aid = zca.Zuora__Zuora_Id__c;
            }

            List<User> users = [SELECT AccountId,Email FROM User WHERE AccountId = :a.Id AND IsActive = true];
            if (!users.isEmpty()) u = users[0];

        }

        // init account when we get here
        if (a == null) {
            a = new Account(Subscription_Signup_Date__c=Date.today(), RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId(), Account_Currency__c = 'USD');
            a.Primary_Phone_Type__c = 'Home';
            a.Type = 'Customer';
            sameAsBilling = true; 
            selectedMarket = null;
            selectedEdition = null;
            selectedProduct = null;
            subscriptionType = 'Annual';
            paymentType = 'Credit Card';

            
        }

        // came back from payment page with error
        if (ApexPages.currentPage().getParameters().get('error') != null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ApexPages.currentPage().getParameters().get('error')));
        }

        // init payment method
        payment_method = new Payment_Method_SF__c(Default_Payment_Method__c = true);
        Integer Month = Date.Today().Month();
        if(Month < 10){
            payment_method.Credit_Card_Expiration_Month__c = '0'+String.ValueOf(Month);
        }else{
            payment_method.Credit_Card_Expiration_Month__c = String.ValueOf(Month);
        }
        if (a.Id != null && payment_method.Credit_Card_Holder_Name__c == null) payment_method.Credit_Card_Holder_Name__c = a.FirstName + ' ' + a.LastName;
        if (a.Id != null && payment_method.ACH_Account_Name__c == null) payment_method.ACH_Account_Name__c = a.FirstName + ' ' + a.Lastname;
        payment_method.Payment_Type__c = paymentType;

        param_map = vvPaymentManagement.initPaymentInfo1(payment_method.Payment_Type__c, false,a.CurrencyISOCode);  // populate signature and page id
        if (param_map == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid initPaymentInfo'));
            return;
        }
        signature_key = (String)param_map.get('key');


        determineFees();
    }

    public void determineFees() {
        // get the rate plan to use
        Map<String,String> params = new Map<String,String>{'edition'=>selectedEdition,'market'=>selectedMarket,'feed'=>selectedProduct,'plantype'=>null,'currency'=>a.Account_Currency__c};
        Map<String,zqu__ProductRatePlan__c> resultproductmap = vvProductManagement.getPlanToUse(params);

        // get back a map of trialmonthly, trialannual, mainmonthly, mainannual
        if (resultproductmap.get('mainmonthly') != null) monthly = resultproductmap.get('mainmonthly').zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c;
        if (resultproductmap.get('mainannual') != null) annual = resultproductmap.get('mainannual').zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c;
        if (monthly != null && annual != null) savings = monthly * 12 - annual;
        
        // professional RT doesn't have trial
        if (selectedEdition == 'Professional' && selectedProduct == 'RT') {
            trialdays = 0;
            trialamount = 0;
        } else {
            if (resultproductmap.get('mainmonthly') != null && resultproductmap.get('mainmonthly').VV_Trial__c != null) {
                Product_Rate_Plan_Trial_Map__c trial = vvProductManagement.findTrialWithTrialName(resultproductmap.get('mainmonthly').VV_Trial__c, a.Account_Currency__c);

                trialamount = trial.Amount__c;
                trialdays = vvProductManagement.calcTrialDays(trial);
            }
        }     
        
    }



    /**
     * called by visual force page
     */
     
    Public String isCalledFrom {get;set;}
    public void tempSave() {
        system.debug('temp==='+tempSaveBool());
        if(tempSaveBool()){
            isCalledFrom = 'section1';
            saveAndSubmit();
            isCalledFrom = '';
        }
        selTab = 'section1';
    }

    public Boolean checkAccountFields() {
        String Errormsg='Please Enter values for';
        string fields='';
        /*if(salutation== null){
            fields+= 'Salutation,';
        }*/
        if(firstname == null || firstname=='')
            fields+= 'firstname,';
        if(lastname == null ||lastname == '')
            fields+= 'lastname,';
        if(a.PersonEmail==null|| a.PersonEmail=='')
            fields+= 'PersonEmail,';
        if(confirmEmail==null || confirmEmail=='')
            fields+= 'confirmEmail,';
        if(a.Phone == null )
            fields+= 'Phone,';
        if(a.BillingStreet == null ||a.billingStreet=='')
            fields+= 'BillingStreet,';
        if(a.BillingPostalCode == null||a.BillingPostalCode =='')
            fields+= 'BillingPostalCode,';
        if(a.BillingCountry == '-1' || a.BillingCountry==''|| a.BillingCountry == null)
            fields+= 'BillingCountry,';
        if(a.BillingCity == null||a.BillingCity=='')
            fields+= 'BillingCity,';
        if(a.BillingState == null || a.BillingState =='')
            fields+= 'BillingState,';       
        
        if(fields != ''){
            fields = fields.substring(0,fields.length()-1);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Errormsg+' '+fields ));
            selTab = 'section1';
            return false;
        }
        /*if (salutation == null || firstname == null ||firstname=='' || lastname == null ||lastname == '' || a.PersonEmail==null|| a.PersonEmail==''|| confirmEmail==null || confirmEmail=='' || a.PersonEmail == null || a.Phone == null 
            || a.BillingStreet == null ||a.billingStreet=='' || a.BillingCountry == '-1' || a.BillingState == null || a.BillingPostalCode == null||a.BillingPostalCode =='' || a.BillingCity == null||a.BillingCity=='') {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter all required fields for account'));
            selTab = 'section1';
            return false;
        }*/
        return true;
    }

    /**
     * called by vf and apex
     */
    public Boolean tempSaveBool() {
        system.debug('same as biiling 111'+sameasbilling);
        String sbcheck = ApexPages.currentPage().getParameters().get('sameAsBilling');
        system.debug('sbcheck=='+sbcheck);
       /* if (sbcheck != null && sbcheck == '1') {
            return true;
        }*/


        if (!checkAccountFields()) return false;
        
        String soqlwhere = u != null ? ' AND Id != \'' + u.Id + '\'' : '';
        String tempemail = a.PersonEmail;
        List<User> users;
        //if(a.id == null){
            if(!test.isRunningtest()){
            users = Database.query('SELECT Id from User WHERE Email = :tempemail ' + soqlwhere + ' AND IsActive = true AND Accountid !=\''+a.id+'\'');
            if (!users.isEmpty()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'This Email has already been taken.'));
                return false;
            }
            }
        //}
        
        
        if (a.PersonEmail != confirmEmail) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Confirm Email Must Match.'));
            return false;
        }

        a.Salutation = salutation;
        a.FirstName = firstname;
        a.LastName = lastname;

        // set the items picked by the user
        a.VV_Market__c = selectedMarket;
        a.VV_Second_Market__c = selectedMarket2;
        a.VV_Feed__c = selectedProduct;
        a.VV_Edition__c = selectedEdition;
        a.Subscription_Plan__c = subscriptionType;

        // setup person account details
        system.debug('same as billling'+ sameAsBilling);
        if (sameAsBilling) {
            a.Shipping_FirstName__c = a.FirstName;
            a.Shipping_LastName__c = a.LastName;
            a.ShippingStreet = a.BillingStreet;
            a.ShippingCity = a.BillingCity;
            a.ShippingState = a.BillingState;
            a.ShippingPostalCode = a.BillingPostalCode;
            a.ShippingCountry = a.BillingCountry;
        }

        a.Referred_By__c = null;
        // determine if referral code is valid (referral code is either the last 15 digit of the id or the email address of the account)
        if (referral_code != null && referral_code != '') {
            String idmask = '001' + referral_code;
            List<Account> tempalist = [SELECT Id FROM Account WHERE Id = :idmask OR PersonEmail LIKE :referral_code];
            if (!tempalist.isEmpty()) a.Referred_By__c = tempalist[0].Id;
            else {
                referral_code = null;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Referral Email'));
                return false;
            }
        }

        system.debug('payment_method###'+payment_method);
        if (payment_method.Credit_Card_Holder_Name__c == null) payment_method.Credit_Card_Holder_Name__c = a.FirstName + ' ' + a.Lastname;
        if (payment_method.ACH_Account_Name__c == null) payment_method.ACH_Account_Name__c = a.FirstName + ' ' + a.Lastname;
        if (paymentType == null) paymentType = 'Credit Card';
        payment_method.Payment_Type__c = paymentType;

        param_map = vvPaymentManagement.initPaymentInfo1(payment_method.Payment_Type__c, false,a.CurrencyISOCode);  // populate signature and page id
        if (param_map == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid initPaymentInfo, Please contact CSR or Refresh the Page and Try Again'));
            return false;
        }
        signature_key = (String)param_map.get('key');


        determineFees();

        fetchCoupon();

        

        

        if (a.Id == null || zca == null || selectedMarket == null || selectedEdition == null || selTab == 'section3') {
            if (selTab == 'section1') {selTab = 'section2';showtab2 = true;}
            else if (selTab == 'section2') {selTab = 'section3';showtab3 = true;}
            else if (selTab == 'section3') {selTab = 'section4';showtab4 = true;}
        } else {
            if (selTab == 'section1') {selTab = 'section4';showtab4 = true;}
        }
        

        system.debug('selectedMarket: ' + selectedMarket);
        system.debug('selectedProduct: ' + selectedProduct);
        return true;
    }

    public void fetchCoupon() {
        if (coupon_code != null && coupon_code != '') {
            List<Coupon_Matrix__c> couponlist = [SELECT Discount_Amount__c, Discount_Percentage__c, Description__c FROM Coupon_Matrix__c WHERE Name = :coupon_code AND Effective_Start_Date__c <= :Date.today() AND Effective_End_Date__c >= :Date.today() ORDER BY Effective_Start_Date__c DESC LIMIT 1];
            if (couponlist.isEmpty()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Coupon.'));
                return;
            }
            effective_coupon = couponlist[0];
            effective_coupon.Discount_Amount__c = vvCurrencyManagement.convertFromUSD(effective_coupon.Discount_Amount__c, a.Account_Currency__c);
        }
        
    }

    public void saveAndSubmit() {
        if (!tempSaveBool()) return;

        Boolean isinsert = (zca == null ? true :false);   // is this update or insert
        
        if (!checkAccountFields()) return;        

        // if it is insert, apple customer is false, need a payment method
         if (isCalledFrom == 'section4'){
            //Adding Code
                if(zca != null && (zca.Zuora__DefaultPaymentMethod__c == 'CreditCard' || zca.Zuora__DefaultPaymentMethod__c == '' || zca.Zuora__DefaultPaymentMethod__c == null)){
                    isinsert = false;
                }
                else{
                    isinsert = true; 
                }
            //Adding code End
             PaymentMsg=true;
           /* if( isInsert && !a.Is_Apple_App_Store_Customer__c &&*/
             system.debug('payment_method.Credit_Card_Number__c****'+payment_method.Credit_Card_Number__c);
            if( (paymentType == 'Credit Card' && (payment_method.Credit_Card_Number__c == null))// || payment_method.Credit_Card_CVV_Code__c==null)) 
                || (paymentType == 'ACH' && payment_method.ACH_Account_Number__c == null) ) {
                   payment_method.Credit_Card_Number__c = null;
                  payment_method.Credit_Card_CVV_Code__c = null;
                    
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter payment information'));
                return;
            }
             if(paymentType == 'Credit Card'){          
             
               if(payment_method.Credit_Card_Number__c.contains('-')) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Card Number'));
                return;
                }
                /*else if(payment_method.Credit_Card_CVV_Code__c.contains('-')) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Card CVV Number'));
                return;
                }*/
            }
            Integer month = Date.Today().Month();
            Integer year = Date.Today().Year();
            
            if( payment_method.Credit_Card_Expiration_Year__c != null && payment_method.Credit_Card_Expiration_Month__c != null &&
                Integer.valueOf(payment_method.Credit_Card_Expiration_Year__c) == year && 
                Integer.valueOf(payment_method.Credit_Card_Expiration_Month__c) < month ){
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Card is already expired'));
                return;
            }
         }
        try {

            if (!isinsert) { // already has zuora_aid, no need to insert
                string res = zuoraAPIHelper.updateContact(a, zca.Zuora__BillToId__c);
                System.debug(zca.Zuora__BillToId__c+'----'+a+'------>>>'+res);
                if (res .containsIgnoreCase('error')) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Update ZContact: ' + res ));
                    return;
                }
                
            } else {    // no billing account, need to insert into zuora
                // insert zuora account
                system.debug('Account BillingCountry ###'+a.billingCountry);
                //system.debug('Region ###'+a.region__c);
                system.debug('paymentType####'+paymentType);
                zuora_aid = zuoraAPIHelper.createAccount(a, paymentType);
                if (zuora_aid.containsIgnoreCase('error')) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to connect to subscription server. ' + zuora_aid));
                    return;
                }
                // insert zuora contact
                zuora_contactid = zuoraAPIHelper.createContact(a, zuora_aid);
                if (zuora_contactid.containsIgnoreCase('error')) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to connect to subscription server. ' + zuora_contactid));
                    return;
                }
            }
            
            param_map = vvPaymentManagement.initPaymentInfo1(payment_method.Payment_Type__c, false,a.CurrencyISOCode);  // populate signature and page id
            if (param_map == null) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid initPaymentInfo, Please contact CSR or Refresh the Page and Try Again'));
                return;
            }

            param_map.put('zuora_aid',zuora_aid);
            
            Map<String, Object> paymentresult = null;
            String paymentResultRefId = null;
            // submit payment only if insert or credit card number is entered
            if (payment_method.Credit_Card_Number__c != null || payment_method.ACH_Account_Number__c != null) {
                paymentresult = (Map<String, Object>)JSON.deserializeUntyped(vvPaymentManagement.postToZuora(payment_method, param_map, a, false));

                if (!Boolean.valueOf(paymentresult.get('success'))) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Payment error: ' + paymentresult.get('errorMessage')));
                    return;
                }
                paymentResultRefId = (String)paymentresult.get('refId');
            }
                
                // only activate account if it is insert, cause it has already been activated
                if (isinsert) {
                    // activate account
                    String result = zuoraAPIHelper.activateAccount(null, zuora_aid, zuora_contactid,paymentResultRefId  );
                    if (!result.containsIgnoreCase('success')) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to activate to account: ' + result));
                        return;
                    }
                } else { // update, so set the default payment
                    if(paymentResultRefId != null)
                    {
                        String gateway = vvCurrencyManagement.getGateway(a, paymentType);
                        String result = zuoraAPIHelper.setDefaultPayment(zuora_aid, paymentResultRefId , gateway);
                        if (!result.containsIgnoreCase('success')) {
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Set Default Payment Method error: ' + result ));
                            return;
                        }
                    }
                }

            

            // get owner id
            VVCommunitySettings__c cs = VVCommunitySettings__c.getOrgDefaults();
            User uowner = [SELECT Id FROM User WHERE Name = :cs.Account_Owner_Name__c];
            if (a.Id == null) a.OwnerId = uowner.Id;

            // since we added a valid payment method here, all subscription billing will be done through zuora, customer cannot be an apple customer from now on
            a.Is_Apple_App_Store_Customer__c = false;

            // activate account, create user, create subscription only if this is an insert
            system.debug('isinsert=='+isinsert);
            if (isinsert) {           
            
                // test if this account is potentially fraudulent, this need to be run before create subscription so we know to suspend or not
                Id q_id = [SELECT Id FROM Group WHERE Name = :vvConstants.CASE_QUEUE_FRAUD AND Type = 'Queue'].Id;
                Case fraudcase = new Case(OwnerId=q_id, Type=vvConstants.CASE_FRAUD_ALERT);
                Boolean isfraud = vvSubscriptionManagement.fraudManagement(a, fraudcase);
    

                // for remote event profile, only create account and create case, do not create subscription
                Id pid = [SELECT Id FROM Profile WHERE Name = :vvConstants.PROFILE_REMOTE_EVENT].Id;
                if (UserInfo.getProfileId() == pid) {
                    upsert a;

                    q_id = [SELECT Id FROM Group WHERE Name = :vvConstants.CASE_START_TRIAL AND Type = 'Queue'].Id;
                    Case trialcase = new Case(OwnerId=q_id,AccountId=a.Id,Coupon_Code__c=coupon_code,Referred_By__c=referral_code,Subject='Remote Event Account Creation',Type=vvConstants.CASE_CREATE_SUB,Reason='Remote Event Account Created',Origin='Web',Description='New Remote Event Account Created, need trial creation');
                    insert trialcase;

                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Account Saved, Community User Created, Case Created'));


                } else { // create new subscription
                    /*if (a.VV_Feed__c == null || a.VV_Edition__c == null || a.VV_Market__c == null || a.Subscription_Plan__c == null) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Account Saved, Community User Created'));
                    } else {*/
                        // marketing params
                        if(isCalledFrom == 'section1'){
                            Map<String, String> customfields = new Map<String, String>();
                            if (a.Campaign__c != null) customfields.put('CampaignId__c',a.Campaign__c);
                            if (a.Id != null) { // for updates, need to set the subscription values into map for create subscription to pick up
                                customfields.put('VV_Edition__c',a.VV_Edition__c);
                                customfields.put('VV_Market__c',a.VV_Market__c);
                                customfields.put('VV_Second_Market__c',a.VV_Second_Market__c);
                                customfields.put('VV_Feed__c',a.VV_Feed__c);
                                customfields.put('Subscription_Plan__c',a.Subscription_Plan__c);
                                customfields.put('BillingCountry',a.BillingCountry);
                                customfields.put('Account_Currency__c',a.Account_Currency__c);
                                customfields.put('Fraud_Score__c',String.valueOf(a.Fraud_Score__c));
                            }
    
                            // this create will insert account if successful
                            /*String result = vvSubscriptionManagement.createSubscription(a, zuora_aid, coupon_code, referral_code, true, customfields);
                            system.debug('result======='+result);
                            if (result.containsIgnoreCase('error')) {
                                system.debug('result====@@@@');
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Zuora Subscription Error: ' + result));
                                return;
                            } else {*/
                                // create community user
                                if(isCalledFrom == 'section1'){
                                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Account Saved, Community User Created'));
                            
                                }   
                            //}
                        }
                        
                   // }      //commented          
                }

                if (isfraud) {
                    fraudcase.accountId = a.Id;
                    insert fraudcase;
                }

                // update account here
                if (a.Id != null){ 
                system.debug('a.billingstreet'+a.BillingStreet);
                system.debug('a.shippingstreet'+a.shippingstreet);
                if (sameAsBilling) {
                    a.Shipping_FirstName__c = a.FirstName;
                    a.Shipping_LastName__c = a.LastName;
                    a.ShippingStreet = a.BillingStreet;
                    a.ShippingCity = a.BillingCity;
                    a.ShippingState = a.BillingState;
                    a.ShippingPostalCode = a.BillingPostalCode;
                    a.ShippingCountry = a.BillingCountry;
                }
                a.Vector_Vest_Customer_Id__c =a.personemail;
                update a;
                }

                // create community user
                if(a.id != null){
                    //adding code to activate user
                     list<user> lstUSer = new list<User>();
                    lstUSer =[select id,isActive from user where Accountid=:a.id];
                    if(lstUSer.size() == 0){
                        vvUserManagement.createCommunityUser(a.Id, false);
                    }
                    else if (lstUSer[0].isActive==false){
                        vvUserManagement.activateUser(lstUSer[0].id);
                    }
                    //
                    
                }
                //adding pageMessage
                if(isCalledFrom == 'section1'){
                   
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Account Saved, Community User Updated'));
                }else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Payment Method is Updated'));
                }
                
            } else {
                
                    system.debug('a.billingstreet'+a.BillingStreet);
                system.debug('a.shippingstreet'+a.shippingstreet);
                        if (sameAsBilling) {
                    a.Shipping_FirstName__c = a.FirstName;
                    a.Shipping_LastName__c = a.LastName;
                    a.ShippingStreet = a.BillingStreet;
                    a.ShippingCity = a.BillingCity;
                    a.ShippingState = a.BillingState;
                    a.ShippingPostalCode = a.BillingPostalCode;
                    a.ShippingCountry = a.BillingCountry;
                }
                    a.Vector_Vest_Customer_Id__c =a.personemail;
                    update a;
                

                // see if need to update user
                if (u != null && u.Email != a.PersonEmail) {
                    u.Email = a.PersonEmail;
                    // do we need to update username? no for now
                    update u;
                }
                else if(u == null){
                    vvUserManagement.createCommunityUser(a.Id, false);
                }

                if(isCalledFrom == 'section1'){
                   
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Account Saved, Community User Updated'));
                }else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Payment Method is Updated'));
                }
                

            }

            
            if(isCalledFrom == 'section4'){

                // insert payment method sf object record, first set other ones to not default
                if (payment_method.Credit_Card_Number__c != null || payment_method.ACH_Account_Number__c != null) {
                    payment_method.Account__c = a.Id;
                
                    // set other payment methods on this account to not default
                    List<Payment_Method_SF__c> paymentlist = [SELECT Id FROM Payment_Method_SF__c WHERE Account__c = :a.Id];
                    for (Payment_Method_SF__c pmsf : paymentlist) {pmsf.Default_Payment_Method__c = false;}
                    if (!paymentlist.isEmpty()) update paymentlist;
                    if(a.id !=null){
                    insert payment_method;
                    }
                   // payment_method.Credit_Card_Number__c  ='163';
                   payment_method = new Payment_Method_SF__c(Default_Payment_Method__c = true);
                    Integer Month = Date.Today().Month();
                    if(Month < 10){
                    payment_method.Credit_Card_Expiration_Month__c = '0'+String.ValueOf(Month);
                    }else{
                    payment_method.Credit_Card_Expiration_Month__c = String.ValueOf(Month);
                    }
                     if (a.Id != null && payment_method.Credit_Card_Holder_Name__c == null) payment_method.Credit_Card_Holder_Name__c = a.FirstName + ' ' + a.LastName;
                     if (a.Id != null && payment_method.ACH_Account_Name__c == null) payment_method.ACH_Account_Name__c = a.FirstName + ' ' + a.Lastname;
                 //payment_method.Payment_Type__c = paymentType;
                }
            }
            // update account in zuora, name and crm id
            zuoraAPIHelper.updateAccount(a.Id, a.FirstName + ' ' + a.LastName, zuora_aid); 

            zuoraAPIHelper.ondemandSyncFuture(zuora_aid);
            
            
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage() + ':' + e.getStackTraceString()));
            return;
        }
    }


}