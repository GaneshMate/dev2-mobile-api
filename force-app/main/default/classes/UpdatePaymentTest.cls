@istest(seeAllData= true)
global class UpdatePaymentTest {

    @isTest static void testUpdatePayment() {
        String JsonStr='{"Payments": [{"method": "PATCH","richInput": {"Id" : "2c92c0f86078c4d40160994c43a51000","CreditCardType" : "Visa"}},{"method": "PATCH","richInput": {"Id" : "2c92c0f86078c4d40160996f995f3f47","CreditCardType" : "Visa"}}]}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/update';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'PATCH';
        req.requestBody = Blob.valueof(JsonStr);
        Test.startTest();
        UpdatePayment.updatePayment();
        Test.stopTest();
        System.assertEquals(200, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
    }
     @isTest static void testBlankId() {
        String JsonStr='{"Payments": [{"method": "PATCH","richInput": {"Id" : "","CreditCardType" : "Visa"}},{"method": "PATCH","richInput": {"Id" : "","CreditCardType" : "Visa"}}]}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/update';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'PATCH';
        req.requestBody = Blob.valueof(JsonStr);
        Test.startTest();
        UpdatePayment.updatePayment();
        Test.stopTest();
        System.assertEquals(500, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
    }
     @isTest static void testBlankCreditCardType() {
        String JsonStr='{"Payments": [{"method": "PATCH","richInput": {"Id" : "2c92c0f86078c4d40160994c43a51000","CreditCardType" : "MasterCard"}},{"method": "PATCH","richInput": {"Id" : "2c92c0f86078c4d40160996f995f3f47","CreditCardType" : "MasterCard"}}]}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/update';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'PATCH';
        req.requestBody = Blob.valueof(JsonStr);
        Test.startTest();
        UpdatePayment.updatePayment();
        Test.stopTest();
        System.assertEquals(200, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
    }
}