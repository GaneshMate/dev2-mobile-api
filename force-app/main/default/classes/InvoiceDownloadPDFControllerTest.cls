@isTest(seeAllData=true) // need see all data for wsdl
private class InvoiceDownloadPDFControllerTest {
	@isTest static void testController() {
		InvoiceDownloadPDFController idpc = new InvoiceDownloadPDFController();

		// test exception
		idpc.createFindAttachment();

		// test exception 2
		ApexPages.currentPage().getParameters().put('id','12345');
		idpc.createFindAttachment();

		// setup invoice
		Zuora__ZInvoice__c zzi = new Zuora__ZInvoice__c(Zuora__InvoiceDate__c = Date.today(),Zuora__TotalAmount__c=10,Zuora__Zuora_Id__c='12345');
		insert zzi;
		ApexPages.currentPage().getParameters().put('id',zzi.Id);
		idpc.createFindAttachment();

	}
}