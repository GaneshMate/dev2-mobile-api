global class UpdateByAdminUserInvocableMethods {
  
  @InvocableMethod(label='Update Opp And Quote with Admin User' description='Update Opp And Quote with Admin User')
  global static void updateOppAndQuote(List<String> quoteIds) {
      
      List<SBQQ__Quote__c> allQuotes = [select id,SBQQ__Primary__c,SBQQ__Opportunity2__c,SBQQ__Opportunity2__r.Created_By_PortalCustomer__c from SBQQ__Quote__c where id IN :quoteIds AND SiteUser_Primary_Check__c = true AND SBQQ__Opportunity2__r.Created_By_PortalCustomer__c = true ];
      
      if(allQuotes.size()>0){
          //CPQIntegrationLayerHelper.syncQuoteToOpportunityWithAdminUser(allQuotes[0].id,allQuotes[0].SBQQ__Opportunity2__c);
      }
  }
}