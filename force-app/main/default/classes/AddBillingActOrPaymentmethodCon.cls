public class AddBillingActOrPaymentmethodCon {

    public PageReference GotoQuote() {
        return new pagereference('/'+ApexPages.currentPage().getParameters().get('id'));
    }


    public boolean showloading{ get; set; }
    Public String isCalledFrom {get;set;}
    // zuora signature key
    Map<String, Object> param_map;  // map of zuora signature items
    public String signature_key {get; private set;}
    public pagereference updateQuote(){
        showloading=true;
        String Qid = ApexPages.currentPage().getParameters().get('id');
        SBQQ__Quote__c objQuote =[select id, SBQQ__Account__c from SBQQ__Quote__c where id=:Qid];
        list<Zuora__CustomerAccount__c> lstBillingAccount=[SELECT Id from Zuora__CustomerAccount__c WHERE Zuora__Account__c=:objQuote.SBQQ__Account__c  and Zuora__DefaultPaymentMethod__c !='Other' and Zuora__DefaultPaymentMethod__c != null and Zuora__DefaultPaymentMethod__c !='' ];
        
        if(lstBillingAccount != null && lstBillingAccount.size()>0){
        update objQuote;
        showloading=false;        
        return new pagereference('/apex/ZSB__ZuoraContractQuote?Id='+objQuote.id);
        
        }
        
        return null;
        
    }
    public void updatequote1(){
                List<Apexpages.Message> msgs = ApexPages.getMessages();
        
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('Payment Method is Updated')) showloading=true;
        }
        
    }
    public void saveAndSubmit() {
    try{
        //showloading=true;
        String Qid = ApexPages.currentPage().getParameters().get('id');
        SBQQ__Quote__c objQuote =[select id, SBQQ__Account__c from SBQQ__Quote__c where id=:Qid];
        ApexPages.currentPage().getParameters().put('id',objQuote.SBQQ__Account__c);
        vvCSRAccountManagementControllerClone obj = new vvCSRAccountManagementControllerClone();
        system.debug('payment_method ###'+payment_method );
        //obj.saveAndSubmit();
        obj.payment_method.Credit_Card_Number__c= payment_method.Credit_Card_Number__c ;
        obj.payment_method.Credit_Card_Holder_Name__c= payment_method.Credit_Card_Holder_Name__c;
        obj.payment_method.Credit_Card_Type__c = payment_method.Credit_Card_Type__c;
        obj.payment_method.Credit_Card_Expiration_Month__c=payment_method.Credit_Card_Expiration_Month__c;
        obj.payment_method.Credit_Card_Expiration_Year__c =payment_method.Credit_Card_Expiration_Year__c;
        obj.payment_method.Credit_Card_CVV_Code__c =payment_method.Credit_Card_CVV_Code__c;
        obj.payment_method.Enc_Value__c = payment_method.Enc_Value__c;
        obj.isCalledFrom= isCalledFrom;
        obj.a.BillingStreet = a.BillingStreet;
        obj.a.BillingCountry = a.BillingCountry;
        obj.a.BillingPostalCode =a.BillingPostalCode;
        obj.a.BillingCity = a.BillingCity;
        obj.a.BillingCountry = a.BillingCountry;
        obj.a.BillingState = a.BillingState;
        obj.saveAndSubmit();
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('Payment Method is Updated')) showloading=true;
        }
        
        }
        catch(Exception e){
            showloading=false;
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'errormessage'));
            return;
        }
        
    }
public AddBillingActOrPaymentmethodCon(){
    a = new Account();
    String Qid = ApexPages.currentPage().getParameters().get('id');
        SBQQ__Quote__c objQuote =[select id, SBQQ__Account__c from SBQQ__Quote__c where id=:Qid];
    a = [SELECT FirstName,CurrencyIsoCode,Vector_Vest_Customer_Id__c , LastName,SBQQ__TaxExempt__c,CertificateID__c,Region1__c,Shipping_FirstName__c, Shipping_LastName__c, Salutation, Phone, PersonEmail, 
                BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode,
                ShippingStreet, ShippingCity, ShippingState, ShippingCountry, ShippingPostalCode,
                VV_Market__c, VV_Second_Market__c, VV_Edition__c, VV_Feed__c, Subscription_Plan__c,IsPersonAccount, Is_Elite__c, Is_Elite_Legacy__c, Account_Currency__c, Account_Currency_Symbol__c,
                (select id from R00N40000001kyLcEAI__r)
                FROM Account WHERE Id =:objQuote.SBQQ__Account__c];
               if(a.billingcountry == null)
               a.billingcountry ='USA';
        showloading=false;
     payment_method = new Payment_Method_SF__c(Default_Payment_Method__c = true,Payment_Type__c='Credit Card',Credit_Card_Holder_Name__c=a.firstname +' '+a.lastname);
        Integer Month = Date.Today().Month();
        if(Month < 10){
            payment_method.Credit_Card_Expiration_Month__c = '0'+String.ValueOf(Month);
        }else{
            payment_method.Credit_Card_Expiration_Month__c = String.ValueOf(Month);
        }
        
        param_map = vvPaymentManagement.initPaymentInfo1(payment_method.Payment_Type__c, false,'USD');  // populate signature and page id
        if (param_map == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid initPaymentInfo'));
            return;
        }
        signature_key = (String)param_map.get('key');
} 
// payment method
    public Payment_Method_SF__c payment_method {get; set;}
     public Account a {get; set;}
}