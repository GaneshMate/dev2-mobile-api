@isTest
private class CPQAPIClassDefinitionsTest{
    @isTest static void testMethod1(){
        test.starttest();
        CPQAPIClassDefinitions.ProductModel obj1 = new CPQAPIClassDefinitions.ProductModel();
        string cs=obj1.currencySymbol;
        id uasid = obj1.upgradedAssetId;
        product2 objprod = obj1.record;
        string ccode = obj1.currencycode;
        String[] featureCategories = obj1.featureCategories;
        //OptionModel
        CPQAPIClassDefinitions.ConstraintModel[] obj2= obj1.constraints;
        CPQAPIClassDefinitions.OptionModel[] obj3= obj1.options;
        CPQAPIClassDefinitions.ConfigAttributeModel[] obj4=obj1.configurationAttributes;
        CPQAPIClassDefinitions.ConfigAttributeModel[] obj4temp=obj1.inheritedConfigurationAttributes;
        CPQAPIClassDefinitions.FeatureModel[] obj5= obj1.features; 
        CPQAPIClassDefinitions.ConfigurationModel obj6= obj1.configuration;
        CPQAPIClassDefinitions.QuoteModel obj7= new CPQAPIClassDefinitions.QuoteModel(); 
        CPQAPIClassDefinitions.QuoteLineModel obj8= new CPQAPIClassDefinitions.QuoteLineModel();
        CPQAPIClassDefinitions.QuoteLineGroupModel obj9= new CPQAPIClassDefinitions.QuoteLineGroupModel();
        CPQAPIClassDefinitions.ProductLoadContext obj10= new CPQAPIClassDefinitions.ProductLoadContext();
        CPQAPIClassDefinitions.SearchContext obj11= new CPQAPIClassDefinitions.SearchContext();
        CPQAPIClassDefinitions.ProductAddContext obj12= new CPQAPIClassDefinitions.ProductAddContext ();
        CPQAPIClassDefinitions.CalculatorContext obj13= new CPQAPIClassDefinitions.CalculatorContext();
        CPQAPIClassDefinitions.ConfigLoadContext obj14= new CPQAPIClassDefinitions.ConfigLoadContext();
        test.stoptest();
    }
    
    }