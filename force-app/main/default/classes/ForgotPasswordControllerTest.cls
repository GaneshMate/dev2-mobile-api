/**
 * An apex page controller that exposes the site forgot password functionality
 */
@IsTest public with sharing class ForgotPasswordControllerTest {
     @IsTest(SeeAllData=true) public static void testForgotPasswordController() {
        // Instantiate a new controller with all parameters in the page
        ForgotPasswordController controller = new ForgotPasswordController();
        controller.username = userinfo.getusername(); 
        controller.forgotPassword();      
        controller.forgotusername();
        System.assertEquals(controller.forgotPassword(),null); 
    }
    @IsTest public static void testForgotPasswordController1() {
        // Instantiate a new controller with all parameters in the page
        ForgotPasswordController controller = new ForgotPasswordController();
        ApexPages.currentPage().getHeaders().put('Host','Partial');
        controller.username = 'test';
        controller.forgotPassword();      
        controller.forgotusername();
        System.assertEquals(controller.forgotPassword(),null); 
    }
}