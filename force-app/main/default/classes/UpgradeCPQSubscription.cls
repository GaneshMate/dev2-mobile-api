public without sharing class UpgradeCPQSubscription
{
    public String sfQuoteId {get;set;}
    public String sfOpportunityId {get;set;}
    public CPQIntegrationLayerHelper cpqHelper;
    public UpgradeCPQSubscription(){
        cpqHelper = new CPQIntegrationLayerHelper(null,null);
    }
    
    public string createAmendQuote(String ContractId)
    {
        CPQAPIClassDefinitions.QuoteModel qmodel = getAmendQuote(ContractId);
        sfQuoteId = qmodel.record.id;
        sfOpportunityId = qmodel.record.SBQQ__Opportunity2__c;
        return qmodel.record.id;
    }
    
    
    public void UpgradeProduct(String QuoteId, String subscriptionId,String ContractId,String productId,String changeToProd,String changeToSMProdId, List<String> optionIds)
    {
        CPQAPIClassDefinitions.QuoteModel qmodel = cpqHelper.getQuoteModel(QuoteId);
        qmodel = updateQuoteModelForUpgrade(subscriptionId,qmodel,productId,changeToProd,changeToSMProdId ,optionIds);
        qmodel = cpqHelper.CalculateQuote(qmodel );
        qmodel = updateBeforeSave(qModel);
        string saveResult = cpqHelper.saveQuoteWithChanges(qmodel );
        
    }   
    
    private CPQAPIClassDefinitions.QuoteModel updateBeforeSave(CPQAPIClassDefinitions.QuoteModel qmodel)
    {
        if(qmodel!=null)
        {
            System.debug('before createAmendOpportunity------------------>>'+qmodel.record.SBQQ__MasterContract__c);
            Opportunity op = createAmendOpportunity(qmodel.record.SBQQ__MasterContract__c);
            System.debug('createAmendOpportunity------------------>>'+op);
            if(op!=null)
            {
               qmodel.record.SBQQ__Opportunity2__c = op.id;              
            }
            qmodel.record.SiteUser_Primary_Check__c = true;
        }
        return qmodel;
    }   
    
    private Opportunity createAmendOpportunity(string ContractId)
    {
        Opportunity opp = null;        
        if(contractId!='' && contractId!=null )
        {
            Contract con = [select id,ContractNumber,ZSB__BillingAccountId__c,AccountId,SBQQ__OpportunityPricebookId__c from contract where id =:ContractId ];
            if(con!=null)
            {
                opp  = new Opportunity();
                opp.Name = 'Ammendment for contract #'+con.ContractNumber;
                opp.AccountId = con.AccountId;
                opp.SBQQ__AmendedContract__c = contractId;
                opp.ZSB__BillingAccount__c = con.ZSB__BillingAccountId__c;
                opp.StageName = 'Proposal/Price Quote';
                opp.CloseDate = Date.Today();
                opp.SBQQ__QuotePricebookId__c=con.SBQQ__OpportunityPricebookId__c;
                opp.Created_By_PortalCustomer__c = true;
                insert opp;                
            }
        }
        return opp;
    }
    
    public CPQAPIClassDefinitions.QuoteModel getAmendQuote(String ContractId)
    {
        CPQAPIClassDefinitions.QuoteModel qmodel = null;
        if(!String.isEmpty(ContractId))
        {
            String qmodelJson = SBQQ.ServiceRouter.load('SBQQ.ContractManipulationAPI.ContractAmender', ContractId, null); 
            qmodel = (CPQAPIClassDefinitions.QuoteModel)JSON.deserialize(qmodelJson, CPQAPIClassDefinitions.QuoteModel.class); 
        }
        return qmodel;
    }
    
    
    public CPQAPIClassDefinitions.QuoteModel updateQuoteModelForUpgrade(String subscriptionId,CPQAPIClassDefinitions.QuoteModel qModel,String prodId,String changeToProd,string changeToSMProdId ,List<string> allProductOptions)
    {
        // Remove the earlier product from QuoteModel
        Decimal updateQuantity = removeProductFromQuote(qModel,prodId);      
            if(changeToProd!='')
            {
                if(changeToSMProdId  == null )
                {
                    changeToSMProdId   = '';
                }
                CPQAPIClassDefinitions.ProductLoadContext plContext = new CPQAPIClassDefinitions.ProductLoadContext(); 
                plContext.pricebookId = CPQIntegrationLayerHelper.cpqPriceBookId;
                //plContext.currencyCode = 'USD';
                
                // Primary Product after upgrade
                List<CPQAPIClassDefinitions.ProductModel> listProdModel = new List<CPQAPIClassDefinitions.ProductModel>();
                String newProductJson = SBQQ.ServiceRouter.load('SBQQ.ProductAPI.ProductLoader', changeToProd, JSON.serialize(plContext)); 
                CPQAPIClassDefinitions.ProductModel newProductModel = (CPQAPIClassDefinitions.ProductModel)JSON.deserialize(newProductJson , CPQAPIClassDefinitions.ProductModel.class);  
                listProdModel.add(newProductModel );
                
                
                // Secondary Market Product after upgrade
                if(changeToSMProdId !='')
                {
                    String newSMProductOptionJson = SBQQ.ServiceRouter.load('SBQQ.ProductAPI.ProductLoader', changeToSMProdId , JSON.serialize(plContext)); 
                    CPQAPIClassDefinitions.ProductModel newSMProductOptionModel = (CPQAPIClassDefinitions.ProductModel)JSON.deserialize(newSMProductOptionJson , CPQAPIClassDefinitions.ProductModel.class);  
                    listProdModel.add(newSMProductOptionModel);
                }
                
                //Add Other Subscription based options 
                for(String prodOpId : allProductOptions){
                    String newProductOptionJson = SBQQ.ServiceRouter.load('SBQQ.ProductAPI.ProductLoader', prodOpId, JSON.serialize(plContext)); 
                    CPQAPIClassDefinitions.ProductModel newProductOptionModel = (CPQAPIClassDefinitions.ProductModel)JSON.deserialize(newProductOptionJson , CPQAPIClassDefinitions.ProductModel.class);  
                    listProdModel.add(newProductOptionModel);
                }
                
                qModel= cpqHelper.AddProductToQuote(listProdModel,qModel); 
                
                // Set SM product Option in Quote Line for the related primary Quote
                if(changeToSMProdId !='')
                {
                    qModel= cpqHelper.setProductOptionForQuoteLine(qModel,listProdModel,changeToProd,changeToSMProdId );
                }
                // Set other subscribed product Option in Quote Line for the related primary Quote
                for(string pOpId : allProductOptions)
                {
                    qModel= cpqHelper.setProductOptionForQuoteLine(qModel,listProdModel,changeToProd,pOpId);
                }               
            }
                          
        return qModel;        
    }
    
    private decimal removeProductFromQuote(CPQAPIClassDefinitions.QuoteModel qModel,String prodId)
    {
        CPQAPIClassDefinitions.QuoteLineModel addedQLineMod = null;
        decimal updatedQuantity = 0;
        for(CPQAPIClassDefinitions.QuoteLineModel qLineMod : qModel.lineItems)
        {
            string qLineProdId = qLineMod.record.SBQQ__Product__c;            
            if(qLineProdId.startsWith(prodId)  || qLineProdId== prodId)
            {
                updatedQuantity = qLineMod.record.SBQQ__Quantity__c;
                qLineMod.record.SBQQ__Quantity__c = 0;
            }            
        }
        return updatedQuantity ; 
                       
    }
    public class ProductMetaData{
        public string Id{get;set;}
        public Product2 prod{get;set;}
        
        public decimal Quantity{get;set;}
        
        public ProductMetaData(string mdProdId)
        {
            id = mdProdId;            
        }
        public ProductMetaData()
        {}
    }
}