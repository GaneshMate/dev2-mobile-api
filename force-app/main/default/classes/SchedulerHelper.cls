public without sharing class SchedulerHelper {
    public static final String CRON_MIDNIGHT_FIRST_OF_THE_MONTH = '0 0 0 1 * ?';
    
    public static void SchedulerHelper (Type cType) {
        scheduleJob(cType, CRON_MIDNIGHT_FIRST_OF_THE_MONTH);
    } 
    public static void scheduleJob(Type targetType, String cronExpression) {
        String jobName = targetType.getName();
        abortJob(jobName);
        
        scheduleJob(targetType, jobName, cronExpression);
    }
    
    public static void scheduleJobEverySpecifiedMinutes(Type targetType, integer mins) {
        if( mins == 0 || mins == null)
        {
            mins = 5;
        }
        integer num = 60/mins;
        String cronExpression = '0 00 * * * ?';
        for(integer i=0;i<num;i++)
        {
            String jobName = targetType.getName()+' '+i;
            integer tempInt = (i*mins)+1;
            abortJob(jobName);
            String intStr =  tempInt<10 ? '0'+String.valueOf(tempInt) : String.valueOf(tempInt);
            cronExpression = '0 '+intStr+' * * * ?';            
            scheduleJob(targetType, jobName, cronExpression);
        }
    }
    
    public static void scheduleJob(Type targetType, String jobName, String cronExpression) {        
        ScheduledDispatcher scheduledDispatcher = new ScheduledDispatcher(targetType);
        System.schedule(jobName, cronExpression, scheduledDispatcher);
    }     
    public static void abortJob(String jobName) {
        Set<String> stateList = new Set<String>{'COMPLETED', 'ERROR', 'DELETED'};
        List<CronTrigger> jobs = [SELECT Id, CronJobDetail.Name, State, NextFireTime FROM CronTrigger WHERE CronJobDetail.Name = :jobName ];
        if (jobs.size()>0) {
            System.abortJob(jobs[0].Id);
        }
    } 
}