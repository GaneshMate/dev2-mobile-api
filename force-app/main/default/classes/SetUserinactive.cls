global class SetUserinactive implements Database.Batchable<sObject>, Database.Stateful {
  
  String query;
  global SetUserinactive(String query) {
    this.query = query;
    
  }
  
  
  global Database.QueryLocator start(Database.BatchableContext BC) {
    return Database.getQueryLocator(query);
  }

     global void execute(Database.BatchableContext BC, List<User> userlist) {
       set<id> setAccountid = new set<id>();
       for (User u : userlist) {         
        setAccountid.add(u.Accountid);  
      }
    map<id, integer> mapAccount = new map<id, integer>();
      for(SBQQ__Quote__c objQuote : [select id,SBQQ__Account__c from SBQQ__Quote__c where SBQQ__Account__c in: setAccountid]){
        if(mapAccount.get(objQuote.SBQQ__Account__c)==null)
        mapAccount.put(objQuote.SBQQ__Account__c,1);
      }
      list<User> lstupdateuser = new list<user>();
      for(User obju:userlist){
        if(mapAccount.get(obju.Accountid) != null){
            obju.Passwordtest__c = 'QuoteExist';
            lstUpdateuser.add(obju);
        }
      }
      if(lstupdateuser.size()>0)
        update lstupdateuser;
      
  }
  
  global void finish(Database.BatchableContext BC){}
  }