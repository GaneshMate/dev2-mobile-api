Public without sharing class ShowingProductList {

    public ShowingProductList() {
        getProdfields() ;
        
        targetType = Schema.getGlobalDescribe().get('Product2'); // To create object instance of sobject type         
        if(targetType!=null){        
            objSelectedObject =  targetType.newSObject();  // Intialize object   
             system.debug('objSelectedObject===='+objSelectedObject);
        } 
        showProducts(); // Calling method to show all produts.
    }

    
   
    Public Schema.SObjectType targetType{get;set;}
    Public SObject objSelectedObject {get;set;}
    Public String selProdId{get;set;}       
    public PageReference showSelectedProduct(){
        system.debug('selProdId::'+selProdId);
       // return new PageReference('/apex/ProductInformation?selProdId='+selProdId);
       PageReference pgr = null ;
         pgr = Page.ProductInformation;
                    
                    pgr.getParameters().put('selProdId', selProdId);
                    pgr.setRedirect(true);
        return pgr;
    }    
    public PageReference showProducts(){
        try{
        PriceBookIds__c  stdPriceBookId = PriceBookIds__c.getValues('Standard Price Book');
        string strPricebookId = stdPriceBookId.Price_Book_Id__c;
        List<Product2> prodList = new List<Product2>();
        string strQuery = 'SELECT id,Name,ProductCode,SBQQ__ProductPictureID__c,Product_Detail__c,Market__c,Feed__C,'+ 
            '(SELECT ID,UnitPrice FROM PricebookEntries where Pricebook2Id = \'' + strPricebookId + '\''+') FROM Product2'; 
        
        // build filter condition
        
        Map<String, Schema.SObjectField> fieldMap = targetType.getDescribe().fields.getMap();
        if(prodFieldMap.size() > 0){
            Boolean isWhereAdded = false;
            for(String fieldName : prodFieldMap.keySet()){
                
                Schema.DisplayType fielddataType = fieldMap.get(fieldName).getDescribe().getType();
                if( objSelectedObject.get(fieldName) != null && isWhereAdded == false){
                    strQuery = strQuery+' Where ' ;
                    isWhereAdded = true ;
                }
                if( fielddataType == Schema.DisplayType.MultiPicklist && objSelectedObject.get(fieldName) != null){
                    
                    List<String> splitStr = new List<String>();
                    splitStr = String.valueOf(objSelectedObject.get(fieldName)).split(';');
                    String  qStr = '';
                    Integer strIndex = 1;
                    for(string str : splitStr){
                    
                       qStr = qStr + '\'' +str + '\'';
                       if(strIndex != splitStr.size()){
                           qStr = qStr  + ',';
                       }
                       
                       strIndex = strIndex  + 1 ;
                    }
                    if(!strQuery.endsWith('Where ')){
                        strQuery = strQuery + ' and ' ; 
                    }
                    strQuery = strQuery + fieldName + ' INCLUDES (' + qStr  + ') ';
                    
                }else if ( (fielddataType == Schema.DisplayType.Integer || 
                        fielddataType == Schema.DisplayType.DateTime ||
                        fielddataType == Schema.DisplayType.Date) && objSelectedObject.get(fieldName) != null
                ){
                    if(!strQuery.endsWith('Where ')){
                        strQuery = strQuery + ' and ' ; 
                    }
                    strQuery = strQuery + fieldName + ' = ' + objSelectedObject.get(fieldName) ;
                }else{
                    if(objSelectedObject.get(fieldName) != null){
                        if(!strQuery.endsWith('Where ')){
                            strQuery = strQuery + ' and ' ; 
                        }
                        strQuery = strQuery + fieldName + ' = \'' + objSelectedObject.get(fieldName)  + '\'';
                    }
                }
            }            
        }  
        system.debug('strQuery=='+strQuery);
        prodList = Database.Query(strQuery);
        
        prdListwp = new List<PrdListWrapper>();
        
        if(prodList != null && prodList.size() > 0){
             for(Product2 eachProduct : prodList){
                PrdListWrapper plw = new PrdListWrapper();
                plw.isSelected = false;
                plw.prod = eachProduct;
                plw.price = ( eachProduct.PricebookEntries != null && eachProduct.PricebookEntries.size() > 0 )? eachProduct.PricebookEntries[0].UnitPrice : 0;
                 prdListwp.add(plw);
               
            }   
        }
        }catch(Exception e){
            System.debug('Exception'+e.getLineNumber());
        }  
        return null ;
    }
    Public Map<String,String>  prodFieldMap {get;set;}
    Public List<String> getProdfields() {
        List<Product_Filters__c> prodFilterCs = Product_Filters__c.getall().values();
        List<String> options = new List<String>();
        prodFieldMap = new Map<String,String> ();
        for(Product_Filters__c pf : prodFilterCs){
           options.add(pf.Field_API__c);
           prodFieldMap.put(pf.Field_API__c,pf.Name);
        }
        return options ;
        
    }
    Public List<PrdListWrapper> prdListwp{get;set;}
    Public Class PrdListWrapper{
        Public Product2 prod {get;set;}
        Public Boolean isSelected{get;set;}
        Public Decimal price {get;set;}
    }
   
}