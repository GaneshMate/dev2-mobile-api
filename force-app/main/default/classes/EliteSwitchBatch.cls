/**
 * scans through all accounts and find accounts that matches elite status, 
 *
 * if found, flip the elite switch, find all rate plans that needs pricing update, 
 * cancel the rateplan subscription on the next renewal term and create new rate plan subscription with the new pricing
 *
 * Customers prior to 1/1/2012: Spent $3,500 or 5 years consecutive service
 *
 * Customers after 1/1/2012:  Spent $5,000 or 5 years consecutive service
 */
global class EliteSwitchBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
	
	String query;
	List<Elite_Requirement__c> erlist;
	
	global EliteSwitchBatch() {
		query = 'SELECT Id, FirstName, LastName, CreatedDate, Is_Elite__c, Is_Elite_Legacy__c, Subscription_Signup_Date__c, ' 
		 + 'Subscription_Cancellation_Date__c, Subscription_Plan__c, Total_Amount_Spent__c, Total_Amount_From_Zuora__c, Account_Currency__c '
		 + 'FROM Account';

		 // retrieve custom setting ordered by the effective date DESC
		 erlist = [SELECT Effective_Date__c, Amount__c, Years__c FROM Elite_Requirement__c ORDER BY Effective_Date__c DESC];
	}

	global EliteSwitchBatch(String query) {
		this.query = query;

		// retrieve custom setting ordered by the effective date DESC
		erlist = [SELECT Effective_Date__c, Amount__c, Years__c FROM Elite_Requirement__c ORDER BY Effective_Date__c DESC];
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Account> acclist) {
   		
   		List<Case> elite_cases = new List<Case>();	// new elite cases to insert
   		List<Account> accounts_to_update = new List<Account>();	// accounts that need to be updated

   		for (Account a : acclist) {
   			Boolean oldelite = a.Is_Elite__c;	// before update

   			Elite_Requirement__c ertouse = null;
			// see which elite requirement this customer falls into, first one will be the most recent effective
			for (Elite_Requirement__c er : erlist) {
				Date compdate = a.Subscription_Signup_Date__c == null ? Date.valueOf(a.CreatedDate) : a.Subscription_Signup_Date__c;
				if (compdate > er.Effective_Date__c) {
					ertouse = er;
					break;
				}
			}
			if (ertouse == null) continue;	// this shouldn't happen, but if it does, that means custom setting is set up way into the future

			// see if customer is above the amount
			if (a.Total_Amount_Spent__c == null) a.Total_Amount_Spent__c = 0;
			if (a.Total_Amount_From_Zuora__c == null) a.Total_Amount_From_Zuora__c = 0;

			// compare total amount with elite required amount, everything is in USD at this moment
			if ((a.Total_Amount_Spent__c + a.Total_Amount_From_Zuora__c) > ertouse.Amount__c ) a.Is_Elite__c = true;	// switch him
			else a.Is_Elite__c = false;	// if he cancels and payment gets reverted, his status drops too

			// see if customer is has subscribed for more than elite required years by looking at the sign up date and not cancelled
			if (a.Subscription_Signup_Date__c != null && a.Subscription_Signup_Date__c <= Date.today().addYears(Integer.valueOf(0-ertouse.Years__c)) && a.Subscription_Cancellation_Date__c == null) {
				a.Is_Elite__c = true;
			}

			if (oldelite != a.Is_Elite__c) accounts_to_update.add(new Account(Id=a.Id,Is_Elite__c=a.Is_Elite__c));

			// now create a new case to send out elite package
			if (a.Is_Elite__c && !oldelite) {
				
				Id q_id = [SELECT Id FROM Group WHERE Name = :vvConstants.CASE_QUEUE_ELITE AND Type = 'Queue'].Id;
				Case c = new Case(OwnerId=q_id,AccountId=a.Id,Type=vvConstants.CASE_ELITE_PACKAGE,Subject='New Elite Customer need Elite Package',Reason='Customer becomes elite',Origin='Web');
				elite_cases.add(c);
			}
   		}

   		if (!elite_cases.isEmpty()) insert elite_cases;
   		if (!accounts_to_update.isEmpty()) update accounts_to_update;
		

	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}