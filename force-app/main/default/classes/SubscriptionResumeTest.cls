@isTest
private class SubscriptionResumeTest {
    @isTest static void testSubscriptionResumeTwoWeekNotice() {
        // test 2 week alert first
        //Account a = new Account(FirstName='testfirst',LastName='testlast',PersonEmail='test@test.com',Subscription_Suspend_Start_Date__c=Date.today().addDays(-30),Subscription_Suspend_End_Date__c=Date.today().addDays(14),Subscription_Status__c='Suspended');
        //insert a;
        Account a = new Account();
            a.firstname='test acc1';
            a.lastName = 'test acc';
            a.billingCity ='Test City';
            a.billingStreet = 'TestStreet';
            a.billingstate ='TestState';
            a.billingPostalcode ='23451';
            a.billingcountry ='United States';
            Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            a.recordtypeID=objAccrec.id;
           a.PersonMailingCity='Test City';
            a.PersonMailingStreet = 'TestStreet';
            a.PersonMailingState ='TestState';
            a.PersonMailingPostalCode ='23451';
            a.PersonMailingCountry ='United States';
            a.phone='090123456987';
            a.Subscription_Suspend_Start_Date__c=Date.today().addDays(-30);
            a.Subscription_Suspend_End_Date__c=Date.today().addDays(14);
            a.Subscription_Status__c='Suspended';
            //acc.ispersonAccount = true;
            insert a;
        Zuora__CustomerAccount__c zca = new Zuora__CustomerAccount__c(Zuora__Status__c='Active',Name='test billing account',Zuora__Account__c=a.Id,Zuora__Zuora_Id__c='12345');
        insert zca;

        Zuora__Subscription__c zs = new Zuora__Subscription__c(Name='a0001',Zuora__SubscriptionStartDate__c = Date.today().addMonths(-12), Zuora__SubscriptionEndDate__c = Date.today().addYears(2), Zuora__InitialTerm__c='12 months',Zuora__RenewalTerm__c='12 months',Zuora__TermStartDate__c=Date.today().addMonths(-3),
            Zuora__TermEndDate__c = Date.today().addMonths(20),Zuora__AutoRenew__c=true, Zuora__Zuora_Id__c='12345', Zuora__ContractEffectiveDate__c=Date.today().addMonths(-3), Zuora__ServiceActivationDate__c=Date.today().addMonths(-3), 
            Zuora__TCV__c=12,Zuora__Account__c=a.Id,Zuora__CustomerAccount__c=zca.Id,Zuora__Status__c='Suspended',Zuora__TermSettingType__c='EVERGREEN');
        insert zs;

        Test.startTest();
        System.schedule('Subscription Resume Batch','0 30 23 * * ?', new SubscriptionResumeScheduler());
        Test.stopTest();

    }

    @isTest static void testSubscriptionResume() {
        // test 2 week alert first
        //Account a = new Account(FirstName='testfirst',LastName='testlast',PersonEmail='test@test.com',Subscription_Suspend_Start_Date__c=Date.today().addDays(-30),Subscription_Suspend_End_Date__c=Date.today(),Subscription_Status__c='Suspended');
        //insert a;
         Account a = new Account();
            a.firstname='test acc1';
            a.lastName = 'test acc';
            a.billingCity ='Test City';
            a.billingStreet = 'TestStreet';
            a.billingstate ='TestState';
            a.billingPostalcode ='23451';
            a.billingcountry ='United States';
            Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            a.recordtypeID=objAccrec.id;
           a.PersonMailingCity='Test City';
            a.PersonMailingStreet = 'TestStreet';
            a.PersonMailingState ='TestState';
            a.PersonMailingPostalCode ='23451';
            a.PersonMailingCountry ='United States';
            a.phone='090123456987';
            a.Subscription_Suspend_Start_Date__c=Date.today().addDays(-30);
            a.Subscription_Suspend_End_Date__c=Date.today();
            a.Subscription_Status__c='Suspended';
            //acc.ispersonAccount = true;
            insert a;

        Zuora__CustomerAccount__c zca = new Zuora__CustomerAccount__c(Zuora__Status__c='Active',Name='test billing account',Zuora__Account__c=a.Id,Zuora__Zuora_Id__c='12345');
        insert zca;

        Zuora__Subscription__c zs = new Zuora__Subscription__c(Name='a0001',Zuora__SubscriptionStartDate__c = Date.today().addMonths(-12), Zuora__SubscriptionEndDate__c = Date.today().addYears(2), Zuora__InitialTerm__c='12 months',Zuora__RenewalTerm__c='12 months',Zuora__TermStartDate__c=Date.today().addMonths(-3),
            Zuora__TermEndDate__c = Date.today().addMonths(20),Zuora__AutoRenew__c=true, Zuora__Zuora_Id__c='12345', Zuora__ContractEffectiveDate__c=Date.today().addMonths(-3), Zuora__ServiceActivationDate__c=Date.today().addMonths(-3), 
            Zuora__TCV__c=12,Zuora__Account__c=a.Id,Zuora__CustomerAccount__c=zca.Id,Zuora__Status__c='Suspended',Zuora__TermSettingType__c='EVERGREEN');
        insert zs;

        // setup custom setting
        Zuora_Integration__c zi = new Zuora_Integration__c(Name='default',apiAccessKeyId__c='12345',apiSecretAccessKey__c='12345',Endpoint__c='test.com');
        insert zi;

        Test.startTest();
        System.schedule('Subscription Resume Batch','0 30 23 * * ?', new SubscriptionResumeScheduler());
        Test.stopTest();

    }
}