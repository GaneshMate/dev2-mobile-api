public without sharing class AmendCPQContract
{
    public String sfQuoteId {get;set;}
    public String sfOpportunityId {get;set;}
    public CPQIntegrationLayerHelper cpqHelper;
    public AmendCPQContract(){
        cpqHelper = new CPQIntegrationLayerHelper(null,null);
    }
    
    @future(callout=true)
    public static void doSwitchBillingProcess(String QuoteId, String subscriptionId,String ContractId,String productId)
    {
        AmendCPQContract amendHelper = new AmendCPQContract();
        amendHelper.sfQuoteId = QuoteId;
        amendHelper.SwitchBilling(QuoteId,subscriptionId,ContractId,productId);
    }
       
    public string createAmendQuote(String ContractId)
    {
        //if(!test.isrunningtest()){
        CPQAPIClassDefinitions.QuoteModel qmodel = getAmendQuote(ContractId);
        //Just for testing
        //AmendCPQContract.doSwitchBillingProcess(qmodel.record.id,subscriptionId,ContractId,productId);
        sfQuoteId = qmodel.record.id;
        sfOpportunityId = qmodel.record.SBQQ__Opportunity2__c;
        return qmodel.record.id;
        //}
        return null;
    }
    
    public void SwitchBilling(String QuoteId, String subscriptionId,String ContractId,String productId)
    {
        CPQAPIClassDefinitions.QuoteModel qmodel = cpqHelper.getQuoteModel(QuoteId);
        
        Map<string,ProductMetaData> smProductOptions = fetchProductOptionIds(qmodel,productId);
        //if(!test.isrunningtest()){
        qmodel = updateQuoteModelForBillingSwitch(subscriptionId,qmodel,productId,smProductOptions );
        qmodel = cpqHelper.CalculateQuote(qmodel );
        qmodel = updateBeforeSave(qModel);
        string saveResult = cpqHelper.saveQuoteWithChanges(qmodel );
       // }
        
    }
    
    private Map<string,ProductMetaData> fetchProductOptionIds(CPQAPIClassDefinitions.QuoteModel qModel,String productId)
    {
        Map<string,ProductMetaData> productOptionsMap = new Map<string, ProductMetaData>();        
        
        string parentQuoteLineId = '';
        for(CPQAPIClassDefinitions.QuoteLineModel qLineMod : qModel.lineItems)
        {
            string qLineProductId = (String) qLineMod.record.SBQQ__Product__c;
            if(qLineProductId.contains(productId) || qLineMod.record.SBQQ__Product__c == productId)
            {
                parentQuoteLineId = qLineMod.record.id;
            }
            
            if(parentQuoteLineId != '' && 
            (!qLineMod.record.SBQQ__Bundled__c && qLineMod.record.SBQQ__RequiredBy__c == parentQuoteLineId ))
            {
                // Change to store Product object with required attributes
                ProductMetaData smProd = new ProductMetaData();
                smProd.id = qLineMod.record.SBQQ__Product__c;
                smProd.Quantity = qLineMod.record.SBQQ__Quantity__c;
                productOptionsMap.put(smProd.id,smProd);
            }
        }
        return productOptionsMap;
    }
    private SBQQ__Quote__c makeQuotePrimary(string qId)
    {
        SBQQ__Quote__c quote = [select id,SBQQ__Opportunity2__c,SBQQ__Primary__c from SBQQ__Quote__c where id=:qId];
        quote.SBQQ__Primary__c = true;
        update quote;
        
        return quote;
    }
    private CPQAPIClassDefinitions.QuoteModel updateBeforeSave(CPQAPIClassDefinitions.QuoteModel qmodel)
    {
        if(qmodel!=null)
        {
            System.debug('before createAmendOpportunity------------------>>'+qmodel.record.SBQQ__MasterContract__c);
            Opportunity op = createAmendOpportunity(qmodel.record.SBQQ__MasterContract__c);
            System.debug('createAmendOpportunity------------------>>'+op);
            if(op!=null)
            {
               qmodel.record.SBQQ__Opportunity2__c = op.id;              
            }
            qmodel.record.SiteUser_Primary_Check__c = true;
        }
        return qmodel;
    }   
    
    private Opportunity createAmendOpportunity(string ContractId)
    {
        Opportunity opp = null;        
        if(contractId!='' && contractId!=null )
        {
            Contract con = [select id,ContractNumber,ZSB__BillingAccountId__c,AccountId,SBQQ__OpportunityPricebookId__c from contract where id =:ContractId ];
            if(con!=null)
            {
                opp  = new Opportunity();
                opp.Name = 'Amendment for contract #'+con.ContractNumber;
                opp.AccountId = con.AccountId;
                opp.SBQQ__AmendedContract__c = contractId;
                opp.ZSB__BillingAccount__c = con.ZSB__BillingAccountId__c;
                opp.StageName = 'Proposal/Price Quote';
                opp.CloseDate = Date.Today();
                opp.SBQQ__QuotePricebookId__c=con.SBQQ__OpportunityPricebookId__c;
                opp.Created_By_PortalCustomer__c = true;
                insert opp;                
            }
        }
        return opp;
    }
    
    public CPQAPIClassDefinitions.QuoteModel getAmendQuote(String ContractId)
    {
        CPQAPIClassDefinitions.QuoteModel qmodel = null;
        if(!String.isEmpty(ContractId))
        {
            String qmodelJson = SBQQ.ServiceRouter.load('SBQQ.ContractManipulationAPI.ContractAmender', ContractId, null); 
            qmodel = (CPQAPIClassDefinitions.QuoteModel)JSON.deserialize(qmodelJson, CPQAPIClassDefinitions.QuoteModel.class); 
        }
        return qmodel;
    }
    
    
    public CPQAPIClassDefinitions.QuoteModel updateQuoteModelForBillingSwitch(String subscriptionId,CPQAPIClassDefinitions.QuoteModel qModel,String prodId,Map<string,ProductMetaData> allProductOptions)
    {
        // Remove the earlier product from QuoteModel
        Decimal updateQuantity = removeProductFromQuote(qModel,prodId);
        // Fetch product data for options and populate in ProductMetaData         
        Map<string,Product2> prodLists = new Map<string,Product2>([select id,Feed__c, Market__c,Professional__c,SBQQ__BillingFrequency__c,productCode,Is_Secondary_Market__c,AddOn_Type__c from product2 where (id =: prodId OR id IN:allProductOptions.keySet() ) AND isActive = true]);
        
        string secondaryMarket = '';
        string secondaryFeed = '';
        Map<String,String> addOnTypes = new Map<string,string>();
        
        for(string pId : allProductOptions.keySet())
        {
            if(prodLists.containsKey(pId))
            {
                ProductMetaData prodMD = allProductOptions.get(pId );
                prodMD.prod = prodLists.get(pId);                
                
                if(prodLists.get(pId).Is_Secondary_Market__c)
                {
                    secondaryMarket = prodLists.get(pId).Market__c;
                    secondaryFeed = prodLists.get(pId).Feed__c;
                }
                if(prodLists.get(pId).AddOn_Type__c!='' && prodLists.get(pId).AddOn_Type__c!=null)
                {
                        addOnTypes.put(prodLists.get(pId).AddOn_Type__c,pId);
                }
            }
        }
        
        Product2 prodToChange = prodLists.get(prodId);
        String upgradeToProductBillingFrequency = '';
        system.debug('prodToChange### '+prodToChange  +'prodId###'+prodId);
        if(prodToChange.SBQQ__BillingFrequency__c == 'Monthly' || prodToChange.productCode.contains('MONTHLY'))
        {
            upgradeToProductBillingFrequency = 'Annual';
        }else if(prodToChange.SBQQ__BillingFrequency__c == 'Annual' || prodToChange.productCode.contains('ANNUAL'))
        {
            upgradeToProductBillingFrequency = 'Monthly';
        }
                
        if(upgradeToProductBillingFrequency !='')
        {
            Map<string,Product2> allProdsToUpdate = new Map<string,Product2>([select id,Feed__c, Market__c,SBQQ__BillingFrequency__c,Is_Secondary_Market__c,AddOn_Type__c,productCode,Name,Professional__c from product2 where 
                ((Feed__c=:prodToChange.Feed__c AND Market__c =:prodToChange.Market__c AND SBQQ__BillingFrequency__c =:upgradeToProductBillingFrequency AND Is_Secondary_Market__c = FALSE AND Professional__c =:prodToChange.Professional__c) 
                OR (Feed__c=:secondaryFeed AND Market__c =:secondaryMarket AND SBQQ__BillingFrequency__c =:upgradeToProductBillingFrequency AND Is_Secondary_Market__c = TRUE ) 
                OR (Feed__c='' AND Market__c ='' AND SBQQ__BillingFrequency__c =:upgradeToProductBillingFrequency AND AddOn_Type__c IN:addOnTypes.keySet())) AND isActive = true  ]) ;
            
            String switchProductId = '';
            String secondaryMarketProdId = '';
            List<String> allAddOnProdIds = new List<String>();
            System.debug('upgradeToProductBillingFrequency---------------->>>>>>'+upgradeToProductBillingFrequency);
            System.debug('allProdsToUpdate---------------->>>>>>'+allProdsToUpdate);
            for(string changeToProdId : allProdsToUpdate.keySet())
            {
                Product2 pd = allProdsToUpdate.get(changeToProdId);
                if(!pd.productCode.contains('_EF_')){
                if(pd.Feed__c==prodToChange.Feed__c && pd.Market__c == prodToChange.Market__c && pd.SBQQ__BillingFrequency__c == upgradeToProductBillingFrequency && pd.Professional__c == prodToChange.Professional__c)
                {
                    switchProductId = changeToProdId ;
                }
                if(pd.Is_Secondary_Market__c)
                {
                    secondaryMarketProdId = pd.Id;
                }
                if(pd.Feed__c == '' && pd.Market__c =='' && pd.SBQQ__BillingFrequency__c ==upgradeToProductBillingFrequency && pd.AddOn_Type__c != '' && pd.AddOn_Type__c !=null && !pd.Is_Secondary_Market__c )
                {
                    allAddOnProdIds.add(pd.Id); 
                }
                }//end
            }
            
            if(switchProductId !='')
            {
                
                CPQAPIClassDefinitions.ProductLoadContext plContext = new CPQAPIClassDefinitions.ProductLoadContext(); 
                plContext.pricebookId = CPQIntegrationLayerHelper.cpqPriceBookId;
                //plContext.currencyCode = 'USD';
                
                // Primary Product after Switch
                List<CPQAPIClassDefinitions.ProductModel> listProdModel = new List<CPQAPIClassDefinitions.ProductModel>();
                String newProductJson = SBQQ.ServiceRouter.load('SBQQ.ProductAPI.ProductLoader', switchProductId , JSON.serialize(plContext)); 
                CPQAPIClassDefinitions.ProductModel newProductModel = (CPQAPIClassDefinitions.ProductModel)JSON.deserialize(newProductJson , CPQAPIClassDefinitions.ProductModel.class);  
                listProdModel.add(newProductModel );
                
                
                // Secondary Market Product after Switch
               if(secondaryMarketProdId !='')
                {
                    String newSMProductOptionJson = SBQQ.ServiceRouter.load('SBQQ.ProductAPI.ProductLoader', secondaryMarketProdId , JSON.serialize(plContext)); 
                    CPQAPIClassDefinitions.ProductModel newSMProductOptionModel = (CPQAPIClassDefinitions.ProductModel)JSON.deserialize(newSMProductOptionJson , CPQAPIClassDefinitions.ProductModel.class);  
                    listProdModel.add(newSMProductOptionModel);
                }
                //Add Other Subscription based options 
                for(String prodOpId : allAddOnProdIds){
                    String newProductOptionJson = SBQQ.ServiceRouter.load('SBQQ.ProductAPI.ProductLoader', prodOpId, JSON.serialize(plContext)); 
                    CPQAPIClassDefinitions.ProductModel newProductOptionModel = (CPQAPIClassDefinitions.ProductModel)JSON.deserialize(newProductOptionJson , CPQAPIClassDefinitions.ProductModel.class);  
                    listProdModel.add(newProductOptionModel);
                }
                
                qModel= cpqHelper.AddProductToQuote(listProdModel,qModel);                
                
                // Set SM product Option in Quote Line for the related primary Quote
                qModel= cpqHelper.setProductOptionForQuoteLine(qModel,listProdModel,switchProductId,secondaryMarketProdId);
                // Set other subscribed product Option in Quote Line for the related primary Quote
                for(string pOpId : allAddOnProdIds)
                {
                    qModel= cpqHelper.setProductOptionForQuoteLine(qModel,listProdModel,switchProductId,pOpId);
                }
                
                /*CPQAPIClassDefinitions.QuoteLineModel addedQLineMod = null;
                decimal updatedQuantity = 0;
                for(CPQAPIClassDefinitions.QuoteLineModel qLineMod : qModel.lineItems)
                {
                    string qLineProdId = qLineMod.record.SBQQ__Product__c;            
                    if(qLineProdId.startsWith(prodId)  || qLineProdId== prodId)
                    {
                        updatedQuantity = qLineMod.record.SBQQ__Quantity__c;
                        qLineMod.record.SBQQ__Quantity__c = 0;
                    }
                    if(qLineProdId.startsWith(switchProductId )  || qLineProdId== switchProductId )
                    {
                        addedQLineMod = qLineMod;
                    }
                }
                
                if(addedQLineMod!=null)
                {
                    addedQLineMod.record.SBQQ__Quantity__c = updatedQuantity;
                }*/
                
            }
        }                  
        return qModel;        
    }
    
    private decimal removeProductFromQuote(CPQAPIClassDefinitions.QuoteModel qModel,String prodId)
    {
        CPQAPIClassDefinitions.QuoteLineModel addedQLineMod = null;
        decimal updatedQuantity = 0;
        for(CPQAPIClassDefinitions.QuoteLineModel qLineMod : qModel.lineItems)
        {
            string qLineProdId = qLineMod.record.SBQQ__Product__c;            
            if(qLineProdId.startsWith(prodId)  || qLineProdId== prodId)
            {
                updatedQuantity = qLineMod.record.SBQQ__Quantity__c;
                qLineMod.record.SBQQ__Quantity__c = 0;
            }            
        }
        return updatedQuantity ;                
    }
    public class ProductMetaData{
        public string Id{get;set;}
        public Product2 prod{get;set;}
        
        public decimal Quantity{get;set;}
        
        public ProductMetaData(string mdProdId)
        {
            id = mdProdId;
            
        }
        public ProductMetaData()
        {}
    }
}