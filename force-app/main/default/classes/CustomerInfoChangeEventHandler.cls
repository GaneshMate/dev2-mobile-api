global class CustomerInfoChangeEventHandler {
    
    global static String EVENT_TYPE_NEW     = 'NEW';
    global static String EVENT_TYPE_UPDATE  = 'UPDATE';
    global static String EVENT_TYPE_DELETE  = 'DELETE';
    
    

    global static String OBJECT_TYPE_ACCOUNT    = 'ACCOUNT';
    global static String OBJECT_TYPE_RIGHT      = 'ACCESS_RIGHT';
    //global static String OBJECT_TYPE_ZSUB       = 'ZSUBSCRIPTION';
    global static String OBJECT_TYPE_SUB       = 'SUBSCRIPTION';


    private static ID cicrtid = [select id from RecordType where sObjectType = 'Application_Event__c' and developerName ='Customer_Info_Change' limit 1].Id;
    
    global static void customerInfoChanged(List<CustomerInfoChangeInvocable.ApplicationEventInfo> eventInfos){
        createEvents(eventInfos);
    }


    public static void customerAccountDeleted(List<Account> accs){
        List<CustomerInfoChangeInvocable.ApplicationEventInfo> eventInfos = new List<CustomerInfoChangeInvocable.ApplicationEventInfo>();
        for(Account acc : accs){
            CustomerInfoChangeInvocable.ApplicationEventInfo eventInfo = new CustomerInfoChangeInvocable.ApplicationEventInfo();
            eventInfo.accId = acc.Id;
            eventInfo.recordId = acc.Id;
            eventInfo.objectType = OBJECT_TYPE_ACCOUNT;
            eventInfo.eventType = EVENT_TYPE_DELETE;
            eventInfos.add(eventInfo);
        }
        createEvents(eventInfos);
    }

    public static void customerAccessRightsInserted(List<Enterprise_Access_Right__c> rights){
        List<CustomerInfoChangeInvocable.ApplicationEventInfo> eventInfos = new List<CustomerInfoChangeInvocable.ApplicationEventInfo>();
        set<id> setAccountid = new set<id>();
        for(Enterprise_Access_Right__c right : rights){
            if(!setAccountid.contains(right.Account__c)){
                CustomerInfoChangeInvocable.ApplicationEventInfo eventInfo = new CustomerInfoChangeInvocable.ApplicationEventInfo();
                eventInfo.accId = right.Account__c;
                //eventInfo.recordId = right.Id;
                eventInfo.objectType = OBJECT_TYPE_RIGHT;
                eventInfo.eventType = EVENT_TYPE_NEW;
                eventInfos.add(eventInfo);
                setAccountid.add(right.Account__c);
            }
        }
        createEvents(eventInfos);
    }
    public static void customerAccessRightsDeleted(List<Enterprise_Access_Right__c> rights){
        List<CustomerInfoChangeInvocable.ApplicationEventInfo> eventInfos = new List<CustomerInfoChangeInvocable.ApplicationEventInfo>();
        set<id> setAccountid = new set<id>();
        for(Enterprise_Access_Right__c right : rights){
            if(!setAccountid.contains(right.Account__c)){
            CustomerInfoChangeInvocable.ApplicationEventInfo eventInfo = new CustomerInfoChangeInvocable.ApplicationEventInfo();
            eventInfo.accId = right.Account__c;
            //eventInfo.recordId = right.Id;
            eventInfo.objectType = OBJECT_TYPE_RIGHT;
            eventInfo.eventType = EVENT_TYPE_DELETE;
            eventInfos.add(eventInfo);
            setAccountid.add(right.Account__c);
            }
        }
        createEvents(eventInfos);
    }

    public static void customerSubscriptionDeletedForCPQ(List<SBQQ__Subscription__c> sfSubs){
        List<CustomerInfoChangeInvocable.ApplicationEventInfo> eventInfos = new List<CustomerInfoChangeInvocable.ApplicationEventInfo>();
        for(SBQQ__Subscription__c sfSub : sfSubs){
            CustomerInfoChangeInvocable.ApplicationEventInfo eventInfo = new CustomerInfoChangeInvocable.ApplicationEventInfo();
            eventInfo.accId = sfSub.SBQQ__Account__c;
            eventInfo.recordId = sfSub.Id;
            eventInfo.objectType = OBJECT_TYPE_SUB;
            eventInfo.eventType = EVENT_TYPE_DELETE;
            eventInfos.add(eventInfo);
        }
        createEvents(eventInfos);
    }
    
    public static void customerZuoraSubscriptionDeleted(List<Zuora__SubscriptionProductCharge__c> subs){
        
    }
    
    private static void createEvents(List<CustomerInfoChangeInvocable.ApplicationEventInfo> eventInfos){
        Map<Id,List<CustomerInfoChangeInvocable.ApplicationEventInfo>> eventInfosByaccId = new Map<Id,List<CustomerInfoChangeInvocable.ApplicationEventInfo>>();
        for(CustomerInfoChangeInvocable.ApplicationEventInfo eventInfo : eventInfos){
            if(null!=eventInfo.accId){
                List<CustomerInfoChangeInvocable.ApplicationEventInfo> eis = eventInfosByaccId.get(eventInfo.accId);
                if(null==eis){
                    eis = new List<CustomerInfoChangeInvocable.ApplicationEventInfo>();
                    eventInfosByaccId.put(eventInfo.accId, eis);
                }
                eis.add(eventInfo);
            } 
        }
        
        Map<Account,User> usrByAcc = getAccountUsers(eventInfosByaccId.keySet());

        List<Application_Event__c> aes = new List<Application_Event__c>();
        for(Account acc :  usrByAcc.keySet()){
            User usr = usrByAcc.get(acc);
            string usrName = null;  
            if(null!=usr) usrName = usr.UserName;
            for(CustomerInfoChangeInvocable.ApplicationEventInfo ei : eventInfosByaccId.get(acc.Id)){
                aes.add(new Application_Event__c(
                        RecordTypeId = cicrtid,
                        Customer_Account_Id__c = acc.Id,
                        Customer_Application_Id__c = acc.ApplicationUserID__c,
                        Customer_Enterprise_Id__c = acc.EnterpriseCustomerID__c,
                        Customer_VV_Id__c = acc.Vector_Vest_Customer_Id__c,
                        User_Name__c = usrName,
                        Event_Type__c = ei.eventType,
                        Object_Id__c = ei.recordId,
                        Object_Name__c = ei.objectType
                )); 
            }
        }
        If(!aes.isEmpty()) insert aes;  
    }


    private static Map<Account,User> getAccountUsers(Set<Id> accIds){
        Map<Id,Account> accById = new Map<Id,Account>([select Id, PersonContactId, ApplicationUserID__c, EnterpriseCustomerID__c, Vector_Vest_Customer_Id__c from Account where Id in :accIds]);        
        Set<Id> conIds = new Set<Id>();
        for(Account acc : accById.values()) if(null!=acc.PersonContactId) conIds.add(acc.PersonContactId);
        List<User> usrs = [select Id, UserName, ContactId from User where ContactId in :conIds];
        Map<Id,User> usrByConId = new Map<Id,User>();
        for(User usr : usrs) usrByConId.put(usr.ContactId, usr);

        Map<Account,User> usrByAcc = new Map<Account,User>();
        for(Account acc :  accById.values()){
            User usr = usrByConId.get(acc.PersonContactId);
            usrByAcc.put(acc, usr);
        }
        return usrByAcc;
    }

}