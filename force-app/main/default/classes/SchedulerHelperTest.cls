@isTest
public class SchedulerHelperTest {
    Public  testmethod  static void testSchedulerHelper(){
        Test.StartTest();
        Type t = Type.forName('UpdateQuoteToPrimaryScheduler');
        string cronexpr = '0 0 0 1 * ?';
        try{
            SchedulerHelper.scheduleJob(t,cronexpr);
            SchedulerHelper.scheduleJobEverySpecifiedMinutes(t,10);
        }Catch(Exception Ex){
            System.debug('Excepion::'+Ex.getMessage());
        }
        //SchedulerHelper.scheduleJob();
        Test.StopTest();
    }
}