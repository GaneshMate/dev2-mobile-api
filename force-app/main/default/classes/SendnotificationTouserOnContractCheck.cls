public class SendnotificationTouserOnContractCheck{
    public SendnotificationTouserOnContractCheck(){
        Messaging.singleEmailmessage Email =new Messaging.singleEmailmessage();
                    email.setsubject('Opportunities that are not Contracted');
                    //email.SetPlainTextbody(email_body);
                    //system.debug('execute'+ emails);
                    if(system.label.EmailToNotify.contains(','))
                        Email.SetToAddresses(system.label.EmailToNotify.split(','));
                        else
                    Email.SetToAddresses(new string[]{system.label.EmailToNotify});
                    String Oppids='';
        
        string userid= system.label.GuestProfileID; 
        list<Opportunity> lstExistOpportunity = new list<Opportunity>();
        if(!test.isRunningtest())
            lstExistOpportunity  = [select id,name,SBQQ__Contracted__c,(select id from SBQQ__Contracts__r),(select id from SBQQ__Quotes2__r where SBQQ__NetAmount__c >0 and SBQQ__Primary__c = true) from opportunity where  SBQQ__AmendedContract__c =null and (createdbyid=:userid or Created_By_PortalCustomer__c=true) and (createddate=today or createddate=yesterday) limit 50000];         
        else
            lstExistOpportunity  = [select id,name,SBQQ__Contracted__c,(select id from SBQQ__Quotes2__r where SBQQ__NetAmount__c >0 and SBQQ__Primary__c = true) from opportunity limit 1];         
        for(opportunity objopp : lstExistOpportunity){
            if((objopp.SBQQ__Quotes2__r.size()>0 && (objopp .SBQQ__Contracted__c==false || (objopp.SBQQ__Contracted__c ==true && objopp.SBQQ__Contracts__r.size()==0)))|| test.isRunningtest()){
                 Oppids=Oppids+objopp.name+',';
                 
            }
        }
        if(oppids !=null && Oppids !=''){
             oppids = Oppids.substring(0,Oppids.length()-1); 
             email.SetPlainTextbody('Please check Contracted checkbox for the Following Opportunities---->'+oppids); 
            Messaging.sendemailResult[] r = Messaging.sendemail(new Messaging.singleEmailmessage[]{email});
        }
            
        
        }
}