global class EliteSwitchScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		// run one account at a time because we need to switch all the subscription inside of the account
		Database.executeBatch(new EliteSwitchBatch());
	}

	/*
	System.schedule('Elite Switcher','0 30 23 * * ?', new EliteSwitchScheduler());



	 */
	 
}