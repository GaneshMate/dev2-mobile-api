public without sharing class AccountTriggerHandler {
    /**
     * convert lead if needed
     * @param acclist [description]
     */
    public static void afterInsert(List<Account> acclist) {
        try {
            
            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted = true LIMIT 1];
            List<Database.LeadConvert> leadConverts = new List<Database.LeadConvert>();

            // create email to account id map
            Map<String, Id> emailmap = new Map<String, Id>();

            for (Account a : acclist) {
                if (!a.ConvertedFromLead__c) emailmap.put(a.PersonEmail, a.Id);
            }

            // create email to lead id map
            Map<String, Id> emailleadmap = new Map<String, Id>();
            for (Lead l : [SELECT Id, Email FROM Lead WHERE Email In :emailmap.keySet()]) {
                // if there is a duplicate email in lead, right now do nothing, let it dangle
                if (emailleadmap.get(l.Email) != null) {
                    // do nothing for now
                }
                emailleadmap.put(l.Email, l.Id);
            }

            // create conversion list
            for (String email : emailmap.keySet()) {
                if (emailleadmap.get(email) != null) {
                    Database.LeadConvert lc = new Database.LeadConvert();
                    lc.setDoNotCreateOpportunity(true);
                    lc.setLeadId(emailleadmap.get(email));
                    lc.setAccountId(emailmap.get(email));
                    lc.setConvertedStatus(convertStatus.MasterLabel);
                    leadConverts.add(lc);

                }
            }
            if (!leadConverts.isEmpty()) {

                for (Integer i = 0; i <= leadConverts.size() / 100 ; i++) {
                    list<Database.LeadConvert> tempList = new list<Database.LeadConvert>();
                    Integer startIndex = i * 100;
                    Integer endIndex = ((startIndex + 100) < leadConverts.size()) ? startIndex + 100 : leadConverts.size();
                    for (Integer j = startIndex; j < endIndex; j++) {
                        tempList.add(leadConverts[j]);
                    }
                    Database.LeadConvertResult[] lcrList = Database.convertLead(tempList, false);
                    system.debug('lcrList: ' + lcrList);
                }

            }
            //Updating Account for application userid
                list<Account> lsttoupdate = new list<Account>();
                for(Account a:acclist){
                    if(a.ApplicationUserID__c == null){
                        Account acttoUpdte = new Account(id=a.id, ApplicationUserID__c =a.EnterpriseCustomerID__c);
                        lsttoupdate.add(acttoUpdte );
                    }
                }
                if(lsttoupdate.size()>0){
                    update lsttoupdate;
                }
            //Updatin end
        } catch (Exception e) {}

    }

    /**
     * after update call on account, if has watchlist becomes true, give credit for next month
     * @param acclist [description]
     * @param oldmap  [description]
     */
    public static void afterUpdate(List<Account> acclist, Map<Id, Account> oldmap) {
        
        // populate account ids that need to be given watchlist credits
        Set<Id> accids = new Set<Id>(); 
        for (Account a : acclist) {
            if (a.Has_WatchList__c && !oldmap.get(a.Id).Has_WatchList__c) {
                accids.add(a.Id);
            }
        }

        // call @future below cause trigger doesn't allow callouts
        if (!accids.isEmpty()) addDiscountFuture(accids);
        
    }

    @future(callout=true)
    public static void addDiscountFuture(Set<Id> accids) {
        // get discount rateplan for watchlist
        zqu__ProductRatePlan__c prp = vvSubscriptionManagement.getDiscountPlan(vvConstants.WATCHLIST, null);
        // get the subscriptions
        String primarysub = 'Primary:%';
        for (Zuora__Subscription__c zs : [SELECT Zuora__CustomerAccount__c, Zuora__TermStartDate__c, Zuora__Zuora_Id__c, Zuora__Account__r.Account_Currency__c FROM Zuora__Subscription__c WHERE Name LIKE :primarysub AND Zuora__Status__c = 'Active' AND Zuora__Account__c IN :accids]) {
            // foreign currency
            if (prp.zqu__R00N40000001mFVKEA2__r[0].zqu__Model__c != 'Discount-Percentage')
                prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c = vvCurrencyManagement.convertFromUSD(prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c, zs.Zuora__Account__r.Account_Currency__c);

            // add watchlist discount
            String result = zuoraAmendHelper.addDiscountToSubscription(zs.Zuora__Zuora_Id__c, prp, zs.Zuora__TermStartDate__c > Date.today() ? zs.Zuora__TermStartDate__c : Date.today(), 1, null);

        }
    }

    public static void beforeDelete(List<Account> acclist) {
        List<Account> accs = new List<Account>();
        for(Account acc : acclist){
            if(acc.Is_Observable__c) accs.add(acc);
        }
        if(!accs.isEmpty()) CustomerInfoChangeEventHandler.customerAccountDeleted(accs);
    }

    /**
     * populate enterprise customer id and application user id
     * @param acclist [description]
     */
    public static void beforeInsert(List<Account> acclist) {
        for (Account a : acclist) {
            if (a.Vector_Vest_Customer_Id__c == null) a.Vector_Vest_Customer_Id__c = a.PersonEmail;
        }
        /*Amar [22-Feb-18] :: EnterpriseCustomerID__c converted to AutoNumber
         * Integer nextid = 700000;
        List<Account> alist = [SELECT ApplicationUserIDNumeric__c FROM Account WHERE ApplicationUserIDNumeric__c <> null ORDER BY ApplicationUserIDNumeric__c DESC LIMIT 1];
        System.debug('alist :: '+ alist);
        if (!alist.isEmpty()) {
            if (alist[0].ApplicationUserIDNumeric__c >= 700000) nextid = Integer.valueOf(alist[0].ApplicationUserIDNumeric__c) + 1;
        }

        for (Account a : acclist) {
            System.debug('For :: a :: '+ a.ApplicationUserID__c);
            
            if (a.ApplicationUserID__c == null && a.EnterpriseCustomerID__c == null) {
                a.ApplicationUserID__c = String.valueOf(nextid);
                a.EnterpriseCustomerID__c = a.ApplicationUserID__c;
                System.debug('For :: IF :: a :: '+ a.EnterpriseCustomerID__c);
                nextid++;
            }
            if (a.Vector_Vest_Customer_Id__c == null) a.Vector_Vest_Customer_Id__c = a.PersonEmail;
        }*/
    }

}