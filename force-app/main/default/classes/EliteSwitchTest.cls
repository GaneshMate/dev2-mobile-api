@isTest
private class EliteSwitchTest {
    @isTest static void testEliteSwitch() {
        // setup custom setting, 1 year or 10 dollars
        Elite_Requirement__c er = new Elite_Requirement__c(Name='test',Effective_Date__c=Date.today().addYears(-10),Amount__c=10,Years__c=1);
        insert er;

        // setup account
        Account acc= new Account();
        acc.firstname='test acc1';
            acc.lastName = 'test acc';
            acc.billingCity ='Test City';
            acc.billingStreet = 'TestStreet';
            acc.billingstate ='TestState';
            acc.billingPostalcode ='23451';
            acc.billingcountry ='United States';
            Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            acc.recordtypeID=objAccrec.id;
           acc.PersonMailingCity='Test City';
            acc.PersonMailingStreet = 'TestStreet';
            acc.PersonMailingState ='TestState';
            acc.PersonMailingPostalCode ='23451';
            acc.PersonMailingCountry ='United States';
        Account a = acc;
        a.Subscription_Signup_Date__c=Date.today().addYears(-2);
        a.Total_Amount_Spent__c=100;
        insert a;

        // start scheduler
        Test.startTest();
        System.schedule('Elite Switcher','0 30 23 * * ?', new EliteSwitchScheduler());
        Test.stopTest();
    }
}