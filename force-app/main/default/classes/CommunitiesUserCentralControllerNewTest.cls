@istest(seealldata=true)
private class CommunitiesUserCentralControllerNewTest{
    private static testmethod void testmethod1(){
    zqu__HostedPageLiteSetting__c objpagesetting = new zqu__HostedPageLiteSetting__c(name='Community Credit Card',zqu__PageId__c='2c92c0f853f45c240154049eb3df20b4',zqu__ComponentName__c='PaymentPage',zqu__PaymentMethodType__c='Credit Card');
        list<zqu__HostedPageLiteSetting__c> lsthost = new list<zqu__HostedPageLiteSetting__c >();
        lsthost =[select id from zqu__HostedPageLiteSetting__c where name='Community Credit Card'] ;
        if(lsthost .size()==0)
        insert objpagesetting;
    Zuora_Integration__c objz = new Zuora_Integration__c();
        objz.name='OrgDefault';
        objz.ACH_Gateway__c='Authorize.net';
        objz.apiAccessKeyId__c='matt.zhu@VV.com';
        objz.apiSecretAccessKey__c='Rad1anc8!';
        objz.Community_Domain__c='https://partial-vectorvestcom.cs18.force.com';
        objz.Endpoint__c='apisandbox-api.zuora.com';
        objz.EUR_Gateway__c='Vantiv - EUR';
        objz.ExternalPaymentId__c='2c92c0f9528db6aa015290834d504e3e';
        objz.GBP_Gateway__c='Vantiv - GBP';
        objz.Internal_Domain__c='https://c.cs18.visual.force.com';
        objz.URI__c='https://apisandbox.zuora.com/apps/PublicHostedPageLite.do';
        objz.Payment_Endpoint__c='apisandbox.zuora.com';
        objz.USD_Gateway__c='Cybersource';
        //insert objZ;
        Account acc = new Account();
            acc.firstname='test acc1';
            acc.lastName = 'test acc';
            acc.billingCity ='Test City';
            acc.billingStreet = 'TestStreet';
            acc.billingstate ='TestState';
            acc.billingPostalcode ='23451';
            acc.billingcountry ='United States';
            Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            acc.recordtypeID=objAccrec.id;
           acc.PersonMailingCity='Test City';
            acc.PersonMailingStreet = 'TestStreet';
            acc.PersonMailingState ='TestState';
            acc.PersonMailingPostalCode ='23451';
            acc.PersonMailingCountry ='United States';
            acc.Account_Currency__c='USD'; 
            //acc.ispersonAccount = true;
            insert acc;
            
            Opportunity opp = new Opportunity(Name=acc.Name + ' Opportunity ' ,
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=acc.Id);
                                       
            Product2 prod = new Product2(Name = 'VectorVest 7 EOD US Monthly', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_MONTHLY' , Family = 'Books');
            insert prod;
            Product2 prod2 = new Product2(Name = 'VectorVest 7 EOD US Annual', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_ANNUAL' , Family = 'Books');
            insert prod2;
            
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            
            CPQCommunity__c cpc = new CPQCommunity__c();
            cpc.Name = 'test';
            cpc.PriceBookId__c = pricebookId ;
            insert cpc ;
            
            
            
            SBQQ__Quote__c quoteRec = new SBQQ__Quote__c();
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SiteUser_Primary_Check__c = true;
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SBQQ__Opportunity2__c = opp.Id;
            //quoteRec.ZSB__BillToContact__c = c.Id;
           // quoteRec.ZSB__SoldToContact__c = c.Id;
            quoteRec.ZSB__BillCycleDay__c =  '1';
            quoteRec.ZSB__BillingBatch__c = 'Batch1';
            insert quoteRec ;
            SBQQ__QuoteLine__c ojQuoteline = new SBQQ__QuoteLine__c();
            ojQuoteline.SBQQ__Quote__c = quoteRec .id;
            ojQuoteline.SBQQ__Product__c = prod.id;
            insert ojQuoteline;
             Contract objContract = new Contract();
             objContract.SBQQ__Quote__c = quoteRec.id;
             objContract.SBQQ__Opportunity__c = opp.id;
             objcontract.Accountid=acc.id;
             insert objContract;
             update objcontract;
             Zuora__CustomerAccount__c objZCustAccount = new Zuora__CustomerAccount__c();
             objZCustAccount .Zuora__Account__c = acc.id;
             objZCustAccount .Zuora__Status__c ='Active';
             objZCustAccount .Zuora__BillToName__c ='test';
             objZCustAccount .Zuora__Zuora_Id__c = '2c92c0f960c105580160d80905a205ea';
             objZCustAccount.Zuora__DefaultPaymentMethod__c ='CreditCard';
             //objZCustAccount .Zuora__CreditCard_Number__c = '4444333322221111';
             //objZCustAccount .Zuora__CreditCard_Expiration__c = '10/2019';
             insert objZCustAccount ;
             Zuora__PaymentMethod__c objZpaymentmethod = new Zuora__PaymentMethod__c();
             objZpaymentmethod .Zuora__CreditCardCountry__c='us';
             objZpaymentmethod .Zuora__CreditCardExpirationMonth__c='10';
             objZpaymentmethod.Zuora__CreditCardExpirationYear__c='2019';
             objZpaymentmethod.Zuora__CreditCardHolderName__c='test';
             objZpaymentmethod.Zuora__CreditCardMaskNumber__c='123';
             objZpaymentmethod.Zuora__DefaultPaymentMethod__c  = true;
             //objZpaymentmethod .
             //objZpaymentmethod .
             
             objZpaymentmethod.Zuora__BillingAccount__c = objZCustAccount .id;
             insert objZpaymentmethod;
             contact con =[select id from contact where accountid=:acc.id];
             user objuser = new user(id=userinfo.getuserid(), contactid=con.id);
             update objuser;
             user objuser1 = [select id, accountid from user where id=:objuser.id];
             system.debug('objuser###'+objuser1.accountid);
             SBQQ__Subscription__c objSub = new SBQQ__Subscription__c();
             objsub.SBQQ__Contract__c = objContract.id;
             objsub.SBQQ__Product__c =prod.id;
             objsub.SBQQ__QuoteLine__c = ojQuoteline.id;
             objsub.SBQQ__Account__c = acc.id;
             objsub.SBQQ__Quantity__c = 1;
             insert objsub;
             
             /*Product_Upgrade_Condition__c objprodcond = new Product_Upgrade_Condition__c();
             objprodcond .Feed__c ='EOD';
             objprodcond.Market__c ='US';
             insert objprodcond;
             Feed_To_Show__c  objfeedtoshow = new Feed_To_Show__c();
             objfeedtoshow.Billing_Frequency__c ='Monthly';
             //objfeedtoshow.Disclaimer__c = 
             objfeedtoshow.Feed__c='IntraDay';
             objfeedtoshow.Market__c ='US';
             objfeedtoshow.Upgrade_Condition__c = objprodcond.id;
             insert objfeedtoshow;
             AddOn_To_Show__c objAddOnToshow = new AddOn_To_Show__c();
             objAddOnToshow.Upgrade_Condition__c = objprodcond.id;
             objAddOnToshow.Disclaimer__c = 'test';
             objAddOnToshow.Billing_Frequency__c ='Monthly';
             insert objAddOnToshow;*/
             //test.starttest();
        CommunitiesUserCentralControllerNew  obj= new CommunitiesUserCentralControllerNew();
        CommunitiesUserCentralControllerNew.currency('');
        //obj.getUpgradeLabel(prod2.id);
        CommunitiesUserCentralControllerNew.QuoteLine objquoteline = new CommunitiesUserCentralControllerNew.QuoteLine();
        objquoteline.listprice=2;
        objquoteline .duration = '6';
        objquoteline .productname='test';
        objquoteline.startdate=date.today();
        objquoteline .nettotal=2;
        obj.selectedupgrade=prod.id;
        //obj.selectedupgradelabel='';
        obj.is_quarterly=true;
        //obj.swicth_zs_id='';
        //obj.signature_key='';
        obj.prodid='';
        obj.prodname='';
        obj.changetype='';
        list<Selectoption> lst = obj.upgradeList1;
        list<Selectoption> lstoption=obj.Addonoptions;
        string AddonDisclaimer = obj.AddonDisclaimer;
        string ProdDisclaimer = obj.ProdDisclaimer;
       string Upgrade_err_msg = obj.Upgrade_err_msg;
       string Upgrade_info_msg = obj.Upgrade_info_msg;
        obj.subscriptionId  = objsub.id;
        obj.contractId = objcontract.id;
       
        obj.selectedprodtoupgrade = prod.id;
        obj.seletedUpgradeProd =prod2.id;
        obj.selectedAddon = prod2.id;
        
        obj.selectedprodtoupgrade = prod.id;
        obj.seletedUpgradeProd =prod2.id;
        
        obj.selectedAddon = prod2.id;
        
        obj.ammendedQuote=quoteRec.id;
        
        
        obj.initPageVars();
        obj.updateUsername();
        obj.updateEmail();
        obj.toggleOptIn();
        obj.cancelSubscription();
        obj.changePassword();
        obj.resetPassword();
       
        obj.updatePayment();
        obj.getShowBundle();
        obj.displayBundle();
        obj.addBundle();
        obj.checklogin();
        
        
        payment_method_sf__c objpaymentmethod = new payment_method_sf__c();
        objpaymentmethod.Payment_Type__c ='Credit Card';
        objpaymentmethod.Account__c = acc.id;
        objpaymentmethod.Credit_Card_Number__c ='4444333322221111';
        objpaymentmethod.Credit_Card_Holder_Name__c ='test';
        objpaymentmethod.Credit_Card_Expiration_Month__c ='09';
        objpaymentmethod.Credit_Card_Expiration_Year__c ='2019';
        objpaymentmethod.Credit_Card_CVV_Code__c ='123';
        objpaymentmethod.Enc_Value__c ='XXXXXXXXXXXXXXXXXXX';
        obj.payment_method=objpaymentmethod;
        obj.Tmethod();
        obj.Tmethod1();
        obj.Tmethod2();
        obj.Tmethod3();
        obj.Tmethod4();
        obj.Tmethod5();
        obj.Tmethod6();
        obj.Tmethod7();
        obj.TogetupgradelistAction();
        obj.prepareAddonOptions();
        obj.prepareDisclaim();
        
        obj.updatepayment();
        obj.showconsentmodal =true;
        obj.isshowoptin= true;
        obj.consent='Yes';
        obj.updateact();
        obj.updateact1();
        
        //test.stoptest();
    }
    /*public static testmethod void testmethod2(){
        
        payment_method_sf__c objpaymentmethod = new payment_method_sf__c();
        objpaymentmethod.Payment_Type__c ='Credit Card';
        objpaymentmethod.Credit_Card_Number__c ='4444333322221111';
        objpaymentmethod.Credit_Card_Holder_Name__c ='test';
        objpaymentmethod.Credit_Card_Expiration_Month__c ='09';
        objpaymentmethod.Credit_Card_Expiration_Year__c ='2019';
        objpaymentmethod.Credit_Card_CVV_Code__c ='123';
        CommunitiesUserCentralControllerNew  obj= new CommunitiesUserCentralControllerNew();
        obj.payment_method=objpaymentmethod;
        obj.updatepayment();
        //obj.updateact();
        
    }*/
    public static testmethod void testmethodtemp(){
         zqu__HostedPageLiteSetting__c objpagesetting = new zqu__HostedPageLiteSetting__c(name='Community Credit Card',zqu__PageId__c='2c92c0f853f45c240154049eb3df20b4',zqu__ComponentName__c='PaymentPage',zqu__PaymentMethodType__c='Credit Card');
        list<zqu__HostedPageLiteSetting__c> lsthost = new list<zqu__HostedPageLiteSetting__c >();
        lsthost =[select id from zqu__HostedPageLiteSetting__c where name='Community Credit Card'] ;
        if(lsthost .size()==0)
        insert objpagesetting;
    Zuora_Integration__c objz = new Zuora_Integration__c();
        objz.name='OrgDefault';
        objz.ACH_Gateway__c='Authorize.net';
        objz.apiAccessKeyId__c='matt.zhu@VV.com';
        objz.apiSecretAccessKey__c='Rad1anc8!';
        objz.Community_Domain__c='https://partial-vectorvestcom.cs18.force.com';
        objz.Endpoint__c='apisandbox-api.zuora.com';
        objz.EUR_Gateway__c='Vantiv - EUR';
        objz.ExternalPaymentId__c='2c92c0f9528db6aa015290834d504e3e';
        objz.GBP_Gateway__c='Vantiv - GBP';
        objz.Internal_Domain__c='https://c.cs18.visual.force.com';
        objz.URI__c='https://apisandbox.zuora.com/apps/PublicHostedPageLite.do';
        objz.Payment_Endpoint__c='apisandbox.zuora.com';
        objz.USD_Gateway__c='Cybersource';
        //insert objZ;
        Account acc = new Account();
            acc.firstname='test acc1';
            acc.lastName = 'test acc';
            acc.billingCity ='Test City';
            acc.billingStreet = 'TestStreet';
            acc.billingstate ='TestState';
            acc.billingPostalcode ='23451';
            acc.billingcountry ='United States';
            Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            acc.recordtypeID=objAccrec.id;
           acc.PersonMailingCity='Test City';
            acc.PersonMailingStreet = 'TestStreet';
            acc.PersonMailingState ='TestState';
            acc.PersonMailingPostalCode ='23451';
            acc.PersonMailingCountry ='United States';
            acc.Account_Currency__c='USD'; 
            //acc.ispersonAccount = true;
            insert acc;
            
            Opportunity opp = new Opportunity(Name=acc.Name + ' Opportunity ' ,
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=acc.Id);
                                       
            Product2 prod = new Product2(Name = 'VectorVest 7 EOD US Monthly', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_MONTHLY' , Family = 'Books');
            insert prod;
            Product2 prod2 = new Product2(Name = 'VectorVest 7 EOD US Annual', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_ANNUAL' , Family = 'Books');
            insert prod2;
            
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            
            CPQCommunity__c cpc = new CPQCommunity__c();
            cpc.Name = 'test';
            cpc.PriceBookId__c = pricebookId ;
            insert cpc ;
            
            
            
            SBQQ__Quote__c quoteRec = new SBQQ__Quote__c();
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SiteUser_Primary_Check__c = true;
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SBQQ__Opportunity2__c = opp.Id;
            //quoteRec.ZSB__BillToContact__c = c.Id;
           // quoteRec.ZSB__SoldToContact__c = c.Id;
            quoteRec.ZSB__BillCycleDay__c =  '1';
            quoteRec.ZSB__BillingBatch__c = 'Batch1';
            insert quoteRec ;
            SBQQ__QuoteLine__c ojQuoteline = new SBQQ__QuoteLine__c();
            ojQuoteline.SBQQ__Quote__c = quoteRec .id;
            ojQuoteline.SBQQ__Product__c = prod.id;
            insert ojQuoteline;
             Contract objContract = new Contract();
             objContract.SBQQ__Quote__c = quoteRec.id;
             objContract.SBQQ__Opportunity__c = opp.id;
             objcontract.Accountid=acc.id;
             insert objContract;
             update objcontract;
             Zuora__CustomerAccount__c objZCustAccount = new Zuora__CustomerAccount__c();
             objZCustAccount .Zuora__Account__c = acc.id;
             objZCustAccount .Zuora__Status__c ='Active';
             objZCustAccount .Zuora__BillToName__c ='test';
             objZCustAccount .Zuora__Zuora_Id__c = '2c92c0f960c105580160d80905a205ea';
             objZCustAccount.Zuora__DefaultPaymentMethod__c ='CreditCard';
             //objZCustAccount .Zuora__CreditCard_Number__c = '4444333322221111';
             //objZCustAccount .Zuora__CreditCard_Expiration__c = '10/2019';
             insert objZCustAccount ;
             Zuora__PaymentMethod__c objZpaymentmethod = new Zuora__PaymentMethod__c();
             objZpaymentmethod .Zuora__CreditCardCountry__c='us';
             objZpaymentmethod .Zuora__CreditCardExpirationMonth__c='10';
             objZpaymentmethod.Zuora__CreditCardExpirationYear__c='2019';
             objZpaymentmethod.Zuora__CreditCardHolderName__c='test';
             objZpaymentmethod.Zuora__CreditCardMaskNumber__c='123';
             objZpaymentmethod.Zuora__DefaultPaymentMethod__c  = true;
             
             objZpaymentmethod.Zuora__BillingAccount__c = objZCustAccount .id;
             insert objZpaymentmethod;
             
             CommunitiesUserCentralControllerNew.testvariable=true;
        CommunitiesUserCentralControllerNew  obj= new CommunitiesUserCentralControllerNew();
        
        
        
        
        payment_method_sf__c objpaymentmethod = new payment_method_sf__c();
        objpaymentmethod.Account__c = acc.id;
        objpaymentmethod.Payment_Type__c ='Credit Card';
        objpaymentmethod.Credit_Card_Number__c ='4444333322221111';
        objpaymentmethod.Credit_Card_Holder_Name__c ='test';
        objpaymentmethod.Credit_Card_Expiration_Month__c ='09';
        objpaymentmethod.Credit_Card_Expiration_Year__c ='2019';
        objpaymentmethod.Credit_Card_CVV_Code__c ='123';
        objpaymentmethod.Enc_Value__c ='XXXXXXXXXXXXXXXXXXX';
        obj.payment_method=objpaymentmethod;
        
        obj.updatepayment();
        
    }
        
   
}