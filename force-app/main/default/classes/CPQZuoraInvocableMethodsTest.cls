@isTest(seealldata=true)
public class CPQZuoraInvocableMethodsTest {
    Public  testmethod  static void testCPQZuoraInvocableMethods(){
       
         Account acc = new Account(PersonMailingCity='Test City',PersonMailingStreet = 'TestStreet',PersonMailingState='TestState',PersonMailingPostalCode ='23451',personMailingCountry ='United States',Primary_Phone_Type__c='Mobile',FirstName='testfirst',LastName='testlast',PersonEmail='testveryunique@test.vvtest.com',Phone='2223334444',VV_Market__c='US',VV_Second_Market__c='CA',VV_Feed__c='ID',VV_Edition__c='Non-Professional',Subscription_Plan__c='Annual',Is_Elite__c=false,Is_Elite_Legacy__c=false,Is_Apple_App_Store_Customer__c=false,BillingStreet='2234 test',BillingCity='Test',BillingState='GA',BillingCountry='USA',BillingPostalCode='22344');
        insert acc;
        
        Account acc1 = new Account(PersonMailingCity='Test City',PersonMailingStreet = 'TestStreet',PersonMailingState='TestState',PersonMailingPostalCode ='23451',personMailingCountry ='United States',Primary_Phone_Type__c='Mobile',FirstName='testfirst',LastName='testlast',PersonEmail='testveryunique@test.vvtest.com',Phone='2223334444',VV_Market__c='US',VV_Second_Market__c='CA',VV_Feed__c='ID',VV_Edition__c='Non-Professional',Subscription_Plan__c='Annual',Is_Elite__c=false,Is_Elite_Legacy__c=false,Is_Apple_App_Store_Customer__c=true,BillingStreet='2234 test',BillingCity='Test',BillingState='GA',BillingCountry='USA',BillingPostalCode='22344');
        insert acc1;
        
        contact c = new contact();
        c.lastname = 'test';
        c.email = 'test@test.com';
        c.AccountId = acc.Id;
        //insert c;
        
        SBQQ__Quote__c sbqqQuote = new SBQQ__Quote__c();
        sbqqQuote.SBQQ__BillingCity__c='Cal';
        insert sbqqQuote;
        
        Opportunity opp = new Opportunity(Name=acc.Name + ' Opportunity ' ,
                                   StageName='Prospecting',
                                   CloseDate=System.today().addMonths(1),
                                   AccountId=acc.Id,Created_By_PortalCustomer__c =true);
        
        insert opp;
        List<Contract> contractList = new List<Contract>();
        Contract con = new COntract();
        con.SBQQ__Opportunity__c = opp.Id;
        con.ZSB__SubscribeToZuora__c = false;
        con.AccountId = acc.Id; 
        con.IsMobile__c = true;
        con.SBQQ__Quote__c = sbqqQuote.Id;
        contractList.add(con);
        Contract con1 = new COntract();
        con1.SBQQ__Opportunity__c = opp.Id;
        con1.ZSB__SubscribeToZuora__c = false;
        con1.AccountId = acc1.Id; 
        con1.IsMobile__c = false;
        con1.SBQQ__Quote__c = sbqqQuote.Id;
        
        contractList.add(con1);
        
        insert contractList;
        
        Zuora__CustomerAccount__c customerAccount = new Zuora__CustomerAccount__c();
        customerAccount.Zuora__Account__c=acc1.Id;
        customerAccount.Zuora__DefaultPaymentMethod__c='other';
        insert customerAccount;
        
        Zuora__CustomerAccount__c customerAccount1 = new Zuora__CustomerAccount__c();
        customerAccount1.Zuora__Account__c=acc.Id;
        customerAccount1.Zuora__DefaultPaymentMethod__c='CreditCard';
        insert customerAccount1;
        Test.StartTest();
        //CPQZuoraInvocableMethods obj = new CPQZuoraInvocableMethods();
        List<ID> contractId = new List<ID>();
        contractId.add(con.Id);
        contractId.add(con1.Id);
        CPQZuoraInvocableMethods.updateAndSyncContract(contractId);
        
        
        Test.StopTest();
    }
}