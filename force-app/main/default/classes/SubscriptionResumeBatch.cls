/**
 * batch class scans through all accounts with suspend end date = today or in two weeks
 * 1. resume these accounts
 * 2. send resume notice email if it is coming up in two weeks
 *
 * need to run at scope 1 at a time
 */
global class SubscriptionResumeBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
	
	String query;
	String errors;	// errors resuming
	Set<Id> cids;	// contact ids to send email on two week notice
	Date twoweeks;	// two weeks from now

	global SubscriptionResumeBatch() {
		twoweeks = Date.today().addDays(14);
		//query = 'Select PersonContactId, Subscription_Suspend_Start_Date__c, Subscription_Suspend_End_Date__c FROM Account WHERE Subscription_Status__c = \'Suspended\' AND (Subscription_Suspend_End_Date__c = TODAY OR Subscription_Suspend_End_Date__c = :twoweeks)';
		cids = new Set<Id>();
		query = 'SELECT SuspendDate__c, ResumeDate__c, Zuora__Zuora_Id__c, Zuora__TermSettingType__c FROM Zuora__Subscription__c WHERE Zuora__Status__c = \'Suspended\' and ResumeDate__c = TODAY';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<Zuora__Subscription__c> zslist) {
		for (Zuora__Subscription__c zs : zslist) {
			String result = zuoraAPIHelper.resumeSubscription(zs.Zuora__Zuora_Id__c, zs.ResumeDate__c, zs.Zuora__TermSettingType__c == 'EVERGREEN' ? false : true);
		}
		
	}
	/*
   	global void execute(Database.BatchableContext BC, List<Account> acclist) {
		for (Account a : acclist) {
			if (a.Subscription_Suspend_End_Date__c == Date.today()) {
				for (Zuora__Subscription__c zs : [SELECT Name,Zuora__Zuora_Id__c,Zuora__TermSettingType__c FROM Zuora__Subscription__c WHERE Zuora__Account__c= :a.Id AND Zuora__Status__c = 'Suspended' ORDER BY CreatedDate DESC]) {
		            String result = zuoraAPIHelper.resumeSubscription(zs.Zuora__Zuora_Id__c, a.Subscription_Suspend_End_Date__c, zs.Zuora__TermSettingType__c == 'EVERGREEN' ? false : true);
		            if (result.containsIgnoreCase('error')) {
		                String error = 'Unable to set resume date for account : ' + a.Id + ', product: ' + zs.Name + ' Reason: ' + result;
		                errors += (errors != null ? '\n' : '') + error;
		            }
		        }
		        a.Subscription_Status__c = 'Active';
		    } else if (a.Subscription_Suspend_End_Date__c == twoweeks && a.Subscription_Suspend_Start_Date__c.daysBetween(a.Subscription_Suspend_End_Date__c) >= 14) {
		    	cids.add(a.PersonContactId);
		    }
			
		}
		update acclist;
	}
	*/
	
	global void finish(Database.BatchableContext BC) {
		/*
		if (errors != null || Test.isRunningTest()) {
			if (Test.isRunningTest()) errors = 'test';
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

		    String[] toAddresses = new String[] {'matt.zhu@cloudwareconnections.com'};
		    mail.setToAddresses(toAddresses);
		    mail.setSubject('resume batch error');
		    mail.setPlainTextBody(errors);
		    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		}
	    
	    EmailTemplate et = [SELECT Id FROM EmailTemplate WHERE Name = 'Two Week Resume Notice'];
        List<Messaging.SingleEmailMessage> emaillist = new List<Messaging.SingleEmailMessage>();

		for (Id cid : cids) {
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	        mail.setTemplateId(et.Id);
	        mail.setTargetObjectId(cid);
	        mail.setSaveAsActivity(false);
	        emaillist.add(mail);
		}
	    
        if (!emaillist.isEmpty()) Messaging.sendEmail(emaillist);

		*/
 
	}
	
}