@isTest
private class LightningTest {
	@isTest static void testLightningCtrls() {
		LightningSelfRegisterController ls = new LightningSelfRegisterController();
		LightningSelfRegisterController.isValidPassword('test','test');
		LightningSelfRegisterController.validatePassword(null,null,null);
		LightningSelfRegisterController.selfRegister();
		LightningSelfRegisterController.getExtraFields(null);
		LightningSelfRegisterController.siteAsContainerEnabled(null);

		LightningForgotPasswordController lf = new LightningForgotPasswordController();
		LightningForgotPasswordController.forgotPassowrd(null,null);

		LightningLoginFormController lc = new LightningLoginFormController();
		LightningLoginFormController.login(null,null,null);
		LightningLoginFormController.getIsUsernamePasswordEnabled();
		LightningLoginFormController.getIsSelfRegistrationEnabled();
		LightningLoginFormController.getSelfRegistrationUrl();
		LightningLoginFormController.getForgotPasswordUrl();
		LightningLoginFormController.getAuthConfig();
	}

}