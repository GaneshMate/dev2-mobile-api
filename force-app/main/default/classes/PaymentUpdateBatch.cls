global class PaymentUpdateBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful{
    
    global final String Query;
    Set<String> SuccessIds = new Set<String>();
    List<PaymentMethod__c> SuccessPaymentList = new List<PaymentMethod__c>();
    global PaymentUpdateBatch(){
        
        this.Query='SELECT PaymentId__c,CreditCardType__c FROM PaymentMethod__c';
        
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<PaymentMethod__c> PaymentMethodList){
        
        for(PaymentMethod__c payments : PaymentMethodList){
            //payment.CreditCardType__c = 'MasterCard';
            //updateList.add(payment);
            system.debug('payments: '+payments);
            Zuora.zObject payment = new Zuora.zObject('PaymentMethod');
            Payment.setValue('Id', payments.PaymentId__c); 
            payment.setValue('CreditCardType', payments.CreditCardType__c);
            system.debug('payment: '+payment);
            
            String errors = '';
            Zuora.zApi.SaveResult[] results = zuoraAPIHelper.updateAPICall(payment);
            system.debug('Check : Update Results: '+results);
            if(results.size()>0){
                for (Zuora.zApi.SaveResult result : results) {
                    if (result.success){
                        SuccessIds.add(result.Id);
                        system.debug('SuccessIds:::::::::::::::::- '+SuccessIds);
                    }
                    else{
                        errors += (errors != '' ? ',' : '') + zuoraAPIHelper.getErrorStr(result.errors);
                        system.debug(' errors '+ errors);
                    }
                }
                
            }  
        }
        List<PaymentMethod__c> PaymentList = new List<PaymentMethod__c>();
        if(test.isRunningTest()){
        	PaymentList =[SELECT PaymentId__c,CreditCardType__c FROM PaymentMethod__c ];    
        }else{
        	PaymentList =[SELECT PaymentId__c,CreditCardType__c FROM PaymentMethod__c where PaymentId__c IN :SuccessIds];   
        }
        
        System.debug('PaymentList----------:'+PaymentList);
        for(PaymentMethod__c paymentMethod : PaymentList){
            if(paymentMethod.PaymentId__c != null || String.isEmpty(paymentMethod.PaymentId__c)){
                paymentMethod.isFlag__c = true;
                SuccessPaymentList.add(paymentMethod);
                System.debug('SuccessPaymentList----------:'+SuccessPaymentList);
            }
            
        }
        if(SuccessPaymentList.size()>0){
            update SuccessPaymentList;
        }
    }
    
    global void finish(Database.BatchableContext BC){
        
        
    }
}