@isTest
public class UpdateByAdminUserInvocableMethodsTest {
     Public  testmethod  static void testUpdateByAdminUserInvocableMethods(){
         Test.StartTest();
           /*Account acc = new Account ();
            acc.Name = 'Test Account';
            insert acc;*/
            Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            //acc.recordtypeID=objAccrec.id;
            Account acc = new Account(Primary_Phone_Type__c='Mobile',PersonMailingCity='Test City',PersonMailingStreet = 'TestStreet',PersonMailingState ='TestState',PersonMailingPostalCode ='23451',PersonMailingCountry ='United States',FirstName='testfirst',LastName='testlast',PersonEmail='u' + System.now().millisecond() + '1@vvsystemtest.com',Phone='2223334444',VV_Market__c='US',VV_Second_Market__c='CA',VV_Feed__c='EOD',VV_Edition__c='Non-Professional',recordtypeID=objAccrec.id,Subscription_Plan__c='Monthly',Is_Elite__c=false,Is_Elite_Legacy__c=false,Is_Apple_App_Store_Customer__c=false,BillingStreet='2234 test',BillingCity='Test',BillingState='GA',BillingCountry='USA',BillingPostalCode='22344');
        insert acc;
            
            contact c = [select id from contact where accountid=:acc.id];
           /* c.lastname = 'test';
            c.email = 'test@test.com';
            c.AccountId = acc.Id;
            insert c;*/
            
            Opportunity opp = new Opportunity(Name=acc.Name + ' Opportunity ' ,
                                       StageName='Prospecting',Created_By_PortalCustomer__c = true,
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=acc.Id);
            
            insert opp;
            
            SBQQ__Quote__c quoteRec = new SBQQ__Quote__c();
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SiteUser_Primary_Check__c = true;
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SBQQ__Opportunity2__c = opp.Id;
            quoteRec.ZSB__BillToContact__c = c.Id;
            quoteRec.ZSB__SoldToContact__c = c.Id;
            quoteRec.ZSB__BillCycleDay__c =  '1';
            quoteRec.ZSB__BillingBatch__c = 'Batch1';
            insert quoteRec ;
            List<String> quoteIds = new List<String> ();
            //quoteIds.add(quoteRec.Id);
            
            UpdateByAdminUserInvocableMethods.updateOppAndQuote(quoteIds);
         Test.StopTest();
     }
}