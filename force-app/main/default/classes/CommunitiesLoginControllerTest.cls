/**
 * An apex page controller that exposes the site login functionality
 */
@IsTest global with sharing class CommunitiesLoginControllerTest {
   /* @IsTest(SeeAllData=true) 
    global static void testCommunitiesLoginController () {
     	CommunitiesLoginController controller = new CommunitiesLoginController();
     	controller.login();    
    }*/  
    
    // @IsTest(SeeAllData=true) 
 	 @IsTest global static void testCommunitiesLoginController() {
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;

        User u = new User();
            u.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id;
            u.LastName = 'Walkar';
            u.Email = 'pWalkar000@amamama.com';
            u.Username = 'pWalkar000@amamama.com' + System.currentTimeMillis();
            u.CompanyName = 'TEST';
            u.Title = 'title';
            u.Alias = 'alias';
            u.TimeZoneSidKey = 'America/Los_Angeles';
            u.EmailEncodingKey = 'UTF-8';
            u.LanguageLocaleKey = 'en_US';
            u.LocaleSidKey = 'en_US';
            u.UserRoleId = r.Id;

        insert u;
        System.debug('********u*****'+u);
        CommunitiesLoginController controller = new CommunitiesLoginController();
        controller.username=u.Username;
        controller.password='abc@123';
     	controller.login(); 
        
    }
}