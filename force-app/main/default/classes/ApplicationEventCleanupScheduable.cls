global class ApplicationEventCleanupScheduable implements Schedulable {
	global void execute(SchedulableContext sc) {
		ApplicationEventCleanupBatchable b = new ApplicationEventCleanupBatchable();
		database.executebatch(b, 1000);
	}
}