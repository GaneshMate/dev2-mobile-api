/**
 * this class handles DML using webservices. because dml calls are not allowed between webservice calls, to prevent errors before calling zuora and not using future, change dml to a webservice call as well under these situations
 */
public with sharing class vvDMLWSManagement {
    static VVCommunitySettings__c cs = VVCommunitySettings__c.getOrgDefaults();
    
    public static String getSessionId() {
        if (Test.isRunningTest()) Test.setMock(HttpCalloutMock.class, new DMLMockImpl());
    
        Http h = new Http();
        String body='grant_type=password&client_id=' + cs.Client_Id__c + '&client_secret=' + cs.Client_Secret__c + '&username=' + cs.Session_Uid__c + '&password=' + cs.Session_Pwd__c + cs.Session_Security_Token__c;
        HttpRequest req = new HttpRequest();
        HTTPResponse res = new HttpResponse();
        req.setEndpoint('https://' + cs.Org_Num__c + '.salesforce.com/services/oauth2/token');
        req.setMethod('POST');
        req.setBody(body);
        try {
            res = h.send(req);
            if (res.getStatusCode() > 204) {
                List<Object> resultlist = (List<Object>)System.JSON.deserializeUntyped(res.getBody());
                Map<String, Object> resultmap = (Map<String, Object>)resultlist[0];
                return 'Error: ' + resultmap.get('message');
            } else {
                Map<String, Object> returnresult = (Map<String, Object>)System.JSON.deserializeUntyped(res.getBody());
                return String.valueOf(returnresult.get('access_token'));
            }

        } catch (System.CalloutException e) {
            return null;
        }
    }
    public static String insertObject(SObject a, String type, String in_sessionId) {

        String sessionId = Userinfo.getSessionId();
        if (sessionId == null) sessionId = in_sessionId;
        if (sessionId == null) sessionId = getSessionId();
        if (sessionId == null || sessionId.containsIgnoreCase('error')) return 'Error: Invalid Session Id';  

        if (Test.isRunningTest()) Test.setMock(HttpCalloutMock.class, new DMLMockImpl());
    
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HTTPResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization','Bearer ' + sessionId);
        req.setEndpoint('https://' + cs.Org_Num__c + '.salesforce.com/services/data/v20.0/sobjects/' + type + '/');
        req.setCompressed(false);
        String JSONString = System.JSON.serialize(a);
        req.setBody(JSONString);

        try {
            res = h.send(req);
            system.debug('body: ' + res.getBody());
            if (res.getStatusCode() > 204) {
            	List<Object> resultlist = (List<Object>)System.JSON.deserializeUntyped(res.getBody());
            	Map<String, Object> resultmap = (Map<String, Object>)resultlist[0];
            	return 'Error: ' + resultmap.get('message');
            } else {
            	Map<String, Object> returnresult = (Map<String, Object>)System.JSON.deserializeUntyped(res.getBody());
            	return String.valueOf(returnresult.get('id'));
            }

        } catch (System.CalloutException e) {
            return  'Error: ' + e;
        }
    }

    public static String updateObject(SObject a, String type, String in_sessionId) {

        String sessionId = Userinfo.getSessionId();
        if (sessionId == null) sessionId = in_sessionId;
        if (sessionId == null) sessionId = getSessionId();
        if (sessionId == null || sessionId.containsIgnoreCase('error')) return 'Error: Invalid Session Id';  
        if (a.Id == null) return 'Error: Invalid object Id';

        if (Test.isRunningTest()) Test.setMock(HttpCalloutMock.class, new DMLMockImpl());
    
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HTTPResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization','Bearer ' + sessionId);
        req.setEndpoint('https://' + cs.Org_Num__c + '.salesforce.com/services/data/v20.0/sobjects/' + type + '/' + a.Id + '?_HttpMethod=PATCH');
        req.setCompressed(false);

        // need to remove id from object post
        a.Id = null;

        String JSONString = System.JSON.serialize(a);
        req.setBody(JSONString);

        try {
            res = h.send(req);
            system.debug('body: ' + res.getBody());
            if (res.getStatusCode() > 204) {
            	List<Object> resultlist = (List<Object>)System.JSON.deserializeUntyped(res.getBody());
            	Map<String, Object> resultmap = (Map<String, Object>)resultlist[0];
            	return 'Error: ' + resultmap.get('message');
            } else {
            	return 'success';
            }

        } catch (System.CalloutException e) {
            return  'Error: ' + e;
        }
    }


    private class DMLMockImpl implements HttpCalloutMock {
        public DMLMockImpl() {}

        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(200);
            resp.setStatus('Complete');
            resp.setBody('{"id":"001xxxxxx","access_token":"12345"}');
            return resp;
        }
    }
}