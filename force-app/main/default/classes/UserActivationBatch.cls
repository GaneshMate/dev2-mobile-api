global class UserActivationBatch implements Database.Batchable<sObject> {
    
    String query;
    
    global UserActivationBatch() {
        if(!test.isrunningtest()){
        query = 'SELECT Id FROM User WHERE IsActive = false AND ProfileId = \'00e36000000EmqV\'';
        }
        else{
            query = 'SELECT Id FROM User WHERE IsActive = false limit 1';
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<User> userlist) {
        for (User u : userlist) {
            u.IsActive = true;
        }
        database.update(userlist, false);
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
}