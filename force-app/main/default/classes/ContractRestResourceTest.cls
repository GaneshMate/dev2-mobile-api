@isTest(seeAllData= true) // need see all data to get zuora wsdl soap testing through
public class ContractRestResourceTest {

    @isTest static void testdoGetEnt_CustomerIdBlank() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/contract';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'GET';
        Test.startTest();
        ContractRestResource.doGet();
        Test.stopTest();
        //System.assertEquals(400, res.statusCode);
    }
     @isTest static void testdoGetAllSubscription() {
        Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer1();
        accountRec = CustomerInfoService.createCustomer(cust);
        System.debug('accountRec-----------'+accountRec);
        String zuora_aid = zuoraAPIHelper.createAccount(accountRec, null);
        SBQQ__Subscription__c subscription = CPQIntegrationAPI.createContractAndSubscription(accountRec,zuora_aid,'Tier 2 Mobile - US Trial',string.valueOf(system.today()),'Draft',12,1,10.9,string.valueOf(system.today()),'Termed','Apple',true);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/contract';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'GET';
        req.addParameter('eid', accountRec.EnterpriseCustomerID__c);
        Test.startTest();
        ContractRestResource.doGet();
        Test.stopTest();
        //System.assertEquals(200, res.statusCode);
    }
    @isTest static void testdoGetNoSubscription() {
        Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer1();
        accountRec = CustomerInfoService.createCustomer(cust);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/contract';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'GET';
        req.addParameter('eid', accountRec.EnterpriseCustomerID__c);
        Test.startTest();
        ContractRestResource.doGet();
        Test.stopTest();
        //System.assertEquals(404, res.statusCode);
    }
    @isTest static void testUpgradeDowngradeRequiredFields() {
        Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer1();
        accountRec = CustomerInfoService.createCustomer(cust);
        String contractNo = TestFactoryData.createContractMock(accountRec);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/contract';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'PUT';
       
        Test.startTest();
        //ContractRestResource.upgradeDowngrade('', string.valueOf(system.today()), 'Termed', 'Tier 2 Mobile - US Trial', string.valueOf(system.today()), 'Draft', 12, 1, 10.9,contractNo,'VV',true);
        ContractRestResource.upgradeDowngradeZuoraSubscription('','','','Tier 2 Mobile - US Trial',5.9,'Month','Month','Month','false','07/08/2019','07/08/2019','RENEW_WITH_SPECIFIC_TERM','Test - crreate subscription with draft mode','false','Draft', 'Termed','',1,1);
        Test.stopTest();
        //System.assertEquals(400, res.statusCode);
    }
    @isTest static void testUpgradeDowngradeNegative() {
        Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer1();
        accountRec = CustomerInfoService.createCustomer(cust);
        String contractNo = TestFactoryData.createContractMock(accountRec);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/contract';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'PUT';
       
        Test.startTest();
        //ContractRestResource.upgradeDowngrade('jkgg2121', string.valueOf(system.today()), 'Termed', 'Tier 2 Mobile - US Trial', string.valueOf(system.today()), 'Draft', 12, 1, 10.9,contractNo,'VV',true);
        ContractRestResource.upgradeDowngradeZuoraSubscription('sdsd','wewewe','','Tier 2 Mobile - US Trial',5.9,'Month','Month','Month','false','07/08/2019','07/08/2019','RENEW_WITH_SPECIFIC_TERM','Test - crreate subscription with draft mode','false','Draft', 'Termed','',1,1);
        Test.stopTest();
        //System.assertEquals(400, res.statusCode);
    }
    //need to check: required account which has active zuora account for this test
    @isTest static void testUpgradeDowngrade() {
        
        Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer1();
        accountRec = CustomerInfoService.createCustomer(cust);
        
        String contractNo = TestFactoryData.createContractMock(accountRec);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/contract';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'PUT';
       
        Test.startTest();
        //ContractRestResource.upgradeDowngrade(accountRec.EnterpriseCustomerID__c,'Tier 2 Mobile - US Trial',string.valueOf(system.today()),'Draft',string.valueOf(system.today()),'Termed',1,1,10.9,zuora_aid);
        //ContractRestResource.upgradeDowngrade(accountRec.EnterpriseCustomerID__c, string.valueOf(system.today()), 'Termed', 'Tier 2 Mobile - US Trial', string.valueOf(system.today()), 'Draft', 12, 1, 10.9, contractNo,'VV',true);
        ContractRestResource.upgradeDowngradeZuoraSubscription('','','','Tier 2 Mobile - US Trial',5.9,'Month','Month','Month','false','07/08/2019','07/08/2019','RENEW_WITH_SPECIFIC_TERM','Test - crreate subscription with draft mode','false','Draft', 'Termed','',1,1);
        Test.stopTest();
        //System.assertEquals(200, res.statusCode);
    }
    @isTest static void testCancelSubscriptionsContractNullCheck(){
        Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer1();
        accountRec = CustomerInfoService.createCustomer(cust);
        
        String zuora_aid = zuoraAPIHelper.createAccount(accountRec, null);
        SBQQ__Subscription__c subscription = CPQIntegrationAPI.createContractAndSubscription(accountRec,zuora_aid,'Tier 2 Mobile - US Trial',string.valueOf(system.today()),'Draft',12,1,10.9,string.valueOf(system.today()),'Termed','Apple',true);
       
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/contract';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'Delete';
        req.addParameter('subscriptionNo', '');
        
        Test.startTest();
        ContractRestResource.cancelSubscriptions();
        Test.stopTest();
        //System.assertEquals(400, res.statusCode);   
    }
    @isTest static void testCancelSubscriptionsNoCustomer(){
        
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/contract';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'Delete';
        req.addParameter('subscriptionNo', '09876790');
        
        Test.startTest();
        ContractRestResource.cancelSubscriptions();
        Test.stopTest();
        //System.assertEquals(404, res.statusCode);   
    }
    //need a scuch a contract which has billing acc ount info,zuora subscription id and which has no cancelled subscription
    @isTest static void testCancelSubscriptions(){
      
        Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer1();
        accountRec = CustomerInfoService.createCustomer(cust);
        String contractNo = TestFactoryData.createContractMock(accountRec);
       
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/contract';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'Delete';
        req.addParameter('subscriptionNo', contractNo);
        
        Test.startTest();
        ContractRestResource.cancelSubscriptions();
        Test.stopTest();
        //System.assertEquals(200, res.statusCode);      
    }
    @isTest static void testCancelSubscriptionsNegative(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/contract';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'Delete';
        req.addParameter('subscriptionNo', '00010563');
        
        Test.startTest();
        ContractRestResource.cancelSubscriptions();
        Test.stopTest();
        //System.assertEquals(500, res.statusCode);   
    }
    /*@isTest static void testCreateContractBlankCheck(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/contract';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
        
        Test.startTest();
        //ContractRestResource.createContract('','2017-12-18','Termed','Tier 2 Mobile - US Trial','','Draft',1,1,10.9,'VV',true);
        ContractRestResource.createZuoraSubscription('','Tier 2 Mobile - US Trial',5.9,'Month','Month','Month','false','07/08/2019','07/08/2019','RENEW_WITH_SPECIFIC_TERM','Test - crreate subscription with draft mode','false','Draft', 'Termed');
        Test.stopTest();
        //System.assertEquals(400, res.statusCode);   
    }
     @isTest static void testCreateContractStatusCheck(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/contract';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
        
        Test.startTest();
        //ContractRestResource.createContract('123125092','2017-12-18','Termed','Tier 2 Mobile - US Trial','2017-12-18','Activated',1,1,10.9,'VV',true);
        ContractRestResource.createZuoraSubscription('','Tier 2 Mobile - US Trial',5.9,'Month','Month','Month','false','07/08/2019','07/08/2019','RENEW_WITH_SPECIFIC_TERM','Test - crreate subscription with draft mode','false','Draft', 'Termed');
         Test.stopTest();
        //System.assertEquals(400, res.statusCode);   
    }
    @isTest static void testCreateContract(){
        Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer1();
        accountRec = CustomerInfoService.createCustomer(cust);
        
        //String zuora_aid = zuoraAPIHelper.createAccount(accountRec, null);
        //Contract contract = CPQIntegrationAPI.createContract(accountRec, zuora_aid, string.valueOf(system.today()), 12, 'Draft', string.valueOf(system.today()), 'Termed')
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/contract';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
        
        Test.startTest();
        //ContractRestResource.createContract(accountRec.EnterpriseCustomerID__c,string.valueOf(system.today()),'Termed','Tier 2 Mobile - US Trial',string.valueOf(system.today()),'Draft',1,1,10.9,'VV',true);
        ContractRestResource.createZuoraSubscription('','Tier 2 Mobile - US Trial',5.9,'Month','Month','Month','false','07/08/2019','07/08/2019','RENEW_WITH_SPECIFIC_TERM','Test - crreate subscription with draft mode','false','Draft', 'Termed');
        Test.stopTest();
        //System.assertEquals(200, res.statusCode);   
    }
   @isTest static void testCreateContractforTaxExempt(){
        Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer1();
        accountRec = CustomerInfoService.createCustomer(cust);
        
        //String zuora_aid = zuoraAPIHelper.createAccount(accountRec, null);
        //Contract contract = CPQIntegrationAPI.createContract(accountRec, zuora_aid, string.valueOf(system.today()), 12, 'Draft', string.valueOf(system.today()), 'Termed')
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/contract';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
        
        Test.startTest();
        //ContractRestResource.createContract('12345',string.valueOf(system.today()),'Termed','Tier 2 Mobile - US Trial',string.valueOf(system.today()),'Draft',1,1,10.9,'VV',true);
       ContractRestResource.createZuoraSubscription('','Tier 2 Mobile - US Trial',5.9,'Month','Month','Month','false','07/08/2019','07/08/2019','RENEW_WITH_SPECIFIC_TERM','Test - crreate subscription with draft mode','false','Draft', 'Termed'); 
       Test.stopTest();
        //System.assertEquals(200, res.statusCode);   
    }*/
    @isTest static void testcancelSubNeg() {
        Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer1();
        accountRec = CustomerInfoService.createCustomer(cust);
        String contractNo = TestFactoryData.createContractMock(accountRec);
        
        contract cont =[SELECT Id, ZSB__SubscriptionId__c FROM Contract WHERE ContractNumber =:contractNo];
         System.debug('****cont*****'+cont);
            cont.ZSB__SubscriptionId__c ='';
        update cont;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/contract';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'PUT';
       
        Test.startTest();
        //ContractRestResource.upgradeDowngrade(accountRec.EnterpriseCustomerID__c, string.valueOf(system.today()), 'Termed', 'Tier 2 Mobile - US Trial', string.valueOf(system.today()), 'Draft', 12, 1, 10.9,contractNo,'VV',true);
        ContractRestResource.upgradeDowngradeZuoraSubscription('','','','Tier 2 Mobile - US Trial',5.9,'Month','Month','Month','false','07/08/2019','07/08/2019','RENEW_WITH_SPECIFIC_TERM','Test - crreate subscription with draft mode','false','Draft', 'Termed','',1,1);
        Test.stopTest();
        //System.assertEquals(400, res.statusCode);
    }
    /*@isTest static void testCreateContractAppleStoreCheck(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/contract';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
        
        Test.startTest();
        //ContractRestResource.createContract('123125092','2017-12-18','Termed','Tier 2 Mobile - US Trial','2017-12-18','Activated',1,1,10.9,'QW',true);
        ContractRestResource.createZuoraSubscription('','Tier 2 Mobile - US Trial',5.9,'Month','Month','Month','false','07/08/2019','07/08/2019','RENEW_WITH_SPECIFIC_TERM','Test - crreate subscription with draft mode','false','Draft', 'Termed');
        Test.stopTest();
        //System.assertEquals(400, res.statusCode);   
    }
    @isTest static void testCreateContractIsMobileAutoConvertCheck(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/contract';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
        
        Test.startTest();
        //ContractRestResource.createContract('123125092','2017-12-18','Termed','Tier 2 Mobile - US Trial','2017-12-18','Activated',1,1,10.9,'Apple',null);
        ContractRestResource.createZuoraSubscription('','Tier 2 Mobile - US Trial',5.9,'Month','Month','Month','false','07/08/2019','07/08/2019','RENEW_WITH_SPECIFIC_TERM','Test - crreate subscription with draft mode','false','Draft', 'Termed');
        Test.stopTest();
        //System.assertEquals(400, res.statusCode);   
    }*/
    /*@isTest static void testCreateContract(){
        Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer1();
        accountRec = CustomerInfoService.createCustomer(cust);
        
        //String zuora_aid = zuoraAPIHelper.createAccount(accountRec, null);
        //Contract contract = CPQIntegrationAPI.createContract(accountRec, zuora_aid, string.valueOf(system.today()), 12, 'Draft', string.valueOf(system.today()), 'Termed')
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/contract';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
        
        Test.startTest();
        //ContractRestResource.createContract(accountRec.EnterpriseCustomerID__c,string.valueOf(system.today()),'Termed','Tier 2 Mobile - US Trial',string.valueOf(system.today()),'Draft',1,1,10.9,'VV',true);
        ContractRestResource.createZuoraSubscription(accountRec.EnterpriseCustomerID__c,'Tier 2 Mobile - US Trial',5.9,'Month','Month','Month','false','07/08/2019','07/08/2019','RENEW_WITH_SPECIFIC_TERM','Test - crreate subscription with draft mode','false','Draft', 'Termed');
        Test.stopTest();
        //System.assertEquals(200, res.statusCode);   
    }*/
    @isTest static void testCreateContract(){
        
        //String JsonStr='{"eid": "11044063","prodName":"Tier 2 Mobile - US Trial","netPrice":6.8,"RenewalTermPeriodType": "Month","InitialTermPeriodType": "Month","CurrentTermPeriodType": "Month","AutoRenew": false,"TermStartDate": "2019-07-05","ContractEffectiveDate": "2019-07-05","Notes": "Test - crreate subscription with draft mode","IsInvoiceSeparate": false,"Status": "Draft","TermType": "Termed","RenewalTerm":"","InitialTerm":1,"Quantity":1}';
        Account accountRec = new Account();
        CustomerInfoService.CustomerInfo cust = new CustomerInfoService.CustomerInfo ();
        cust = TestFactoryData.createCustomer1();
        accountRec = CustomerInfoService.createCustomer(cust);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/contract';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
        //req.requestBody = Blob.valueof(JsonStr);
        Test.startTest();
        ContractRestResource.createZuoraSubscription(accountRec.EnterpriseCustomerID__c,'Tier 2 Mobile - US Trial',5.9,'Month','Month','Month','false','07/08/2019','07/08/2019','RENEW_WITH_SPECIFIC_TERM','Test - crreate subscription with draft mode','false','Draft', 'Termed','',1,1);
        Test.stopTest();
       /* Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        ContractRestResource.createZuoraSubscription('','Tier 2 Mobile - US Trial',5.9,'Month','Month','Month','false','07/08/2019','07/08/2019','RENEW_WITH_SPECIFIC_TERM','Test - crreate subscription with draft mode','false','Draft', 'Termed','',1,1);
        Test.stopTest();*/
        
    }
     @isTest static void testCreateContract1(){
        
        //String JsonStr='{"eid": "11044063","prodName":"Tier 2 Mobile - US Trial","netPrice":6.8,"RenewalTermPeriodType": "Month","InitialTermPeriodType": "Month","CurrentTermPeriodType": "Month","AutoRenew": false,"TermStartDate": "2019-07-05","ContractEffectiveDate": "2019-07-05","Notes": "Test - crreate subscription with draft mode","IsInvoiceSeparate": false,"Status": "Draft","TermType": "Termed","RenewalTerm":"","InitialTerm":1,"Quantity":1}';
        List<Zuora__CustomerAccount__c>  zuoraCust = [SELECT Zuora__Zuora_Id__c,Zuora__Default_Payment_Method__c,Zuora__DefaultPaymentMethod__c,Zuora__Account__c FROM Zuora__CustomerAccount__c WHERE Zuora__Zuora_Id__c !=null and Zuora__DefaultPaymentMethod__c =: 'Other' Limit 1];
         List<Account> acclist = [SELECT EnterpriseCustomerID__c from account where id =:zuoraCust[0].Zuora__Account__c];
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/contract';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
        //req.requestBody = Blob.valueof(JsonStr);
        Test.startTest();
        ContractRestResource.createZuoraSubscription(acclist[0].EnterpriseCustomerID__c,'Tier 2 Mobile - US Trial',5.9,'Month','Month','Month','false','07/08/2019','07/08/2019','RENEW_WITH_SPECIFIC_TERM','Test - crreate subscription with draft mode','false','Draft', 'Termed','',1,1);
        Test.stopTest();
       /* Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        ContractRestResource.createZuoraSubscription('','Tier 2 Mobile - US Trial',5.9,'Month','Month','Month','false','07/08/2019','07/08/2019','RENEW_WITH_SPECIFIC_TERM','Test - crreate subscription with draft mode','false','Draft', 'Termed','',1,1);
        Test.stopTest();*/
        
    }
    @isTest static void testdoGetAllSubscription1() {
        
        Zuora__SubscriptionProductCharge__c zuoraSub=[SELECT Name,Zuora__Account__c FROM Zuora__SubscriptionProductCharge__c limit 1];
        Account acc=[SELECT EnterpriseCustomerID__c FROM Account where id =:zuoraSub.Zuora__Account__c limit 1];
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/contract';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'GET';
        req.addParameter('eid', acc.EnterpriseCustomerID__c);
        Test.startTest();
        ContractRestResource.doGet();
        Test.stopTest();
        //System.assertEquals(200, res.statusCode);
    }
}