@istest
global class MockHttpResponseGenerator implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest req) {
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"eid": "11044063","prodName":"Tier 2 Mobile - US Trial","netPrice":6.8,"RenewalTermPeriodType": "Month","InitialTermPeriodType": "Month","CurrentTermPeriodType": "Month","AutoRenew": false,"TermStartDate": "2019-07-05","ContractEffectiveDate": "2019-07-05","Notes": "Test - crreate subscription with draft mode","IsInvoiceSeparate": false,"Status": "Draft","TermType": "Termed","RenewalTerm":"","InitialTerm":1,"Quantity":1}');
        
        res.setStatusCode(200);
        return res;
    }
}