global class ApplicationEventCleanupBatchable implements Database.Batchable<sObject> {
	
	String query = 'select Id from Application_Event__c where Handled__c=true';
	
	global ApplicationEventCleanupBatchable() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		delete scope;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}