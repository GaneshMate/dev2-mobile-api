@isTest
public class HttpRestTest {
    //  --------------------------------------------------------------
    //  TEST METHODS - MOCKS
    //  --------------------------------------------------------------- 
    public class authenticateByUserNamePasswordMock implements HttpCalloutMock {
         
        Boolean isMockResponseSuccessful;
         
        public authenticateByUserNamePasswordMock(Boolean isMockResponseSuccessful) {
            this.isMockResponseSuccessful   = isMockResponseSuccessful;
        }
         
        public HttpResponse respond(HttpRequest rqst) {
            HttpResponse hResp      = new HttpResponse();
            if (this.isMockResponseSuccessful) {
                hResp.setStatusCode(200);
                hResp.setBody('{' +         // data copied from SFDC example
                                ' "id":"https://login.salesforce.com/id/00Dx0000000BV7z/005x00000012Q9P", ' +
                                ' "issued_at":"1278448832702",' +
                                ' "instance_url": "https://na1.salesforce.com",' +
                                ' "signature":"0CmxinZir53Yex7nE0TD+zMpvIWYGb/bdJh6XfOH6EQ=",' +
                                ' "access_token": "00Dx0000000BV7z!AR8AQAxo9UfVkh8AlV0Gomt9Czx9LjHnSSpwBMmbRcgKFmxOtvxjTrKW19ye6PE3Ds1eQz3z8jr3W7_VbWmEu4Q8TVGSTHxs"'
                            +'}');          
            }
            else {
                hResp.setStatusCode(400);
                hResp.setStatus('Bad request');
            }
            return hResp;
        }
    }
     
    public class doSoqlQueryMock implements HttpCalloutMock {
         
        Boolean isMockResponseSuccessful;
         
        public doSoqlQueryMock(Boolean isMockResponseSuccessful) {
            this.isMockResponseSuccessful   = isMockResponseSuccessful;
        }
         
        public HttpResponse respond(HttpRequest rqst) {
            HttpResponse hResp      = new HttpResponse();
            if (this.isMockResponseSuccessful) {
                hResp.setStatusCode(200);
                hResp.setBody('{' +         // data copied from SFDC example
                                ' "totalSize": 2,'      + 
                                ' "done"    : true,'    +
                                ' "records" : ['        +
                                '              {    "attributes"    : {' +
                                '                                       "type"          : "Account",' +
                                '                                       "url"           : "/services/data/v29.0/sobjects/Account/00id"' +
                                '                                   },' +
                                '               "ID"    : "00id"' +
                                '               },' +
                                '              {    "attributes"    : {' +
                                '                                       "type"          : "Account",' +
                                '                                       "url"           : "/services/data/v29.0/sobjects/Account/01id"' +
                                '                                   },' +
                                '               "ID"    : "01id"' +
                                '               }' +
                                '           ]' +
                                '}');           
            }
            else {
                hResp.setStatusCode(404);
                hResp.setStatus('Not found');
            }
            return hResp;
        }
    }
     
     
    //  --------------------------------------------------------------
    //  TEST METHODS - tests
    //  --------------------------------------------------------------- 
    @isTest
    private static void testAuthenticateByUserNamePassword() {
        HttpRest hr = new HttpRest();
        try {
            Test.setMock(HttpCalloutMock.class, new authenticateByUserNamePasswordMock(true));  
            hr.authenticateByUserNamePassword('somekey','somesecret','someusername@somedomain','somepw+token',false);
            //System.assertEquals('00Dx0000000BV7z!AR8AQAxo9UfVkh8AlV0Gomt9Czx9LjHnSSpwBMmbRcgKFmxOtvxjTrKW19ye6PE3Ds1eQz3z8jr3W7_VbWmEu4Q8TVGSTHxs',hr.accessToken);
            //System.assertEquals('https://na1.salesforce.com',hr.sfdcInstanceUrl);
        }
        catch (Exception e) {System.assert(false,'shouldnt. Exception:' + e.getMessage() + e.getStackTraceString());}
        try {
            Test.setMock(HttpCalloutMock.class, new authenticateByUserNamePasswordMock(false));
            hr.authenticateByUserNamePassword('somekey','somesecret','someusername@somedomain','somepw+token',false);
            System.assert(false,'shouldnt happen, mock should produce error');
        }
        catch (Exception e) {System.assert(e.getMessage().contains('[HTTP-01]'),e.getMessage());}
    }
 
    @isTest
    private static void testDoSoqlQuery() {
        HttpRest hr = new HttpRest();
        List<Sobject> sobjResList;
        try {
            Test.setMock(HttpCalloutMock.class, new doSoqlQueryMock(true)); 
            sObjResList = hr.doSoqlQuery('select id from Account');
            System.assertEquals(2,sObjResList.size());
        }
        catch (Exception e) {System.assert(false,'shouldnt. Exception:' + e.getMessage() + e.getStackTraceString());}
        try {
            Test.setMock(HttpCalloutMock.class, new doSoqlQueryMock(false));
            sObjResList = hr.doSoqlQuery('select id from Account');
            System.assert(false,'shouldnt happen, mock should produce error');
        }
        catch (Exception e) {System.assert(e.getMessage().contains('[HTTP-02]'),e.getMessage());}
    }
 @isTest
 Private static void testdoRecordUpdate(){
     HttpRest hr = new HttpRest();
     try{
         //Test.setMock(HttpCalloutMock.class, new doRecordUpdate(true)); 
         hr.doRecordUpdate('SomeApiname','SomeID','Somefield');
     }
     catch(Exception e){}
 }
 
}