@RestResource(urlMapping='/invoice/*')
global with sharing class InvoiceRestResource {

    global class Error {
        global integer code;
        global string msg;
        global Error(integer code, string msg) {
            this.code = code;
            this.msg = msg;
        }
    }
    
    global class Payment {
        global String InvoiceNumber; 
        global String AccountId; 
        global String EffectiveDate; 
        global decimal AppliedInvoiceAmount; 
        global decimal AppliedCreditBalanceAmount; 
        global decimal Amount; 
        global String Type; 
        global String Status; 
        global String PaymentMethodId; 
        global String Comment; 
        global String ReferenceId; 
    }
    private static string serialize(object o) {
        return JSON.serialize(o);
    }
    private static string ID_TYPE_APP_USER_ID  = 'aid';    // Application User Id
    private static string ID_TYPE_ENT_CUST_ID  = 'eid';    // Enterprise Customer Id
    private static string Email  = 'email';
    @HttpGet
    global static void getUnpaidInvoices() {

        integer retCode = 0;
        string retBody = '';

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        integer idcnt = 0;
        string aid = req.params.get(ID_TYPE_APP_USER_ID);   if (!string.isBlank(aid)) idcnt++;
        string eid = req.params.get(ID_TYPE_ENT_CUST_ID);   if (!string.isBlank(eid)) idcnt++;
        string email = req.params.get(Email);           if (!string.isBlank(email)) idcnt++;
        System.debug('***aid***'+aid);
        System.debug('***eid***'+eid);
        System.debug('***email***'+email);
        if (idcnt == 0) {
            retCode = 400;
            retBody = serialize(new Error(retCode, 'No customer identifier found'));
        } else if (idcnt > 1) {
            retCode = 400;
            retBody = serialize(new Error(retCode, 'Multiple customer identifiers found'));
        } else {
            List<Zuora__ZInvoice__c> invoiceList = new List<Zuora__ZInvoice__c>();
            try {
                if (null != aid) invoiceList = getInvoicesByAid(aid);
                else if (null != eid) invoiceList = getInvoicesByEid(eid);
                else if (null != email) invoiceList = getInvoicesByEmail(email); 
                System.debug('*****invoiceList****'+invoiceList);
                if (invoiceList.isEmpty() || invoiceList==null) {
                    // user not found
                    retCode = 404;
                    retBody = serialize(new Error(retCode, 'Invoices are not found'));
                } else {
                    
                    retCode = 200;
                    retBody = serialize(invoiceList);
                }
            } catch (Exception e) {
                retCode = 500;
                retBody = serialize(new Error(retCode, 'Error retrieving Invoices [' + e.getMessage() + ']'));
            }
        }

        res.headers.put('Content-type', 'application/json');
        res.statusCode = retCode;
        res.responseBody = Blob.valueOf(retBody);
    }
    
    private static List<Zuora__ZInvoice__c> getInvoicesByAid(String aid){
        List<Zuora__ZInvoice__c> invoiceList = new List<Zuora__ZInvoice__c>();
        System.debug('****in getInvoicesByAid*****');
        String accId = getAccountInfo(aid ,'aid');
        System.debug('****accId*****'+accId);
        
        if(!String.isBlank(accId))
        invoiceList = getAllInvoicesFromAccount(accId);
        return invoiceList;
    }
    private static List<Zuora__ZInvoice__c> getInvoicesByEid(String eid){
        List<Zuora__ZInvoice__c> invoiceList = new List<Zuora__ZInvoice__c>();
        String accId = getAccountInfo(eid,'eid');
        System.debug('*****accId***'+accId);
        if(!String.isBlank(accId))
        invoiceList = getAllInvoicesFromAccount(accId);
        System.debug('*****invoiceList***'+invoiceList);
        return invoiceList;
    }
    private static List<Zuora__ZInvoice__c> getInvoicesByEmail(String email){
        List<Zuora__ZInvoice__c> invoiceList = new List<Zuora__ZInvoice__c>();
        String accId = getAccountInfo(email,'email');
        if(!String.isBlank(accId))
        invoiceList = getAllInvoicesFromAccount(accId);
        return invoiceList;
    }
    private static String getAccountInfo(String accInfo ,String parameterType){
        List<Account> accountList = new List<Account>();
        String query = '';
        if(parameterType.equals('aid')){
             query= 'SELECT Id FROM Account WHERE ApplicationUserID__c=\''+accInfo+'\'';  
           
        }
        if(parameterType.equals('eid')){
             query= 'SELECT Id FROM Account WHERE EnterpriseCustomerID__c=\''+accInfo+'\'';    
        }
        if(parameterType.equals('email')){
             query= 'SELECT Id FROM Account WHERE PersonEmail=\''+accInfo+'\'';
        }
       
        System.debug('query=========>'+query);
       // accountList = [SELECT Id FROM Account WHERE ApplicationUserID__c=:accInfo OR EnterpriseCustomerID__c=:accInfo OR PersonEmail=:accInfo ORDER BY CreatedDate DESC LIMIT 1];
        if(!String.isBlank(query)){
            query +=' '+'Limit 49000';
            accountList = Database.query(query);    
        }
             
        
        if(accountList.size()==0)return null;
        return accountList[0].id;
    }
    private static List<Zuora__ZInvoice__c> getAllInvoicesFromAccount(String accId){
        List<Zuora__ZInvoice__c> invoiceList = new List<Zuora__ZInvoice__c>();
        invoiceList = [SELECT   Id,
                                Name,
                                Zuora__Status__c,
                                Zuora__Balance2__c,
                                Zuora__InvoiceDate__c,
                                Zuora__TargetDate__c,
                                Zuora__PostedDate__c,
                                Zuora__DueDate__c,
                                Zuora__PaymentAmount__c,
                                Zuora__RefundedAmount__c,
                                Zuora__AdjustmentAmount__c,
                                Zuora__TaxExemptAmount__c,
                                Zuora__TaxAmount__c,
                                Zuora__TotalAmount__c,
                                Zuora__Account__c,
                                Zuora__BillingAccount__r.Zuora__Zuora_Id__c,
                                Zuora__BillingAccount__r.Zuora__DefaultPaymentMethod__c 
                       FROM Zuora__ZInvoice__c WHERE Zuora__Account__c=:accId AND Zuora__Balance2__c != 0 AND Zuora__BillingAccount__r.Zuora__DefaultPaymentMethod__c='Other' LIMIT 49000];
        return invoiceList;
    }
    
    @HttpPost
    global static void createPaymentAgainstInvoice() {
        integer retCode = 0;
        string retBody = '';
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        Payment pay =(Payment) System.JSON.deserialize(body.toString(), Payment.class);
        System.debug('***pay****'+pay);  
        if(!String.isBlank(String.valueOf(pay.InvoiceNumber)) &&!String.isBlank(String.valueOf(pay.AccountId))&& !String.isBlank(String.valueOf(pay.EffectiveDate))&& !String.isBlank(String.valueOf(pay.AppliedInvoiceAmount))&& !String.isBlank(String.valueOf(pay.AppliedCreditBalanceAmount))&& !String.isBlank(String.valueOf(pay.Amount))&& !String.isBlank(String.valueOf(pay.Type))&& !String.isBlank(String.valueOf(pay.Status))&& !String.isBlank(String.valueOf(pay.PaymentMethodId))){
            String result = createPayment(pay.InvoiceNumber,pay.AccountId,pay.EffectiveDate,pay.AppliedInvoiceAmount,pay.AppliedCreditBalanceAmount,pay.Amount,pay.Type,pay.Status,pay.PaymentMethodId,pay.Comment,pay.ReferenceId);    
            System.debug('****createPayment result****'+result);
            if(result.contains('Error')){
                retCode = 500;
                retBody = serialize(new Error(retCode, 'Error in Create Payment [' + result + ']')); 
            }else{
                retCode = 200;  
                retBody = serialize(new Error(retCode,'Success'));   
            }
        }else{
            retCode = 500;
            retBody = serialize(new Error(retCode, 'Error: All Fields Are Mandatory Except Comment And ReferenceId!' ));
            System.debug('****retCode****'+retCode);
           
        }
        res.headers.put('Content-type', 'application/json');
        res.statusCode = retCode;
        res.responseBody = Blob.valueOf(retBody);
        System.debug('****retCode****'+retCode);
    }
     public static String createPayment(String InvoiceNumber,String AccountId,String EffectiveDate,Decimal AppliedInvoiceAmount,Decimal AppliedCreditBalanceAmount,Decimal Amount,String Type,String Status,String PaymentMethodId,String Comment,String ReferenceId) {
        
        Zuora.zObject payment = new Zuora.zObject('Payment');
        Payment.setValue('InvoiceNumber', InvoiceNumber); 
        payment.setValue('AccountId', AccountId);
        payment.setValue('EffectiveDate',EffectiveDate); 
        payment.setValue('AppliedInvoiceAmount', AppliedInvoiceAmount);
        payment.setValue('AppliedCreditBalanceAmount', AppliedCreditBalanceAmount); 
        payment.setValue('Amount', Amount);
        payment.setValue('Type', Type);
        payment.setValue('Status', Status);
        payment.setValue('PaymentMethodId', PaymentMethodId); 
        payment.setValue('Comment', Comment);
        payment.setValue('ReferenceId', ReferenceId);    
        
        String errors = '';
        Zuora.zApi.SaveResult[] results = zuoraAPIHelper.createAPICall(payment);
        system.debug('Check : Account Creation Results: '+results);
            
        for (Zuora.zApi.SaveResult result : results) {
            if (result.success) return result.Id;
            else errors += (errors != '' ? ',' : '') + zuoraAPIHelper.getErrorStr(result.errors);
        }

        return errors;
        
     }
    
}