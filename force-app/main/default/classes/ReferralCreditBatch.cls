/**
 * goes through all account with referred_by field filled in and based on conversion date, give referrer credit by adding REFERRAL coupon to the subscription
 *
 * batch can only be run 1 at a time to prevent callout exception
 */
global class ReferralCreditBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
	
	String query;
	zqu__ProductRatePlan__c prp_monthly;
	zqu__ProductRatePlan__c prp_annual;
	zqu__ProductRatePlan__c prp_quarterly;
	
	global ReferralCreditBatch() {
		// find accounts with referral id and not cancelled and conversion date = today AND referer is not cancelled or suspended either
		query = 'SELECT Conversion_Date__c, Subscription_Status__c, Referred_By__c, Referred_By__r.Account_Currency__c, Referred_By__r.Conversion_Date__c, Referred_By__r.Subscription_Status__c, IsPersonAccount ' 
			+ 'FROM Account WHERE Referred_By__c != null AND Conversion_Date__c = TODAY ' 
			+ 'AND Subscription_Status__c = \'Active\''
			+ 'AND Referred_By__r.Subscription_Status__c = \'Active\'';

		if (Test.isRunningTest()) query += ' AND FirstName = \'referraltestingaccount\' LIMIT 1';

		// populate the referral coupon prp, don't have account currency so keep it at USD for now
		prp_monthly = vvSubscriptionManagement.getDiscountPlan(vvConstants.REFERRAL_MONTHLY, null);
		prp_annual = vvSubscriptionManagement.getDiscountPlan(vvConstants.REFERRAL_ANNUAL, null);
		prp_quarterly = vvSubscriptionManagement.getDiscountPlan(vvConstants.REFERRAL_QUARTERLY, null);
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Account> acclist) {
   		Account a = acclist[0];
   		system.debug('###a: ' + a);
   		String primarysub = 'Primary:%';
   		List<Zuora__Subscription__c> zslist = [SELECT Zuora__CustomerAccount__c, Zuora__TermSettingType__c, Zuora__Zuora_Id__c FROM Zuora__Subscription__c WHERE Name LIKE :primarysub AND Zuora__Status__c = 'Active' AND Zuora__Account__c = :a.Referred_By__c];
   		system.debug('###zslist: ' + zslist);
   		if (!zslist.isEmpty()) {

   			zqu__ProductRatePlan__c prp = null;
   			String plantype = vvSubscriptionManagement.getCurrentPlanType(zslist[0]);
   			if (plantype == 'Monthly') prp = prp_monthly;
   			else if (plantype == 'Quarterly') prp = prp_quarterly;
   			else prp = prp_annual;

   			// foreign currency
   			if (prp.zqu__R00N40000001mFVKEA2__r[0].zqu__Model__c != 'Discount-Percentage')
   				prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c = vvCurrencyManagement.convertFromUSD(prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c, a.Referred_By__r.Account_Currency__c);
   			
   			String result = zuoraAmendHelper.addDiscountToSubscription(zslist[0].Zuora__Zuora_Id__c, prp, a.Referred_By__r.Conversion_Date__c, 1, null);
			system.debug('###' + result);
			if (result.containsIgnoreCase('success')) zuoraAPIHelper.ondemandSync(zslist[0].Zuora__CustomerAccount__c);
   		}
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}