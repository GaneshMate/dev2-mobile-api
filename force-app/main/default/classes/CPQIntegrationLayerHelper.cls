public without sharing class CPQIntegrationLayerHelper{
    public string zAccId {get;set;}
    public Account sfAcc {get;set;}
    public String sfQuoteId {get;set;}
    public String sfOpportunityId {get;set;}
    public boolean isExistingCustomer =false;
    
    private CPQIntegrationLayerHelper(){}
    public CPQIntegrationLayerHelper(String zoura_AccID,Account acc)
    {
            zAccId = zoura_AccID;
            sfAcc = acc;
    }
    /*-----------------------Property to get Configured CPQ PriceBookId-----------------------*/
    public static string cpqPriceBookId{
        get{
            if(String.isEmpty(cpqPriceBookId))
            {
                List<CPQCommunity__c> cpqCommunity = [select PriceBookId__c from CPQCommunity__c]; 
                cpqPriceBookId = (cpqCommunity.size()>0?cpqCommunity[0].PriceBookId__c:'');
            }
            return cpqPriceBookId;
        }
        set {
            cpqPriceBookId = value;
        }
    }
    
    
    /*--------------Method to Get Product Info using Product ID-----------------*/
    public Product2 getProductInfo(String prodId)
    {
        CPQAPIClassDefinitions.ProductLoadContext plc = new CPQAPIClassDefinitions.ProductLoadContext(); 
        plc.pricebookId = CPQIntegrationLayerHelper.cpqPriceBookId; 
 
        String productJson = SBQQ.ServiceRouter.load('SBQQ.ProductAPI.ProductLoader', prodId, JSON.serialize(plc)); 
        CPQAPIClassDefinitions.ProductModel pmodel = (CPQAPIClassDefinitions.ProductModel)JSON.deserialize(productJson, CPQAPIClassDefinitions.ProductModel.class); 
        
        return pmodel.record;
    }
    
    public void createOppAndQuote(String accountID,String AccountName)
    {
        Opportunity opp = createOpportunity(accountID,AccountName);
        SBQQ__Quote__c newQuote = createQuote(opp.id);
        
        sfQuoteId = newQuote.id;
        sfOpportunityId = opp.id;
    }
    
    public string createContract(String accountID,String AccountName,List<String> productIdLists,Map<string,string> productOption,Map<string,string> SMProducts)
    {
        system.debug('productIdLists##########'+productIdLists);
        Opportunity opp = createOpportunity(accountID,AccountName);
        sfOpportunityId = opp.id;
        SBQQ__Quote__c newQuote = createQuote(opp.id);
        sfQuoteId = newQuote.id;
        
        system.debug('productOption###'+productOption);
        system.debug('SMProducts###'+SMProducts);
        //CPQIntegrationLayerHelper.updateQuoteWithProductsAndSaveAsync(newQuote.id ,productIdLists,productOption,SMProducts);
       //CPQIntegrationLayerHelper.updateQuoteWithProductsAndSave(newQuote.id ,productIdLists,productOption);
        System.debug('Quote ID---->>'+newQuote.id);
        return '';
    }
    
    public void createQuoteAndOpportunity(String accountID,String AccountName)
    {
        Opportunity opp = createOpportunity(accountID,AccountName);
        sfOpportunityId = opp.id;
        SBQQ__Quote__c newQuote = createQuote(opp.id);
        sfQuoteId = newQuote.id;        
    }
    
    public string createContract(String accountID,String AccountName,string promoQuoteId)
    {
        system.debug('createcontract');
        system.debug('promoQuoteIddd'+promoQuoteId);
        Opportunity opp = createOpportunity(accountID,AccountName); 
        sfOpportunityId = opp.id;
        if (promoQuoteId != null && promoQuoteId!='' ) {
            SBQQ__Quote__c newQuote  = cloneQuote(promoQuoteId);
            sfQuoteId = newQuote.id;           
        }
        return '';   
    }   
    public SBQQ__Quote__c cloneQuote(String promoQuoteId)
    {
        Account tempAccRec = [select id,Firstname,Lastname,PersonContactId,BillingCountry,BillingStreet,BillingCity,BillingPostalcode,Billingstate,shippingstreet,shippingstate,shippingcity,shippingcountry,shippingpostalcode from Account where id=:sfAcc.id ];
        List<Campaign> campaignlist = new list<Campaign>();
        campaignlist = [SELECT Id,PromoCode__c,Secondarysources__c FROM Campaign WHERE Quote__c =:promoQuoteId limit 1];   
        Map<string,Object> allFieldsToUpdate = new Map<string,Object>();
        if(campaignlist.size()>0){
            allFieldsToUpdate.put('Promo_code__c',campaignlist[0].PromoCode__c);
            allFieldsToUpdate.put('SecondaryCampaignsource__c',campaignlist[0].Secondarysources__c);
        }
        System.debug('tempAccRec.ShippingCity == ' + tempAccRec.ShippingCity);
        System.debug('tempAccRec.ShippingCountry == ' + tempAccRec.ShippingCountry);
        allFieldsToUpdate.put('SBQQ__Account__c',sfAcc.id);
        allFieldsToUpdate.put('SBQQ__Opportunity2__c',sfOpportunityId);
        allFieldsToUpdate.put('SBQQ__Primary__c',false);
        allFieldsToUpdate.put('ZSB__ZuoraBillingAccountId__c',zAccId);
        allFieldsToUpdate.put('ZSB__SoldToContact__c',tempAccRec.PersonContactId);
        allFieldsToUpdate.put('ZSB__BillToContact__c',tempAccRec.PersonContactId);
        allFieldsToUpdate.put('ZSB__BillingBatch__c','Batch1');
        allFieldsToUpdate.put('ZSB__TaxExemptStatus__c','No');
        allFieldsToUpdate.put('ZSB__BillCycleDay__c','1');
        allFieldsToUpdate.put('ZSB__BillingAccount__c',null);
        //allFieldsToUpdate.put('Custom_Billing_Account__c',null);
        allFieldsToUpdate.put('SBQQ__BillingCountry__c',tempAccRec.BillingCountry);
        allFieldsToUpdate.put('SBQQ__BillingStreet__c',tempAccRec.BillingStreet);
        allFieldsToUpdate.put('SBQQ__BillingState__c',tempAccRec.BillingState);
        allFieldsToUpdate.put('SBQQ__BillingCity__c',tempAccRec.BillingCity);
        allFieldsToUpdate.put('SBQQ__BillingPostalCode__c',tempAccRec.Billingpostalcode);
        allFieldsToUpdate.put('SBQQ__ShippingCountry__c',tempAccRec.ShippingCountry);
        allFieldsToUpdate.put('SBQQ__ShippingStreet__c',tempAccRec.ShippingStreet);
        allFieldsToUpdate.put('SBQQ__ShippingState__c',tempAccRec.ShippingState);
        allFieldsToUpdate.put('SBQQ__ShippingCity__c',tempAccRec.shippingCity);
        allFieldsToUpdate.put('SBQQ__ShippingPostalCode__c',tempAccRec.shippingpostalcode);
        allFieldsToUpdate.put('SBQQ__ShippingName__c',tempAccRec.Firstname+' '+tempAccRec.Lastname);
        allFieldsToUpdate.put('SBQQ__BillingName__c',tempAccRec.Firstname+' '+tempAccRec.Lastname);
        
        
        CloneQuote cQ = new CloneQuote(promoQuoteId,allFieldsToUpdate,true);
        SBQQ__Quote__c newQuote = (SBQQ__Quote__c)cQ.doIt();
        syncQuoteToOpportunity(newQuote);        
        return newQuote;        
    }
        
    /*-------------- Method to Create Opportunity-----------------*/
    public Opportunity createOpportunity(String AccountId,String AccountName)
    {
        Opportunity opp = new Opportunity();
        //existing Customer check
        system.debug('IsexistingCustomerrrr'+IsexistingCustomer);
        /*if(IsexistingCustomer){
            Case objcase = new Case(Accountid=AccountId, Subject='Existing Customer');
            insert objcase;
        }*/
        opp.Is_Existing_Customer__c = IsexistingCustomer;
        
        //end existing Customercheck
        opp.Name = AccountName + '-' +DateTime.now().getTime();
        opp.CloseDate = Date.today();
        opp.StageName = 'Prospecting';
        If(!String.isBlank(AccountId))
        {
            opp.AccountId = AccountId;
        }
        opp.Pricebook2Id = CPQIntegrationLayerHelper.cpqPriceBookId;
        opp.SBQQ__QuotePricebookId__c = CPQIntegrationLayerHelper.cpqPriceBookId;
        opp.Created_By_PortalCustomer__c = true;
        opp.StageName = 'Closed Won';
       // opp.CurrencyISOCode='USD';
        
        insert opp;
        return opp;        
    }
    
    /*--------------Method to Create Quote-----------------*/
    public SBQQ__Quote__c createQuote(String OpportunityId)
    {
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        If(!String.isBlank(OpportunityId))
        {
            q.SBQQ__Opportunity2__c = OpportunityId;
        }
        q.SBQQ__PriceBook__c = CPQIntegrationLayerHelper.cpqPriceBookId; 
        q.SBQQ__StartDate__c = Date.Today(); 
        //q.SBQQ__Primary__c = true; 
        
        
        // Zuora related fields setup
        
        Account tempAcc = [select id,Ecomm_Referrer_ID__c,PersonContactId from Account where id=:sfAcc.id ];
        list<SecondaryCampaignsource__c> lstSource = new list<SecondaryCampaignsource__c >();
        if(tempAcc.Ecomm_Referrer_ID__c != null){            
            lstSource =[select id from SecondaryCampaignsource__c where id=:tempAcc.Ecomm_Referrer_ID__c];
        }
        q.ZSB__ZuoraBillingAccountId__c = zAccId  ; 
        q.ZSB__SoldToContact__c = tempAcc.PersonContactId;  
        q.ZSB__BillToContact__c =  q.ZSB__SoldToContact__c;
        q.ZSB__BillingBatch__c = 'Batch1';
        q.ZSB__TaxExemptStatus__c = 'No';
        q.ZSB__BillCycleDay__c = '1';
        if(lstSource.size()>0){
            q.SecondaryCampaignsource__c = tempAcc.Ecomm_Referrer_ID__c;
        }        
        
        insert q;
        return q;        
    }
    
    public CPQAPIClassDefinitions.QuoteModel getQuoteModel(string quoteId)
    {
        String startingQuoteJson = SBQQ.ServiceRouter.read('SBQQ.QuoteAPI.QuoteReader', quoteId); 
        CPQAPIClassDefinitions.QuoteModel qModel = (CPQAPIClassDefinitions.QuoteModel)JSON.deserialize(startingQuoteJson, CPQAPIClassDefinitions.QuoteModel.class);        
        
        return qModel ;
    }
    
    
    /*-------------- Method for Saving Products to Quote-----------------*/
    
   /* public void updateQuoteWithProductsAndSaveOld(String quoteId,List<string> productIdList)
    {
        CPQAPIClassDefinitions.ProductLoadContext plContext = new CPQAPIClassDefinitions.ProductLoadContext(); 
        plContext.pricebookId = CPQIntegrationLayerHelper.cpqPriceBookId;  
        //plContext.currencyCode = 'USD';
                
        String startingQuoteJson = SBQQ.ServiceRouter.read('SBQQ.QuoteAPI.QuoteReader', quoteId); 
        CPQAPIClassDefinitions.QuoteModel startingQuoteModel = (CPQAPIClassDefinitions.QuoteModel)JSON.deserialize(startingQuoteJson, CPQAPIClassDefinitions.QuoteModel.class);
        System.debug('startingQuoteModel ------>>'+startingQuoteModel);
        
        List<CPQAPIClassDefinitions.ProductModel> listProdModel = new List<CPQAPIClassDefinitions.ProductModel>();
        
        for(String prodId : productIdList)
        {
            String newProductJson = SBQQ.ServiceRouter.load('SBQQ.ProductAPI.ProductLoader', prodId , JSON.serialize(plContext)); 
            CPQAPIClassDefinitions.ProductModel newProductModel = (CPQAPIClassDefinitions.ProductModel)JSON.deserialize(newProductJson, CPQAPIClassDefinitions.ProductModel.class);  
            listProdModel.add(newProductModel);
        } 
        
        CPQAPIClassDefinitions.QuoteModel updatedQuoteModel = AddProductToQuote(listProdModel,startingQuoteModel);
        updatedQuoteModel = setParametersToQuoteLineItem(updatedQuoteModel);
        updatedQuoteModel = CalculateQuote(updatedQuoteModel); 
        System.debug('----updatedQuoteModel ---------------->>>'+updatedQuoteModel );                   
        string saveResult = saveQuoteWithChanges(updatedQuoteModel); 
    } */  
    
    
    public static void UpdateQuoteWithProductsAndSave(string quoteId,List<string> productIdList,Map<string,string> productOption)
    {
        //try{
        boolean linkProducts = true;
        CPQIntegrationLayerHelper cpqHelper = new CPQIntegrationLayerHelper();
        CPQAPIClassDefinitions.ProductLoadContext plContext = new CPQAPIClassDefinitions.ProductLoadContext(); 
        plContext.pricebookId = cpqPriceBookId;  
        
        String startingQuoteJson = SBQQ.ServiceRouter.read('SBQQ.QuoteAPI.QuoteReader', quoteId); 
        CPQAPIClassDefinitions.QuoteModel startingQuoteModel = (CPQAPIClassDefinitions.QuoteModel)JSON.deserialize(startingQuoteJson, CPQAPIClassDefinitions.QuoteModel.class);
        
        
        List<CPQAPIClassDefinitions.ProductModel> listProdModel = new List<CPQAPIClassDefinitions.ProductModel>();
        
        for(String prodId : productIdList)
        {
            String newProductJson = SBQQ.ServiceRouter.load('SBQQ.ProductAPI.ProductLoader', prodId, JSON.serialize(plContext)); 
            CPQAPIClassDefinitions.ProductModel newProductModel = (CPQAPIClassDefinitions.ProductModel)JSON.deserialize(newProductJson, CPQAPIClassDefinitions.ProductModel.class);  
            listProdModel.add(newProductModel);
            if(productOption!=null && productOption.containsKey(prodId))
            {
                System.debug('Inside-------------------->>>>>');
                //cpqHelper.selectProductOptionFromProductModel(newProductModel,productOption.get(prodId));
                String newProductOptionJson = SBQQ.ServiceRouter.load('SBQQ.ProductAPI.ProductLoader', productOption.get(prodId), JSON.serialize(plContext)); 
                CPQAPIClassDefinitions.ProductModel newProductOptionModel = (CPQAPIClassDefinitions.ProductModel)JSON.deserialize(newProductOptionJson , CPQAPIClassDefinitions.ProductModel.class);  
                listProdModel.add(newProductOptionModel);
            }
            System.debug('newProductModel-------------------->>>>>'+newProductModel);
            
        } 
        
        
        startingQuoteModel= cpqHelper.AddProductToQuote(listProdModel,startingQuoteModel);
        startingQuoteModel= cpqHelper.setParametersToQuoteLineItem(startingQuoteModel);
        //startingQuoteModel= cpqHelper.setDateFieldForQuoteLine(startingQuoteModel,listProdModel,productIdList[0],productOption.get(productIdList[0]));              
        if(productIdList!=null && productIdList.size()>0 && productOption!=null)
        {
            startingQuoteModel= cpqHelper.setProductOptionForQuoteLine(startingQuoteModel,listProdModel,productIdList[0],productOption.get(productIdList[0]));
        }
        startingQuoteModel= cpqHelper.CalculateQuote(startingQuoteModel);
         System.debug('updateQuoteWithProductsAndSaveAsync -- startingQuoteModel ---->>>'+startingQuoteModel);
              
        string saveResult = cpqHelper.saveQuoteWithChanges(startingQuoteModel);
        SBQQ__Quote__c finalQuoteOppSync = startingQuoteModel.record;
        cpqHelper.syncQuoteToOpportunity(finalQuoteOppSync);
        //cpqHelper.contractOpp(finalQuoteOppSync.SBQQ__Opportunity2__c);        
        //cpqHelper.contractQuoteZuora(quoteId);
        /*}catch(Exception e)
        {
            SBQQ__Quote__c q = [select id,SBQQ__Account__c,Name from SBQQ__Quote__c where id =:quoteId ];
            Account a = [select id,Firstname,LastName from account where id =: q.SBQQ__Account__c];
            
            string prodStr = 'productIdList : '+string.join(productIdList,',');
            prodStr = prodStr + 'productOption : '+JSON.serialize(productOption);
            prodStr = prodStr +' \n\n\n ---->>'+e.getMessage()+' ---- '+e.getStackTraceString();
            String caseId = CPQIntegrationLayerHelper.errorCaseCreation(a,prodStr ,'Trial','QuoteLine Addition to quote '+q.name) ;
            
            System.debug('----->>'+caseId );
        }*/
    }
     
    /*-------------- Async-Method for Saving Products to Quote-----------------*/
    @future(callout=true)
    public static void updateQuoteWithProductsAndSaveAsync(string quoteId,List<string> productIdList,Map<string,string> productOption,Map<string,string> SMProdIds)
    {
        try{
        boolean linkProducts = true;
        CPQIntegrationLayerHelper cpqHelper = new CPQIntegrationLayerHelper();
        CPQAPIClassDefinitions.ProductLoadContext plContext = new CPQAPIClassDefinitions.ProductLoadContext(); 
        plContext.pricebookId = cpqPriceBookId;  
        
        String startingQuoteJson = SBQQ.ServiceRouter.read('SBQQ.QuoteAPI.QuoteReader', quoteId); 
        CPQAPIClassDefinitions.QuoteModel startingQuoteModel = (CPQAPIClassDefinitions.QuoteModel)JSON.deserialize(startingQuoteJson, CPQAPIClassDefinitions.QuoteModel.class);
        
        
        List<CPQAPIClassDefinitions.ProductModel> listProdModel = new List<CPQAPIClassDefinitions.ProductModel>();
        
        for(String prodId : productIdList)
        {
            // Adding main Product 
            String newProductJson = SBQQ.ServiceRouter.load('SBQQ.ProductAPI.ProductLoader', prodId, JSON.serialize(plContext)); 
            CPQAPIClassDefinitions.ProductModel newProductModel = (CPQAPIClassDefinitions.ProductModel)JSON.deserialize(newProductJson, CPQAPIClassDefinitions.ProductModel.class);  
            listProdModel.add(newProductModel);
            if(productOption!=null && productOption.containsKey(prodId))
            {
                System.debug('Inside-------------------->>>>>');
                //cpqHelper.selectProductOptionFromProductModel(newProductModel,productOption.get(prodId));
                // Add option for main Product 
                String newProductOptionJson = SBQQ.ServiceRouter.load('SBQQ.ProductAPI.ProductLoader', productOption.get(prodId), JSON.serialize(plContext)); 
                CPQAPIClassDefinitions.ProductModel newProductOptionModel = (CPQAPIClassDefinitions.ProductModel)JSON.deserialize(newProductOptionJson , CPQAPIClassDefinitions.ProductModel.class);  
                listProdModel.add(newProductOptionModel);
                
                // Add Secondary Market Product for Option
                if(SMProdIds!=null && SMProdIds.containsKey(productOption.get(prodId)))
                {
                    String newSMMainProdJson = SBQQ.ServiceRouter.load('SBQQ.ProductAPI.ProductLoader', SMProdIds.get(productOption.get(prodId)), JSON.serialize(plContext)); 
                    CPQAPIClassDefinitions.ProductModel newSMProdOptionModel = (CPQAPIClassDefinitions.ProductModel)JSON.deserialize(newSMMainProdJson , CPQAPIClassDefinitions.ProductModel.class);  
                    listProdModel.add(newSMProdOptionModel );
                }
            }
            
            // Add Secondary Market Product for Main Product
            if(SMProdIds!=null && SMProdIds.containsKey(prodId))
            {
                String newSMProdJson = SBQQ.ServiceRouter.load('SBQQ.ProductAPI.ProductLoader', SMProdIds.get(prodId), JSON.serialize(plContext)); 
                CPQAPIClassDefinitions.ProductModel newProductOptionModel = (CPQAPIClassDefinitions.ProductModel)JSON.deserialize(newSMProdJson , CPQAPIClassDefinitions.ProductModel.class);  
                listProdModel.add(newProductOptionModel);
            }
            
            System.debug('newProductModel-------------------->>>>>'+newProductModel);
            
        }   
        system.debug('listProdModel####'+listProdModel);
        startingQuoteModel= cpqHelper.AddProductToQuote(listProdModel,startingQuoteModel);
        startingQuoteModel= cpqHelper.setParametersToQuoteLineItem(startingQuoteModel);
        //startingQuoteModel= cpqHelper.setDateFieldForQuoteLine(startingQuoteModel,listProdModel,productIdList[0],productOption.get(productIdList[0]));              
        
        // Link all the options to main product
        if(productIdList!=null && productIdList.size()>0 && productOption!=null)
        {
            startingQuoteModel= cpqHelper.setProductOptionForQuoteLine(startingQuoteModel,listProdModel,productIdList[0],productOption.get(productIdList[0]));          
        }
        
        // Link all the secondary market products to their parent products
        
        if(productIdList!=null && productIdList.size()>0 && SMProdIds!=null)
        {
            for(string prd : SMProdIds.keySet())
            {
                startingQuoteModel= cpqHelper.setProductOptionForQuoteLine(startingQuoteModel,listProdModel,prd,SMProdIds.get(prd));          
            }
        }
        startingQuoteModel= cpqHelper.CalculateQuote(startingQuoteModel);
        System.debug('updateQuoteWithProductsAndSaveAsync -- startingQuoteModel ---->>>'+startingQuoteModel);  
        //startingQuoteModel= cpqHelper.setDateFieldForQuoteLineToday(startingQuoteModel,listProdModel,productIdList[0],productOption.get(productIdList[0]));              
        
        string saveResult = cpqHelper.saveQuoteWithChanges(startingQuoteModel);
        SBQQ__Quote__c finalQuoteOppSync = startingQuoteModel.record;
                    
        //cpqHelper.syncQuoteToOpportunity(finalQuoteOppSync);
        //cpqHelper.contractOpp(finalQuoteOppSync.SBQQ__Opportunity2__c);        
        //cpqHelper.contractQuoteZuora(quoteId);
        }catch(Exception e)
        {
            SBQQ__Quote__c q = [select id,SBQQ__Account__c,Name from SBQQ__Quote__c where id =:quoteId ];
            Account a = [select id,Firstname,LastName from account where id =: q.SBQQ__Account__c];
            
            string prodStr = 'productIdList : '+string.join(productIdList,',');
            prodStr = prodStr + 'productOption : '+JSON.serialize(productOption);
            CPQIntegrationLayerHelper.errorCaseCreation(a,prodStr ,'Trial','QuoteLine Addition to quote '+q.name) ;
        }
    }
    
    public void syncQuoteToOpportunity(SBQQ__Quote__c quote)
    {
        if(quote!=null && quote.id!=null)
        {
            quote.SiteUser_Primary_Check__c = true;
            quote.SBQQ__Status__c = 'Approved';
            update quote;
        }
        
    }
    
    public void contractOppZuora(String oppId)
    {
        if(!String.isEmpty(oppId)){
            ZSB.ZSBConnectorUtils.createContractFromOpportunity(oppId);
        }
    }
    public void contractQuoteZuora(String qId)
    {
        if(!String.isEmpty(qId)){
            ZSB.ZSBConnectorUtils.createContractFromQuote(qId);
        }
    }
    public void contractOpp(String oppId)
    {
        if(!String.isEmpty(oppId)){
            List<Opportunity> oppList = [select id,StageName,SBQQ__Contracted__c,SBQQ__CreateContractedPrices__c from opportunity where id =:oppId ];
            if(oppList.size()>0)
            {
                oppList[0].StageName = 'Closed Won';
                oppList[0].SBQQ__Contracted__c = true;
                //oppList[0].SBQQ__CreateContractedPrices__c = true;
                update oppList[0];         
            }
        }
    }
    //******* Method for Adding products to Quote ********
    public CPQAPIClassDefinitions.QuoteModel AddProductToQuote(List<CPQAPIClassDefinitions.ProductModel> listProdModel,CPQAPIClassDefinitions.QuoteModel startingQuoteModel)
    {
        CPQAPIClassDefinitions.ProductAddContext paContext = new CPQAPIClassDefinitions.ProductAddContext(); 
        paContext.quote = startingQuoteModel; 
        paContext.products = listProdModel; 
        paContext.ignoreCalculate = true; 
 
        System.debug('paContext ---->>>'+paContext);
        String updatedQuoteJSON = SBQQ.ServiceRouter.load('SBQQ.QuoteAPI.QuoteProductAdder', null, JSON.serialize(paContext)); 
        CPQAPIClassDefinitions.QuoteModel updatedQuoteModel = (CPQAPIClassDefinitions.QuoteModel)JSON.deserialize(updatedQuoteJSON, CPQAPIClassDefinitions.QuoteModel.class);        
        
        return updatedQuoteModel ;
    }
    
    //******* Method to Calculate Quote without Saving ********
    public CPQAPIClassDefinitions.QuoteModel CalculateQuote(CPQAPIClassDefinitions.QuoteModel updatedQuoteModel)
    {
        CPQAPIClassDefinitions.CalculatorContext context = new CPQAPIClassDefinitions.CalculatorContext(); 
        context.quote = updatedQuoteModel; 
        
        System.debug('QMCalculate------>>>>'+updatedQuoteModel);
        
        String resultJson = SBQQ.ServiceRouter.load('SBQQ.QuoteAPI.QuoteCalculator', null, JSON.serialize(context)); 
        CPQAPIClassDefinitions.QuoteModel resultModel = (CPQAPIClassDefinitions.QuoteModel)JSON.deserialize(resultJson, CPQAPIClassDefinitions.QuoteModel.class);
        
        return resultModel;
    }
    
    //******* Method to Save Quote with changes ********
    public string saveQuoteWithChanges(CPQAPIClassDefinitions.QuoteModel updatedQuoteModel)
    {
        String savedJson = SBQQ.ServiceRouter.save('SBQQ.QuoteAPI.QuoteSaver', JSON.serialize(updatedQuoteModel)); 
        return savedJson;
    }
    
    //******* Method to set attributes/parameter of QuoteLines before calculation or Saving *********
    public CPQAPIClassDefinitions.QuoteModel setParametersToQuoteLineItem(CPQAPIClassDefinitions.QuoteModel updatedQuoteModel)
    {
        //updatedQuoteModel.record.SBQQ__Primary__c = true;
        return updatedQuoteModel;
    }    
    
    //******* Method to set attributes/parameter of QuoteLines before calculation or Saving *********
    public CPQAPIClassDefinitions.OptionModel getProductOptionFromProductModel(List<CPQAPIClassDefinitions.ProductModel> listProdModel,String prodModelId,String prodOptionId)
    {
         CPQAPIClassDefinitions.OptionModel opModel = null;
         //CPQAPIClassDefinitions.ProductModel pModel = listProdModel[0];
         for(CPQAPIClassDefinitions.ProductModel pModel : listProdModel){
             String pModelRecId = pModel.record.id;
             if(pModelRecId.startsWith(prodModelId)  || pModelRecId== prodModelId  ){
                 for(CPQAPIClassDefinitions.OptionModel option : pModel.options)
                 {
                    String optionModelProdId = option.record.SBQQ__OptionalSKU__c;
                    if(optionModelProdId.startsWith(prodOptionId) || optionModelProdId== prodOptionId)
                    {
                        opModel = option;
                    }
                 }
             }
         }
         return opModel;
    }
    
    
     
    //******* Method to set productOptions required fields for productOption QuoteLines before calculation or Saving *********
    public CPQAPIClassDefinitions.QuoteModel setProductOptionForQuoteLine(CPQAPIClassDefinitions.QuoteModel startingQuoteModel,List<CPQAPIClassDefinitions.ProductModel> listProdModel,String prodModelId ,String prodOptionId)
    {
        integer initialKey = 0;
        integer extProductKey = 0;
        CPQAPIClassDefinitions.QuoteLineModel tempQLineMod = null;
        for(CPQAPIClassDefinitions.QuoteLineModel qLineMod : startingQuoteModel.lineItems)
        {
            string qLineProdId = qLineMod.record.SBQQ__Product__c;            
            if(qLineProdId.startsWith(prodModelId)  || qLineProdId== prodModelId  )
            {
                initialKey = qLineMod.key;                
            }
            if(qLineProdId.startsWith(prodOptionId) || qLineProdId== prodOptionId)
            {
                extProductKey  = qLineMod.key;
                tempQLineMod =  qLineMod ;             
            }            
        }       
        
        if(tempQLineMod !=null)
        {
            CPQAPIClassDefinitions.OptionModel opModel = getProductOptionFromProductModel(listProdModel,prodModelId,prodOptionId);
            if(opModel!=null)
            {
                tempQLineMod.parentItemKey = initialKey;
                tempQLineMod .record.SBQQ__ProductOption__c = opModel.record.id;
                tempQLineMod .record.SBQQ__OptionType__c= opModel.record.SBQQ__Type__c;
                tempQLineMod .record.SBQQ__OptionLevel__c=1.0;
                tempQLineMod .record.SBQQ__DynamicOptionId__c = opModel.record.SBQQ__Feature__c;
                //tempQLineMod .record.SBQQ__StartDate__c = Date.Today().addDays(31);
            }
        }
        return startingQuoteModel;
    }  
   /* public CPQAPIClassDefinitions.QuoteModel setDateFieldForQuoteLineToday(CPQAPIClassDefinitions.QuoteModel startingQuoteModel,List<CPQAPIClassDefinitions.ProductModel> listProdModel,String prodModelId ,String prodOptionId)
    {
        integer initialKey = 0;
        integer extProductKey = 0;
        for(CPQAPIClassDefinitions.QuoteLineModel qLineMod : startingQuoteModel.lineItems)
        {
            qLineMod.record.SBQQ__StartDate__c = Date.Today();
        }
        return startingQuoteModel;
    }*/
    
    //******* Method to set attributes/parameter of QuoteLines before calculation or Saving *********
   /* public CPQAPIClassDefinitions.QuoteModel setDateFieldForQuoteLine(CPQAPIClassDefinitions.QuoteModel startingQuoteModel,List<CPQAPIClassDefinitions.ProductModel> listProdModel,String prodModelId ,String prodOptionId)
    {
        integer initialKey = 0;
        integer extProductKey = 0;
        for(CPQAPIClassDefinitions.QuoteLineModel qLineMod : startingQuoteModel.lineItems)
        {
            string qLineProdId = qLineMod.record.SBQQ__Product__c;            
            if(qLineProdId.startsWith(prodModelId)  || qLineProdId== prodModelId  )
            {
                initialKey = qLineMod.key;
                qLineMod.record.SBQQ__StartDate__c = Date.Today();
                qLineMod.record.SBQQ__EndDate__c = Date.Today().addDays(30);
            }
            if(qLineProdId.startsWith(prodOptionId) || qLineProdId== prodOptionId)
            {
                //qLineMod.record.SBQQ__StartDate__c = Date.Today().addDays(31);
                extProductKey  = qLineMod.key;              
            }
            
            if(qLineMod.parentItemKey == initialKey )
            {
                qLineMod.record.SBQQ__StartDate__c = Date.Today();
            }
            if(qLineMod.parentItemKey == extProductKey )
            {
                qLineMod.record.SBQQ__StartDate__c = Date.Today().addDays(31);
            }
        }       
        
        return startingQuoteModel;
    }*/ 
    
    //@future(callout=true)
    /*public static void syncQuoteToOpportunityWithAdminUser(String quoteId,String OppId)
    {
        if(quoteId!=null && quoteId!='' && OppId!=null && OppId!='')
        {
            updateQuoteOppWithAdminUser(quoteId,OppId);            
        }        
    }*/
    
    /*public static void updateQuoteOppWithAdminUser(String qId,String OppId)
    { 
        HttpRest hRest = new HttpRest ();
        hRest.authenticateByUserNamePassword('3MVG98RqVesxRgQ5VEQ7zQ0ZneBoxnO70.FZkgMcey7EzpJfT9vBaIQIYMqpH9QZ8kHHf44z8QiatCt4FkSfv','1226336843678337315','develop@vieosolutions.com.partial','Devvvp17!',true);
        if(qId!=null && qId!='')
        {
            hRest.doRecordUpdate('SBQQ__Quote__c',qId,'{ "SBQQ__Primary__c" : "true" }');
        }
        if(OppId!=null && OppId!='')
        {
            hRest.doRecordUpdate('Opportunity',OppId,'{ "StageName" : "Closed Won"}');
        }
    }
    
    public static void updateOpportunityWithAdminUser(String oId)
    { 
        HttpRest hRest = new HttpRest ();
        hRest.authenticateByUserNamePassword('3MVG98RqVesxRgQ5VEQ7zQ0ZneBoxnO70.FZkgMcey7EzpJfT9vBaIQIYMqpH9QZ8kHHf44z8QiatCt4FkSfv','1226336843678337315','develop@vieosolutions.com.partial','Devvvp17!',true);
        hRest.doRecordUpdate('Opportunity',oId,'{ "StageName" : "Closed Won" ,"SBQQ__Contracted__c" : "true"  }');
    }*/
    
    public void selectProductOptionFromProductModel(CPQAPIClassDefinitions.ProductModel pModel ,String prodOptionId)
    {
         if(!String.isBlank(prodOptionId) &&  pModel!= null  ){
                 for(CPQAPIClassDefinitions.OptionModel option : pModel.options)
                 {
                    String optionModelProdId = option.record.SBQQ__OptionalSKU__c;
                    if(optionModelProdId.startsWith(prodOptionId) || optionModelProdId== prodOptionId)
                    {
                        
                        CPQAPIClassDefinitions.ConfigurationModel optionConfigModel = new  CPQAPIClassDefinitions.ConfigurationModel();
                        optionConfigModel.configuredProductId = optionModelProdId;
                        optionConfigModel.changedByProductActions = false;
                        optionConfigModel.configurationData = new SBQQ__ProductOption__c();
                        optionConfigModel.configured = true;
                        optionConfigModel.isDynamicOption = false;
                        optionConfigModel.isUpgrade = false;
                        optionConfigModel.optionData = new SBQQ__ProductOption__c();
                        optionConfigModel.optionData.SBQQ__Discount__c = option.record.SBQQ__Discount__c;
                        optionConfigModel.optionData.SBQQ__ConfiguredSKU__c = option.record.SBQQ__ConfiguredSKU__c;
                        optionConfigModel.optionData.SBQQ__DiscountAmount__c = option.record.SBQQ__DiscountAmount__c;
                        optionConfigModel.optionData.SBQQ__Quantity__c = option.record.SBQQ__Quantity__c;
                        optionConfigModel.optionData.SBQQ__OptionalSKU__c = option.record.SBQQ__OptionalSKU__c;
                        optionConfigModel.optionData.id = option.record.id;
                        
                        optionConfigModel.optionId = option.record.id;                                                
                        pModel.configuration.optionConfigurations.add(optionConfigModel);   
                        //option.configurationRequired = false;
                        //option.configurable = false;
                        //option.record.SBQQ__Selected__c = true;
                        //option.record.SBQQ__Required__c = true;
                        
                        System.debug('***option---------->>>>>'+pModel);
                    }
                 }
             }         
     }
     
     
     public static string errorCaseCreation(Account a,String extraInfo,string type,string occurFor) {
        Case c = new Case(AccountId = a.Id, Subject = 'Ecomm : Error in '+occurFor, Type = type, Reason = 'New Problem', Origin = 'Web', Description = extraInfo);
        
        insert c;
        return c.id;
    }
}