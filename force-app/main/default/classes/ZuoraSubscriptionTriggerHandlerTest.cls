@isTest
private class ZuoraSubscriptionTriggerHandlerTest {
    @isTest static void testInsert() {
        Campaign cam = new Campaign(Name='test');
        insert cam;

        Upgrade_Suspended_Subscription__c uss = new Upgrade_Suspended_Subscription__c(Upgraded_Sub_Zuora_Id__c = '12345');
        insert uss;
        Zuora__Subscription__c zs = new Zuora__Subscription__c(Zuora__Zuora_Id__c = '12345', CampaignId__c = cam.Id,SuspendDate__c =Date.today());
        insert zs;

        uss = [SELECT Upgraded_Subscription__c FROM Upgrade_Suspended_Subscription__c];
        //System.assertEquals(zs.Id, uss.Upgraded_Subscription__c, 'upgraded subscription needs to equal to zs.id');
        ZuoraSubscriptionTriggerHandler.updateLookupFields(new list<Zuora__Subscription__c>{zs});
        ZuoraSubscriptionTriggerHandler.updateUSSSubId(new list<Zuora__Subscription__c>{zs});
        ZuoraSubscriptionTriggerHandler.updatecontract(new list<Zuora__Subscription__c>{zs});
    }
}