@isTest
private class SubscriptionAfterUpdateTest {

    @isTest static void testHasBatch() {
        //Account a = new Account(FirstName='testfirst',LastName='testlast',PersonEmail='test@test.com',Subscription_Suspend_Start_Date__c=Date.today().addDays(-30),Subscription_Suspend_End_Date__c=Date.today().addDays(14),Subscription_Status__c='Suspended');
        //insert a;
        Account a = new Account();
            a.firstname='test acc1';
            a.lastName = 'test acc';
            a.billingCity ='Test City';
            a.billingStreet = 'TestStreet';
            a.billingstate ='TestState';
            a.billingPostalcode ='23451';
            a.billingcountry ='United States';
            Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            a.recordtypeID=objAccrec.id;
           a.PersonMailingCity='Test City';
            a.PersonMailingStreet = 'TestStreet';
            a.PersonMailingState ='TestState';
            a.PersonMailingPostalCode ='23451';
            a.PersonMailingCountry ='United States';
            a.phone='090123456987';
            a.Subscription_Suspend_Start_Date__c=Date.today().addDays(-30);
            a.Subscription_Suspend_End_Date__c=Date.today().addDays(14);
            a.Subscription_Status__c='Suspended';
            //acc.ispersonAccount = true;
            insert a;

        Zuora__CustomerAccount__c zca = new Zuora__CustomerAccount__c(Zuora__Status__c='Active',Name='test billing account',Zuora__Account__c=a.Id,Zuora__Zuora_Id__c='12345');
        insert zca;

        Zuora__Subscription__c zs = new Zuora__Subscription__c(Name='a0001',Zuora__SubscriptionStartDate__c = Date.today().addMonths(-12), Zuora__SubscriptionEndDate__c = Date.today().addYears(2), Zuora__InitialTerm__c='12 months',Zuora__RenewalTerm__c='12 months',Zuora__TermStartDate__c=Date.today().addMonths(-3),
            Zuora__TermEndDate__c = Date.today().addMonths(20),Zuora__AutoRenew__c=true, Zuora__Zuora_Id__c='12345', Zuora__ContractEffectiveDate__c=Date.today().addMonths(-3), Zuora__ServiceActivationDate__c=Date.today().addMonths(-3), 
            Zuora__TCV__c=12,Zuora__Account__c=a.Id,Zuora__CustomerAccount__c=zca.Id,Zuora__Status__c='Active',Zuora__TermSettingType__c='EVERGREEN');
        insert zs;

        Zuora__Subscription__c zs2 = new Zuora__Subscription__c(Name='a00012',Zuora__SubscriptionStartDate__c = Date.today().addMonths(-12), Zuora__SubscriptionEndDate__c = Date.today().addYears(2), Zuora__InitialTerm__c='12 months',Zuora__RenewalTerm__c='12 months',Zuora__TermStartDate__c=Date.today().addMonths(-3),
            Zuora__TermEndDate__c = Date.today().addMonths(20),Zuora__AutoRenew__c=true, Zuora__Zuora_Id__c='12345', Zuora__ContractEffectiveDate__c=Date.today().addMonths(-3), 
            Zuora__TCV__c=12,Zuora__Account__c=a.Id,Zuora__CustomerAccount__c=zca.Id,Zuora__Status__c='Active',Zuora__TermSettingType__c='EVERGREEN');
        insert zs2;

        Zuora__SubscriptionRatePlan__c zsrp = new Zuora__SubscriptionRatePlan__c(Name='testtest1',Zuora__OriginalProductRatePlanId__c='12345');
        insert zsrp;
        //adding
        list<product2> lstproduct = new list<product2>();                          
             Product2 prod1 = new Product2(Name = 'test US EOD Monthly', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_TRIAL' , Family = 'Books');
            
            lstproduct.add(prod1);
            
            Product2 prod2 = new Product2(Name = 'test CA RT Monthly Trial', Feed__c = 'RealTime',Market__c = 'CA',
                             ProductCode = 'VV_7.0_RT_CA_MONTHLY_TRIAL' , Family = 'Books', isTrial__c=true);
            
            lstproduct.add(prod2);
            Product2 prod3 = new Product2(Name = 'test CA ID Monthly', Feed__c = 'IntraDay',Market__c = 'CA',
                             ProductCode = 'VV_7.0_EOD_CA_MONTHLY' , Is_Secondary_market__c =false,Family = 'Books');
           
           lstproduct.add(prod3);
           insert lstproduct;
           
           list<Zuora__Product__c> lstZproduct = new list<Zuora__Product__c>();
           Zuora__Product__c zprod1 = new Zuora__Product__c(Name = 'test US EOD Monthly',Sf_Product__c=prod1.id);
            lstZproduct.add(zprod1);
            
            Zuora__Product__c Zprod2 = new Zuora__Product__c(Name = 'test CA RT Monthly Trial',Sf_Product__c=prod2.id);
            
            lstZproduct.add(Zprod2);
            Zuora__Product__c Zprod3 = new Zuora__Product__c(Name = 'test CA ID Monthly',Sf_Product__c=prod3.id );
           
           lstZproduct.add(Zprod3);
           insert lstZproduct;
           
        //adding
        List<Zuora__SubscriptionProductCharge__c> zspclist = new List<Zuora__SubscriptionProductCharge__c>();
        Zuora__SubscriptionProductCharge__c zspc = new Zuora__SubscriptionProductCharge__c(Zuora__Product__c=zprod1.id,
            Zuora__Subscription__c=zs.Id,Zuora__SubscriptionRatePlan__c=zsrp.Id,Name='test US EOD Monthly',Zuora__Description__c='Primary: test test',Zuora__Price__c=12,Zuora__RatePlanId__c='12345',Zuora__RatePlanName__c='testtest1',Zuora__EffectiveStartDate__c=Date.today().addMonths(-3),Zuora__EffectiveEndDate__c=Date.today().addMonths(15),Zuora__EndDateCondition__c=null,Zuora__Type__c='Recurring');
        zspclist.add(zspc);

        zspc = new Zuora__SubscriptionProductCharge__c(Zuora__Product__c=zprod3.id,
            Zuora__Subscription__c=zs2.Id,Zuora__SubscriptionRatePlan__c=zsrp.Id,Name='test CA ID Monthly',Zuora__Description__c='Primary: test test',Zuora__Price__c=12,Zuora__RatePlanId__c='12345',Zuora__RatePlanName__c='testtest1',Zuora__EffectiveStartDate__c=Date.today().addMonths(-2),Zuora__EffectiveEndDate__c=Date.today().addMonths(15),Zuora__EndDateCondition__c=null,Zuora__Type__c='Recurring');
        zspclist.add(zspc);

        zspc = new Zuora__SubscriptionProductCharge__c(Zuora__Product__c=zprod2.id,
            Zuora__Subscription__c=zs2.Id,Zuora__SubscriptionRatePlan__c=zsrp.Id,Name='test CA RT Monthly Trial',Zuora__Description__c='Primary: test RT Monthly Trial',Zuora__Price__c=12,Zuora__RatePlanId__c='12345',Zuora__RatePlanName__c='testtest1',Zuora__EffectiveStartDate__c=Date.today().addMonths(-1),Zuora__EffectiveEndDate__c=Date.today().addMonths(15),Zuora__EndDateCondition__c=null,Zuora__Type__c='Recurring');
        zspclist.add(zspc);

        zspc = new Zuora__SubscriptionProductCharge__c(Zuora__Product__c=zprod2.id,
            Zuora__Subscription__c=zs2.Id,Zuora__SubscriptionRatePlan__c=zsrp.Id,Name='test CA RT Monthly Trial',Zuora__Description__c='Primary: test RT Monthly Trial',Zuora__Price__c=12,Zuora__RatePlanId__c='12345',Zuora__RatePlanName__c='testtest1',Zuora__EffectiveStartDate__c=Date.today().addMonths(-1),Zuora__EffectiveEndDate__c=Date.today().addMonths(15),Zuora__EndDateCondition__c=null,Zuora__Type__c='One-Time');
        zspclist.add(zspc);

        insert zspclist;

        Test.startTest();
        Database.executeBatch(new SubscriptionAfterUpdateBatch());
        System.schedule('Subscription EAR Regeneration Batch','0 5 0 * * ?', new SubscriptionEARScheduler());
        Test.stopTest();

    }

    @isTest static void testHadBatch() {
        //Account a = new Account(FirstName='testfirst',LastName='testlast',PersonEmail='test@test.com',Subscription_Suspend_Start_Date__c=Date.today().addDays(-30),Subscription_Suspend_End_Date__c=Date.today().addDays(14),Subscription_Status__c='Suspended');
        //insert a;
        Account a = new Account();
            a.firstname='test acc1';
            a.lastName = 'test acc';
            a.billingCity ='Test City';
            a.billingStreet = 'TestStreet';
            a.billingstate ='TestState';
            a.billingPostalcode ='23451';
            a.billingcountry ='United States';
            Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            a.recordtypeID=objAccrec.id;
           a.PersonMailingCity='Test City';
            a.PersonMailingStreet = 'TestStreet';
            a.PersonMailingState ='TestState';
            a.PersonMailingPostalCode ='23451';
            a.PersonMailingCountry ='United States';
            a.phone='090123456987';
            a.Subscription_Suspend_Start_Date__c=Date.today().addDays(-30);
            a.Subscription_Suspend_End_Date__c=Date.today().addDays(14);
            a.Subscription_Status__c='Suspended';
            //acc.ispersonAccount = true;
            insert a;
        Zuora__CustomerAccount__c zca = new Zuora__CustomerAccount__c(Zuora__Status__c='Active',Name='test billing account',Zuora__Account__c=a.Id,Zuora__Zuora_Id__c='12345');
        insert zca;

        Zuora__Subscription__c zs = new Zuora__Subscription__c(Name='a0001',Zuora__SubscriptionStartDate__c = Date.today().addMonths(-12), Zuora__SubscriptionEndDate__c = Date.today().addDays(-1), Zuora__InitialTerm__c='12 months',Zuora__RenewalTerm__c='12 months',Zuora__TermStartDate__c=Date.today().addMonths(-3),
            Zuora__TermEndDate__c = Date.today().addMonths(20),Zuora__AutoRenew__c=true, Zuora__Zuora_Id__c='12345', Zuora__ContractEffectiveDate__c=Date.today().addMonths(-3), Zuora__ServiceActivationDate__c=Date.today().addMonths(-3), 
            Zuora__TCV__c=12,Zuora__Account__c=a.Id,Zuora__CustomerAccount__c=zca.Id,Zuora__Status__c='Cancelled',Zuora__TermSettingType__c='EVERGREEN');
        insert zs;

        Zuora__Subscription__c zs2 = new Zuora__Subscription__c(Name='a00012',Zuora__SubscriptionStartDate__c = Date.today().addMonths(-12), Zuora__SubscriptionEndDate__c = Date.today().addYears(2), Zuora__InitialTerm__c='12 months',Zuora__RenewalTerm__c='12 months',Zuora__TermStartDate__c=Date.today().addMonths(-3),
            Zuora__TermEndDate__c = Date.today().addMonths(20),Zuora__AutoRenew__c=true, Zuora__Zuora_Id__c='12345', Zuora__ContractEffectiveDate__c=Date.today().addMonths(-3), Zuora__ServiceActivationDate__c=Date.today().addMonths(-3), 
            Zuora__TCV__c=12,Zuora__Account__c=a.Id,Zuora__CustomerAccount__c=zca.Id,Zuora__Status__c='Cancelled',Zuora__TermSettingType__c='EVERGREEN');
        insert zs2;

        Zuora__SubscriptionRatePlan__c zsrp = new Zuora__SubscriptionRatePlan__c(Name='testtest1',Zuora__OriginalProductRatePlanId__c='12345');
        insert zsrp;
        //adding
        list<product2> lstproduct = new list<product2>();                          
             Product2 prod1 = new Product2(Name = 'test US EOD Monthly', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_TRIAL' , Family = 'Books');
            
            lstproduct.add(prod1);
            
            Product2 prod2 = new Product2(Name = 'test CA RT Monthly Trial', Feed__c = 'RealTime',Market__c = 'CA',
                             ProductCode = 'VV_7.0_RT_CA_MONTHLY_TRIAL' , Family = 'Books', isTrial__c=true);
            
            lstproduct.add(prod2);
            Product2 prod3 = new Product2(Name = 'test CA ID Monthly', Feed__c = 'IntraDay',Market__c = 'CA',
                             ProductCode = 'VV_7.0_EOD_CA_MONTHLY' , Is_Secondary_market__c =false,Family = 'Books');
           
           lstproduct.add(prod3);
           insert lstproduct;
           
           list<Zuora__Product__c> lstZproduct = new list<Zuora__Product__c>();
           Zuora__Product__c zprod1 = new Zuora__Product__c(Name = 'test US EOD Monthly',Sf_Product__c=prod1.id);
            lstZproduct.add(zprod1);
            
            Zuora__Product__c Zprod2 = new Zuora__Product__c(Name = 'test CA RT Monthly Trial',Sf_Product__c=prod2.id);
            
            lstZproduct.add(Zprod2);
            Zuora__Product__c Zprod3 = new Zuora__Product__c(Name = 'test CA ID Monthly',Sf_Product__c=prod3.id );
           
           lstZproduct.add(Zprod3);
           insert lstZproduct;
           
        //adding
        List<Zuora__SubscriptionProductCharge__c> zspclist = new List<Zuora__SubscriptionProductCharge__c>();
        Zuora__SubscriptionProductCharge__c zspc = new Zuora__SubscriptionProductCharge__c(Zuora__Product__c=zprod1.id,
            Zuora__Subscription__c=zs.Id,Zuora__SubscriptionRatePlan__c=zsrp.Id,Name='test US EOD Monthly',Zuora__Description__c='Primary: test test',Zuora__Price__c=12,Zuora__RatePlanId__c='12345',Zuora__RatePlanName__c='testtest1',Zuora__EffectiveStartDate__c=Date.today().addMonths(-3),Zuora__EffectiveEndDate__c=Date.today().addMonths(15),Zuora__EndDateCondition__c=null,Zuora__Type__c='Recurring');
        zspclist.add(zspc);

        zspc = new Zuora__SubscriptionProductCharge__c(Zuora__Product__c=zprod3.id,
            Zuora__Subscription__c=zs2.Id,Zuora__SubscriptionRatePlan__c=zsrp.Id,Name='test CA ID Monthly',Zuora__Description__c='Primary: test test',Zuora__Price__c=12,Zuora__RatePlanId__c='12345',Zuora__RatePlanName__c='testtest1',Zuora__EffectiveStartDate__c=Date.today().addMonths(-3),Zuora__EffectiveEndDate__c=Date.today().addMonths(15),Zuora__EndDateCondition__c=null,Zuora__Type__c='One-Time');
        zspclist.add(zspc);

        zspc = new Zuora__SubscriptionProductCharge__c(Zuora__Product__c=zprod2.id,
            Zuora__Subscription__c=zs2.Id,Zuora__SubscriptionRatePlan__c=zsrp.Id,Name='test CA ID Monthly Trial',Zuora__Description__c='Primary: test RT Monthly Trial',Zuora__Price__c=12,Zuora__RatePlanId__c='12345',Zuora__RatePlanName__c='testtest1',Zuora__EffectiveStartDate__c=Date.today().addMonths(-3),Zuora__EffectiveEndDate__c=Date.today().addMonths(15),Zuora__EndDateCondition__c=null,Zuora__Type__c='One-Time');
        zspclist.add(zspc);

        insert zspclist;

        Test.startTest();
        Database.executeBatch(new SubscriptionAfterUpdateBatch());
        Test.stopTest();
    }
}