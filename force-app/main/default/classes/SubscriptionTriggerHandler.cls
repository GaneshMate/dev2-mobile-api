/**
 * when subscription charge is inserted, updated, or delete, recalculate all the account fields and enterprise access rights
 */
public without sharing class SubscriptionTriggerHandler {
    public static Boolean RUNBATCH = true;

    public static void afterChange(List<Zuora__SubscriptionProductCharge__c> zspclist) {
        Set<Id> aids = new Set<Id>();
        for (Zuora__SubscriptionProductCharge__c zs : zspclist) {
            if(zs.Zuora__Product__c != null || test.isRunningtest()){
                aids.add(zs.Zuora__Account__c);
            }
        }

        // populate enterprise access right
        List<Zuora__Subscription__c> complete_zs_list = [SELECT Zuora__AutoRenew__c,Zuora__Account__r.Subscription_Status__c, 
            Zuora__Account__r.VV_Market__c, Zuora__Account__r.VV_Feed__c, Zuora__ContractEffectiveDate__c, 
            Zuora__Status__c, Zuora__CancelledDate__c, Zuora__Account__c, Zuora__TermSettingType__c, 
            SuspendDate__c, ResumeDate__c, Category__c,
            Zuora__TermStartDate__c, Zuora__TermEndDate__c,Zuora__ServiceActivationDate__c,
                (SELECT Name,Zuora__Subscription__r.Zuora__ServiceActivationDate__c,Zuora__Subscription__r.Zuora__TermEndDate__c,zuora__Product__r.SF_Product__r.ismobile__c,zuora__Product__r.SF_Product__r.AddOn_Type__c,zuora__Product__r.SF_Product__r.IsTrial__c,zuora__Product__r.SF_Product__r.feed__c,zuora__Product__r.SF_Product__r.Market__c,zuora__Product__r.SF_Product__r.Name,zuora__Product__r.SF_Product__r.ProductCode, Zuora__Description__c, Zuora__EffectiveStartDate__c, Zuora__EffectiveEndDate__c, 
                Zuora__Price__c, Zuora__Type__c, Zuora__SubscriptionRatePlan__r.Zuora__OriginalProductRatePlanId__c,
                Market__c, Feed__c, Edition__c 
                FROM Zuora__Subscription_Product_Charges__r where Zuora__EffectiveStartDate__c <= Today ORDER BY Zuora__EffectiveStartDate__c) 
            FROM Zuora__Subscription__c WHERE Zuora__Account__c IN :aids AND Zuora__TermStartDate__c <= TODAY]; //and (Zuora__TermEndDate__c = null or Zuora__TermEndDate__c > Today) AND((Zuora__TermSettingType__c = 'Evergreen' AND Zuora__TermStartDate__c <= TODAY) OR (Zuora__TermSettingType__c != 'Evergreen' AND Zuora__TermStartDate__c <= TODAY AND Zuora__TermEndDate__c >= TODAY)) Order By Zuora__Status__c DESC];
            
       // populateEnterpriseAccessRight(complete_zs_list);

        updateMarketoFields(complete_zs_list);
    }

    

    /**
     * this method takes the trial's description, map it to a rateplan name and query and get the rateplan ids and populate trial_rateplan_zuora_map
     * @param account_to_trial_desc_map [description]
     * @param trial_rateplan_zuora_map  [description]
     */
     /*
    private static void mapTrialDescToRatePlan(Map<Id, Set<String>> account_to_trial_desc_map, Map<Id, List<zqu__ProductRatePlan__c>> trial_rateplan_zuora_map) {
        // get all rate plans
        Map<String, zqu__ProductRatePlan__c> rateplan_name_to_id_map = new Map<String, zqu__ProductRatePlan__c>();
        for (zqu__ProductRatePlan__c tempzprp : [SELECT Name, VV_Market__c, VV_Type__c, zqu__ZuoraId__c FROM zqu__ProductRatePlan__c WHERE zqu__Deleted__c = false]) {
            rateplan_name_to_id_map.put(tempzprp.Name, tempzprp);
        }

        for (Id aid : account_to_trial_desc_map.keySet()) {
            if (trial_rateplan_zuora_map.get(aid) == null) trial_rateplan_zuora_map.put(aid, new List<zqu__ProductRatePlan__c>());
            
            for (String s : account_to_trial_desc_map.get(aid)) {
                String planname = '';
                if (s.containsIgnoreCase('UK')) planname = 'VectorVest 7.0 UK EOD Monthly';
                else if (s.containsIgnoreCase('europe') || s.containsIgnoreCase('EU')) planname = 'VectorVest 7.0 EU EOD Monthly';
                else if (s.containsIgnoreCase('india') || s.containsIgnoreCase('IN')) planname = 'VectorVest 7.0 IN EOD Monthly';
                else if (s.containsIgnoreCase('south africa') || s.containsIgnoreCase('SA')) planname = 'VectorVest 7.0 SA EOD Monthly';
                else if (s.containsIgnoreCase('australia') || s.containsIgnoreCase('AU')) planname = 'VectorVest 7.0 AU EOD Monthly';
                else if (s.containsIgnoreCase('hong kong') || s.containsIgnoreCase('HK')) planname = 'VectorVest 7.0 HK EOD Monthly';
                
                if (s.containsIgnoreCase('US') || s.containsIgnoreCase('canada')) {
                    if (s.containsIgnoreCase('US')) planname = 'VectorVest 7.0 US';
                    else if (s.containsIgnoreCase('canada')) planname = 'VectorVest 7.0 CA';

                    if (s.containsIgnoreCase('end of day') || s.containsIgnoreCase('EOD')) planname += ' EOD Monthly';
                    else if (s.containsIgnoreCase('intraday') || s.containsIgnoreCase('ID')) planname += ' ID Monthly';
                    else if (s.containsIgnoreCase('realtime') || s.containsIgnoreCase('RT')) planname += ' RT Non-Professional Monthly';
                }
                
                if (s.containsIgnoreCase('protrader')) planname = 'ProTrader';
                if (s.containsIgnoreCase('derby')) planname = 'Derby RT Monthly';
                if (s.containsIgnoreCase('autotester')) planname = 'AutoTimer';
                if (s.containsIgnoreCase('optionspro')) planname = 'OptionsPro Monthly';
                if (s.containsIgnoreCase('robotrader')) planname = 'RoboTrader Monthly';
                if (s.containsIgnoreCase('watchdog')) planname = 'WatchDog';
                if (s.containsIgnoreCase('Options Analyzer')) planname = 'Options Analyzer';
                if (s.containsIgnoreCase('Simulator')) planname = 'Simulator';
                if (s.containsIgnoreCase('Variator')) planname = 'Variator';
                system.debug('rateplan_name_to_id_map.get(planname): ' + planname + ': ' + rateplan_name_to_id_map.get(planname));
                // add it to the map
                trial_rateplan_zuora_map.get(aid).add(rateplan_name_to_id_map.get(planname));  
            }
        }
    }*/

    /**
     * update marketo has and had fields
     */
   public static void updateMarketoFields(List<Zuora__Subscription__c> zslist) {
    Map<Id,Account> account_update_map = new Map<Id,Account>();
       // call query again to get the subscription charge info as well, order charge by start date 
    for (Zuora__Subscription__c sub : zslist) {
          // create new account to update or get it from the map
        //8/23 added false initialization.
          Account a = new Account(Id = sub.Zuora__Account__c, Has_RT__c = false,Has_ProTrader__c =false, Has_RT_Trial__c = false, Weeks_Since_Last_Sub__c = 1000, Weeks_Since_Last_Trial__c = 1000,had_trial__c=false,has_trial__c=false,Has_Subscription__c=false,Had_Subscription__c=false,Had_RT_Trial__c=false,isMonthlySubscription__c=false,Has_Derby__c=false,Has_OptionsPro__c=false,Has_AutoTester__c=false);
          if (account_update_map.get(sub.Zuora__Account__c) != null) a = account_update_map.get(sub.Zuora__Account__c);
          else {
           a.Has_Subscription__c = false;
          account_update_map.put(sub.Zuora__Account__c, a);
          }


          // go thorugh the charges
          for (Zuora__SubscriptionProductCharge__c zspc : sub.Zuora__Subscription_Product_Charges__r) {
            //adding
            if(zspc.zuora__Product__r.SF_Product__c != null || test.isRunningtest()){
            if (sub.Zuora__TermEndDate__c < Date.today() || (sub.Zuora__ServiceActivationDate__c < Date.today() && sub.Zuora__AutoRenew__c==true) || sub.Zuora__Status__c == 'Cancelled'){ // sub.Zuora__TermSettingType__c != 'Evergreen'
              // already finished charge plans, use had
              updateHadFields(a, zspc,sub);
            }
            // one time items
            else if (sub.Zuora__Status__c == 'Active' && zspc.Zuora__Type__c == 'One-Time' &&(zspc.zuora__Product__r.SF_Product__r.Feed__c == null || zspc.zuora__Product__r.SF_Product__r.Feed__c == '' || zspc.zuora__Product__r.SF_Product__r.IsTrial__c )) {
              updateHasFields(a, zspc, true,sub);  
            } else if (sub.Zuora__Status__c == 'Active' && (sub.Zuora__TermSettingType__c == 'EVERGREEN' || sub.Zuora__ServiceActivationDate__c >= Date.today() || sub.Zuora__TermEndDate__c>=Date.today())&&((zspc.zuora__Product__r.SF_Product__r.Feed__c != null || zspc.zuora__Product__r.SF_Product__r.IsMobile__c)&& !zspc.zuora__Product__r.SF_Product__r.IsTrial__c)) {
              a.Has_Subscription__c = true;
                a.Had_Subscription__c=false;
                    // update account market if it doesn't exist, set the market to the highest feed possible, always update to prevent issues with account
                    if((zspc.zuora__Product__r.SF_Product__r.Feed__c == 'Express' && a.VV_Feed__c== null) || a.VV_Feed__c== 'Express' )
                            {
                                a.VV_Feed__c =  zspc.zuora__Product__r.SF_Product__r.Feed__c;
                                a.VV_Market__c = zspc.zuora__Product__r.SF_Product__r.Market__c;
                            }else{
                                if (a.VV_Market__c == null) a.VV_Market__c = zspc.zuora__Product__r.SF_Product__r.Market__c;
                                else if (a.VV_Market__c != null && a.VV_Feed__c != null && a.VV_Feed__c == 'EOD' && (zspc.zuora__Product__r.SF_Product__r.Feed__c == 'IntraDay' || zspc.zuora__Product__r.SF_Product__r.Feed__c == 'RealTime')) {
                                    a.VV_Market__c = zspc.zuora__Product__r.SF_Product__r.Market__c;
                                } else if (a.VV_Market__c != null && a.VV_Feed__c != null && a.VV_Feed__c == 'IntraDay' && zspc.zuora__Product__r.SF_Product__r.Feed__c == 'RealTime') {
                                    a.VV_Market__c = zspc.zuora__Product__r.SF_Product__r.Market__c;
                                }
    
                                if (a.VV_Feed__c == null) a.VV_Feed__c = zspc.zuora__Product__r.SF_Product__r.Feed__c == 'EOD' ? 'EOD' : (zspc.zuora__Product__r.SF_Product__r.Feed__c == 'IntraDay' ? 'IntraDay' : (zspc.zuora__Product__r.SF_Product__r.Feed__c == 'RealTime' ? 'RealTime' : null));
                                else if (a.VV_Feed__c != null && a.VV_Feed__c == 'EOD' && (zspc.zuora__Product__r.SF_Product__r.Feed__c == 'IntraDay' || zspc.zuora__Product__r.SF_Product__r.Feed__c == 'RealTime')) {
                                    a.VV_Feed__c = zspc.zuora__Product__r.SF_Product__r.Feed__c == 'IntraDay' ? 'IntraDay' : 'RealTime';
                                } else if (a.VV_Feed__c != null && a.VV_Feed__c == 'IntraDay' && zspc.zuora__Product__r.SF_Product__r.Feed__c == 'RealTime') {
                                    a.VV_Feed__c = 'RealTime';
                                }
                            }

                    // recurring items that are current
              updateHasFields(a, zspc, true,sub);
            }
            }//adding 
          }
          
        }
        if (!account_update_map.isEmpty()) {
          List<Account> accounts_to_update = new List<Account>(account_update_map.values());
          system.debug('##update: ' + accounts_to_update);
          update accounts_to_update;
        }
  }
    static boolean checkProductOptions(string prodOption,Zuora__SubscriptionProductCharge__c zspc){
            boolean isPresent = false;
            if(zspc!=null && prodOption!=null && ((zspc.zuora__Product__r.SF_Product__r.productCode != null && zspc.zuora__Product__r.SF_Product__r.productCode.containsIgnoreCase(prodOption) )|| 
            ( zspc.zuora__Product__r.SF_Product__r.AddOn_Type__c != null && 
            zspc.zuora__Product__r.SF_Product__r.AddOn_Type__c.equalsIgnoreCase(prodOption))))
            {
                isPresent = true;   
            }
        return isPresent;
    }
    
    static boolean checkProduct(string prodType,Zuora__SubscriptionProductCharge__c zspc){
            boolean isPresent = false;
            if(zspc!=null && prodType!=null && zspc.zuora__Product__r.SF_Product__c != null && ((zspc.zuora__Product__r.SF_Product__r.productCode != null && zspc.zuora__Product__r.SF_Product__r.productCode.containsIgnoreCase(prodType)) || 
            (zspc.zuora__Product__r.SF_Product__r.Feed__c!=null && zspc.zuora__Product__r.SF_Product__r.Feed__c.equalsIgnoreCase(prodType))))
            {
                isPresent = true;   
            }
        return isPresent;
    }
  // has
    public static void updateHasFields(Account a, Zuora__SubscriptionProductCharge__c zspc, Boolean bool,Zuora__Subscription__c sub) {
        Date ZuoraEffectiveEnddate = sub.Zuora__TermEndDate__c;
        if(ZuoraEffectiveEnddate == null)
            ZuoraEffectiveEnddate =  sub.Zuora__ServiceActivationDate__c;
        a.Had_Subscription__c = false;

        if (checkProductOptions('AutoTester',zspc)) a.Has_AutoTester__c = bool;
        else if (checkProductOptions('OptionAnalyzer',zspc) || checkProductOptions('Option Analyzer',zspc)) a.Has_Options_Analyzer__c = bool;
        else if (checkProductOptions('Simulator',zspc)) a.Has_Simulator__c = bool; // Not being used
        else if (checkProductOptions('Variator',zspc)) a.Has_Variator__c = bool; // Not being used
        else if (checkProductOptions('Pro Trader',zspc) || checkProductOptions('ProTrader',zspc)) a.Has_ProTrader__c = bool;
        else if (checkProductOptions('Derby',zspc)) a.Has_Derby__c = bool;
        else if (checkProductOptions('OptionsPro',zspc)) a.Has_OptionsPro__c= bool;
        
        if (checkProduct('RealTime',zspc)) {
          a.Has_RT__c = bool;
          if (a.Has_RT__c) a.Had_RT__c = false;
        }
         //added
         String productname = zspc.zuora__Product__r.SF_Product__r.Name;
            if(!productname.containsIgnoreCase('Trial') && productname.containsIgnoreCase('Monthly')){
            a.isMonthlySubscription__c =true;
        }
        //added
        
        if (checkProduct('RealTime',zspc) && zspc.zuora__Product__r.SF_Product__r.IsTrial__c) {
          a.Has_RT_Trial__c = bool;
          a.Days_To_RT_Trial_End__c = Date.today().daysBetween(ZuoraEffectiveEnddate);
          a.RT_Trial_Day_Num__c = zspc.Zuora__EffectiveStartDate__c.daysBetween(Date.today());
          a.RT_Trial_Start__c = zspc.Zuora__EffectiveStartDate__c;
          a.RT_Trial_End__c =ZuoraEffectiveEnddate;
          if (a.Has_RT_Trial__c) {
            a.Weeks_Since_Last_RT_Trial__c = 0;
            a.Had_RT_Trial__c = false;
          }
        }
        if (zspc.zuora__Product__r.SF_Product__r.IsTrial__c) {
            if(zspc.zuora__Product__r.SF_Product__r.isMobile__c){
                a.VV_MobileTrialAccessStart__c =zspc.Zuora__EffectiveStartDate__c;
                a.VV_MobileTrialAccessEnd__c =ZuoraEffectiveEnddate;
                a.VV_MobileTrialProduct__c = zspc.zuora__Product__r.SF_Product__r.id;
            }
          a.Has_Trial__c = bool;
          a.Trial_Day_Num__c = zspc.Zuora__EffectiveStartDate__c.daysBetween(Date.today());
          a.Trial_Start__c = zspc.Zuora__EffectiveStartDate__c;
          a.Trial_End__c =ZuoraEffectiveEnddate;
          if (a.Has_Trial__c) {
            a.Weeks_Since_Last_Trial__c = 0;
            a.Had_Trial__c = false;
          }
          a.Days_To_Trial_End__c = Date.today().daysBetween(ZuoraEffectiveEnddate);
        }
        //Check for subscribed Main products
        //added
        if(zspc.zuora__Product__r.SF_Product__r.ismobile__c && !zspc.zuora__Product__r.SF_Product__r.IsTrial__c){
            //VV_MobileSubscriptionAccessEnd__c
            //VV_MobileSubscriptionAccessStart__c = 
            a.VV_MobileSubscriptionAccessStart__c = a.VV_MobileSubscriptionAccessStart__c == null ? zspc.Zuora__EffectiveStartDate__c : (a.VV_MobileSubscriptionAccessStart__c < zspc.Zuora__EffectiveStartDate__c ? zspc.Zuora__EffectiveStartDate__c : a.VV_MobileSubscriptionAccessStart__c );
            a.VV_MobileSubscriptionProducts__c = zspc.zuora__Product__r.SF_Product__r.Name;
            a.VV_MobileSubscriptionAccessEnd__c = null;
        }
        //added end
        if (zspc.zuora__Product__r.SF_Product__r.Feed__c != null && !zspc.zuora__Product__r.SF_Product__r.IsTrial__c) {
            // last subscription start date, if it is null, use this plan's start, if it is not and before this start, use this one
            a.Last_Sub_Start__c = a.Last_Sub_Start__c == null ? zspc.Zuora__EffectiveStartDate__c : (a.Last_Sub_Start__c < zspc.Zuora__EffectiveStartDate__c ? zspc.Zuora__EffectiveStartDate__c : a.Last_Sub_Start__c);
            
            // has effective subscription, clear out some fields
            a.Weeks_Since_Last_Sub__c = 0;
            a.Last_Sub_End__c = null;
        }

        // count all the trials
        if (zspc.zuora__Product__r.SF_Product__r.IsTrial__c) {
            a.Past_Year_Trial_Count__c = a.Past_Year_Trial_Count__c == null ? a.Past_Year_Trial_Count__c = 1 : (a.Past_Year_Trial_Count__c + 1);
        }
    }

    // had
    public static void updateHadFields(Account a, Zuora__SubscriptionProductCharge__c zspc,Zuora__Subscription__c sub) {
        Date ZuoraEffectiveEnddate = sub.Zuora__TermEndDate__c;
        if(ZuoraEffectiveEnddate == null)
            ZuoraEffectiveEnddate =  sub.Zuora__ServiceActivationDate__c;
        a.Had_Subscription__c = false;
     String productname = zspc.zuora__Product__r.SF_Product__r.Name;
      if (!a.Has_Subscription__c && (zspc.zuora__Product__r.SF_Product__r.Feed__c != null && !zspc.zuora__Product__r.SF_Product__r.IsTrial__c) && sub.Zuora__Status__c == 'Cancelled' ) 
      a.Had_Subscription__c = true;
      //added
      /*
      if(!productname.containsIgnoreCase('Trial') && productname.containsIgnoreCase('Monthly')){
            a.isMonthlySubscription__c =false;
        }
        */ //Commented 8/23
      //added end

      if (checkProduct('RealTime',zspc)){
            if(!a.Has_RT__c){// add 8/23
                a.Had_RT__c = true;
            }
        }
        /*if (checkProductOptions('Derby',zspc)) a.Has_Derby__c = false;
        if (checkProductOptions('OptionsPro',zspc)) a.Has_OptionsPro__c = false;*///commenetd 8/23
        if(zspc.zuora__Product__r.SF_Product__r.IsTrial__c){
          system.debug('hadddd'+zspc.id);
          if(!a.has_trial__c && zspc.zuora__Product__r.SF_Product__r.IsTrial__c){
            a.had_trial__c=true;
            //a.has_trial__c=false; comment 8/23
          }
      }
      
        if (checkProduct('RealTime',zspc) && zspc.zuora__Product__r.SF_Product__r.IsTrial__c) {
            if(!a.Has_RT_Trial__c){//add 8/23
                a.Had_RT_Trial__c = true;
            }
        if (!a.Has_RT_Trial__c) a.Weeks_Since_Last_RT_Trial__c = Math.roundToLong(ZuoraEffectiveEnddate.daysBetween(Date.today()) / 7);
      }
       
        if (zspc.zuora__Product__r.SF_Product__r.Feed__c != null && !zspc.zuora__Product__r.SF_Product__r.IsTrial__c) {
        if (a.Weeks_Since_Last_Sub__c != 0) { // only calculate this if subscription is not current
          Long weeks = Math.roundToLong(ZuoraEffectiveEnddate.daysBetween(Date.today()) / 7);
          // only assign if this is more current because we could be upgrading or downgrading multiple times, we only want the most current last sub
          if (weeks < a.Weeks_Since_Last_Sub__c) a.Weeks_Since_Last_Sub__c = weeks;  
        }
      }
      
        if (zspc.zuora__Product__r.SF_Product__r.IsTrial__c && !a.Has_Trial__c) {
        if (a.Weeks_Since_Last_Trial__c != 0) {
          Long weeks = Math.roundToLong(ZuoraEffectiveEnddate.daysBetween(Date.today()) / 7);
          if (weeks < a.Weeks_Since_Last_Trial__c) a.Weeks_Since_Last_Trial__c = Math.roundToLong(ZuoraEffectiveEnddate.daysBetween(Date.today()) / 7);
        }
      }
    //added
        if (zspc.zuora__Product__r.SF_Product__r.ismobile__c && !zspc.zuora__Product__r.SF_Product__r.IsTrial__c ) {
            // last subscription end date, if it is null, then use the effective end date, if it is not, and it is before this sub's effective end date, use this one
            a.VV_MobileSubscriptionAccessEnd__c= a.VV_MobileSubscriptionAccessEnd__c== null ?ZuoraEffectiveEnddate : (a.VV_MobileSubscriptionAccessEnd__c <ZuoraEffectiveEnddate ?ZuoraEffectiveEnddate : a.VV_MobileSubscriptionAccessEnd__c);
        }
    //added end
        // no more primary subscription
        if (zspc.zuora__Product__r.SF_Product__r.Feed__c != null && !zspc.zuora__Product__r.SF_Product__r.IsTrial__c && a.Weeks_Since_Last_Sub__c != 0) {
            // last subscription end date, if it is null, then use the effective end date, if it is not, and it is before this sub's effective end date, use this one
            a.Last_Sub_End__c = a.Last_Sub_End__c == null ?ZuoraEffectiveEnddate : (a.Last_Sub_End__c <ZuoraEffectiveEnddate ?ZuoraEffectiveEnddate : a.Last_Sub_End__c);
        }

        // count all the trials
        if (zspc.zuora__Product__r.SF_Product__r.IsTrial__c && zspc.Zuora__EffectiveStartDate__c.daysBetween(Date.today()) < 365) {
            a.Past_Year_Trial_Count__c = a.Past_Year_Trial_Count__c == null ? a.Past_Year_Trial_Count__c = 1 : (a.Past_Year_Trial_Count__c + 1);
        }

    }


}