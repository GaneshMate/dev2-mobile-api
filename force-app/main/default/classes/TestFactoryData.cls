public class TestFactoryData {

   
    public static CustomerInfoService.CustomerInfo createCustomer(){
        
        VVCommunitySettings__c setting = new VVCommunitySettings__c();
        setting.Name = 'OrgDefault';
        setting.Account_Owner_Name__c='Cloudware Team';
        setting.Customer_Profile__c='Customer Community Login User (Custom)';
        setting.Mobile_Customer_Profile__c='Customer Community Login User (Mobile)';
        //setting.Session_Uid__c='ps.vectorvest@cloudwareconnections.com.partial';
        //setting.Session_Pwd__c='pass4cwc4';
        //setting.Session_Security_Token__c='WAnfRbKiDc4plxJwuqJAMN43i';
        //setting.Org_Num__c='cs18';
        //setting.Client_Id__c='3MVG98RqVesxRgQ5VEQ7zQ0ZneBoxnO70.FZkgMcey7EzpJfT9vBaIQIYMqpH9QZ8kHHf44z8QiatCt4FkSfv';
        //setting.Client_Secret__c='1226336843678337315';
        insert setting;
        
        
        CustomerInfoService.CustomerInfo ci = new CustomerInfoService.CustomerInfo();
        ci.userName = 'vvtest1@vectorvest-salesforce.com.partial';
        ci.firstName = 'Testfirstname';
        ci.lastName = 'Testlastname';
        ci.salutation = 'Mr.';
        string s= 'vvtest1'+system.now().format().remove(' ')+'@email.com';
        string s1 = s.remove(':');
        string s2 = s.remove('/');
        system.debug('****s****'+s);
        system.debug('****s****'+s2);
        ci.email = 'vvtest1@email.com';
        ci.optionsProUserName = 'testuser';
        ci.optionsProUserPassword = 'testpass';
        ci.countryOfResidence = 'USA';
        ci.BillingStreet = '222 test street';
        ci.BillingPostalCode = '30097';
        ci.BillingCity = 'testcity';
        ci.BillingState = 'NY';
        ci.BillingCountry = 'USA';
        ci.feed = 'EOD';
        ci.market = 'US';
        ci.edition = 'Non-Professional';
        ci.subscriptiontype = 'Monthly';
        ci.applestore = true; 
        ci.certificateId = 'apple';
        /* LastModified Date 07/05/2017 - Added GDPR_Opt_in__pc feild */
        ci.optinMarketing = 'Yes';
        return ci;
    }
     public static CustomerInfoService.CustomerInfo createCustomer1(){
         CustomerInfoService.CustomerInfo ci = new CustomerInfoService.CustomerInfo();
         string user= 'vvtest1'+system.now().format().remove(' ')+'@vectorvest-salesforce.com.partial1';
         string user1 = user.remove(':');
         string user2 = user1.remove('/');
         ci.userName = user2;
         ci.firstName = 'Testfirstname'+system.now().format().remove(' ');
         ci.lastName = 'Testlastname'+system.now().format().remove(' ');
         ci.salutation = 'Mr.';
         string s= 'vvtest1'+system.now().format().remove(' ')+'@email.com';
         string s1 = s.remove(':');
         string s2 = s1.remove('/');
         ci.email = s2;
         ci.optionsProUserName = 'testuser';
         ci.optionsProUserPassword = 'testpass';
         ci.countryOfResidence = 'USA';
         ci.BillingStreet = '222 test street';
         ci.BillingPostalCode = '30097';
         ci.BillingCity = 'testcity';
         ci.BillingState = 'NY';
         ci.BillingCountry = 'USA';
         ci.feed = 'EOD';
         ci.market = 'US';
         ci.edition = 'Non-Professional';
         ci.subscriptiontype = 'Monthly';
         ci.applestore = true;
         ci.certificateId = 'apple';
         ci.accountcurrency = 'USD';
         return ci;
    }
    
    public static String createContractMock(Account accountRec){
        String zuora_aid = zuoraAPIHelper.createAccount(accountRec, null);
        string zuora_contactid;
        if(!zuora_aid.containsIgnoreCase('error')) {
            // insert zuora contact
            zuora_contactid = zuoraAPIHelper.createContact(accountRec, zuora_aid); 
            System.debug('*****zuora_contactid******'+zuora_contactid);
        }
        String result;
        if (!zuora_contactid.containsIgnoreCase('error')) {
            // activate account without any payment
            result = zuoraAPIHelper.activateAccount(null, zuora_aid, zuora_contactid, null);
            System.debug('*****result******'+result);
        }
        if(result == 'success'){
            zuoraAPIHelper.updateAcc(accountRec.Id, accountRec.FirstName + ' ' + accountRec.LastName, zuora_aid);
        }
        SBQQ__Subscription__c subscription = new SBQQ__Subscription__c();
        if(result =='success'){
            subscription = CPQIntegrationAPI.createContractAndSubscription(accountRec,zuora_aid,'Tier 2 Mobile - US Trial',string.valueOf(system.today()),'Draft',12,1,10.9,string.valueOf(system.today()),'Termed','Apple',true);
        System.debug('****subscription*****'+subscription);
            contract cont =[SELECT Id, ZSB__SubscriptionId__c FROM Contract WHERE ContractNumber =:subscription.SBQQ__ContractNumber__c];
         System.debug('****cont*****'+cont);
            cont.ZSB__SubscriptionId__c ='askjhdszfsfkfhsh';
        update cont;
        System.debug('subscription.SBQQ__ContractNumber__c***'+subscription.SBQQ__ContractNumber__c);
        return subscription.SBQQ__ContractNumber__c;
        }
        return null;
        
    }
    
    public static string configureZuoraAccount(Account accountRec){
        String zuora_aid = zuoraAPIHelper.createAccount(accountRec, null);
        System.debug('*****zuora_aid******'+zuora_aid);
        String zuora_contactid;
        if(!zuora_aid.containsIgnoreCase('error')) {
            // insert zuora contact
            zuora_contactid = zuoraAPIHelper.createContact(accountRec, zuora_aid); 
            System.debug('*****zuora_contactid******'+zuora_contactid);
        }
        if (!zuora_contactid.containsIgnoreCase('error')) {
            // activate account without any payment
            String result = zuoraAPIHelper.activateAccount(null, zuora_aid, zuora_contactid, null);
            System.debug('*****result******'+result);
        }
        return zuora_aid;
    }
}