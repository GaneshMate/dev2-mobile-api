@isTest
public class MagalogCleanerBatchTest {
    
    @isTest static void testMagalog(){
        test.startTest();
        List <Magalog_Finder__c> mgfinderList = new List<Magalog_Finder__c>();
        
        for(integer i = 0; i<200; i++){
            Magalog_Finder__c a = new Magalog_Finder__c(FirstName__c='testAccount'+i); 
            mgfinderList.add(a);
        }
        insert mgfinderList; 
        List <Magalog_Finder__c> mgfinderList1 = [SELECT Id FROM Magalog_Finder__c WHERE CreatedDate != LAST_N_DAYS:180];
        
        MagalogCleanerBatch mcb= new MagalogCleanerBatch();
        Database.executeBatch(mcb);
        test.stopTest();
        
    }
}