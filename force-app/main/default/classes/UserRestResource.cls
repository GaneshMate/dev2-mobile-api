@RestResource(urlMapping='/user/update/*')
global class UserRestResource {
    
    global class Error {
        global integer code;
        global string msg;
        global Error(integer code, string msg) {
            this.code = code;
            this.msg = msg;
        }
    }
    private static string serialize(object o) {
        return JSON.serialize(o);
    }
    @HttpPut
    global static void updateUser(String userName){
        integer retCode = 0;
        string retBody = '';
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        List<User> updateUser = new List<User>();
        if(!String.isBlank(userName)){
            List<User> UserData=[SELECT name,userName,IsActive FROM User where UserName =: userName];
            System.debug('******UserData*******'+UserData);
            if(UserData!=null && UserData.size()!=0){
                for(User user : UserData){
                    if(user.IsActive == false){
                        user.IsActive = true;
                        updateUser.add(user);
                    }
                }
                
                List<Database.SaveResult> failedUserList = new List<Database.SaveResult>();
                if(updateUser!=null && updateUser.size()!=0){
                      System.debug('******updateUser*******'+updateUser);
                    Database.SaveResult[] userResults  = Database.update(updateUser, false);
                    for(Database.SaveResult ds :userResults){
                        if (ds.isSuccess() == false)
                            failedUserList.add(ds);    
                    }
                }
                if(!updateUser.isEmpty() && failedUserList.size()==0 && failedUserList.isEmpty()){
                    retCode = 200;
                    retBody = serialize(new Error(retCode, 'User Successfully Activated!'));    
                }else if(updateUser.isEmpty() && failedUserList.size()==0 && failedUserList.isEmpty()){
                    retCode = 400;
                    retBody = serialize(new Error(retCode, 'This user is already active'));
                }
            }else{
                retCode = 400;
                retBody = serialize(new Error(retCode, 'User not found'));
            }   
        }else{
            retCode = 400; 
            retBody = serialize(new Error(retCode, 'Error:Email is required!'));
        }
        
        res.headers.put('Content-type', 'application/json');
        res.statusCode = retCode;
        res.responseBody = Blob.valueOf(retBody);
    }
}