@istest(seealldata=true)
private class EcommNewTest{
    private static testmethod void testmethod1(){
           
                                       
            Product2 prod = new Product2(Name = 'VectorVest 7 EOD US Monthly', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_MONTHLY' , Family = 'Books');
            //insert prod;
            Product2 prod2 = new Product2(Name = 'VectorVest 7 EOD US Annual', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_ANNUAL' , Family = 'Books');
            //insert prod2;
            Product2 prod3 = new Product2(Name = 'VectorVest 7 EOD CA Monthly', Feed__c = 'EOD',Market__c = 'CA',
                             ProductCode = 'VV_7.0_EOD_CA_MONTHLY' , Is_Secondary_market__c =false,Family = 'Books');
           // insert prod3;
            
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            //insert standardPrice;
            PricebookEntry standardPrice2 = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod2.Id,
                UnitPrice = 10000, IsActive = true);
            //insert standardPrice2;
            PricebookEntry standardPrice3 = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod3.Id,
                UnitPrice = 10000, IsActive = true);
            //insert standardPrice3;
             PriceBookIds__c objpb = new PriceBookIds__c();
            objpb.name='Standard Price Book';
            objpb.Price_Book_Id__c = pricebookId;
            //insert objpb;
            
            CPQCommunity__c cpc = new CPQCommunity__c();
            cpc.Name = 'test';
            cpc.PriceBookId__c = pricebookId ;
           // insert cpc ;
            
            
            
            
        Zuora_Integration__c obj = new Zuora_Integration__c();
        obj.name='OrgDefault';
        obj.ACH_Gateway__c='Authorize.net';
        obj.apiAccessKeyId__c='matt.zhu@VV.com';
        obj.apiSecretAccessKey__c='Rad1anc8!';
        obj.Community_Domain__c='https://partial-vectorvestcom.cs18.force.com';
        obj.Endpoint__c='apisandbox-api.zuora.com';
        obj.EUR_Gateway__c='Vantiv - EUR';
        obj.ExternalPaymentId__c='2c92c0f9528db6aa015290834d504e3e';
        obj.GBP_Gateway__c='Vantiv - GBP';
        obj.Internal_Domain__c='https://c.cs18.visual.force.com';
        obj.URI__c='https://apisandbox.zuora.com/apps/PublicHostedPageLite.do';
        obj.Payment_Endpoint__c='apisandbox.zuora.com';
        obj.USD_Gateway__c='Cybersource';
        //insert obj;
        zqu__HostedPageLiteSetting__c objpagesetting = new zqu__HostedPageLiteSetting__c(name='Community Credit Card',zqu__PageId__c='2c92c0f853f45c240154049eb3df20b4',zqu__ComponentName__c='PaymentPage',zqu__PaymentMethodType__c='Credit Card');
        //insert objpagesetting;
        test.starttest();
        //apexpages.currentpage().getparameters().put('Promo','abcde');
        Ecommnew objEcommnew = new Ecommnew();
        objEcommnew.salutation ='mr';
        objEcommnew.firstname  = 'testcls009';
        objEcommnew.lastname='lastcls009';
        objEcommnew.confirmEmail  = 'Test009test@gmail.com';
        objEcommnew.password ='Password123';
        objEcommnew.confirmpassword ='Password123';
        objEcommnew.a= new Account(phone='546712345689',personemail = 'Test009test@gmail.com',BillingStreet ='test',BillingCity='test',BillingState='test',BillingCountry='USA',BillingPostalcode='12345');
        objEcommnew.redirectpage();
        objEcommnew.enteruserinfo();
        objEcommnew .sameasbilling = true;
        objEcommnew .selectedmarket='US';
        objEcommnew.selectedmarket2='CA';
        objEcommnew .choosemarket();
        objEcommnew .selectedproduct='RT';
        objEcommnew .showpickpro=false;
        objEcommnew.selectproduct();
        //objEcommnew.enteruserinfo();
        
        //getFeedRelatedProducts('EOD','USA','CA','Monthly','');
        objEcommnew.credit_card_num  ='4444333322221111';
        objEcommnew.credit_card_cvv  ='123';
        objEcommnew.payment_method = new Payment_Method_SF__c(Credit_Card_Holder_Name__c ='Test',Credit_Card_Type__c='Visa',Credit_Card_Expiration_Month__c ='12',Credit_Card_Expiration_Year__c='2018');
        objEcommnew.choosetrial();
        objEcommnew.createtrialcontract();
        objEcommnew.goback();
        
        test.stoptest();
    }
    private static testmethod void testmethod2(){
         Account acc = new Account();
            acc.firstname='test acc1';
            acc.lastName = 'test acc';
            acc.billingCity ='Test City';
            acc.billingStreet = 'TestStreet';
            acc.billingstate ='TestState';
            acc.billingPostalcode ='23451';
            acc.billingcountry ='United States';
            acc.recordtypeID='0121100000014fO';
           acc.PersonMailingCity='Test City';
            acc.PersonMailingStreet = 'TestStreet';
            acc.PersonMailingState ='TestState';
            acc.PersonMailingPostalCode ='23451';
            acc.PersonMailingCountry ='United States';
            acc.phone='090123456987';
            //acc.ispersonAccount = true;
            //insert acc;
            
            Opportunity opp = new Opportunity(Name=acc.Name + ' Opportunity ' ,
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=acc.Id);
                                       Product2 prod = new Product2(Name = 'VectorVest 7 EOD US Monthly', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_MONTHLY' , Family = 'Books');
            //insert prod;
            Product2 prod2 = new Product2(Name = 'VectorVest 7 EOD US Annual', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_ANNUAL' , Family = 'Books');
            //insert prod2;
            Product2 prod3 = new Product2(Name = 'VectorVest 7 EOD CA Monthly', Feed__c = 'EOD',Market__c = 'CA',
                             ProductCode = 'VV_7.0_EOD_CA_MONTHLY' , Is_Secondary_market__c =false,Family = 'Books');
            //insert prod3;
            
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            //insert standardPrice;
             PricebookEntry standardPrice2 = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod2.Id,
                UnitPrice = 10000, IsActive = true);
            //insert standardPrice2;
            PricebookEntry standardPrice3 = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod3.Id,
                UnitPrice = 10000, IsActive = true);
            //insert standardPrice3;
            PriceBookIds__c objpb = new PriceBookIds__c();
            objpb.name='Standard Price Book';
            objpb.Price_Book_Id__c = pricebookId;
            //insert objpb;
            
            CPQCommunity__c cpc = new CPQCommunity__c();
            cpc.Name = 'test';
            cpc.PriceBookId__c = pricebookId ;
            //insert cpc ;
          SBQQ__Quote__c quoteRec = new SBQQ__Quote__c();
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SiteUser_Primary_Check__c = true;
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SBQQ__Opportunity2__c = opp.Id;
            //quoteRec.ZSB__BillToContact__c = c.Id;
           // quoteRec.ZSB__SoldToContact__c = c.Id;
            quoteRec.ZSB__BillCycleDay__c =  '1';
            quoteRec.ZSB__BillingBatch__c = 'Batch1';
            //insert quoteRec ;
            SBQQ__QuoteLine__c ojQuoteline = new SBQQ__QuoteLine__c();
            ojQuoteline.SBQQ__Quote__c = quoteRec .id;
            ojQuoteline.SBQQ__Product__c = prod.id;
            //insert ojQuoteline;
        campaign objcampaign = new campaign();
        objcampaign.name='test';
        objcampaign .PromoCode__c ='abcde';
        objcampaign.startdate=date.today();
        objcampaign .enddate= date.today();
        objcampaign .Quote__c = quoteRec .id;
        objcampaign.Promo_Description__c ='abcde';
        objcampaign .Promo_Name__c = 'test';
        //insert objcampaign;
        Zuora_Integration__c obj = new Zuora_Integration__c();
        obj.name='OrgDefault';
        obj.ACH_Gateway__c='Authorize.net';
        obj.apiAccessKeyId__c='matt.zhu@VV.com';
        obj.apiSecretAccessKey__c='Rad1anc8!';
        obj.Community_Domain__c='https://partial-vectorvestcom.cs18.force.com';
        obj.Endpoint__c='apisandbox-api.zuora.com';
        obj.EUR_Gateway__c='Vantiv - EUR';
        obj.ExternalPaymentId__c='2c92c0f9528db6aa015290834d504e3e';
        obj.GBP_Gateway__c='Vantiv - GBP';
        obj.Internal_Domain__c='https://c.cs18.visual.force.com';
        obj.URI__c='https://apisandbox.zuora.com/apps/PublicHostedPageLite.do';
        obj.Payment_Endpoint__c='apisandbox.zuora.com';
        obj.USD_Gateway__c='Cybersource';
       // insert obj;
       Test.starttest();
        apexpages.currentpage().getparameters().put('Promo','abcde');
        Ecommnew objEcommnew = new Ecommnew();
        objEcommnew .sfquoteid=quoteRec.id;
        objEcommnew.checkquotelines();
        objEcommnew.salutation ='mr';
        objEcommnew.firstname  = 'testcls009';
        objEcommnew.lastname='lastcls009';
        objEcommnew.confirmEmail  = 'Test009test@gmail.com';
        objEcommnew.password ='Password123';
        objEcommnew.confirmpassword ='Password123';
        objEcommnew.a= new Account(personemail = 'Test009test@gmail.com',BillingStreet ='test',BillingCity='test',BillingState='test',BillingCountry='USA',BillingPostalcode='12345');
        objEcommnew.redirectpage();
        objEcommnew.enteruserinfo();
        objEcommnew .sameasbilling = true;
        objEcommnew .selectedmarket='US';
        objEcommnew.selectedmarket2='CA';
        objEcommnew .choosemarket();
        objEcommnew .selectedproduct='RT';
        objEcommnew .showpickpro=false;
        objEcommnew.selectproduct();
        //objEcommnew.enteruserinfo();
        
        //getFeedRelatedProducts('EOD','USA','CA','Monthly','');
        objEcommnew.credit_card_num  ='4444333322221111';
        objEcommnew.credit_card_cvv  ='123';
        objEcommnew.payment_method = new Payment_Method_SF__c(Credit_Card_Holder_Name__c ='Test',Credit_Card_Type__c='Visa',Credit_Card_Expiration_Month__c ='12',Credit_Card_Expiration_Year__c='2018');
        objEcommnew.choosetrial();
        objEcommnew.createtrialcontract();
        objEcommnew.selectedEdition='Professional';
        objEcommnew.getProplanpricing();
        objEcommnew.showSelectProduct= true;
        objEcommnew.goback();
        test.stoptest();
        
        
    }
    
    }