/**
 * bundle apex page controller.
 *
 * find bundle information campaign promo code
 *
 * display bundle information, send existing customer to login page and send new customer to sign up page
 */
public without sharing class vvBundleController {
    public String promocode {get; private set;}
    public Bundle__c bundle {get; private set;}
    public String errmsg {get;private set;}

    public Map<Id, Decimal> regular_price_map {get; private set;}
    public vvBundleController() {
        promocode = ApexPages.currentPage().getParameters().get('promo');
        system.debug('promocode***'+promocode);
        if (String.isBlank(promocode)) {
            system.debug('Empty===');
            errmsg = 'Invalid Promo Code';
            return;
        }

        List<Campaign> cam = [SELECT Bundle__c FROM Campaign WHERE PromoCode__c LIKE :promocode AND StartDate <= TODAY AND EndDate >= TODAY];
        if (cam.isEmpty() || cam[0].Bundle__c == null) {
            system.debug('Empty=none==');
            errmsg = 'Invalid Promo Code';
            return;
        }

        bundle = getBundleInfo(cam[0].Bundle__c);
        if (bundle == null) {
            errmsg = 'Invalid Bundle';
            return;
        }

        updateRegularPrice(bundle.Bundle_Products__r);

    }

    /**
     * pass in bundle id, get bundle + bundle product information
     * @param  bundleid [description]
     * @return          [description]
     */
    public static Bundle__c getBundleInfo(Id bundleid) {
        if (bundleid == null) return null;

        for(Bundle__c bundle : getBundleList(new Set<Id>{bundleid})) {
            return bundle;
        }
        
        return null;
    }

    public static List<Bundle__c> getBundleList(Set<Id> bundleidset) {
        if (bundleidset.isEmpty()) return new List<Bundle__c>();
        return [SELECT 
            Name,
            Cannot_CoExist__c, 
            Existing_Customer_Only__c, 
            New_Customer_Only__c, 
            Required__c, 
            Total_Price__c,
            (SELECT 
                Name,
                HasTrial__c,
                One_Time_Product__c,
                One_Time_Product__r.Family,
                One_Time_Product__r.Name,
                Price__c,
                Regular_Price__c,
                Special_Period_Month__c,
                Trial_Duration_Days__c,
                Trial_Price__c,
                Zuora_Product__c,
                Zuora_Product__r.Name,
                Sub_For_Replace__c,
                Sub_For_Replace_Zuora_Id__c,
                Sub_To_Suspend__c
            FROM Bundle_Products__r
            )
            FROM Bundle__c WHERE Id IN :bundleidset];
    }

    /**
     * determine if we have trial, if we do, how much is trial, if we don't, how much is normal price
     * @param  b [description]
     * @return   [description]
     */
    /*public static Decimal getBundleInitialCharge(Bundle__c b) {
        Decimal totalcharge = 0;
        for (Bundle_Product__c bp : b.Bundle_Products__r) {
            if (bp.HasTrial__c) {
                totalcharge += bp.Trial_Price__c;
            } else totalcharge += bp.Price__c;
        }
        return totalcharge;
    }*/

    /**
     * called by trigger to update regular price on the bundle based on the zuora or product2 selected
     * @param bplist [description]
     */
    public static void updateRegularPrice(List<Bundle_Product__c> bplist) {
        Map<Id, Decimal> pricemap = new Map<Id, Decimal>();
        for (Bundle_Product__c bp : bplist) {
            if (bp.One_Time_Product__c != null) pricemap.put(bp.One_Time_Product__c, 0);
            if (bp.Zuora_Product__c != null) pricemap.put(bp.Zuora_Product__c, 0);
        }

        if (!pricemap.isEmpty()) {
            for (PriceBookEntry pbe : [SELECT Product2Id, UnitPrice FROM PriceBookEntry WHERE IsActive = true AND product2Id IN :pricemap.keySet()]) {
                pricemap.put(pbe.Product2Id, pbe.UnitPrice);
            }

            for (zqu__ProductRatePlanCharge__c prpc : [SELECT zqu__ProductRatePlan__c, zqu__ListPrice__c FROM zqu__ProductRatePlanCharge__c 
                            WHERE zqu__ProductRatePlan__c IN :pricemap.keySet()]) {
                pricemap.put(prpc.zqu__ProductRatePlan__c, prpc.zqu__ListPrice__c);
            }
        }

        for (Bundle_Product__c bp : bplist) {
            if (bp.One_Time_Product__c != null) bp.Regular_Price__c = pricemap.get(bp.One_Time_Product__c);
            if (bp.Zuora_Product__c != null) bp.Regular_Price__c = pricemap.get(bp.Zuora_Product__c);
        }
    }

    /**
     * get a list of bundles currently used in this account
     */
    public static List<Bundle__c> getBundlesInAccount(Id accountId) {
        Set<Id> bundleids = new Set<Id>();
        for (Zuora__Subscription__c zs : [SELECT Bundle_Product__c, Bundle_Product__r.Bundle__c FROM Zuora__Subscription__c WHERE Zuora__Status__c = 'Active' And Zuora__Account__c = :accountId]) {
            if (zs.Bundle_Product__c != null) bundleids.add(zs.Bundle_Product__r.Bundle__c);
        }
        return getBundleList(bundleids);
    }


    /**
     * called by bundle trigger before 
     */
    public static void autoSetExisting(List<Bundle__c> blist) {
        // if required and prereq are selected, checked existing customer only, uncheck new customer only
        for (Bundle__c b : blist) {
            if (b.Cannot_CoExist__c != null || b.Required__c != null) {
                b.Existing_Customer_Only__c = true;
                b.New_Customer_Only__c = false;
            }
        }
    }
}