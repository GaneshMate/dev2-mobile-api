global class testSubAccountsBatch_1 implements Database.Batchable<sObject>,database.stateful{
    
    global static final List<String> weekDays = new List<String>{'Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'};
    global final Date d;
    global final Date dn;
    global final String weekDay;
    global final Date weekStart;
    global final Date weekEnd;
    global Set<String> historyFields;
    global List<Account_Sub_Field_API_Names__mdt> WeekToAccToSub;
    global List<Account_History_Field_API_Names__mdt> histFieldsToAdd;
    
    global testSubAccountsBatch_1(Date dd){
        d = dd;
        dn = d.addDays(1);
        system.debug('date to compare'+dn);
        Datetime dt = DateTime.newInstance(d, Time.newInstance(0, 0, 0, 0));
        weekDay = dt.format('EEEE');
        weekStart = d.addDays(-weekDays.indexOf(dt.format('EEEE')));
        weekEnd = weekStart.addDays(6);
        system.debug ('Entered Constructor');
        WeekToAccToSub = NEW List <Account_Sub_Field_API_Names__mdt> ();
        histFieldsToAdd = NEW List <Account_History_Field_API_Names__mdt> ();
        WeekToAccToSub = [SELECT Account_API__c, Subscription_API__c, MasterLabel FROM Account_Sub_Field_API_Names__mdt];
        histFieldsToAdd = [SELECT MasterLabel FROM Account_History_Field_API_Names__mdt];
        
        historyFields = NEW Set<String>();
        for (Account_History_Field_API_Names__mdt temp : histFieldsToAdd) {
            historyFields.add(temp.MasterLabel);
        }
        system.debug('historyFields'+historyFields);
    }
    private static Test_Weekly_Customers__c createWC(Date d, Date weekStart, Date weekEnd, String weekDay, Test_Weekly_Customers__c wc, 
                                                     Account a, List<AccountHistory> aHs, SBQQ__Subscription__c sb, Map<String, AccountHistory> fidWithItsHists, List<Account_Sub_Field_API_Names__mdt> WeekToAccToSub){
                                                        for (Account_Sub_Field_API_Names__mdt setting : WeekToAccToSub) {
                                                            if (setting.Account_API__c != NULL) {
                                                                if (a.get(setting.Account_API__c) != NULL)
                                                                    wc.put(setting.MasterLabel, a.get(setting.Account_API__c));
                                                            }
                                                            if (setting.subscription_API__c != NULL) {
                                                                if (sb.get(setting.subscription_API__c) != NULL)
                                                                    wc.put(setting.MasterLabel, sb.get(setting.subscription_API__c));
                                                            }
                                                         }
                                                        wc.Week_Start_Date__c = weekStart;
                                                        wc.Week_End_Date__c = weekEnd;
                                                        wc.put(weekDay+'__c',d);
                                                        if (a.Vector_Vest_Customer_ID__c != NULL)
                                                            wc.Vector_Vest_Customer_ID__c = a.Vector_Vest_Customer_ID__c.replace('@vectorvest-salesforce.com','');
                                                             
                                                        if(!Test.isRunningTest()) {
                                                            wc.Product_Name__c = sb.SBQQ__ProductName__c;
                                                            wc.Source__c = sb.SecondaryCampaignSource__c;
                                                        }

                                                        if(!aHs.isEmpty()){
                                                            for(AccountHistory ah: aHs){
                                                                if(ah.Field == 'Type')
                                                                    wc.put('Customer_Type__c',ah.oldValue);
                                                                else if(ah.Field.endsWith('__c'))
                                                                    wc.put(ah.Field,ah.oldValue);
                                                                else
                                                                    wc.put(ah.Field+'__c',ah.oldValue);
                                                            }
                                                        }
                                                        for(String ahf:fidWithItsHists.keyset()){
                                                            AccountHistory ah = fidWithItsHists.get(ahf);
                                                            if(ah.Field == 'Type')
                                                                wc.put('Customer_Type__c',ah.newValue);
                                                            else if(ah.Field.endsWith('__c'))
                                                                wc.put(ah.Field,ah.newValue);
                                                            else
                                                                wc.put(ah.Field+'__c',ah.newValue);
                                                        }
                                                        return wc;
                                                    } 
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        system.debug ('Entered Start');
        String Query = 'SELECT CreatedDate, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, EnterpriseCustomerID__c, ';
        Query+='FirstName, LastName, MiddleName,';
        Query+='Type, Id, HasOptedOutOfMassEmail__c, PersonHasOptedOutOfEmail, HasOptedOutOfMassPostMail__c,';
        Query+='PersonDoNotCall, Name, Vector_Vest_Customer_Id__c, YahooId__c, ';
        Query+='(SELECT AccountId, Field, OldValue, NewValue, Id, CreatedDate FROM Histories WHERE CreatedDate >= :dn AND Field IN :historyFields ';
        Query+='ORDER BY CreatedDate DESC), ';
        Query+='(SELECT ID, Name, SBQQ__ProductName__c, SecondaryCampaignSource__c FROM SBQQ__Subscriptions__r WHERE (SBQQ__TerminatedDate__c = null or SBQQ__TerminatedDate__c > :d) AND';
        Query+='((SBQQ__Contract__r.ZSB__TermType__c = \'Evergreen\' AND SBQQ__StartDate__c <= :d) ';
        Query+='OR (SBQQ__Contract__r.ZSB__TermType__c != \'Evergreen\' AND SBQQ__StartDate__c <= :d AND SBQQ__EndDate__c >= :d)) ) FROM Account ';
        Query+='WHERE Id IN (SELECT SBQQ__Account__c FROM SBQQ__Subscription__c WHERE (SBQQ__TerminatedDate__c = null or SBQQ__TerminatedDate__c > :d) AND';
        Query+='((SBQQ__Contract__r.ZSB__TermType__c = \'Evergreen\' AND SBQQ__StartDate__c <= :d)';
        Query+='OR (SBQQ__Contract__r.ZSB__TermType__c != \'Evergreen\' AND SBQQ__StartDate__c <= :d AND SBQQ__EndDate__c >= :d)) AND SBQQ__Quantity__c > 0)';
        return Database.getQueryLocator(query);
        
    }
    
    global void execute(Database.BatchableContext BC, List<Account> accs){
        system.debug ('Entered Execute');
        Map<Id, SBQQ__Subscription__c> subcsWithWeekInfo = new Map<Id, SBQQ__Subscription__c>(
            [SELECT ID, Name, SBQQ__Account__c , SBQQ__StartDate__c, SBQQ__EndDate__c, SBQQ__TerminatedDate__c, 
             SecondaryCampaignSource__c, SBQQ__ProductId__c , SBQQ__ProductName__c, Enterprise_Subscription_ID__c,
             SBQQ__QuoteLine__r.SBQQ__Quote__r.SecondaryCampaignsource__r.Primarysources__r.PrimarysourceID__c, SBQQ__QuoteLine__r.SBQQ__Quote__r.SecondaryCampaignsource__r.Primarysources__r.Name,
             (SELECT Id, Name, Saturday__c, Sunday__c, Monday__c, Tuesday__c, Wednesday__c, Thursday__c, Friday__c, 
              Customer_ID__c, Year_Week__c, Product_Name__c, Source__c, Customer_Type__c, BillingStreet__c, BillingCity__c, 
              BillingState__c, BillingPostalCode__c, BillingCountry__c, 
              Week_Start_Date__c, Week_End_Date__c, Account__c, Subscription__c  
              FROM Test_Weekly_Customers__r WHERE Week_Start_Date__c = :weekStart AND Week_End_Date__c = :weekEnd //AND Do_not_Update__c = false 
              ORDER By CreatedDate)
             FROM SBQQ__Subscription__c WHERE SBQQ__Account__c IN :accs AND (SBQQ__TerminatedDate__c = null or SBQQ__TerminatedDate__c > :d) AND // Terminated date should not be populated
             ((SBQQ__Contract__r.ZSB__TermType__c = 'Evergreen' AND SBQQ__StartDate__c <= :d) // if Contract is EverGreen check Start Date it should be less that today
              OR (SBQQ__Contract__r.ZSB__TermType__c != 'Evergreen' AND SBQQ__StartDate__c <= :d AND SBQQ__EndDate__c >= :d)) ]
        );
        //historyFields
        Map<Id, Account> accsWithTodaysHists = new Map<Id, Account>([Select Id, 
                                                                 (SELECT AccountId, Field, OldValue, NewValue, Id, CreatedDate FROM Histories 
                                                                  WHERE CreatedDate >= :d AND CreatedDate < :dn AND Field IN :historyFields ORDER BY CreatedDate DESC) 
                                                                FROM Account Where Id IN :accs]);
            
        List<Test_Weekly_Customers__c> wsLst = new List<Test_Weekly_Customers__c>();
        
        for(Account a:accs){
            List<AccountHistory> todaysHists = accsWithTodaysHists.get(a.Id).Histories;
            Map<String, AccountHistory> fidWithItsHists = new Map<String, AccountHistory>();
            for(AccountHistory ah:todaysHists){
                if(!fidWithItsHists.containsKey(ah.Field))
                    fidWithItsHists.put(ah.Field, ah);
            }
            for(SBQQ__Subscription__c sb:a.SBQQ__Subscriptions__r){
                List<Test_Weekly_Customers__c> weekRecs = subcsWithWeekInfo.get(sb.Id).Test_Weekly_Customers__r;
                if(weekRecs.isEmpty()){
                    wsLst.add(createWC(d, weekStart, weekEnd, weekDay, new Test_Weekly_Customers__c(), a, a.Histories, sb, fidWithItsHists, WeekToAccToSub));
                }
                else {
                    Test_Weekly_Customers__c wc = weekRecs[weekRecs.size()-1];
                    if(!todaysHists.isEmpty())
                        wsLst.add(createWC(d, weekStart, weekEnd, weekDay, new Test_Weekly_Customers__c(), a, a.Histories, sb, fidWithItsHists, WeekToAccToSub));
                    else
                        wsLst.add(createWC(d, weekStart, weekEnd, weekDay, wc, a, a.Histories, sb, fidWithItsHists, WeekToAccToSub));
                }
            }
        }
        upsert wsLst; 
    }
    
    global void finish(Database.BatchableContext BC){
    system.debug ('Entered finish');
    }
}