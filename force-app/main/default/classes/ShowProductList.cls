public class ShowProductList{
    public list<WrapProduct> lstproduct2{get;set;}
    public list<SBQQ__Quote__c> lstQuote{get;set;}
    public boolean showproductlist{get;set;}
    public boolean showquotehistory{get;set;}
    public string selectedQuote{get;set;}
    public list<SBQQ__Quoteline__c> lstQuoteline{get;set;}
    public boolean showQuoteline{get;set;}
    public string selectedQuoteName{get;set;}
    
    public class WrapProduct{
        public product2 objproduct{get;set;}
        public boolean ischecked{get;set;}
        public decimal price{get;set;}
    }
    public void selectProducts(){
        showproductlist =true;
        showquotehistory=false;
        showquoteline=false;
        for(WrapProduct objwrapproduct : lstproduct2){
            objwrapproduct.ischecked=false;              
        }
    }
    public void ShowQuotehistory(){
        showproductlist=false;
        showquotehistory=true;
        showquoteline=false;
        user objuser =[select id, Accountid from user where id=:Userinfo.getuserid()]; 
        Account Acc ;
        if(objuser.Accountid != null){
        Acc = [select id,Name from account where id=:objuser.accountid];
        }  
        else
        Acc = [Select id,Name from account where id='0011100001XUH7N' limit 1];
        lstQuote = new list<SBQQ__Quote__c>();
        lstQuote.addAll([select id, name, SBQQ__Startdate__c from SBQQ__Quote__c where SBQQ__Account__c=:Acc.id]);
    }
    public showProductlist(){
        //showproductlist =true;
        showquoteline=false;
        lstproduct2 = new list<WrapProduct>();
        lstQuote = new list<SBQQ__Quote__c>();
        WrapProduct objWrapProduct ;
        //
             PriceBookIds__c  stdPriceBookId = PriceBookIds__c.getValues('Standard Price Book');
        string strPricebookId = stdPriceBookId.Price_Book_Id__c;
        
        //
        for(Product2 objProduct2 :[select id, productcode, name,(SELECT ID,UnitPrice FROM PricebookEntries where Pricebook2Id =:strPricebookId) from product2 where isactive=true limit 10]){
            objWrapProduct = new WrapProduct();
            objWrapProduct.objProduct=objproduct2;
            objWrapProduct.price=objProduct2 .PricebookEntries[0].UnitPrice;
            lstproduct2.add(objWrapProduct);
        }
    }
    public void goToQuoteDetail(){
        showquoteline=true;
        if(selectedQuote != null){
            selectedQuoteName = [select id, name from SBQQ__Quote__c where id=:selectedQuote].name;
            lstQuoteline = [select id, name, SBQQ__StartDate__c,SBQQ__NetTotal__c,SBQQ__Product__r.name from SBQQ__Quoteline__c where SBQQ__Quote__c=:selectedQuote];
        }
    
    }
    public void submitorder(){
        showproductlist = false;
        user objuser =[select id, Accountid from user where id=:Userinfo.getuserid()]; 
        Account Acc ;
        if(objuser.Accountid != null){
        Acc = [select id,Name from account where id=:objuser.accountid];
        }  
        else
        Acc = [Select id,Name from account where id='0011100001XUH7N' limit 1];
          
        Opportunity objopportunity = new Opportunity(name='Test',closedate=Date.today(), stagename='Prospecting',Accountid=objuser.Accountid);
        objopportunity .Pricebook2Id = CPQIntegrationLayerHelper.cpqPriceBookId;
        objopportunity .SBQQ__QuotePricebookId__c = CPQIntegrationLayerHelper.cpqPriceBookId;
        system.debug('pbookkkk'+CPQIntegrationLayerHelper.cpqPriceBookId);
        objopportunity .Created_By_PortalCustomer__c = true;
        objopportunity .StageName = 'Closed Won';
        
        insert objopportunity;
         
        SBQQ__Quote__c objQuote= new SBQQ__Quote__c(OwnerId='005110000053b8OAAQ',CurrencyIsoCode='USD', SBQQ__Account__c=Acc.id, SBQQ__BillingCity__c='Napa', SBQQ__BillingCountry__c='USA', SBQQ__BillingName__c='Promoacc Accpromo', SBQQ__BillingPostalCode__c='94558', SBQQ__BillingState__c='California', SBQQ__BillingStreet__c=null, SBQQ__ContractingMethod__c='By Subscription End Date',  SBQQ__LineItemsGrouped__c=false, SBQQ__LineItemsPrinted__c=true, SBQQ__Opportunity2__c=objopportunity.id, SBQQ__Ordered__c=false, SBQQ__PaymentTerms__c='Net 30', SBQQ__PriceBook__c='01s110000007WfrAAE', SBQQ__PricebookId__c='01s110000007WfrAAE', SBQQ__Primary__c=false, SBQQ__QuoteProcessId__c='a4X110000005mNl', SBQQ__SalesRep__c='005110000053b8OAAQ', SBQQ__ShippingCity__c='Napa', SBQQ__ShippingCountry__c='USA', SBQQ__ShippingName__c='Promoacc Accpromo', SBQQ__ShippingPostalCode__c='94558', SBQQ__ShippingState__c='Alabama', SBQQ__ShippingStreet__c='123 st', SBQQ__StartDate__c=date.today(), SBQQ__Status__c='Approved',  SBQQ__Type__c='Quote', SBQQ__WatermarkShown__c=false, ZSB__BillCycleDay__c='1', ZSB__BillToContact__c='0031100001Oyp7iAAB', ZSB__BillingAccount__c=null, ZSB__BillingBatch__c='Batch1', ZSB__SoldToContact__c='0031100001Oyp7iAAB', SiteUser_Primary_Check__c=true,SBQQ__OrderByQuoteLineGroup__c=false, SBQQ__PaperSize__c='Default', SBQQ__Unopened__c=false, How_many_days_in_the_month__c=30, Month__c=6, Year__c=2018,  isCaseCreated__c=false, SecondaryCampaignsource__c='a3I11000000NtnpEAC',ZSB__ZuoraBillingAccountId__c='a011100000fWWn1', ZSB__TaxExemptStatus__c='No');
        insert objQuote;
        
        
        SBQQ__Quoteline__c objQuoteline;
        list<SBQQ__QuoteLine__c> lstQuoteline= new list<SBQQ__Quoteline__c>();
        for(WrapProduct objwrapproduct : lstproduct2){
            if(objwrapproduct.ischecked){
                //need to insert quotelines
                objQuoteline= new SBQQ__Quoteline__c(SBQQ__Product__c=objwrapproduct.objproduct.id,SBQQ__Quote__c =objQuote.id);
                lstQuoteline.add(objQuoteline);
                
            }
        }
        if(lstQuoteline.size()>0)
        insert lstQuoteline;
        lstQuote = new list<SBQQ__Quote__c>();
        lstQuote.add([select id, name, SBQQ__Startdate__c from SBQQ__Quote__c where id=:objQuote.id]);
        showproductlist=false;
        showquotehistory=true;
        //lstQuote.addAll([select id, name, SBQQ__Startdate__c from SBQQ__Quote__c where SBQQ__Account__c=:Acc.id]);
    }
}