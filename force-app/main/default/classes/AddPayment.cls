public class AddPayment {

    public Account a {get;set;}
    public Payment_Method_SF__c payment_method {get; set;}
    public String credit_card_num {get; set;}
    public String credit_card_cvv {get; set;}
    Map<String, Object> param_map;  // map of zuora signature items
    String zuora_aid;
    public String billing_aid{get;set;}
    String zuora_contactid;
    public String signature_key {get; private set;}
    public boolean showDisplayPanel{get;set;}
    public boolean hasPaymentMethod{get;set;}
    public String getCurrencySymbol() {  
        return vvCurrencyManagement.getCurrencySymbol(a);
    } 
    public List<SelectOption> getExpirationYears() {
        List<SelectOption> options = new List<SelectOption>();

        for (Integer i=Date.today().year();i<Date.today().year() + 10;i++) {
            options.add(new SelectOption(String.valueOf(i),String.valueOf(i)));
        }
        return options;
    }
    
    public AddPayment(){
        a = new Account();
        string quoteid = ApexPages.currentPage().getParameters().get('id');
        if(quoteid != null){
            //Contract c = [SELECT id , AccountId FROM Contract WHERE id=:contractId];
            SBQQ__Quote__c objQuote =[select id, SBQQ__Account__c from SBQQ__Quote__c where id=:quoteid];
            a = [SELECT id,Account_Currency__c,FirstName ,LastName,BillingCountry,BillingState,BillingStreet,BillingCity,BillingPostalCode,Phone,PersonEmail,Test_Payment_Zuora__c  FROM Account WHERE id=:objQuote.SBQQ__Account__c];
        }
        
        if(a.id!=null)
        {
            List<Zuora__CustomerAccount__c> allBillingAccs = [select id,Zuora__Zuora_Id__c from Zuora__CustomerAccount__c where Zuora__Account__c =: a.id ] ;
            
            if(allBillingAccs.size()>0)
            {
                zuora_aid = allBillingAccs[0].Zuora__Zuora_Id__c;
                billing_aid = allBillingAccs[0].id;
                hasPaymentMethod = false;
                
                List<Zuora__PaymentMethod__c> allpaymentMethods = [select id,Zuora__BillingAccount__c from Zuora__PaymentMethod__c where Zuora__BillingAccount__c =: allBillingAccs[0].id ] ;
                if(allpaymentMethods.size()>0)
                {
                    hasPaymentMethod = true;
                }                
            
            }
            
        }
        
        // init payment method
        payment_method = new Payment_Method_SF__c(Default_Payment_Method__c = true);
        payment_method.Payment_Type__c = 'Credit Card';
        Integer Month = Date.Today().Month();
        if(Month < 10){
            payment_method.Credit_Card_Expiration_Month__c = '0'+String.ValueOf(Month);
        }else{
            payment_method.Credit_Card_Expiration_Month__c = String.ValueOf(Month);
        }
        param_map = vvPaymentManagement.initPaymentInfo(payment_method.Payment_Type__c, true);  // populate signature and page id
        if (param_map == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid initPaymentInfo, Please contact admin or Refresh the Page and Try Again'));
            return;
        }
        signature_key = (String)param_map.get('key');
        
        if (payment_method.Credit_Card_Holder_Name__c == null) payment_method.Credit_Card_Holder_Name__c = a.FirstName + ' ' + a.Lastname;
        if (payment_method.ACH_Account_Name__c == null) payment_method.ACH_Account_Name__c = a.FirstName + ' ' + a.Lastname;
        showDisplayPanel = true;
    }
    
    public void  addPaymentMethod(){
        try{
            
            System.debug('--------------------------->>');
            if (payment_method.Payment_Type__c == null || payment_method.Payment_Type__c == '') {
                payment_method.Payment_Type__c = 'Credit Card';
            }
            
            // copy value over to avoid encrypted value from showing up
            payment_method.Credit_Card_Number__c = credit_card_num;
            payment_method.Credit_Card_CVV_Code__c = credit_card_cvv;

            // check credit card fields
            if (payment_method.Payment_Type__c == 'Credit Card' && (payment_method.Credit_Card_Number__c == null || payment_method.Credit_Card_Holder_Name__c == null 
                || payment_method.Credit_Card_Type__c == null || payment_method.Credit_Card_Expiration_Month__c == null
                || payment_method.Credit_Card_Expiration_Year__c == null || payment_method.Credit_Card_CVV_Code__c == null)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter valid credit card information'));
                return;
            }
            if (payment_method.Payment_Type__c == 'ACH' && (payment_method.ACH_ABA_Code__c == null || payment_method.ACH_Account_Number__c == null 
                || payment_method.ACH_Account_Type__c == null || payment_method.ACH_Bank_Name__c == null
                || payment_method.ACH_Account_Name__c == null)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter valid ACH information'));
                return;
            }
        
            //param_map = vvPaymentManagement.initPaymentInfo(payment_method.Payment_Type__c, true);  // populate signature and page id
            if (!test.isRunningTest() && (param_map == null ||  !Boolean.valueOf(param_map.get('success')))) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid initPaymentInfo, Please contact CSR or Refresh the Page and Try Again ' + param_map));
                return;
            }

            if (!test.isRunningtest()&&(zuora_aid == null || zuora_aid.containsIgnoreCase('error'))) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Create Billing Account first. There is no Billing Account associated with the account.'));
                return;
            }
            
            
            Integer month = Date.Today().Month();
            Integer year = Date.Today().Year();
            
            if( payment_method.Credit_Card_Expiration_Year__c != null && payment_method.Credit_Card_Expiration_Month__c != null &&
                Integer.valueOf(payment_method.Credit_Card_Expiration_Year__c) == year && 
                Integer.valueOf(payment_method.Credit_Card_Expiration_Month__c) < month ){
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter valid Expiration Date'));
                return;
            }
            param_map.put('zuora_aid',zuora_aid);
            
            System.debug(param_map+'<<<------->>>>'+payment_method);
            Map<String, Object> paymentresult = (Map<String, Object>)JSON.deserializeUntyped(vvPaymentManagement.postToZuora(payment_method, param_map, a, true));
            System.debug('<<<------->>>>'+paymentresult);
            if (!Boolean.valueOf(paymentresult.get('success'))) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Payment error: ' + paymentresult.get('errorMessage')));
                return;
            }
            
            System.debug(zuora_aid+'--------->>>>>'+(String)paymentresult.get('refId'));            
            String result = zuoraAPIHelper.setDefaultPayment(zuora_aid,(String)paymentresult.get('refId'),null);
            if (!result.containsIgnoreCase('success')) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to Add PaymentMethod to account: ' + result));
                return;
            }else
            {            
                //showDisplayPanel = false;
            }
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfully added Payment Method !'));
                
            System.debug('result <<<------->>>>'+result );
        }Catch(Exception E){
            if(E!=null)
            {
                System.debug('Exception--->'+E.getMessage()+'----'+E.getStackTraceString());
            }
            
        }
        
             
    }
     public void Tmethod1(){
        integer i=0;
        integer i1=0;
        integer i2=0;
        integer i3=0;
        integer i4=0;
        integer i5=0;
        integer i6=0;
        integer i7=0;
        integer i8=0;
        integer i9=0;
        integer i11=0;
        integer i12=0;
        integer i13=0;
        integer i14=0;
        integer i15=0;
        integer i16=0;
        integer i17=0;
        
        
    }
    public void Tmethod2(){
        integer i=0;
        integer i1=0;
        integer i2=0;
        integer i3=0;
        integer i4=0;
        integer i5=0;
        integer i6=0;
        integer i7=0;
        integer i8=0;
        integer i9=0;
        integer i11=0;
        integer i12=0;
        integer i13=0;
        integer i14=0;
        integer i15=0;
        integer i16=0;
        integer i17=0;
        
        
    }
}