global class MagalogFinderCSVParserBatch implements Database.Batchable<String> {
	
    String[] source;
    global MagalogFinderCSVParserBatch(String[] source) {
        this.source = source;
    }

    global Iterable<String> start(Database.BatchableContext bc) {
        return source;
    }
    
    global void execute(Database.BatchableContext bc, String[] scope) {
    	List<Magalog_Finder__c> mflist = new List<Magalog_Finder__c>();

        List<List<String>> finder = parseCSV(scope);
        System.debug('**parsed csv**finder******'+finder);
        String regExp = '[-,._ ]';
        try {
            for (List<String> slist : finder) {
                
                String magalogDate = prepareDate(slist[1],slist[2]);
                date magDate = Date.parse(magalogDate);
                System.debug('slist[0]+slist[1]+slist[2]+slist[3]+slist[4]&*****'+slist[0]+'----'+slist[1]+'----'+slist[2]+'----'+slist[3]+'----'+slist[4]);
                Magalog_Finder__c mf = new Magalog_Finder__c(
                    Magalog_Id__c = slist[0]+slist[1]+slist[2]+slist[3]+slist[4], 
                    PostalCode__c = slist[5],
                    BillingPostalCode__c = slist[5].replaceall(regExp,''),
                    Street__c = slist[6],
                    LastName__c = slist[7],
                    FirstName__c = slist[8],
                    Sequence_Number__c = slist[9],
                    Key_Type__c = slist[10],
                    Start_Date__c = magDate
                );
                mflist.add(mf);
                System.debug('mflist*****'+mflist);
            }
        } catch (Exception e) {
         	System.debug('****Exception******'+e.getMessage());   
        }
		System.debug('****mflist******'+mflist);
		if (!mflist.isEmpty()) insert mflist;

    }
    
    global void finish(Database.BatchableContext bc) {
    
    }

	/**
	 * csv parser, "text","text","text"
	 * @param  contents    [description]
	 * @param  skipHeaders [description]
	 * @return             [description]
	 */
	public static List<List<String>> parseCSV(List<String> lines) {
		List<List<String>> allFields = new List<List<String>>();

		// we are not attempting to handle fields with a newline inside of them
		// so, split on newline to get the spreadsheet rows
		//List<String> lines = safeSplit(contents,(contents.indexOf('\r\n') > 0 ? '\r\n' : '\n'));
		Integer num = 0;
		for (String line : lines) {
			// check for blank CSV lines (only commas)
			if (line.replaceAll(',', '').trim().length() == 0) break;

			line = line.replaceAll(',"""', ',"DBLQT').replaceall('""",', 'DBLQT",');
            System.debug('****line1****'+line);
            
			line = line.replaceAll('""', 'DBLQT');
            
            System.debug('****line2****'+line);

			List<String> fields = line.split(',');
           
			List<String> cleanFields = new List<String>();
			String compositeField;
			Boolean makeCompositeField = false;
			for (String field : fields) {
				system.debug(field);
			if (field.startsWith('"') && field.endsWith('"')) {
					// remove the quotes
					field = field.substringBetween('"');
					cleanFields.add(field.replaceAll('DBLQT', '"'));
                    
				} else if (field.startsWith('"')) {
					makeCompositeField = true;
					compositeField = field;
                    
				} else if (field.endsWith('"')) {
					compositeField += ',' + field;
					field = field.substringBetween('"');
					cleanFields.add(compositeField.replaceAll('DBLQT', '"'));
					makeCompositeField = false;
                    
				} else if (makeCompositeField) {
					compositeField +=  ',' + field;
                   
				} else {
					//field = field.substringBetween('"');
                    //System.debug(field);
					if (field == null) cleanFields.add('');
                    else if (field.equalsIgnoreCase('DBLQT')) {
                        cleanFields.add(null);
                    }
					else cleanFields.add(field);
                }
            }
			allFields.add(cleanFields);
		}
		return allFields;
	}

    private static String prepareDate(String week,String year){
        String MagalogYear=Label.Magalog_Date_Year;
        Integer Days =(Integer.valueOf(week) * 7);
        year=MagalogYear+year;
        Date myDate = Date.newInstance(Integer.valueOf(year), 1, 0);
        Date addedDayinDate = mydate.addDays(Days);
        Date myDate1 = Date.valueof(addedDayinDate);
       	String newWeekDate = String.valueof(myDate1);
        String newyear = newWeekDate.substring(0, 4);
        String month = newWeekDate.substring(5, 7);
        String newdate = newWeekDate.substring(8, 10);
        newWeekDate = (month+'/'+newdate+'/'+newyear);
        return newWeekDate; 
    }
	
}