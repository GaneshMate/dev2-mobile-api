@RestResource(URLMapping='/update/*')
global class UpdatePayment {
    
    global class Payment {
        public List<Payments> payments;
        //global String Id; 
        //global String CreditCardType;  
    }
    global class Payments {
        public String method;
        public RichInput richInput;
    }
    global class RichInput {
        public String Id;
        public String CreditCardType;
    }
    global class BatchRequestResult {
        boolean hasErrors;
        List<BatchResult> results;
    }
    
    global class BatchResult {
        public integer statusCode;
        public string result;
        
        public BatchResult(integer status,string result) {
            this.statusCode = status;
            this.result = result;
        }
    }
    @HttpPatch
    global static BatchRequestResult updatePayment() {
        integer retCode = 0;
        string retBody = '';
        
        RestResponse res=RestContext.response;
        
        
        RestRequest req = RestContext.request;
        Payment input = (Payment)JSON.deserialize(req.requestBody.toString(), Payment.class);
        System.debug('***input****'+input);
        System.debug('***input sfsfsdf****'+input.Payments);
        
        
        BatchRequestResult result = new BatchRequestResult();
        //result.hasErrors = false;
        result.results = new List<BatchResult>();
        
        List<Payments> olisToUpdate = new List<Payments>();
        for(Payments br : input.payments) {
            
            
            if(!String.isBlank(String.valueOf(br.RichInput.id)) &&!String.isBlank(String.valueOf(br.RichInput.CreditCardType))){
                String updatePaymentResult = updatePayment(br.RichInput.id,br.RichInput.CreditCardType);    
                System.debug('****updatePayment updatePaymentResult****'+updatePaymentResult);
                if(updatePaymentResult.contains('Error')){
                    result.results.add(new BatchResult(500,updatePaymentResult));
				}else{
                    retCode = 200; 
                    result.results.add(new BatchResult(retCode,updatePaymentResult));
                }
            }else{
                retCode = 500;
                result.results.add(new BatchResult(retCode,'Error: Id,CreditCardType is Mandatory!'));
                System.debug('****retCode****'+retCode);
                
            }  
        }
        res.headers.put('Content-type', 'application/json');
        res.statusCode = retCode;
        res.responseBody = Blob.valueOf(retBody);
        System.debug('****retCode****'+retCode);
        return result;
    }
    public static String updatePayment(String Id,String CreditCardType) {
        
        Zuora.zObject payment = new Zuora.zObject('PaymentMethod');
        Payment.setValue('Id', Id); 
        payment.setValue('CreditCardType', CreditCardType);
        system.debug('payment: '+payment);
        
        String errors = '';
        Zuora.zApi.SaveResult[] results = zuoraAPIHelper.updateAPICall(payment);
        system.debug('Check : Update Results: '+results);
        
        for (Zuora.zApi.SaveResult result : results) {
            if (result.success){
                system.debug('result.Id::::::::- '+result.Id);
                return result.Id;
            }
            else{
                errors += (errors != '' ? ',' : '') + zuoraAPIHelper.getErrorStr(result.errors);
            }
        }
        
        return errors;
    }
}