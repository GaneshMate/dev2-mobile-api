global class UserSetPasswordBatchVieo implements Database.Batchable<sObject>, Database.Stateful {
  
  String query;
  global UserSetPasswordBatchvieo(String query) {
    this.query = query;
    
  }
  
  
  global Database.QueryLocator start(Database.BatchableContext BC) {
    return Database.getQueryLocator(query);
  }

     global void execute(Database.BatchableContext BC, List<User> userlist) {
       for (User u : userlist) {
         try {
           System.setPassword(u.Id, u.Account.Password__c);
           
      } catch (Exception e) {
        UserpasswordException__c objUserException = new UserpasswordException__c(Exception__c =e.getmessage(),UserId__c=u.id);
        insert objuserexception;
       
         

       }
       
  
      }
  }
  
  global void finish(Database.BatchableContext BC){}
  }