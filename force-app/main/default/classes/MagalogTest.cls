@isTest
private class MagalogTest {
    @isTest static void testEmailHandler() {
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        email.subject = 'Create Contact';
        email.fromAddress = 'someaddress@email.com';
        email.plainTextBody = 'email body\n2225256325\nTitle';
        
        Messaging.InboundEmail.TextAttachment attachment = new Messaging.InboundEmail.TextAttachment();
        //attachment.body = '"VEC1916E3","DRISCOLL","T","37 TAUN","02370"';
        attachment.body = '"VEC","19","16","E","3","75056 78054","4-1-2","Vance","",1000,"k1"';
        attachment.fileName = 'textfileone.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.textAttachments = new Messaging.inboundEmail.TextAttachment[] { attachment };
            
            
            Test.startTest();
        MagalogFinderEmailHandler testInbound = new MagalogFinderEmailHandler();
        testInbound.handleInboundEmail(email, env);
        Test.stopTest();
        
    }
    @isTest static void testEmailHandlerStart() {
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        email.subject = 'Create Contact';
        email.fromAddress = 'someaddress@email.com';
        email.plainTextBody = 'email body\n2225256325\nTitle';
        
        Messaging.InboundEmail.TextAttachment attachment = new Messaging.InboundEmail.TextAttachment();
        //attachment.body = '"VEC1916E3,"DRISCOLL,"T,"37 TAUN,"02370';
        attachment.body = '"VEC,"19,"16,"E,"3,"75056 78054,"4-1-2,"Vance,"",1000,"k1';
        attachment.fileName = 'textfileone.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.textAttachments = new Messaging.inboundEmail.TextAttachment[] { attachment };
            
            
            Test.startTest();
        MagalogFinderEmailHandler testInbound = new MagalogFinderEmailHandler();
        testInbound.handleInboundEmail(email, env);
        Test.stopTest();
        
    }
    @isTest static void testEmailHandlerEnd() {
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        email.subject = 'Create Contact';
        email.fromAddress = 'someaddress@email.com';
        email.plainTextBody = 'email body\n2225256325\nTitle';
        
        Messaging.InboundEmail.TextAttachment attachment = new Messaging.InboundEmail.TextAttachment();
        //attachment.body = 'VEC1916E3",DRISCOLL",T",37 TAUN",02370';
        attachment.body = 'VEC",19",16",E",3",75056 78054",4-1-2",Vance","",1000,k1"';
        attachment.fileName = 'textfileone.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.textAttachments = new Messaging.inboundEmail.TextAttachment[] { attachment };
            
            
            Test.startTest();
        MagalogFinderEmailHandler testInbound = new MagalogFinderEmailHandler();
        testInbound.handleInboundEmail(email, env);
        Test.stopTest();
        
    }
    @isTest static void testMatchingScheduler() {
        //Magalog_Finder__c mf = new Magalog_Finder__c(Magalog_Id__c = 'VEC1916E3', FirstName__c = 'T', LastName__c = 'DRISCOLL', Street__c = '37 TAUN', PostalCode__c = '02370');
        //insert mf;

        List<Magalog_Finder__c> magalogList = new List<Magalog_Finder__c>();
        Magalog_Finder__c magalogFinder = new Magalog_Finder__c();
        magalogFinder.Magalog_Id__c='CJO0818E1';
        magalogFinder.LastName__c='Down';
        magalogFinder.FirstName__c='M';
        magalogFinder.Street__c='4-1-20 shib';
        magalogFinder.PostalCode__c='60610';
        magalogFinder.Key_Type__c = 'k1';
        magalogFinder.BillingPostalCode__c = '60610';
        magalogFinder.Sequence_Number__c = '1000';
        magalogFinder.Start_Date__c = Date.parse('1/1/2017');
        magalogList.add(magalogFinder);
        
        Magalog_Finder__c magalogFinder1 = new Magalog_Finder__c();
        magalogFinder1.Magalog_Id__c='CJO0818E1';
        magalogFinder1.LastName__c='Downy';
        magalogFinder1.FirstName__c='';
        magalogFinder1.Street__c='';
        magalogFinder1.PostalCode__c='60610';
        magalogFinder1.Key_Type__c = 'k2';
        magalogFinder1.Sequence_Number__c = '1002';
        magalogFinder1.BillingPostalCode__c = '60610';
        magalogFinder1.Start_Date__c = Date.parse('1/1/2017');
        magalogList.add(magalogFinder1);
        
        Magalog_Finder__c magalogFinder2 = new Magalog_Finder__c();
        magalogFinder2.Magalog_Id__c='CJO1218E1';
        magalogFinder2.LastName__c='Downy';
        magalogFinder2.FirstName__c='';
        magalogFinder2.Street__c='4-1-20 shibuya';
        magalogFinder2.PostalCode__c='60610';
        magalogFinder2.Key_Type__c = 'k1';
        magalogFinder2.Sequence_Number__c = '1003';
        magalogFinder2.BillingPostalCode__c = '60610';
        magalogFinder2.Start_Date__c = Date.parse('1/1/2017');
        magalogList.add(magalogFinder2);
        
        Magalog_Finder__c magalogFinder3 = new Magalog_Finder__c();
        magalogFinder3.Magalog_Id__c='CJO1218E1';
        magalogFinder3.LastName__c='Downy';
        magalogFinder3.FirstName__c='';
        magalogFinder3.Street__c='';
        magalogFinder3.PostalCode__c='60610';
        magalogFinder3.Key_Type__c = 'k2';
        magalogFinder3.Sequence_Number__c = '1004';
        magalogFinder3.BillingPostalCode__c = '60610';
        magalogFinder3.Start_Date__c = Date.parse('1/1/2017');
        magalogList.add(magalogFinder3);
        
        
        
        if(magalogList.size()>0){
            insert magalogList;
        }
        
        Account acc= new Account();
        acc.firstname='Marsh';
        acc.lastName = 'Downy';
        acc.BillingCity ='Test City';
        acc.BillingStreet = '4-1-20 shibuya';
        acc.BillingState ='TestState';
        acc.BillingPostalCode ='60610';
        acc.BillingCountry ='United States';
        Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
        acc.recordtypeID=objAccrec.id;
        acc.PersonMailingCity='Test City';
        acc.PersonMailingStreet = 'TestStreet';
        acc.PersonMailingState ='TestState';
        acc.PersonMailingPostalCode ='23451';
        acc.PersonMailingCountry ='United States';
        insert acc;
        
        Contract contract = new Contract();
        contract.AccountId = acc.Id;
        contract.Status = 'Draft';
        contract.StartDate = Date.parse('06/18/2020');
        contract.ContractTerm = 36;
        insert contract;
        
        SBQQ__Subscription__c subscription = new SBQQ__Subscription__c();
        subscription.SBQQ__Contract__c = contract.id;
        subscription.SBQQ__Account__c = acc.Id;
        subscription.SBQQ__SubscriptionStartDate__c = Date.parse('1/1/2020');
        subscription.SBQQ__Quantity__c = 1.00;
        insert subscription;
        
        Test.startTest();
        System.schedule('Magalog Matching Scheduler Test','0 30 23 * * ?', new MagalogMatchingScheduler());
        Test.stopTest();
        
    }
}