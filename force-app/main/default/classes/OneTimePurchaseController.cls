/**
 * this controller handles one time purchases such as books, hats, etc...
 */
public with sharing class OneTimePurchaseController {
	public Account a {get; set;}
	public Zuora__CustomerAccount__c zca {get; private set;}

    public String selected_family_id {get; set;}    // product family chosen

    public String selected_product_id {get; set;}

    public List<Account_Product__c> account_product_list {get; set;}

	Map<Id,PriceBookEntry> productmap;


	// zuora signature key
	public Payment_Method_SF__c payment_method {get; set;}
	public String paymentType {get; set;}       // card or ach
    Map<String, Object> param_map;  // map of zuora signature items
    public String signature_key {get; private set;}

	public OneTimePurchaseController() {
		String aid = ApexPages.CurrentPage().getparameters().get('aid');
        List<Account> alist = [SELECT FirstName, LastName, Shipping_FirstName__c, Shipping_LastName__c, Salutation, Phone, PersonEmail, PersonContactId, 
                BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode,
                ShippingStreet, ShippingCity, ShippingState, ShippingCountry, ShippingPostalCode,
                VV_Market__c, VV_Second_Market__c, VV_Edition__c, VV_Feed__c, Subscription_Plan__c, Is_Elite__c, Is_Elite_Legacy__c, 
                Account_Currency__c, Account_Currency_Symbol__c
                FROM Account WHERE Id = :aid];
        if (alist.isEmpty()) {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR ,'Invalid Customer Account'));
            return;
        }
        a = alist[0];
        if (a.Shipping_FirstName__c == null) a.Shipping_FirstName__c = a.FirstName;
		if (a.Shipping_LastName__c == null) a.Shipping_LastName__c = a.LastName;


        // check to see if we already have a billing account, if yes, then we don't need to enter new payment information
        List<Zuora__CustomerAccount__c> zcalist = [SELECT 
				Zuora__BillToName__c,
				Zuora__CreditCard_Number__c,
				Zuora__CreditCard_Expiration__c,
				Zuora__CreditCardType__c,
				Zuora__BillToCity__c,
				Zuora__BillToCountry__c,
				Zuora__BillToAddress1__c,
				Zuora__BillToAddress2__c,
				Zuora__BillToState__c,
				Zuora__BillToPostalCode__c,
				Zuora__BillToWorkEmail__c,
				Zuora__BillToWorkPhone__c,
				Zuora__BillToId__c,
				Zuora__Zuora_Id__c

			FROM Zuora__CustomerAccount__c 
			WHERE 
				Zuora__Account__c = :a.Id
				AND Zuora__Status__c = 'Active'];
		if (!zcalist.isEmpty()) {
			zca = zcalist[0];
		}

        account_product_list = new List<Account_Product__c>();

		// populate book list
		productmap = new Map<Id,PriceBookEntry>();
		for (PriceBookEntry pbe : [SELECT Product2Id, Product2.Family, Product2.Name, UnitPrice FROM PriceBookEntry WHERE Product2.Family <> null AND IsActive = true ORDER BY Product2.Name]) {
            pbe.UnitPrice = vvCurrencyManagement.convertFromUSD(pbe.UnitPrice, a.Account_Currency__c);
        	productmap.put(pbe.Id,pbe);
        }
        
        paymentType = 'Credit Card';
        

        // init payment method
        payment_method = new Payment_Method_SF__c(Default_Payment_Method__c = true);
        if (a.Id != null && payment_method.Credit_Card_Holder_Name__c == null) payment_method.Credit_Card_Holder_Name__c = a.FirstName + ' ' + a.LastName;
        if (a.Id != null && payment_method.ACH_Account_Name__c == null) payment_method.ACH_Account_Name__c = a.FirstName + ' ' + a.Lastname;
        payment_method.Payment_Type__c = paymentType; 
        
        param_map = vvPaymentManagement.initPaymentInfo(payment_method.Payment_Type__c, false);  // populate signature and page id
        if (param_map == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid initPaymentInfo, Please contact CSR or Refresh the Page and Try Again'));
            return;
        }

        signature_key = (String)param_map.get('key');

        if (zca != null) paymentType = 'Existing';
        
	}

	public void doPurchase() {
        if (account_product_list.isEmpty()) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select an item to purchase.'));
            return;
        }

		// first check to see if we have billing account
		String zuora_aid = (zca != null ? zca.Zuora__Zuora_Id__c : null);
		String zuora_contactid = null;
		if (zca == null) {
			if (
                (paymentType == 'Credit Card' && payment_method.Credit_Card_Number__c == null) 
                || (paymentType == 'ACH' && payment_method.ACH_Account_Number__c == null)
	        ) {
	            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter payment information'));
	            return;
	        }

	        zuora_aid = zuoraAPIHelper.createAccount(a,paymentType);
            if (zuora_aid.containsIgnoreCase('error')) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to connect to subscription server. ' + zuora_aid));
                return;
            }
            // insert zuora contact
            zuora_contactid = zuoraAPIHelper.createContact(a, zuora_aid);
            if (zuora_contactid.containsIgnoreCase('error')) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to connect to subscription server. ' + zuora_contactid));
                return;
            }

		}

		param_map = vvPaymentManagement.initPaymentInfo(payment_method.Payment_Type__c, false);  // populate signature and page id
        if (param_map == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid initPaymentInfo, Please contact CSR or Refresh the Page and Try Again'));
            return;
        }

		param_map.put('zuora_aid',zuora_aid);

        // submit payment only if insert or credit card number is entered
        if (payment_method.Credit_Card_Number__c != null || payment_method.ACH_Account_Number__c != null) {
            system.debug('a: ' + a);
            Map<String, Object> paymentresult = (Map<String, Object>)JSON.deserializeUntyped(vvPaymentManagement.postToZuora(payment_method, param_map, a, false));

            if (!Boolean.valueOf(paymentresult.get('success'))) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Payment error: ' + paymentresult.get('errorMessage')));
                return;
            }

            // only activate account if it is insert, cause it has already been activated
            if (zca == null) {
                // activate account
                String result = zuoraAPIHelper.activateAccount(null, zuora_aid, zuora_contactid, (String)paymentresult.get('refId'));
                if (!result.containsIgnoreCase('success')) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to activate to account: ' + result));
                    return;
                }
            } else { // update, so set the default payment
                String gateway = vvCurrencyManagement.getGateway(a, paymentType);
                String result = zuoraAPIHelper.setDefaultPayment(zuora_aid, (String)paymentresult.get('refId'), gateway);
            }

        }


		// get the rate plan
		zqu__ProductRatePlan__c prp = vvProductManagement.getOneTimePurchasePlan();

        // loop through all the books purchased and create subscription
        String booknames = '';
        for (Account_Product__c ap : account_product_list) {

            prp.zqu__R00N40000001mFVKEA2__r[0].zqu__ListPrice__c = ap.Price__c;
            prp.Name = ap.Name;

            String subId = zuoraAPIHelper.createOneTimePurchase(zuora_aid, prp, null);
            booknames += '(' + ap.Name + ') ';
        }

        zuoraAPIHelper.ondemandSync(zuora_aid);

        upsert account_product_list;

        // update account for shipping name changes
        update a;

        ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.INFO, booknames + ' Purchased'));

        // clear list
        account_product_list = new List<Account_Product__c>();
       	
	}

    /**
     * return all product families
     */
    public List<SelectOption> getProductFamilies() { 
        List<Schema.PicklistEntry> fieldResult = Schema.SobjectType.Product2.fields.getMap().get('Family').getDescribe().getPicklistValues();   
        List<SelectOption> options = new List<SelectOption>{new SelectOption('','Please Select')};

        for (Schema.PicklistEntry f : fieldResult) {
            options.add(new SelectOption( f.getValue(),f.getLabel() ));
        }       
        return options;
    }

    public void selectFamily() {
    }

    /**
     * add a book to the selected table
     */
	public void selectItem() {
        if (selected_product_id == null || selected_product_id == '') return;
        account_product_list.add(new Account_Product__c(Account__c = a.Id, Product__c = productmap.get(selected_product_id).Product2Id, Price__c = productmap.get(selected_product_id).UnitPrice, Name = productmap.get(selected_product_id).Product2.Family + ': ' + productmap.get(selected_product_id).Product2.Name, Status__c = 'Purchased'));
	}

    /**
     * remove a book from the selected table
     */
    public void removeProduct() {
        String i = ApexPages.CurrentPage().getparameters().get('i');
        account_product_list.remove(Integer.valueOf(i)); 
    }

	public List<SelectOption> getProducts() {
		List<SelectOption> solist = new List<SelectOption>();
        solist.add(new SelectOption('','Select a purchase item'));
		// get list of products
        for (PriceBookEntry pbe : productmap.values()) {
            if (selected_family_id != null && selected_family_id != '') {
                if (pbe.Product2.Family == selected_family_id) solist.add(new SelectOption(pbe.Id, pbe.Product2.Name + ' (' + currency(String.valueOf(pbe.UnitPrice)) + ')'));
            } else {
              break;    // don't show anything without choosing a family  
            }
        	
        }
        return solist;
	}

	public List<SelectOption> getPaymentMethods() {
		List<SelectOption> solist = new List<SelectOption>();
		// get list of current payment methods
		for (Payment_Method_SF__c pm : [SELECT Name, Default_Payment_Method__c, Credit_Card_Number__c, ACH_Account_Number__c FROM Payment_Method_SF__c]) {

		}
		return solist;
	}

	public static String currency(String i) {
        String s = ( Decimal.valueOf(i==null||i.trim()==''?'0':i).setScale(2) + 0.001 ).format();
        return s.substring(0,s.length()-1);
    }


}