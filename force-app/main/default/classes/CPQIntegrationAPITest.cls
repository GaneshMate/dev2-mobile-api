@istest(seeALLData=true)
public class CPQIntegrationAPITest {
    
    @istest static void createContractAndSubscription(){
        test.startTest();
       	Account accountRec = createAccount();
        SBQQ__Subscription__c sub = CPQIntegrationAPI.createContractAndSubscription(accountRec, '215421', 'Tier 2 Mobile - US Trial', String.valueOf(System.today()), 'Draft', 12, 1, 10.00, String.valueOf(System.today()),'termed','Apple',true);
        test.stopTest();
        
    }
    @istest static void cancelContract(){
        test.startTest();
        Account accountRec = createAccount();
        Contract contract = createContract(accountRec, '12345', String.valueOf(System.today()), 12, 'Draft', String.valueOf(System.today()), 'termed');
        SBQQ__Subscription__c subscription = createSubscription(contract.Id,'Tier 2 Mobile - US Trial',2,25.00);
        System.debug('*****contract.ContractNumber*****'+contract.ContractNumber);
        System.debug('*****subscription*****'+subscription);
        Contract cont =[SELECT Id, ContractNumber FROM Contract WHERE Id =:contract.Id];
        CPQIntegrationAPI.cancelSubscription(accountRec, true, cont.ContractNumber);
        test.stopTest();    
    }
    private static Account createAccount(){
        Account accountRec = new Account();
        accountRec.FirstName ='Test';
        accountRec.LastName ='Demo';
        accountRec.PersonEmail = 'td@email.com';
        accountRec.Account_Currency__c = 'USD';
        accountRec.BillingStreet = '4-1-20 shibuya';
        accountRec.BillingCity = 'chicago';
        accountRec.BillingState = 'Illinois';
        accountRec.BillingPostalCode ='60610';
        accountRec.BillingCountry ='USA';
        accountRec.Subscription_Plan__c = 'Monthly';
        accountRec.Password__c ='User@123';
        accountRec.Vector_Vest_Customer_Id__c ='td@email.com';
        accountRec.Is_Apple_App_Store_Customer__c = false;
        accountRec.CertificateID__c = 'Apple';
        insert accountRec;
        return accountRec;
    }
    private static contract createContract(Account acc, String zuora_Id, String StartDate,Integer ContractTerm,String Status,String subscriptionStartDate,String termType){
        Contract contract = new Contract();
            contract.AccountId = acc.Id;
            contract.StartDate =Date.valueOf(StartDate);
            contract.Status = Status;
            contract.CurrencyIsoCode = acc.Account_Currency__c;
            contract.ContractTerm = ContractTerm;    
            contract.SBQQ__RenewalTerm__c = contract.ContractTerm;
            contract.VV_SubscriptionStartDate__c =Date.valueOf(subscriptionStartDate);
            contract.ZSB__TermType__c = termType;
            contract.ZSB__SubscriptionId__c = '12345';
            insert contract;
        return contract;
    }
    private static SBQQ__Subscription__c createSubscription(Id contractId,String prodName,Integer Quantity,Decimal NetPrice){
        String vvPricebook=Label.vvPricebook;
            PriceBookIds__c stdPriceBookId = PriceBookIds__c.getValues(vvPricebook);
             System.debug('****stdPriceBookId******'+stdPriceBookId);
            if (stdPriceBookId == null) {
                return null;
            }
            string strPricebookId = stdPriceBookId.Price_Book_Id__c;
            List<Product2> prodList=[select Id, Name,(SELECT ID,UnitPrice, ZSB__PRPChargeId__c, ZSB__PRPlanId__c FROM PricebookEntries where Pricebook2Id = :strPricebookId) 
                                  from product2 where Name =:prodName]; 
            
            if (prodList == null || prodList.size() == 0) {
               return null;
            }
            
            List<Contract> contractList =[select Account.id, Account.Name from Contract where Id =:contractId];
            
            SBQQ__Subscription__c subscribe = new SBQQ__Subscription__c();
            subscribe.SBQQ__Contract__c = contractId != null ? contractId : null;
            subscribe.SBQQ__Product__c = prodList != null ? prodList[0].Id :null;
            subscribe.SBQQ__Quantity__c = Quantity;
            subscribe.SBQQ__NetPrice__c =NetPrice;
            subscribe.SBQQ__SpecialPrice__c = NetPrice;
            subscribe.SBQQ__ListPrice__c = NetPrice;
            subscribe.SBQQ__CustomerPrice__c = NetPrice;
            subscribe.SBQQ__RegularPrice__c = NetPrice;
            subscribe.SBQQ__Account__c =contractList != null ? contractList[0].Account.Id : null;
            subscribe.ZSB__PRPChargeId__c = prodList[0].PricebookEntries != null ? prodList[0].PricebookEntries[0].ZSB__PRPChargeId__c : null;
            subscribe.ZSB__PRPlanId__c = prodList[0].PricebookEntries != null ? prodList[0].PricebookEntries[0].ZSB__PRPlanId__c : null;
            
            insert subscribe; 
        
        return subscribe;
    }
}