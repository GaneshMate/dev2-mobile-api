/**
 * An apex page controller that exposes the site forgot password functionality
 */
public with sharing class ForgotPasswordController {
    public String username {get; set;}   
     public boolean showforgotusername{get;set;} 
    public ForgotPasswordController() {}
    public pagereference forgotUsername(){
        PageReference pr = Page.ForgotUsername;
            pr.setRedirect(true);
        return pr;
        
    }
    public PageReference forgotPassword() {
        string tempUsername;
        tempUsername =username ;
         if (username != null && !username.contains('@')) {
            System.debug('*****username****'+username);
            
            username += '@vectorvest-salesforce.com';
            if (ApexPages.currentPage().getHeaders().get('Host').containsIgnoreCase('partial')) username += '.partial1';   
            
            
            
        }
        list<User> lstUser = new list<User>(); 
            lstUser=[select id from User where username=:username];
            if(lstUser.size() ==0){
               username =tempUsername; 
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'There is no username associated in our system. Please click on the forgot Username link below '));
                showforgotusername = true;
                
                return null;
            }
            else{
            boolean success = Site.forgotPassword(username);
            PageReference pr = Page.ForgotPasswordConfirm;
            pr.setRedirect(true);
            if (success) {              
            return pr;
            }
        }
        
        return null;
    }
}