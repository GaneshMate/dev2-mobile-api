@isTest(seeAllData= true) 
public class CustomerRestResourceTest {
    
    @isTest static void testCustomerAsNull() {
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/update';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
        Test.startTest();
        CustomerRestResource.updateCustomer('', '','No');
        Test.stopTest();
         //System.assertEquals(400, res.statusCode);
    }
    @isTest static void testUpdateCustomer() {
       Account accountRec = new Account();
       
        accountRec.FirstName ='CustomerTestingAccount';
        accountRec.LastName ='DemoTestingAccount';
        accountRec.PersonEmail = 'TestDemoAccount@email.com';
        accountRec.Account_Currency__c = 'USD';
        accountRec.BillingStreet = '4-1-20 shibuya';
        accountRec.BillingCity = 'chicago';
        accountRec.BillingState = 'Illinois';
        accountRec.BillingPostalCode ='60610';
        accountRec.BillingCountry ='USA';
        accountRec.Subscription_Plan__c = 'Monthly';
        accountRec.Password__c ='User@123';
        accountRec.Vector_Vest_Customer_Id__c ='TestDemoAccount@email.com';
        accountRec.Is_Apple_App_Store_Customer__c = false;
        accountRec.CertificateID__c = 'Apple';
        accountRec.GDPR_Opt_in__pc = 'No';
        insert accountRec;
        
        accountRec = [Select Id,  Salutation,FirstName,LastName,
                                            PersonEmail,EnterpriseCustomerID__c,
                                            ApplicationUserID__c,BillingStreet,
                                            BillingCity,BillingState,
                                            BillingPostalCode,BillingCountry,
                                            PersonMailingCountry,VV_Feed__c,
                                            VV_Edition__c,VV_Market__c,
                                            VV_Second_Market__c,Subscription_Plan__c,
                                            Is_Apple_App_Store_Customer__c,Account_Currency__c,
                                            SBQQ__TaxExempt__c,CertificateID__c,Region1__c,Phone,Account_Id__c,GDPR_Opt_in__pc
                                  FROM Account WHERE Id =:accountRec.Id];
        System.debug('***accountRec***'+accountRec);
            
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/update';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
        Test.startTest();
        CustomerRestResource.updateCustomer(accountRec.EnterpriseCustomerID__c,accountRec.CertificateID__c,accountRec.GDPR_Opt_in__pc);
        Test.stopTest();
        //System.assertEquals(200, res.statusCode);
    }
    
    @isTest static void testUpdateCustomerNeg() {
        Test.startTest();
        Account accountRec = new Account();
       
        accountRec.FirstName ='CustomerTestingAccount';
        accountRec.LastName ='DemoTestingAccount';
        accountRec.PersonEmail = 'TestDemoAccount@email.com';
        accountRec.Account_Currency__c = 'USD';
        accountRec.BillingStreet = '4-1-20 shibuya';
        accountRec.BillingCity = 'chicago';
        accountRec.BillingState = 'Illinois';
        accountRec.BillingPostalCode ='60610';
        accountRec.BillingCountry ='USA';
        accountRec.Subscription_Plan__c = 'Monthly';
        accountRec.Password__c ='User@123';
        accountRec.Vector_Vest_Customer_Id__c ='TestDemoAccount@email.com';
        accountRec.Is_Apple_App_Store_Customer__c = false;
        accountRec.CertificateID__c = 'Apple';
        accountRec.GDPR_Opt_in__pc = 'No';
        insert accountRec;
        
       
        System.debug('***accountRec***'+accountRec);
       String zuora_aid = zuoraAPIHelper.createAccount(accountRec, null);
        SBQQ__Subscription__c subscription = CPQIntegrationAPI.createContractAndSubscription(accountRec,zuora_aid,'Tier 2 Mobile - US Trial',string.valueOf(system.today()),'Draft',12,1,10.9,string.valueOf(system.today()),'Termed','Apple',true);
        System.debug('*****************'+subscription.Enterprise_Access_Rights__r);
        Enterprise_Access_Right__c EAR = new Enterprise_Access_Right__c();
        EAR.ClaimValue__c ='Demo';
        EAR.Mask__c ='30';
        EAR.Is_Trial__c =True;
        EAR.Subscription_End_Date__c= system.today();
        EAR.Account__c = accountRec.Id;
        EAR.Subscription__c = subscription.id;
        //EAR.ProductID__c = prod.ProductID__c;
        insert EAR;
         accountRec = [Select Id,  Salutation,FirstName,LastName,
                                            PersonEmail,EnterpriseCustomerID__c,
                                            ApplicationUserID__c,BillingStreet,
                                            BillingCity,BillingState,
                                            BillingPostalCode,BillingCountry,
                                            PersonMailingCountry,VV_Feed__c,
                                            VV_Edition__c,VV_Market__c,
                                            VV_Second_Market__c,Subscription_Plan__c,
                                            Is_Apple_App_Store_Customer__c,Account_Currency__c,
                                            SBQQ__TaxExempt__c,CertificateID__c,Region1__c,Phone,Account_Id__c,GDPR_Opt_in__pc,
                                            (select ClaimValue__c, Mask__c, Is_Trial__c,Subscription_End_Date__c,
                        Is_Observable__c,Non_Converting_Trial__c from Enterprise_Access_Rights__r where Non_Converting_Trial__c =true)
                                  FROM Account WHERE Id =:accountRec.Id];
        System.debug('***EAR***'+EAR);
        System.debug('***accountRec***'+accountRec.Enterprise_Access_Rights__r);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/update';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
        
        //ContractRestResource.createContract(accountRec.EnterpriseCustomerID__c,string.valueOf(system.today()),'Termed','Tier 2 Mobile - US Trial',string.valueOf(system.today()),'Draft',1,1,10.9);
        
        CustomerRestResource.updateCustomer(accountRec.EnterpriseCustomerID__c,accountRec.CertificateID__c,accountRec.GDPR_Opt_in__pc);
        Test.stopTest();
      //  System.assertEquals(200, res.statusCode);
    }
     @isTest static void testUpdateCustomerUser() {
        test.startTest();
        CustomerInfoService.CustomerInfo ci = new CustomerInfoService.CustomerInfo();
         ci =TestFactoryData.createCustomer1();
         ci.applestore =false;
         ci.optinMarketing='No';
         Account accountRec = CustomerInfoService.createCustomer(ci);
         
        System.debug('***accountRec***'+accountRec);
       String zuora_aid = zuoraAPIHelper.createAccount(accountRec, null);
        SBQQ__Subscription__c subscription = CPQIntegrationAPI.createContractAndSubscription(accountRec,zuora_aid,'Tier 2 Mobile - US Trial',string.valueOf(system.today()),'Draft',12,1,10.9,string.valueOf(system.today()),'Termed','Apple',true);
        System.debug('*****************'+subscription.Enterprise_Access_Rights__r);
        Enterprise_Access_Right__c EAR = new Enterprise_Access_Right__c();
        EAR.ClaimValue__c ='Demo';
        EAR.Mask__c ='30';
        EAR.Is_Trial__c =True;
        EAR.Subscription_End_Date__c= system.today();
        EAR.Account__c = accountRec.Id;
        EAR.Subscription__c = subscription.id;
        //EAR.ProductID__c = prod.ProductID__c;
        insert EAR;
         accountRec = [Select Id,  Salutation,FirstName,LastName,
                                            PersonEmail,EnterpriseCustomerID__c,
                                            ApplicationUserID__c,BillingStreet,
                                            BillingCity,BillingState,
                                            BillingPostalCode,BillingCountry,
                                            PersonMailingCountry,VV_Feed__c,
                                            VV_Edition__c,VV_Market__c,
                                            VV_Second_Market__c,Subscription_Plan__c,
                                            Is_Apple_App_Store_Customer__c,Account_Currency__c,
                                            SBQQ__TaxExempt__c,CertificateID__c,Region1__c,Phone,Account_Id__c,GDPR_Opt_in__pc,
                                            (select ClaimValue__c, Mask__c, Is_Trial__c,Subscription_End_Date__c,
                        Is_Observable__c,Non_Converting_Trial__c from Enterprise_Access_Rights__r where Non_Converting_Trial__c =true)
                                  FROM Account WHERE Id =:accountRec.Id];
        System.debug('***EAR***'+EAR);
        System.debug('***accountRec***'+accountRec.Enterprise_Access_Rights__r);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/update';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
         
        CustomerRestResource.updateCustomer(accountRec.EnterpriseCustomerID__c,accountRec.CertificateID__c,String.valueof(accountRec.GDPR_Opt_in__pc));
        Test.stopTest();
      //  System.assertEquals(200, res.statusCode);
    }
      
    @isTest static void testUpdateCustomerUserMobileCustomerCheck() {
        test.startTest();
         Account accountRec = new Account();
        VVCommunitySettings__c cs = VVCommunitySettings__c.getOrgDefaults();
         User uowner = [SELECT Id FROM User WHERE Name = :cs.Account_Owner_Name__c];
         accountRec.OwnerId = uowner.Id;
        accountRec.FirstName ='CustomerTestingAccount';
        accountRec.LastName ='DemoTestingAccount';
        accountRec.PersonEmail = 'TestDemoAccount@email.com';
        accountRec.Account_Currency__c = 'USD';
        accountRec.BillingStreet = '4-1-20 shibuya';
        accountRec.BillingCity = 'chicago';
        accountRec.BillingState = 'Illinois';
        accountRec.BillingPostalCode ='60610';
        accountRec.BillingCountry ='USA';
        accountRec.Subscription_Plan__c = 'Monthly';
        accountRec.Password__c ='User@123';
        accountRec.Vector_Vest_Customer_Id__c ='TestDemoAccount@email.com';
        accountRec.Is_Apple_App_Store_Customer__c = true;
        accountRec.CertificateID__c = 'Apple';
        accountRec.GDPR_Opt_in__pc = 'No';
        insert accountRec;
        
        System.debug('***accountRec***'+accountRec);
      
         accountRec = [Select Id,  Salutation,FirstName,LastName,
                                            PersonEmail,EnterpriseCustomerID__c,
                                            ApplicationUserID__c,BillingStreet,
                                            BillingCity,BillingState,
                                            BillingPostalCode,BillingCountry,
                                            PersonMailingCountry,VV_Feed__c,
                                            VV_Edition__c,VV_Market__c,
                                            VV_Second_Market__c,Subscription_Plan__c,
                                            Is_Apple_App_Store_Customer__c,Account_Currency__c,
                                            SBQQ__TaxExempt__c,CertificateID__c,Region1__c,Phone,Account_Id__c,GDPR_Opt_in__pc,
                                            (select ClaimValue__c, Mask__c, Is_Trial__c,Subscription_End_Date__c,
                        Is_Observable__c,Non_Converting_Trial__c from Enterprise_Access_Rights__r where Non_Converting_Trial__c =true)
                                  FROM Account WHERE Id =:accountRec.Id];
        System.debug('***accountRec***'+accountRec.Enterprise_Access_Rights__r);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/update';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
         
        CustomerRestResource.updateCustomer(accountRec.EnterpriseCustomerID__c,accountRec.CertificateID__c,accountRec.GDPR_Opt_in__pc);
        Test.stopTest();
      //  System.assertEquals(200, res.statusCode);
    }
       @isTest static void testUpdateCustomerUserList() {
       /*Account accountRec = new Account();
       
        accountRec.FirstName ='Test';
        accountRec.LastName ='Demo';
        accountRec.PersonEmail = 'td@email.com';
        accountRec.Account_Currency__c = 'USD';
        accountRec.BillingStreet = '4-1-20 shibuya';
        accountRec.BillingCity = 'chicago';
        accountRec.BillingState = 'Illinois';
        accountRec.BillingPostalCode ='60610';
        accountRec.BillingCountry ='USA';
        accountRec.Subscription_Plan__c = 'Monthly';
        accountRec.Password__c ='User@123';
        accountRec.Vector_Vest_Customer_Id__c ='td@email.com';
        accountRec.Is_Apple_App_Store_Customer__c = false;
        accountRec.CertificateID__c = 'Apple';
        insert accountRec;*/
        test.startTest();
        CustomerInfoService.CustomerInfo ci = new CustomerInfoService.CustomerInfo();
         ci =TestFactoryData.createCustomer1();
         ci.applestore =false;
         ci.optinMarketing='No';
         Account accountRec = CustomerInfoService.createCustomer(ci);
        accountRec = [Select Id,  Salutation,FirstName,LastName,
                                            PersonEmail,EnterpriseCustomerID__c,
                                            ApplicationUserID__c,BillingStreet,
                                            BillingCity,BillingState,
                                            BillingPostalCode,BillingCountry,
                                            PersonMailingCountry,VV_Feed__c,
                                            VV_Edition__c,VV_Market__c,
                                            VV_Second_Market__c,Subscription_Plan__c,
                                            Is_Apple_App_Store_Customer__c,Account_Currency__c,
                                            SBQQ__TaxExempt__c,CertificateID__c,Region1__c,Phone,Account_Id__c,GDPR_Opt_in__pc
                                  FROM Account WHERE Id =:accountRec.Id];
        System.debug('***accountRec***'+accountRec);
            
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/update';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
  
        CustomerRestResource.updateCustomer(accountRec.EnterpriseCustomerID__c,accountRec.CertificateID__c,String.valueof(accountRec.GDPR_Opt_in__pc));
        Test.stopTest();
       // System.assertEquals(200, res.statusCode);
    }
     @isTest static void testCustomerNotFoundError() {
       Account accountRec = new Account();
       
        accountRec.FirstName ='Test';
        accountRec.LastName ='Demo';
        accountRec.PersonEmail = 'td@email.com';
        accountRec.Account_Currency__c = 'USD';
        accountRec.BillingStreet = '4-1-20 shibuya';
        accountRec.BillingCity = 'chicago';
        accountRec.BillingState = 'Illinois';
        accountRec.BillingPostalCode ='60610';
        accountRec.BillingCountry ='USA';
        accountRec.Subscription_Plan__c = 'Monthly';
        accountRec.Password__c ='User@123';
        accountRec.Vector_Vest_Customer_Id__c ='td@email.com';
        accountRec.Is_Apple_App_Store_Customer__c = false;
        accountRec.CertificateID__c = 'Apple';
        accountRec.GDPR_Opt_in__pc = 'No';
        insert accountRec;
        
        accountRec = [Select Id,  Salutation,FirstName,LastName,
                                            PersonEmail,EnterpriseCustomerID__c,
                                            ApplicationUserID__c,BillingStreet,
                                            BillingCity,BillingState,
                                            BillingPostalCode,BillingCountry,
                                            PersonMailingCountry,VV_Feed__c,
                                            VV_Edition__c,VV_Market__c,
                                            VV_Second_Market__c,Subscription_Plan__c,
                                            Is_Apple_App_Store_Customer__c,Account_Currency__c,
                                            SBQQ__TaxExempt__c,CertificateID__c,Region1__c,Phone,Account_Id__c,GDPR_Opt_in__pc
                                  FROM Account WHERE Id =:accountRec.Id];
        System.debug('***accountRec***'+accountRec);
            
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/update';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
        Test.startTest();
        CustomerRestResource.updateCustomer('123',accountRec.CertificateID__c,accountRec.GDPR_Opt_in__pc);
        Test.stopTest();
        //System.assertEquals(200, res.statusCode);
    }
    @isTest static void testOptinMarketingIsNotPickList() {
       Account accountRec = new Account();
       
        accountRec.FirstName ='Test';
        accountRec.LastName ='Demo';
        accountRec.PersonEmail = 'td@email.com';
        accountRec.Account_Currency__c = 'USD';
        accountRec.BillingStreet = '4-1-20 shibuya';
        accountRec.BillingCity = 'chicago';
        accountRec.BillingState = 'Illinois';
        accountRec.BillingPostalCode ='60610';
        accountRec.BillingCountry ='USA';
        accountRec.Subscription_Plan__c = 'Monthly';
        accountRec.Password__c ='User@123';
        accountRec.Vector_Vest_Customer_Id__c ='td@email.com';
        accountRec.Is_Apple_App_Store_Customer__c = false;
        accountRec.CertificateID__c = 'Apple';
        accountRec.GDPR_Opt_in__pc = 'abc';
        insert accountRec;
        
        accountRec = [Select Id,  Salutation,FirstName,LastName,
                                            PersonEmail,EnterpriseCustomerID__c,
                                            ApplicationUserID__c,BillingStreet,
                                            BillingCity,BillingState,
                                            BillingPostalCode,BillingCountry,
                                            PersonMailingCountry,VV_Feed__c,
                                            VV_Edition__c,VV_Market__c,
                                            VV_Second_Market__c,Subscription_Plan__c,
                                            Is_Apple_App_Store_Customer__c,Account_Currency__c,
                                            SBQQ__TaxExempt__c,CertificateID__c,Region1__c,Phone,Account_Id__c,GDPR_Opt_in__pc
                                  FROM Account WHERE Id =:accountRec.Id];
        System.debug('***accountRec***'+accountRec);
            
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/customer/update';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
        Test.startTest();
        CustomerRestResource.updateCustomer('123',accountRec.CertificateID__c,accountRec.GDPR_Opt_in__pc);
        Test.stopTest();
        //System.assertEquals(200, res.statusCode);
    }
    
}