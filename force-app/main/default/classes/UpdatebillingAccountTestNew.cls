@istest
private class UpdatebillingAccountTestNew{
    private static testmethod void testmethod1(){
        //update [select id from SBQQ__Quote__c where createddate=today limit 1];
        Account acc = new Account();
            acc.firstname='test acc1';
            acc.lastName = 'test acc';
            acc.billingCity ='Test City';
            acc.billingStreet = 'TestStreet';
            acc.billingstate ='TestState';
            acc.billingPostalcode ='23451';
            acc.billingcountry ='United States';
            Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            acc.recordtypeID=objAccrec.id;
           acc.PersonMailingCity='Test City';
            acc.PersonMailingStreet = 'TestStreet';
            acc.PersonMailingState ='TestState';
            acc.PersonMailingPostalCode ='23451';
            acc.PersonMailingCountry ='United States';
            acc.phone='090123456987';
            //acc.ispersonAccount = true;
            insert acc;
            Zuora__CustomerAccount__c objZCustAccount = new Zuora__CustomerAccount__c();
             objZCustAccount .Zuora__Account__c = acc.id;
             objZCustAccount .Zuora__Status__c ='Active';
             objZCustAccount .Zuora__BillToName__c ='test';
             objZCustAccount .Zuora__Zuora_Id__c = '2c92c0f960c105580160d80905a205ea';
             //objZCustAccount .Zuora__CreditCard_Number__c = '4444333322221111';
             //objZCustAccount .Zuora__CreditCard_Expiration__c = '10/2019';
             //insert objZCustAccount ;
             test.starttest();
            Opportunity opp = new Opportunity(Name=acc.Name + ' Opportunity ' ,
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                      AccountId=acc.Id);
                                      insert opp; 
                                        
             SBQQ__Quote__c quoteRec = new SBQQ__Quote__c();
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SiteUser_Primary_Check__c = true;
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SBQQ__Opportunity2__c = opp.Id;
            quoteRec.SBQQ__Account__c = acc.id;
            
            //quoteRec.ZSB__BillToContact__c = c.Id;
           // quoteRec.ZSB__SoldToContact__c = c.Id;
            quoteRec.ZSB__BillCycleDay__c =  '1';
            quoteRec.ZSB__BillingBatch__c = 'Batch1';
            insert quoteRec ;
            test.stoptest();
            }
    }