global class CustomerInfoChangeInvocable {
	
	global class ApplicationEventInfo {
		@InvocableVariable(required=true)
    	global String eventType;

    	@InvocableVariable(required=true)
    	global String objectType;

    	@InvocableVariable(required=true)
    	global ID recordId;

    	@InvocableVariable(required=true)
    	global ID accId;
	}

	@InvocableMethod(label='Customer Info Change Notification' description='Raises a notification when a customer info changes')
	global static void customerInfoChanged(List<ApplicationEventInfo> eventInfos){
		CustomerInfoChangeEventHandler.customerInfoChanged(eventInfos);
	}
}