global class weeklySubAccountsBatch_1 implements Database.Batchable<sObject>{
    global static final List<String> weekDays = new List<String>{'Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'};
    global final Date d;
    global final Date dn;
    global final Datetime dt;
    global final Datetime dtn;
    global final String weekDay;
    global final Date weekStart;
    global final Date weekEnd;
    global static final Set<String> historyFields = new Set<String>{'BillingStreet','BillingCity','BillingState','BillingPostalCode','BillingCountry','PersonHasOptedOutOfEmail',
        'HasOptedOutOfMassEmail__c','HasOptedOutOfMassPostMail__c','PersonDoNotCall','Type'};
    global weeklySubAccountsBatch_1(Date dd){
        //d = dd;
        //dn = d.addDays(1);
        dt = DateTime.newInstance(dd, Time.newInstance(0, 0, 0, 0));
        d = dt.date();
        dtn = dt.addDays(1);
        dn = d.addDays(1);
        weekDay = dt.format('EEEE');
        weekStart = d.addDays(-weekDays.indexOf(dt.format('EEEE')));
        weekEnd = weekStart.addDays(6);
        
    }
    private static Test_Weekly_Customers__c createWC(Date d, Date weekStart, Date weekEnd, String weekDay, Test_Weekly_Customers__c wc, 
                                                     Account a, List<AccountHistory> aHs, SBQQ__Subscription__c sb, Map<String, AccountHistory> fidWithItsHists){

                                                             wc.Week_Start_Date__c = weekStart;
                                                             wc.Week_End_Date__c = weekEnd;
                                                             wc.Account__c = a.Id;
                                                             wc.Subscription__c = sb.Id;
                                                             wc.put(weekDay+'__c',d);
                                                         	 if (sb.Enterprise_Subscription_ID__c != NULL && !sb.Enterprise_Subscription_ID__c.contains('FUTURESUB') && !sb.Enterprise_Subscription_ID__c.contains('CONVERTTRIAL'))
                                                             	wc.Sub_Id__c = sb.Enterprise_Subscription_ID__c;
                                                         	 else
                                                                wc.Sub_Id__c = sb.Name;
                                                             wc.Customer_Type__c = a.Type;
                                                             wc.Account_First_Name__c = a.FirstName;
                                                             wc.Account_Last_Name__c = a.LastName;
                                                             wc.EnterpriseCustomerID__c = a.EnterpriseCustomerID__c;
                                                             if (a.Vector_Vest_Customer_ID__c != NULL)
                                                                wc.Vector_Vest_Customer_ID__c = a.Vector_Vest_Customer_ID__c.replace('@vectorvest-salesforce.com','');
                                                             wc.BillingStreet__c = a.BillingStreet;
                                                             wc.BillingCity__c = a.BillingCity;
                                                             wc.BillingState__c = a.BillingState;
                                                             wc.BillingPostalCode__c = a.BillingPostalCode;
                                                             wc.BillingCountry__c = a.BillingCountry;
                                                             wc.PersonHasOptedOutOfEmail__c = a.PersonHasOptedOutOfEmail;
                                                             wc.HasOptedOutOfMassEmail__c = a.HasOptedOutOfMassEmail__c;
                                                             wc.HasOptedOutOfMassPostMail__c = a.HasOptedOutOfMassPostMail__c;
                                                             wc.PersonDoNotCall__c = a.PersonDoNotCall;
                                                             if(!Test.isRunningTest()) {
                                                                wc.Product_Name__c = sb.SBQQ__ProductName__c;
                                                                wc.Source__c = sb.SecondaryCampaignSource__c;
                                                             }

                                                         if(!aHs.isEmpty()){
                                                             for(AccountHistory ah: aHs){
                                                                 if(ah.Field == 'Type')
                                                                     wc.put('Customer_Type__c',ah.oldValue);
                                                                 else if(ah.Field.endsWith('__c'))
                                                                     wc.put(ah.Field,ah.oldValue);
                                                                 else
                                                                     wc.put(ah.Field+'__c',ah.oldValue);
                                                             }
                                                         }
                                                         for(String ahf:fidWithItsHists.keyset()){
                                                             AccountHistory ah = fidWithItsHists.get(ahf);
                                                             if(ah.Field == 'Type')
                                                                 wc.put('Customer_Type__c',ah.newValue);
                                                             else if(ah.Field.endsWith('__c'))
                                                                 wc.put(ah.Field,ah.newValue);
                                                             else
                                                                 wc.put(ah.Field+'__c',ah.newValue);
                                                         }
                                                         return wc;
                                                     } 
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String Query = 'SELECT CreatedDate, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, EnterpriseCustomerID__c, ';
        Query+='FirstName, LastName, MiddleName,';
        Query+='Type, Id, HasOptedOutOfMassEmail__c, PersonHasOptedOutOfEmail, HasOptedOutOfMassPostMail__c,';
        Query+='PersonDoNotCall, Name, Vector_Vest_Customer_Id__c, YahooId__c, ';
        Query+='(SELECT AccountId, Field, OldValue, NewValue, Id, CreatedDate FROM Histories WHERE CreatedDate >= :dtn AND Field IN :historyFields ';
        Query+='ORDER BY CreatedDate DESC), ';
        Query+='(SELECT ID, Name, Enterprise_Subscription_ID__c, SBQQ__ProductName__c, SecondaryCampaignSource__c FROM SBQQ__Subscriptions__r WHERE (SBQQ__TerminatedDate__c = null or SBQQ__TerminatedDate__c > :d) AND';
        Query+='((SBQQ__Contract__r.ZSB__TermType__c = \'Evergreen\' AND SBQQ__StartDate__c <= :d) ';
        Query+='OR (SBQQ__Contract__r.ZSB__TermType__c != \'Evergreen\' AND SBQQ__StartDate__c <= :d AND SBQQ__EndDate__c >= :d)) AND SBQQ__Quantity__c > 0) FROM Account ';
        Query+='WHERE Id IN (SELECT SBQQ__Account__c FROM SBQQ__Subscription__c WHERE (SBQQ__TerminatedDate__c = null or SBQQ__TerminatedDate__c > :d) AND';
        Query+='((SBQQ__Contract__r.ZSB__TermType__c = \'Evergreen\' AND SBQQ__StartDate__c <= :d)';
        Query+='OR (SBQQ__Contract__r.ZSB__TermType__c != \'Evergreen\' AND SBQQ__StartDate__c <= :d AND SBQQ__EndDate__c >= :d)) AND SBQQ__Quantity__c > 0 AND SBQQ__Account__c != \'0013600001n1RxVAAU\')';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Account> accs){
        Map<Id, SBQQ__Subscription__c> subcsWithWeekInfo = new Map<Id, SBQQ__Subscription__c>(
            [SELECT ID, Name, SBQQ__Account__c , SBQQ__StartDate__c, SBQQ__EndDate__c, SBQQ__TerminatedDate__c, 
             SecondaryCampaignSource__c, SBQQ__ProductId__c , SBQQ__ProductName__c, Enterprise_Subscription_ID__c,
             SBQQ__QuoteLine__r.SBQQ__Quote__r.SecondaryCampaignsource__r.Primarysources__r.PrimarysourceID__c, SBQQ__QuoteLine__r.SBQQ__Quote__r.SecondaryCampaignsource__r.Primarysources__r.Name,
             (SELECT Id, Name, Saturday__c, Sunday__c, Monday__c, Tuesday__c, Wednesday__c, Thursday__c, Friday__c, 
              Customer_ID__c, Year_Week__c, Product_Name__c, Source__c, Customer_Type__c, BillingStreet__c, BillingCity__c, 
              BillingState__c, BillingPostalCode__c, BillingCountry__c, 
              Week_Start_Date__c, Week_End_Date__c, Account__c, Subscription__c  
              FROM Test_Weekly_Customers__r WHERE Week_Start_Date__c = :weekStart AND Week_End_Date__c = :weekEnd //AND Do_not_Update__c = false 
              ORDER By CreatedDate)
             FROM SBQQ__Subscription__c WHERE SBQQ__Account__c IN :accs AND (SBQQ__TerminatedDate__c = null or SBQQ__TerminatedDate__c > :d) AND // Terminated date should not be populated
             ((SBQQ__Contract__r.ZSB__TermType__c = 'Evergreen' AND SBQQ__StartDate__c <= :d) // if Contract is EverGreen check Start Date it should be less that today
              OR (SBQQ__Contract__r.ZSB__TermType__c != 'Evergreen' AND SBQQ__StartDate__c <= :d AND SBQQ__EndDate__c >= :d)) AND SBQQ__Quantity__c > 0 AND SBQQ__Account__c != '0013600001n1RxVAAU']
        );
        //historyFields
        Map<Id, Account> accsWithTodaysHists = new Map<Id, Account>([Select Id, 
                                                                 (SELECT AccountId, Field, OldValue, NewValue, Id, CreatedDate FROM Histories 
                                                                  WHERE CreatedDate >= :dt AND CreatedDate < :dtn AND Field IN :historyFields ORDER BY CreatedDate DESC) 
                                                                FROM Account Where Id IN :accs]);
            
        List<Test_Weekly_Customers__c> wsLst = new List<Test_Weekly_Customers__c>();
        
        for(Account a:accs){
            List<AccountHistory> todaysHists = accsWithTodaysHists.get(a.Id).Histories;
            Map<String, AccountHistory> fidWithItsHists = new Map<String, AccountHistory>();
            for(AccountHistory ah:todaysHists){
                if(!fidWithItsHists.containsKey(ah.Field))
                    fidWithItsHists.put(ah.Field, ah);
            }
            for(SBQQ__Subscription__c sb:a.SBQQ__Subscriptions__r){
                List<Test_Weekly_Customers__c> weekRecs = subcsWithWeekInfo.get(sb.Id).Test_Weekly_Customers__r;
                if(weekRecs.isEmpty()){ // If Week rec exists for a subscription
                    //if(a.Histories.isEmpty())
                    //  wsLst.add(createWC(d, weekStart, weekEnd, weekDay, new Test_Weekly_Customers__c(), a, a.Histories, sb));
                    //else
                    wsLst.add(createWC(d, weekStart, weekEnd, weekDay, new Test_Weekly_Customers__c(), a, a.Histories, sb, fidWithItsHists));
                }else{
                    Test_Weekly_Customers__c wc = weekRecs[weekRecs.size()-1];
                    
                    if(!todaysHists.isEmpty())
                        wsLst.add(createWC(d, weekStart, weekEnd, weekDay, new Test_Weekly_Customers__c(), a, a.Histories, sb, fidWithItsHists));
                    else
                        wsLst.add(createWC(d, weekStart, weekEnd, weekDay, wc, a, a.Histories, sb, fidWithItsHists));
                    
                }
            }
            //system.debug(a.Histories);
        }
        upsert wsLst;
    }
    
    global void finish(Database.BatchableContext BC){
    }
}