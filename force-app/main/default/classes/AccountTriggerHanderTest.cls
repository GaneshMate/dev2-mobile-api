@isTest
private class AccountTriggerHanderTest {
    @isTest static void testInsert() {
        // set up lead
        Lead l = new Lead(FirstName='firstnameunique',LastName='lastnameunique',Email='testveryunique@test.vvtest.com');
        insert l;

        // set up account
        /*Account a = new Account(Primary_Phone_Type__c='Mobile',FirstName='firstnameunique',LastName='lastnameunique',PersonEmail='test@test.com');
        insert a;*/
         Account a = new Account(PersonMailingCity='Test City',PersonMailingStreet = 'TestStreet',PersonMailingState='TestState',PersonMailingPostalCode ='23451',personMailingCountry ='United States',Primary_Phone_Type__c='Mobile',FirstName='testfirst',LastName='testlast',PersonEmail='testveryunique@test.vvtest.com',Phone='2223334444',VV_Market__c='US',VV_Second_Market__c='CA',VV_Feed__c='ID',VV_Edition__c='Non-Professional',Subscription_Plan__c='Annual',Is_Elite__c=false,Is_Elite_Legacy__c=false,Is_Apple_App_Store_Customer__c=false,BillingStreet='2234 test',BillingCity='Test',BillingState='GA',BillingCountry='USA',BillingPostalCode='22344');
        insert a;
        Zuora__CustomerAccount__c zca = new Zuora__CustomerAccount__c(Zuora__Status__c='Active',Name='test billing account',Zuora__Account__c=a.Id,Zuora__Zuora_Id__c='12345');
        insert zca;

        // free trial before initital testing
        Zuora__Subscription__c zs = new Zuora__Subscription__c(Name='a0001',Zuora__SubscriptionStartDate__c = Date.today().addMonths(-12), Zuora__SubscriptionEndDate__c = Date.today().addYears(2), Zuora__InitialTerm__c='12 months',Zuora__RenewalTerm__c='12 months',Zuora__TermStartDate__c=Date.today().addMonths(-3),
            Zuora__TermEndDate__c = Date.today().addMonths(20),Zuora__AutoRenew__c=true, Zuora__Zuora_Id__c='12345', Zuora__ContractEffectiveDate__c=Date.today().addMonths(-3), Zuora__ServiceActivationDate__c=Date.today().addMonths(-3), 
            Zuora__TCV__c=12,Zuora__Account__c=a.Id,Zuora__CustomerAccount__c=zca.Id,Zuora__Status__c='Active');
        insert zs;

        Zuora__SubscriptionRatePlan__c zsrp = new Zuora__SubscriptionRatePlan__c(Name='testtest1',Zuora__OriginalProductRatePlanId__c='12345');
        insert zsrp;

        Zuora__SubscriptionProductCharge__c zspc = new Zuora__SubscriptionProductCharge__c(
            Zuora__Subscription__c=zs.Id,Zuora__SubscriptionRatePlan__c=zsrp.Id,Name='testtest1',Zuora__Description__c='Primary: test test',Zuora__Price__c=12,Zuora__RatePlanId__c='12345',Zuora__RatePlanName__c='testtest1',Zuora__EffectiveStartDate__c=Date.today().addMonths(-3),Zuora__EffectiveEndDate__c=Date.today().addMonths(15),Zuora__EndDateCondition__c=null,Zuora__Type__c='Recurring');
        insert zspc;

        Test.startTest();
        a.Has_WatchList__c = true;
        update a;
        delete a;
        Test.stopTest();

    }
}