/**
 * currency management 
 */
public with sharing class vvCurrencyManagement {
    /**
     * following two static methods getter prevent soql from being run more than once
     */
    public static Currency_Pricing_Rate__c cprGBP {
        get {
            if (cprGBP == null) {
                List<Currency_Pricing_Rate__c> cpr = [SELECT Rate__c FROM Currency_Pricing_Rate__c WHERE Name = 'GBP' AND Effective_Date__c <= TODAY ORDER BY Effective_Date__c DESC LIMIT 1];
                if (!cpr.isEmpty()) cprGBP = cpr[0];
            }
            return cprGBP;
        }
        set;
    }
    public static Currency_Pricing_Rate__c cprEUR {
        get {
            if (cprEUR == null) {
                List<Currency_Pricing_Rate__c> cpr = [SELECT Rate__c FROM Currency_Pricing_Rate__c WHERE Name = 'EUR' AND Effective_Date__c <= TODAY ORDER BY Effective_Date__c DESC LIMIT 1];
                if (!cpr.isEmpty()) cprEUR = cpr[0];
            }
            return cprEUR;
        }
        set;
    }

    /**
     * convert from USD to currency
     * @param  amount [description]
     * @param  curr   [description]
     * @return        [description]
     */
    public static Decimal convertFromUSD(Decimal amount, String curr) {
        if (curr == null) return amount;
        
        Decimal returnamount = amount;
        // get the currency pricing rate
        Currency_Pricing_Rate__c cpr = null;
        if (curr == 'GBP') cpr = cprGBP;
        else if (curr == 'EUR') cpr = cprEUR;

        if (cpr != null) {
            returnamount = amount * cpr.Rate__c;
            // round 0.05 cent at a time: 0.14 = 0.15, 0.17 = 0.20
            Decimal p = Math.roundToLong(returnamount * 20);
            returnamount = p / 20;
            //if (returnamount < amount) returnamount += 0.05;

        }
        return returnamount;
    }

    /**
     * convert from a currency to other format
     */
    public static Decimal convertCurrency(Decimal amount, String fromcurr, String tocurr) {
        if (fromcurr == null) return amount;

        Decimal rate = null;
        Decimal returnamount = amount;

        if (fromcurr == 'USD' && tocurr == 'EUR') rate = cprEUR.Rate__c;
        else if (fromcurr == 'USD' && tocurr == 'GBP') rate = cprGBP.Rate__c;
        else if (fromcurr == 'EUR' && tocurr == 'USD') {
            // eur = usd * eurrate;
            // usd = eur / eurrate  = eur * 1 / eurrate
            rate = 1 / cprEUR.Rate__c;
        } else if (fromcurr == 'GBP' && tocurr == 'USD') {
            rate = 1 / cprGBP.Rate__c;
        } else if (fromcurr == 'EUR' && tocurr == 'GBP') {
            // eur = usd * eurrate;
            // usd = eur / eurrate  = eur * 1 / eurrate
            // gbp = usd * gbprate = eur * 1 / eurrate * gbprate
            rate = (1 / cprEUR.Rate__c) * cprGBP.Rate__c;
        } else if (fromcurr == 'GBP' && tocurr == 'EUR') {
            rate = (1 / cprGBP.Rate__c) * cprEUR.Rate__c;
        }

        if (rate != null) {
            returnamount = amount * rate;
            // round 0.05 cent at a time: 0.14 = 0.15, 0.17 = 0.20
            Decimal p = Math.roundToLong(returnamount * 20);
            returnamount = p / 20;
            //if (returnamount < amount) returnamount += 0.05;

        }
        return returnamount;
    }

    /**
     * get zuora gateway to use for a currency
     * @param  curr [description]
     * @return      [description]
     */
    public static String getGateway(Account a, String paymenttype) {
        // get gateway names from custom setting
        Zuora_Integration__c zid = Zuora_Integration__c.getOrgDefaults();

        if (paymenttype == null) return null;
        if (a.BillingCountry == 'USA' && paymenttype == 'ACH') return zid.ACH_Gateway__c;

        if (a.Account_Currency__c == 'USD') return zid.USD_Gateway__c;

        if (a.Account_Currency__c == 'EUR') return zid.EUR_Gateway__c;

        if (a.Account_Currency__c == 'GBP') return zid.GBP_Gateway__c;
        if (a.Account_Currency__c == 'AUD') return zid.AUD_Gateway__c;

        return null;
    }


    public static String getCurrencySymbol(Account a) {
        if (a.Account_Currency__c == 'EUR') return '€';

        if (a.Account_Currency__c == 'GBP') return '£';

        return '$';
    }
}