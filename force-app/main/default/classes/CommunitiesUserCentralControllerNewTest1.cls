/**
 * test class for user central, again we need seealldata here to pass zuora wsdl init
 */
@isTest(SeeAllData=true)
public class CommunitiesUserCentralControllerNewTest1 {
    public static User setupAccountUserSub() {
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

        // create user with role
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
            

        User portalAccountOwner1 = new User(
                UserRoleId = portalRole.Id,
                ProfileId = profile1.Id,
                Username = System.now().millisecond() + 'test4@test.com',
                Alias = 'batman',
                Email='bruce.wayne@wayneenterprises.com',
                EmailEncodingKey='UTF-8',
                Firstname='Bruce',
                Lastname='Wayne',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                TimeZoneSidKey='America/Chicago'
            );

        System.runAs ( thisUser ) {
            insert portalAccountOwner1;
        }


        // create account
        Account a = new Account(PersonMailingCity='Test City',PersonMailingStreet = 'TestStreet',PersonMailingState='TestState',PersonMailingPostalCode ='23451',personMailingCountry ='United States',Primary_Phone_Type__c='Mobile',OwnerId=portalAccountOwner1.Id,FirstName='testfirst',LastName='testlast',PersonEmail='testveryunique@test.vvtest.com',Phone='2223334444',VV_Market__c='US',VV_Second_Market__c='CA',VV_Feed__c='ID',VV_Edition__c='Non-Professional',Subscription_Plan__c='Annual',Is_Elite__c=false,Is_Elite_Legacy__c=false,Is_Apple_App_Store_Customer__c=false,BillingStreet='2234 test',BillingCity='Test',BillingState='GA',BillingCountry='USA',BillingPostalCode='22344');
        insert a;

        Zuora__CustomerAccount__c zca = new Zuora__CustomerAccount__c(Zuora__Status__c='Active',Name='test billing account',Zuora__Account__c=a.Id,Zuora__Zuora_Id__c='12345');
        insert zca;

        // free trial before initital testing
        Zuora__Subscription__c zs = new Zuora__Subscription__c(Name='Primary: VectorVest 7.0 US ID Annual',Zuora__SubscriptionStartDate__c = Date.today().addMonths(-12), Zuora__SubscriptionEndDate__c = Date.today().addYears(2), Zuora__InitialTerm__c='12 months',Zuora__RenewalTerm__c='12 months',Zuora__TermStartDate__c=Date.today().addMonths(-3),
            Zuora__TermEndDate__c = Date.today().addMonths(20),Zuora__AutoRenew__c=true, Zuora__Zuora_Id__c='12345', Zuora__ContractEffectiveDate__c=Date.today().addMonths(-3), Zuora__ServiceActivationDate__c=Date.today().addMonths(-3), 
            Zuora__TCV__c=12,Zuora__Account__c=a.Id,Zuora__CustomerAccount__c=zca.Id,Zuora__Status__c='Active');
        insert zs;

        zqu__ProductRatePlan__c zpp = [SELECT zqu__ZuoraId__c FROM zqu__ProductRatePlan__c WHERE zqu__Deleted__c = false AND Name = 'WatchDog'];


        Zuora__SubscriptionRatePlan__c zsrp = new Zuora__SubscriptionRatePlan__c(Name='VectorVest 7.0 US ID Annual',Zuora__OriginalProductRatePlanId__c=zpp.zqu__ZuoraId__c);
        insert zsrp;

        Zuora__SubscriptionProductCharge__c zspc = new Zuora__SubscriptionProductCharge__c(
            Zuora__Subscription__c=zs.Id,Zuora__SubscriptionRatePlan__c=zsrp.Id,Name='VectorVest 7.0 US ID Annual',Zuora__Description__c='Primary: VectorVest 7.0 US ID Annual',Zuora__Price__c=12,Zuora__RatePlanId__c='12345',Zuora__RatePlanName__c='testtest1',Zuora__EffectiveStartDate__c=Date.today().addMonths(-3),Zuora__EffectiveEndDate__c=Date.today().addMonths(15),Zuora__EndDateCondition__c=null,Zuora__Type__c='Recurring');
        insert zspc;

        Zuora__PaymentMethod__c zpm = new Zuora__PaymentMethod__c(Zuora__CreditCardAddress1__c='test',Zuora__BillingAccount__c=zca.Id,Zuora__DefaultPaymentMethod__c=true);
        insert zpm;

        // setup coupon
        Coupon_Matrix__c cm = new Coupon_Matrix__c(Name='12345',Discount_Amount__c=10,Description__c='testcoupon',Effective_Start_Date__c=Date.today().addYears(-1),Effective_End_Date__c=Date.today().addYears(1));
        insert cm;

        // setup campaign and bundle
        Bundle__c bundle = new Bundle__c(Name='testbundle');
        insert bundle;

        Bundle__c bundle2 = new Bundle__c(Name='testb2');//,Required__c = 'RT');
        insert bundle2;
        
        Bundle_Product__c bp = new Bundle_Product__c(Bundle__c = bundle.Id, Zuora_Product__c = zpp.Id, Price__c = 10);
        insert bp;
        Opportunity opp = new Opportunity(Name=a.Name + ' Opportunity ' ,
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=a.Id);
        SBQQ__Quote__c quoteRec = new SBQQ__Quote__c();
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SiteUser_Primary_Check__c = true;
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SBQQ__Opportunity2__c = opp.Id;
           // quoteRec.ZSB__BillToContact__c = c.Id;
           // quoteRec.ZSB__SoldToContact__c = c.Id;
            quoteRec.ZSB__BillCycleDay__c =  '1';
            quoteRec.ZSB__BillingBatch__c = 'Batch1';
            insert quoteRec ;
        Campaign camp = new Campaign(Name = 'test campaign',Quote__c=quoteRec.id,Promo_Name__c='Test', PromoCode__c = '1qaz2wsx',Bundle__c = bundle.Id, StartDate = Date.today().addDays(-1), EndDate = Date.today().addDays(1));
        insert camp;
        

        Contact c = [SELECT FirstName, LastName, Email FROM Contact WHERE AccountId = :a.Id];
        // create user
        Id profileId = [select Id from Profile WHERE Name = 'Customer Community Login User'].Id;

        User userObj                = new User();
        String lang                 = 'en_US';
        String userPrefix           = c.Email.substring(0, c.Email.indexOf('@'));

        userObj.Username            = c.Email;
        userObj.FirstName           = c.FirstName;
        userObj.LastName            = c.LastName;
        userObj.Email               = c.Email;
        userObj.contactId           = c.Id;
        userObj.CommunityNickname   = c.Email.abbreviate(19) + string.valueof(DateTime.now());
        userObj.LanguageLocaleKey   = lang;
        userObj.ProfileId           = profileId;
        userObj.emailencodingkey    = 'UTF-8';
        userObj.timezonesidkey      = 'America/New_York';
        userObj.localesidkey        = lang;
        userObj.Alias               = (userPrefix.length() > 8 ? userPrefix.substring(0, 8) : userPrefix);
        userObj.IsActive            = true;

        insert userObj;

        return userObj;

    }

    @isTest static void  testCommunitiesUserCentralControllerNew() {

        User userObj = CommunitiesUserCentralControllerNewTest1.setupAccountUserSub();

        
        System.runAs(userObj) {
            Test.startTest();
            CommunitiesUserCentralControllerNew ctrl = new CommunitiesUserCentralControllerNew();

            ctrl.u.Username = 'newtest@testtest.com';   
            ctrl.updateUsername();
            ctrl.u.Email = 'newtest3@testtest3234.com'; 
            ctrl.updateEmail();

            ctrl.u.Username = 'asdfsad';
            ctrl.updateUsername();
            ctrl.u.Email = 'newtest3';
            ctrl.updateEmail();

            ctrl.toggleOptIn();
            
            ctrl.newPassword = '1qaz@wsxEDC';
            ctrl.newPasswordConfirm = ctrl.newPassword;
            ctrl.changePassword();

            ctrl.newPassword = '11';
            ctrl.newPasswordConfirm = ctrl.newPassword;
            ctrl.changePassword();

            ctrl.resetPassword();


            vvAccountManagementExtension ame = new vvAccountManagementExtension(ctrl);
            
           Test.stopTest();
        }

        
    }

    
    @isTest static void  testUpdateAccount() {

        User userObj = CommunitiesUserCentralControllerNewTest1.setupAccountUserSub();

        
        System.runAs(userObj) {
            Test.startTest();
            CommunitiesUserCentralControllerNew ctrl = new CommunitiesUserCentralControllerNew();
            ctrl.updateAccount();
            Test.stopTest();
        }
        
    }

    @isTest static void  testUpgradeAndCancel() {

        User userObj = CommunitiesUserCentralControllerNewTest1.setupAccountUserSub();

        
        System.runAs(userObj) {
            Test.startTest();
            CommunitiesUserCentralControllerNew ctrl = new CommunitiesUserCentralControllerNew();

            for (zqu__ProductRatePlan__c prp : vvProductManagement.getUpgradeList(ctrl.a.Id)) {
                ctrl.selectedUpgrade = prp.Id;
                break;
            }
            //ctrl.upgradeSubscription();

            ctrl.cancelSubscription();
            Test.stopTest();
        }

        
    }
    @isTest static void  testUpdatePayment() {

        User userObj = CommunitiesUserCentralControllerNewTest1.setupAccountUserSub();

        
        System.runAs(userObj) {
            Test.startTest();
             payment_method_sf__c objpaymentmethod = new payment_method_sf__c();
        objpaymentmethod.Payment_Type__c ='Credit Card';
        objpaymentmethod.Credit_Card_Number__c ='4444333322221111';
        objpaymentmethod.Credit_Card_Holder_Name__c ='test';
        objpaymentmethod.Credit_Card_Expiration_Month__c ='09';
        objpaymentmethod.Credit_Card_Expiration_Year__c ='2019';
        objpaymentmethod.Credit_Card_CVV_Code__c ='123';
        CommunitiesUserCentralControllerNew  obj= new CommunitiesUserCentralControllerNew();
        obj.payment_method=objpaymentmethod;
        obj.updatepayment();
            Test.stopTest();


        }

        
    }

    @isTest static void  testSwitchBilling() {

        User userObj = CommunitiesUserCentralControllerNewTest1.setupAccountUserSub();

        
        System.runAs(userObj) {
            Test.startTest();
            CommunitiesUserCentralControllerNew ctrl = new CommunitiesUserCentralControllerNew();
            //ctrl.switchBilling('Annual');
            Test.stopTest();
        }

        
    }

    @isTest static void testBundle() {
         // create account
        Account a = new Account(PersonMailingCity='Test City',PersonMailingStreet = 'TestStreet',PersonMailingState='TestState',PersonMailingPostalCode ='23451',personMailingCountry ='United States',Primary_Phone_Type__c='Mobile',FirstName='testfirst',LastName='testlast',PersonEmail='testveryunique@test.vvtest.com',Phone='2223334444',VV_Market__c='US',VV_Second_Market__c='CA',VV_Feed__c='ID',VV_Edition__c='Non-Professional',Subscription_Plan__c='Annual',Is_Elite__c=false,Is_Elite_Legacy__c=false,Is_Apple_App_Store_Customer__c=false,BillingStreet='2234 test',BillingCity='Test',BillingState='GA',BillingCountry='USA',BillingPostalCode='22344');
        insert a;
        User userObj = CommunitiesUserCentralControllerNewTest1.setupAccountUserSub();

        List<zqu__ProductRatePlan__c> zpp = [SELECT Id FROM zqu__ProductRatePlan__c WHERE zqu__Deleted__c = false AND (Name = 'Simulator' OR Name = 'Variator')];
        Bundle__c bundle = new Bundle__c(Name = 'testbundle');
        insert bundle;
        Bundle_Product__c bp = new Bundle_Product__c(Bundle__c = bundle.Id, Zuora_Product__c = zpp[0].Id, Price__c = 10);
        insert bp;
        Bundle_Product__c bp2 = new Bundle_Product__c(Bundle__c = bundle.Id, Zuora_Product__c = zpp[1].Id, Price__c = 10, HasTrial__c = true, Trial_Duration_Days__c = 10, Trial_Price__c = 10, Special_Period_Month__c = 3);
        insert bp2;
        Opportunity opp = new Opportunity(Name=a.Name + ' Opportunity ' ,
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=a.Id);
        SBQQ__Quote__c quoteRec = new SBQQ__Quote__c();
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SiteUser_Primary_Check__c = true;
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SBQQ__Opportunity2__c = opp.Id;
           // quoteRec.ZSB__BillToContact__c = c.Id;
           // quoteRec.ZSB__SoldToContact__c = c.Id;
            quoteRec.ZSB__BillCycleDay__c =  '1';
            quoteRec.ZSB__BillingBatch__c = 'Batch1';
            insert quoteRec ;
        Campaign camp = new Campaign(Name = 'test campaign',Quote__c=quoteRec.id,Promo_Name__c='Test', PromoCode__c = 'mattbestcoder',Bundle__c = bundle.Id, StartDate = Date.today().addDays(-1), EndDate = Date.today().addDays(1));
        insert camp;

        
        System.runAs(userObj) {
            Test.startTest();
            CommunitiesUserCentralControllerNew ctrl = new CommunitiesUserCentralControllerNew();
            ctrl.displayBundle();
            ApexPages.CurrentPage().getParameters().put('promo','abc'); // wrong
            ctrl.displayBundle();
            ApexPages.CurrentPage().getParameters().put('promo','mattbestcoder'); // correct
            ctrl.displayBundle();

            ctrl.addBundle();

            Test.stopTest();
        }

    }
    
}