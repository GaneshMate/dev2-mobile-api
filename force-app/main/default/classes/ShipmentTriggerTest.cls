@isTest
private class ShipmentTriggerTest {

    @isTest static void testShipmentTriggerHandler() {
        // setup product
        Product2 p = new Product2(Family='Book',Name='thebook',IsActive=true);
        insert p;

        /*Account a = new Account(FirstName='testfirst',LastName='testlast');
        insert a;*/
        Account acc = new Account();
            acc.firstname='test acc1';
            acc.lastName = 'test acc';
            acc.billingCity ='Test City';
            acc.billingStreet = 'TestStreet';
            acc.billingstate ='TestState';
            acc.billingPostalcode ='23451';
            acc.billingcountry ='United States';
            Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            acc.recordtypeID=objAccrec.id;
           acc.PersonMailingCity='Test City';
            acc.PersonMailingStreet = 'TestStreet';
            acc.PersonMailingState ='TestState';
            acc.PersonMailingPostalCode ='23451';
            acc.PersonMailingCountry ='United States';
            acc.phone='090123456987';
            //acc.ispersonAccount = true;
            insert acc;

        Account_Product__c ap = new Account_Product__c(Account__c = acc.Id, Product__c = p.Id, Status__c = 'Purchased');
        insert ap;

        zkfedex__Shipment__c s = new zkfedex__Shipment__c(Account__c = acc.Id);
        insert s;

    }
}