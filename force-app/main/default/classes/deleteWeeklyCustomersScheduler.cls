global class deleteWeeklyCustomersScheduler implements Schedulable {
    
    
    global deleteWeeklyCustomersScheduler () {
        
    }
    
    global void execute(SchedulableContext sc){
        
        Date d;
        Datetime dt;
        //Date PriorweekStart;
        //List<String> weekDays = new List<String>{'Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'};
    
        dt = DateTime.newInstance(Date.Today(), Time.newInstance(0, 0, 0, 0));
        d = dt.date();
        //PriorweekStart = d.addDays(-7-weekDays.indexOf(dt.format('EEEE')));
        
        deleteWeeklyCustomers Batch = new deleteWeeklyCustomers (d);
        Id BatchProcessId = Database.ExecuteBatch(Batch);
    }
}