@isTest
private class ApplicationEventCleanupTest {
	@isTest static void testMain() {
		Application_Event__c ae = new Application_Event__c(Handled__c = true);
		insert ae;
		Test.startTest();
		System.schedule('Application Event Scheduler','0 5 0 * * ?', new ApplicationEventCleanupScheduable());
		Test.stopTest();
	}
}