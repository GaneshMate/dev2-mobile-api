@istest
private class Contractrgtest{
    private static testmethod void testmethod1(){
        Account acc = new Account();
            acc.firstname='test acc1';
            acc.lastName = 'test acc';
            acc.billingCity ='Test City';
            acc.billingStreet = 'TestStreet';
            acc.billingstate ='TestState';
            acc.billingPostalcode ='23451';
            acc.billingcountry ='United States';
            Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            acc.recordtypeID=objAccrec.id;
           acc.PersonMailingCity='Test City';
            acc.PersonMailingStreet = 'TestStreet';
            acc.PersonMailingState ='TestState';
            acc.PersonMailingPostalCode ='23451';
            acc.PersonMailingCountry ='United States';
            acc.Account_Currency__c='USD'; 
            //acc.ispersonAccount = true;
            insert acc;
            
            Opportunity opp = new Opportunity(Name=acc.Name + ' Opportunity ' ,
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=acc.Id);
        Contract objContract = new Contract();
             //objContract.SBQQ__Quote__c = quoteRec.id;
             objContract.SBQQ__Opportunity__c = opp.id;
             objcontract.Accountid=acc.id;
             insert objContract;
             update objcontract;
    }
    
    private static testmethod void subscriptionRatePlanIdTest(){
        Account acc = new Account();
            acc.firstname='test acc1';
            acc.lastName = 'test acc';
            acc.billingCity ='Test City';
            acc.billingStreet = 'TestStreet';
            acc.billingstate ='TestState';
            acc.billingPostalcode ='23451';
            acc.billingcountry ='United States';
            Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            acc.recordtypeID=objAccrec.id;
           acc.PersonMailingCity='Test City';
            acc.PersonMailingStreet = 'TestStreet';
            acc.PersonMailingState ='TestState';
            acc.PersonMailingPostalCode ='23451';
            acc.PersonMailingCountry ='United States';
            acc.Account_Currency__c='USD'; 
            //acc.ispersonAccount = true;
            insert acc;
            
            Opportunity opp = new Opportunity(Name=acc.Name + ' Opportunity ' ,
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=acc.Id);
        Contract objContract = new Contract();
             //objContract.SBQQ__Quote__c = quoteRec.id;
             objContract.SBQQ__Opportunity__c = opp.id;
             objcontract.Accountid=acc.id;
        	objcontract.ZSB__SubscribeToZuora__c = true;
             insert objContract;
             update objcontract;
        
        Product2 prod = new Product2(Name = 'VectorVest 7 EOD US Monthly', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_MONTHLY' , Family = 'Books');
            insert prod;
        
        
        SBQQ__Quote__c quoteRec = new SBQQ__Quote__c();
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SiteUser_Primary_Check__c = true;
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SBQQ__Opportunity2__c = opp.Id;
            quoteRec.ZSB__BillCycleDay__c =  '1';
            quoteRec.ZSB__BillingBatch__c = 'Batch1';
            quoterec.SBQQ__StartDate__C =Date.today();
            quoterec.SbQQ__Enddate__c =Date.today().Adddays(30);
            insert quoteRec ;
        
        SBQQ__QuoteLine__c ojQuoteline = new SBQQ__QuoteLine__c();
        ojQuoteline.SBQQ__Quote__c = quoteRec .id;
        ojQuoteline.SBQQ__Product__c = prod.id;
        ojQuoteline.SBQQ__StartDate__C =Date.today();
        ojQuoteline.SbQQ__Enddate__c =Date.today().Adddays(30);
        insert ojQuoteline;
        
        SBQQ__Subscription__c objSub = new SBQQ__Subscription__c();
             objsub.SBQQ__Contract__c = objcontract.id;
             objsub.SBQQ__Product__c =prod.id;
             objsub.SBQQ__QuoteLine__c = ojQuoteline.id;
             objsub.SBQQ__Account__c = acc.id;
             objsub.SBQQ__Quantity__c = -1;
             //objsub.SBQQ__StartDate__c = Date.today();
             insert objsub;
    }
    }