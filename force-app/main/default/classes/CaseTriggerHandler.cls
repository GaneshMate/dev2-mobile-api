/**
 * after case update
 * 1. if it is an optionpro suspend case, if case is closed, unsuspend
 * 2. if it is a new RT Pro case, if case is closed, unsuspend
 * 3. if it is a RT Pro upgrade case, if case is closed, upgrade to RT pro
 */
public without sharing class CaseTriggerHandler {
	public static void beforeInsert(List<Case> caselist) {
		Map<Id,Id> accountcontactmap = new Map<Id,Id>();
		for (Case c : caselist) {
			if (c.AccountId != null) accountcontactmap.put(c.AccountId, null);
		}
		for (Contact c : [SELECT AccountId FROM Contact WHERE AccountId IN :accountcontactmap.keySet()]) {
			accountcontactmap.put(c.AccountId, c.Id);
		}
		for (Case c : caselist) {
			if (c.AccountId != null) c.ContactId = accountcontactmap.get(c.AccountId);
		}
	}

	public static void afterUpdate(List<Case> caselist, Map<Id,Case> oldmap) {
		Set<String> optionsproset = new Set<String>();	// options pro sub ids that need to be resumed
		Set<Id> newaccounts = new Set<Id>();	// newly created RT Pro accounts
		List<Case> upgradecaselist = new List<Case>(); // cases that points to RT Pro upgrade

		List<Account> eliteupdatelist = new List<Account>();	// case that switch elite and send out elite packages

		Map<Id,Case> newsub_acc_case_map = new Map<Id,Case>();	// new subscription cases for remote event sign ups

		for (Case c : caselist) {
			// case closed
			if (c.Status == 'Closed' && oldmap.get(c.Id).Status != 'Closed') {
				// options pro suspend case
				if (c.Type == vvConstants.CASE_OPTIONSPRO && c.OptionsPro_Sub__c != null) {
					optionsproset.add(c.OptionsPro_Sub__c);
				} else if (c.Type == vvConstants.CASE_ELITE_PACKAGE) {
					eliteupdatelist.add(new Account(Id=c.AccountId,Elite_Card_Printed__c=true,Elite_Cert_Printed__c=true,Elite_Ship_Label_Printed__c=true,Elite_Ship_Letter_Printed__c=true));
				} else if (c.Type == vvConstants.CASE_FRAUD_ALERT) {
					newaccounts.add(c.AccountId);
				} else if (c.Type == vvConstants.CASE_CREATE_SUB) {
					newsub_acc_case_map.put(c.AccountId,c);
				}
			}
		}

		// resume options pro
		if (!optionsproset.isEmpty()) {
			for (Zuora__Subscription__c zs : [SELECT Name,Zuora__Zuora_Id__c, Zuora__TermStartDate__c, Zuora__TermSettingType__c FROM Zuora__Subscription__c WHERE Zuora__Zuora_Id__c IN :optionsproset AND Zuora__Status__c = 'Suspended']) {
	        	zuoraAPIHelper.resumeSubscriptionFuture(zs.Zuora__Zuora_Id__c, zs.Zuora__TermStartDate__c >= Date.today() ? zs.Zuora__TermStartDate__c.addDays(1) : Date.today(), zs.Zuora__TermSettingType__c == 'EVERGREEN' ? false : true);
	        }
		}

		// resume new RT Pro or Fraudulent accounts
		if (!newaccounts.isEmpty()) {
			for (Zuora__Subscription__c zs : [SELECT Name,Zuora__Zuora_Id__c,Zuora__TermStartDate__c, Zuora__TermSettingType__c FROM Zuora__Subscription__c WHERE Zuora__Account__c IN :newaccounts AND Zuora__Status__c = 'Suspended' ORDER BY CreatedDate DESC]) {
	            zuoraAPIHelper.resumeSubscriptionFuture(zs.Zuora__Zuora_Id__c, zs.Zuora__TermStartDate__c >= Date.today() ? zs.Zuora__TermStartDate__c.addDays(1) : Date.today(), zs.Zuora__TermSettingType__c == 'EVERGREEN' ? false : true);
	        }
	        List<Account> accs_to_update = new List<Account>();
	        for (Id aid : newaccounts) {
	        	accs_to_update.add(New Account(Id=aid, Subscription_Status__c='Active', Subscription_Suspend_End_Date__c = Date.today()));
	        }
	        update accs_to_update;
		}

		// remove event sign ups, now create subscriptions for them
		if (!newsub_acc_case_map.isEmpty()) {
			for (Zuora__CustomerAccount__c zca : [SELECT Zuora__Account__c, Zuora__Zuora_Id__c, Zuora__Account__r.Campaign__c FROM Zuora__CustomerAccount__c WHERE Zuora__Account__c IN :newsub_acc_case_map.keySet()]) {
				Map<String, String> customfields = new Map<String, String>();
				if (zca.Zuora__Account__r.Campaign__c != null) customfields.put('CampaignId__c',zca.Zuora__Account__r.Campaign__c);
				vvSubscriptionManagement.createSubscription(new Account(Id = zca.Zuora__Account__c), zca.Zuora__Zuora_Id__c, newsub_acc_case_map.get(zca.Zuora__Account__c).Coupon_Code__c, newsub_acc_case_map.get(zca.Zuora__Account__c).Referred_By__c, true, customfields);
			} 
		}

		// upgrade to RT Pro
		/*
		if (!upgradecaselist.isEmpty()) {
			for (Case c : upgradecaselist) {
				vvSubscriptionManagement.upgradeSubscriptionFuture(c.AccountId, c.RT_Pro_Rate_Plan__c, c.RT_Pro_Trial_First__c, c.RT_Pro_Trial_Amount__c);
			}
		}
		*/

		// update account for elite case closed
		if (!eliteupdatelist.isEmpty()) update eliteupdatelist;

	}
}