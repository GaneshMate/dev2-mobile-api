/**
 * payment trigger handler does payment rollup to account's Total Amount Spent
 */
public without sharing class ZuoraPaymentTriggerHandler {
	public static void rollupTotalAmount(List<Zuora__Payment__c> zplist) {
		Set<Id> accids = new Set<Id>();
		for (Zuora__Payment__c zp : zplist) {
			accids.add(zp.Zuora__Account__c);
		}

		// now find all payments within account and recalculate
		List<Account> account_update_list = [SELECT Account_Currency__c, Total_Amount_From_Zuora__c, (SELECT Zuora__Amount__c, Zuora__RefundedAmount__c FROM Zuora__Payments__r WHERE Zuora__Status__c = 'Posted' OR Zuora__Status__c = 'Processed') FROM Account WHERE Id IN :accids];
		for (Account a : account_update_list) {
			Double amountpaid = 0.0;
			for (Zuora__Payment__c zp : a.Zuora__Payments__r) {
				// we only track total amount by USD, so we need to convert any currency to USD and then roll up
				amountpaid += vvCurrencyManagement.convertCurrency(zp.Zuora__Amount__c - zp.Zuora__RefundedAmount__c, a.Account_Currency__c, 'USD');
			}
			a.Total_Amount_From_Zuora__c = amountpaid;
		}

		if (!account_update_list.isEmpty()) update account_update_list;
				
	}
}