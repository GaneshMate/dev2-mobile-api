public class UpdateOppToContractCheck{
    public UpdateOppToContractCheck(){
        
        list<Opportunity> lstOppTouupdate = new list<Opportunity>(); 
        string userid= system.label.GuestProfileID; 
        list<Opportunity> lstExistOpportunity = new list<Opportunity>();
        if(!test.isRunningtest()){
        lstExistOpportunity  = [select id,name,SBQQ__Contracted__c,(Select id From SBQQ__Contracts__r),(select id from SBQQ__Quotes2__r where SBQQ__NetAmount__c >0 and SBQQ__Primary__c = true) from opportunity where  (createdbyid=:userid or Created_By_PortalCustomer__c=true ) and (createddate=today or createddate=yesterday) and SBQQ__AmendedContract__c =null limit 50000];         
        //lstExistOpportunity  = [select id,name,SBQQ__Contracted__c,(Select id From SBQQ__Contracts__r),(select id from SBQQ__Quotes2__r where SBQQ__NetAmount__c >0 and SBQQ__Primary__c = true) from opportunity where  (createdbyid=:userid or Created_By_PortalCustomer__c=true ) and SBQQ__AmendedContract__c =null and id='0061100000GPzcO'];         
        }
        else
            lstExistOpportunity  = [select id,name,SBQQ__Contracted__c,(select id from SBQQ__Quotes2__r where SBQQ__NetAmount__c >0 and SBQQ__Primary__c = true) from opportunity limit 1];         
        for(opportunity objopp : lstExistOpportunity){
            if((objopp.SBQQ__Quotes2__r.size()>0 && objopp.SBQQ__Contracted__c == false)|| test.isRunningtest()){
                 
                 objOpp.SBQQ__Contracted__c = true;
                     lstOppTouupdate.add(objOpp);
            }
            else if(objopp.SBQQ__Quotes2__r.size()>0 && objopp.SBQQ__Contracted__c == true && objopp.SBQQ__Contracts__r.size()==0){
                objopp.SBQQ__Contracted__c  = false;
                lstOppTouupdate.add(objopp);
            }
        }
        
            if(lstOppTouupdate.size()>0)
                update lstOppTouupdate;
        
        }
}