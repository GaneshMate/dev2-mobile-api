public class forgotusername{
public string useremail{get;set;}
public forgotusername(){

}
public pagereference submitemail(){
    if(useremail == null || useremail ==''){
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please Enter Email');
            ApexPages.addMessage(myMsg);
            return null;
    }
    else{
        list<User> lstUser = new list<User>();
        lstuser =[select id, username,Firstname,Lastname,email from user where email=:useremail and AccountId!= null and isActive=true];
        if(lstuser.size()==0){
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please provide valid Email address');
            ApexPages.addMessage(myMsg);
            return null;
        }
        else{
        //send email
            //Emailtemplate tempID=[select id,body, subject from EmailTemplate where name='ForgotUserName'];
            Emailtemplate tempID=[select id,body,HTMLValue, subject from EmailTemplate where name='ForgotUserNameCustom'];
            

            String emailBody = tempID.HTMLValue;
            
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] {useremail};
            
            message.optOutPolicy = 'FILTER';
            message.subject = tempID.subject;
            string StrUsername;
            if(lstuser[0].username.contains('@vectorvest-salesforce.com'))
            StrUsername = lstuser[0].username.split('@')[0];
            else
             StrUsername = lstuser[0].username;
             emailBody=emailBody.replace('strUsername',StrUsername);
             emailBody=emailBody.replace('Firstname',lstuser[0].Firstname);
             emailBody=emailBody.replace('Lastname',lstuser[0].Lastname);
            //message.plainTextBody = emailBody;
            message.setHTMLBody(emailBody);
            Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            
            if (results[0].success) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.info,'Email Sent Successfully');
            ApexPages.addMessage(myMsg);
            return null;
            } else {
                 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.info,'Email Sending Failed Please Try again');
                ApexPages.addMessage(myMsg);
                return null;
            }
        
        }
    }
   

}
}