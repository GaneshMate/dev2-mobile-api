global class ScheduledDispatcher implements Schedulable {
    
    private Type targetType;
    
    public ScheduledDispatcher(Type targetType) {
        System.debug('Creating new dispatcher for class: ' + targetType.getName());
        this.targetType = targetType;
    }

    global void execute(SchedulableContext sc) {
        if(targetType!=null)
        {
            ((IScheduleDispatched)targetType.newInstance()).execute(sc);
        }
    }
    
    public interface IScheduleDispatched {
        void execute(SchedulableContext sc);
    }
}