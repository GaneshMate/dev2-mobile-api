global class SubscriptionResumeScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		Database.executeBatch(new SubscriptionResumeBatch(),1);
	}

	/*
	runs daily at 23:30
	
	System.schedule('Subscription Resume Batch','0 30 23 * * ?', new SubscriptionResumeScheduler());
	 */
}