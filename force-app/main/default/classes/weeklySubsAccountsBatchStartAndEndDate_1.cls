global class weeklySubsAccountsBatchStartAndEndDate_1 implements Database.Batchable<sObject>, Database.Stateful {
    global static final List<String> weekDays = new List<String>{'Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'};
    global final Date d;
    global final Date dn;
    global final Datetime dt;
    global final Datetime dtn;
    global final String weekDay;
    global final Date weekStart;
    global final Date weekEnd;
    Date endDateVal;
    Date startDateval;
    global final Map <String, SF_Product_Mapping__c> productNames;
    global static final Set<String> historyFields = new Set<String>{'BillingStreet','BillingCity','BillingState','BillingPostalCode','BillingCountry','PersonHasOptedOutOfEmail',
        'HasOptedOutOfMassEmail__c','HasOptedOutOfMassPostMail__c','PersonDoNotCall','Type'};
    global weeklySubsAccountsBatchStartAndEndDate_1(Date startDate, Date endDate){
        
        //d = dd;
        //dn = d.addDays(1);
        productNames = NEW Map <String, SF_Product_Mapping__c> ();
        for (SF_Product_Mapping__c p : [SELECT Product_Name__c, IsTrial__c, IsRoboTrader__c, IsRealTime__c, IsMarketAccess__c, IsDesktop__c, IsDerby__c, IsMobile__c FROM SF_Product_Mapping__c]) {
            productNames.put (p.Product_Name__c, p);   
        }
        endDateVal = endDate;
        startDateval = startDate;
        dt = DateTime.newInstance(startDate, Time.newInstance(0, 0, 0, 0));
        d = dt.date();
        dtn = dt.addDays(1);
        dn = d.addDays(1);
        weekDay = dt.format('EEEE');
        weekStart = d.addDays(-weekDays.indexOf(dt.format('EEEE')));
        weekEnd = weekStart.addDays(6);
        
    }
    private static Test_Weekly_Customers__c createWC(Date d, Date weekStart, Date weekEnd, String weekDay, Test_Weekly_Customers__c wc, 
                                                     Account a, List<AccountHistory> aHs, SBQQ__Subscription__c sb, Map<String, AccountHistory> fidWithItsHists, List <SBQQ__Subscription__c> childSubs, Map <String, SF_Product_Mapping__c> productName){
                                                             Date startDate = sb.Actual_Start_Date__c .addDays(-1);
                                                             system.debug ('productNameproductNameproductName'+productName);
                                                             
                                                             if (productName.containsKey (sb.SBQQ__ProductName__c)) {
                                                                 if (productName.get (sb.SBQQ__ProductName__c).IsTrial__c) {
                                                                     for (SBQQ__Subscription__c s : childSubs) {
                                                                         if (productName.containsKey (s.SBQQ__ProductName__c)) {
                                                                             if (productName.get (s.SBQQ__ProductName__c).IsMarketAccess__c) {
                                                                                 if ((s.SBQQ__TerminatedDate__c == null || s.SBQQ__TerminatedDate__c > startDate) && ((s.SBQQ__Contract__r.ZSB__TermType__c == 'Evergreen' && s.Actual_Start_Date__c <= startDate) || (s.SBQQ__Contract__r.ZSB__TermType__c != 'Evergreen' && s.Actual_Start_Date__c <= startDate && s.SBQQ__EndDate__c >= startDate)) && s.SBQQ__Quantity__c > 0 && s.isdeleted != true) {
                                                                             
                                                                                     if (productName.get (s.SBQQ__ProductName__c).IsDesktop__c) {
                                                                                         if (productName.get (s.SBQQ__ProductName__c).IsTrial__c) {
                                                                                             wc.hasDesktopTrial__c = true;   
                                                                                         } else {
                                                                                             wc.hasDesktopSub__c = true;
                                                                                         }
                                                                                     } else if (productName.get (s.SBQQ__ProductName__c).IsMobile__c) {
                                                                                         if (productName.get (s.SBQQ__ProductName__c).IsTrial__c) {
                                                                                             wc.hasMobileTrial__c = true;
                                                                                         } else {
                                                                                             wc.hasMobileSub__c = true;
                                                                                         }
                                                                                     }
                                                                                 }
                                                                             }
                                                                         }
                                                                     }
                                                                 }
                                                             }
                                                             wc.Week_Start_Date__c = weekStart;
                                                             wc.Week_End_Date__c = weekEnd;
                                                             wc.Account__c = a.Id;
                                                             wc.Subscription__c = sb.Id;
                                                             wc.put(weekDay+'__c',d);
                                                            if (sb.Enterprise_Subscription_ID__c != NULL && !sb.Enterprise_Subscription_ID__c.contains('FUTURESUB') && !sb.Enterprise_Subscription_ID__c.contains('CONVERTTRIAL'))
                                                               wc.Sub_Id__c = sb.Enterprise_Subscription_ID__c;
                                                            else
                                                                wc.Sub_Id__c = sb.Name;
                                                             wc.Customer_Type__c = a.Type;
                                                             wc.Account_First_Name__c = a.FirstName;
                                                             wc.Account_Last_Name__c = a.LastName;
                                                             wc.EnterpriseCustomerID__c = a.EnterpriseCustomerID__c;
                                                             if (a.Vector_Vest_Customer_ID__c != NULL)
                                                                wc.Vector_Vest_Customer_ID__c = a.Vector_Vest_Customer_ID__c.replace('@vectorvest-salesforce.com','');
                                                             wc.BillingStreet__c = a.BillingStreet;
                                                             wc.BillingCity__c = a.BillingCity;
                                                             wc.BillingState__c = a.BillingState;
                                                             wc.BillingPostalCode__c = a.BillingPostalCode;
                                                             wc.BillingCountry__c = a.BillingCountry;
                                                             wc.PersonHasOptedOutOfEmail__c = a.PersonHasOptedOutOfEmail;
                                                             wc.HasOptedOutOfMassEmail__c = a.HasOptedOutOfMassEmail__c;
                                                             wc.HasOptedOutOfMassPostMail__c = a.HasOptedOutOfMassPostMail__c;
                                                             wc.PersonDoNotCall__c = a.PersonDoNotCall;
                                                             if(!Test.isRunningTest()) {
                                                                wc.Product_Name__c = sb.SBQQ__ProductName__c;
                                                                wc.Source__c = sb.SecondaryCampaignSource__c;
                                                             }

                                                         if(!aHs.isEmpty()){
                                                             for(AccountHistory ah: aHs){
                                                                 if(ah.Field == 'Type')
                                                                     wc.put('Customer_Type__c',ah.oldValue);
                                                                 else if(ah.Field.endsWith('__c'))
                                                                     wc.put(ah.Field,ah.oldValue);
                                                                 else
                                                                     wc.put(ah.Field+'__c',ah.oldValue);
                                                             }
                                                         }
                                                         for(String ahf:fidWithItsHists.keyset()){
                                                             AccountHistory ah = fidWithItsHists.get(ahf);
                                                             if(ah.Field == 'Type')
                                                                 wc.put('Customer_Type__c',ah.newValue);
                                                             else if(ah.Field.endsWith('__c'))
                                                                 wc.put(ah.Field,ah.newValue);
                                                             else
                                                                 wc.put(ah.Field+'__c',ah.newValue);
                                                         }
                                                         return wc;
                                                     } 
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String Query = 'SELECT CreatedDate, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, EnterpriseCustomerID__c, ';
        Query+='FirstName, LastName, MiddleName,';
        Query+='Type, Id, HasOptedOutOfMassEmail__c, PersonHasOptedOutOfEmail, HasOptedOutOfMassPostMail__c,';
        Query+='PersonDoNotCall, Name, Vector_Vest_Customer_Id__c, YahooId__c, ';
        Query+='(SELECT AccountId, Field, OldValue, NewValue, Id, CreatedDate FROM Histories WHERE CreatedDate >= :dtn AND Field IN :historyFields ';
        Query+='ORDER BY CreatedDate DESC), ';
        Query+='(SELECT ID, Name, Enterprise_Subscription_ID__c, SBQQ__ProductName__c, SecondaryCampaignSource__c, Actual_Start_Date__c FROM SBQQ__Subscriptions__r WHERE (SBQQ__TerminatedDate__c = null or SBQQ__TerminatedDate__c > :d) AND';
        Query+='((SBQQ__Contract__r.ZSB__TermType__c = \'Evergreen\' AND Actual_Start_Date__c <= :d) ';
        Query+='OR (SBQQ__Contract__r.ZSB__TermType__c != \'Evergreen\' AND Actual_Start_Date__c <= :d AND SBQQ__EndDate__c >= :d)) AND SBQQ__Quantity__c > 0 AND isdeleted != true) FROM Account ';
        Query+='WHERE Id IN (SELECT SBQQ__Account__c FROM SBQQ__Subscription__c WHERE (SBQQ__TerminatedDate__c = null or SBQQ__TerminatedDate__c > :d) AND';
        Query+='((SBQQ__Contract__r.ZSB__TermType__c = \'Evergreen\' AND Actual_Start_Date__c <= :d)';
        Query+='OR (SBQQ__Contract__r.ZSB__TermType__c != \'Evergreen\' AND Actual_Start_Date__c <= :d AND SBQQ__EndDate__c >= :d)) AND SBQQ__Quantity__c > 0 AND isdeleted != true)';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Account> accs){
        Map<Id, SBQQ__Subscription__c> subcsWithWeekInfo = new Map<Id, SBQQ__Subscription__c>(
            [SELECT ID, Name, SBQQ__Account__c , Actual_Start_Date__c, SBQQ__EndDate__c, SBQQ__TerminatedDate__c, 
             SecondaryCampaignSource__c, SBQQ__ProductId__c , SBQQ__ProductName__c, Enterprise_Subscription_ID__c,
             SBQQ__QuoteLine__r.SBQQ__Quote__r.SecondaryCampaignsource__r.Primarysources__r.PrimarysourceID__c, SBQQ__QuoteLine__r.SBQQ__Quote__r.SecondaryCampaignsource__r.Primarysources__r.Name,
             (SELECT Id, Name, Saturday__c, Sunday__c, Monday__c, Tuesday__c, Wednesday__c, Thursday__c, Friday__c, 
              Customer_ID__c, Year_Week__c, Product_Name__c, Source__c, Customer_Type__c, BillingStreet__c, BillingCity__c, 
              BillingState__c, BillingPostalCode__c, BillingCountry__c, 
              Week_Start_Date__c, Week_End_Date__c, Account__c, Subscription__c  
              FROM Test_Weekly_Customers__r WHERE Week_Start_Date__c = :weekStart AND Week_End_Date__c = :weekEnd //AND Do_not_Update__c = false 
              ORDER By CreatedDate)
             FROM SBQQ__Subscription__c WHERE SBQQ__Account__c IN :accs AND (SBQQ__TerminatedDate__c = null or SBQQ__TerminatedDate__c > :d) AND // Terminated date should not be populated
             ((SBQQ__Contract__r.ZSB__TermType__c = 'Evergreen' AND Actual_Start_Date__c <= :d) // if Contract is EverGreen check Start Date it should be less that today
              OR (SBQQ__Contract__r.ZSB__TermType__c != 'Evergreen' AND Actual_Start_Date__c <= :d AND SBQQ__EndDate__c >= :d)) AND SBQQ__Quantity__c > 0 AND isdeleted != true]
        );
        //historyFields
        Map<Id, Account> accsWithTodaysHists = new Map<Id, Account>([Select Id, 
                                                                 (SELECT AccountId, Field, OldValue, NewValue, Id, CreatedDate FROM Histories 
                                                                  WHERE CreatedDate >= :dt AND CreatedDate < :dtn AND Field IN :historyFields ORDER BY CreatedDate DESC) 
                                                                FROM Account Where Id IN :accs]);
            
        List<Test_Weekly_Customers__c> wsLst = new List<Test_Weekly_Customers__c>();
        /*Set <Id> accIds = NEW Set <Id> ();
        for (Account a : accs) {
            accIds.add (a.Id);
        }*/
        Map <Id, Account> accIds = NEW Map <Id, Account> (accs);
        Map <Id, Account> acctsMap = NEW Map <Id, Account> ([SELECT Id, (SELECT Id, SBQQ__Account__c, Actual_Start_Date__c, SBQQ__EndDate__c, SBQQ__ProductName__c, SBQQ__TerminatedDate__c, SBQQ__Contract__r.ZSB__TermType__c, SBQQ__Quantity__c, isdeleted FROM SBQQ__Subscriptions__r WHERE isdeleted = false) FROM Account WHERE Id IN : accIds.keySet()]);
        
        
        /*Map <Id, List <SBQQ__Subscription__c>> subsWithAccId = NEW Map <Id, List <SBQQ__Subscription__c>> ();
        for (SBQQ__Subscription__c subs : [SELECT Id, SBQQ__Account__c, Actual_Start_Date__c, SBQQ__EndDate__c, SBQQ__ProductName__c, SBQQ__TerminatedDate__c, SBQQ__Contract__r.ZSB__TermType__c, SBQQ__Quantity__c, isdeleted FROM SBQQ__Subscription__c WHERE SBQQ__Account__c IN : accIds]) {
            if (subsWithAccId.containsKey(subs.SBQQ__Account__c))
                subsWithAccId.get(subs.SBQQ__Account__c).add (subs);
            else 
                subsWithAccId.put (subs.SBQQ__Account__c, NEW List <SBQQ__Subscription__c> {subs});
                
        }*/
        system.debug ('productNamesproductNamesproductNames'+productNames);
        for(Account a:accs){
            List<AccountHistory> todaysHists = accsWithTodaysHists.get(a.Id).Histories;
            Map<String, AccountHistory> fidWithItsHists = new Map<String, AccountHistory>();
            for(AccountHistory ah:todaysHists){
                if(!fidWithItsHists.containsKey(ah.Field))
                    fidWithItsHists.put(ah.Field, ah);
            }
            for(SBQQ__Subscription__c sb:a.SBQQ__Subscriptions__r){
                List <Test_Weekly_Customers__c> weekRecs = NEW List <Test_Weekly_Customers__c> ();
                    //system.debug('Subscriptions ....'+subcsWithWeekInfo.get(sb.Id));
                    //system.debug ('With test weekly customers'+subcsWithWeekInfo.get(sb.Id).Test_Weekly_Customers__r);
                    if (subcsWithWeekInfo.get(sb.Id) != NULL) { 
                        if (subcsWithWeekInfo.get(sb.Id).Test_Weekly_Customers__r != NULL) { 
                            weekRecs.addAll(subcsWithWeekInfo.get(sb.Id).Test_Weekly_Customers__r);
                        }
                    }
                if(weekRecs.isEmpty()){ // If Week rec exists for a subscription
                    //if(a.Histories.isEmpty())
                    //  wsLst.add(createWC(d, weekStart, weekEnd, weekDay, new Test_Weekly_Customers__c(), a, a.Histories, sb));
                    //else
                    wsLst.add(createWC(d, weekStart, weekEnd, weekDay, new Test_Weekly_Customers__c(), a, a.Histories, sb, fidWithItsHists, acctsMap.get(a.Id).SBQQ__Subscriptions__r, productNames));
                }else{
                    Test_Weekly_Customers__c wc = weekRecs[weekRecs.size()-1];
                    
                    if(!todaysHists.isEmpty())
                        wsLst.add(createWC(d, weekStart, weekEnd, weekDay, new Test_Weekly_Customers__c(), a, a.Histories, sb, fidWithItsHists, acctsMap.get(a.Id).SBQQ__Subscriptions__r, productNames));
                    else
                        wsLst.add(createWC(d, weekStart, weekEnd, weekDay, wc, a, a.Histories, sb, fidWithItsHists, acctsMap.get(a.Id).SBQQ__Subscriptions__r, productNames));
                    
                }
            }
            //system.debug(a.Histories);
        }
        upsert wsLst;
    }
    
    global void finish(Database.BatchableContext BC){
        if (startDateVal < endDateVal) {
            Database.executeBatch (new weeklySubsAccountsBatchStartAndEndDate_1 (startDateVal.addDays (1), endDateVal));
        }    
    }
}