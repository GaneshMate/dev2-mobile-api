global class BatchDeleteUsageHistoryScheduler implements Schedulable{
    global void execute(SchedulableContext sc){
        Integer batchDeleteUsageHistoryDays = String.isBlank(Label.Batch_Delete_Usage_History_Days) ? 21 : Integer.valueOf(Label.Batch_Delete_Usage_History_Days) ;
        String query = 'SELECT id FROM Usage_History__c where Login_Date_time__C < LAST_N_Days:'+batchDeleteUsageHistoryDays;
        BatchDeleteUsageHistory delBatch = new BatchDeleteUsageHistory(query);
        Id BatchProcessId = Database.ExecuteBatch(delBatch);
    }
}