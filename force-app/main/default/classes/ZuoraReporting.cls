@RestResource(URLMapping='/getReport/*')
global class ZuoraReporting {
    
    private static string ReportRunId    = 'ReportRunId';  
    private static string offset  = 'offset';  
    private static string rows  = 'rows';  
    
    
    global class Report {
        public List<Reports> reports;
        //global String Id; 
        //global String CreditCardType;  
    }
    global class Reports {
        public String method;
        public RichInput richInput;
    }
    global class RichInput {
        public String Id;
        public String CreditCardType;
    }
    global class BatchRequestResult {
        boolean hasErrors;
        List<BatchResult> results;
    }
    
    global class BatchResult {
        public integer statusCode;
        public string result;
        
        public BatchResult(integer status,string result) {
            this.statusCode = status;
            this.result = result;
        }
    }
    @HttpGet
    global static BatchRequestResult getReport() {
        integer retCode = 0;
        string retBody = '';
        
        RestResponse res=RestContext.response;
        
        
        RestRequest req = RestContext.request;
        string ReportRunId = req.params.get(ReportRunId); 
        string offset = req.params.get(offset);  
        string rows = req.params.get(rows);  
        
        
        
        BatchRequestResult result = new BatchRequestResult();
        //result.hasErrors = false;
        result.results = new List<BatchResult>();
        
        List<Reports> olisToUpdate = new List<Reports>();
        
        
        
        if(!String.isBlank(String.valueOf(ReportRunId)) &&!String.isBlank(offset) &&!String.isBlank(rows)){
            String getReportResult = getReport(ReportRunId,offset,rows);    
            System.debug('****getReport getReportResult****'+getReportResult);
            if(getReportResult.contains('Error')){
                result.results.add(new BatchResult(500,getReportResult));
            }else{
                retCode = 200; 
                result.results.add(new BatchResult(retCode,getReportResult));
            }
        }else{
            retCode = 500;
            result.results.add(new BatchResult(retCode,'Error: ReportRunId,offset,pageSize is Mandatory!'));
            System.debug('****retCode****'+retCode);
            
        }  
        
        res.headers.put('Content-type', 'application/json');
        res.statusCode = retCode;
        res.responseBody = Blob.valueOf(retBody);
        System.debug('****retCode****'+retCode);
        return result;
    }
    public static String getReport(String ReportRunId,String offset,String rows) {
        
        Zuora.zApi zuoraApi = new Zuora.zApi();
        Zuora.zApi.LoginResult login_result = zuoraApi.zlogin(); 
        String endPointURL = 'https://zconnectsandbox.zuora.com/api/rest/v1/reportruns/reportdata/'+ReportRunId+'?offset='+offset+'&rows='+rows+'';
        //String endPointURL = 'https://zconnectsandbox.zuora.com/api/rest/v1/reportruns/reportdata/ff80808167bdfeff0167cb9839777a03?offset=1&pageSize=250';
        //String endPointURL = 'https://zconnectsandbox.zuora.com/api/rest/v1/reportruns/reportdata/ff80808167bdfeff0167cb9839777a03?offset=1&rows=250';
        
        System.debug('endPointURL: =--------------==='+endPointURL);
        System.debug('Session------------='+login_result.Session);
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPointURL);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json;charset=UTF-8');
        req.setHeader('Accept', 'application/json, text/plain, */*');
        
        
        HttpResponse resp = zuoraApi.sendRequest(req);
       // List<Zuora.zObject> resp= zuoraApi.zquery(endPointURL);
        System.debug('resp-------------:'+resp);
        /*if(resp.getStatusCode() == 200) {
            Map < String, Object > productData =
                (Map < String, Object > ) JSON.deserializeUntyped( resp.getBody() );
            System.debug('productData----------------:'+productData);
        }*/
        return String.valueOf(resp);
    }
}