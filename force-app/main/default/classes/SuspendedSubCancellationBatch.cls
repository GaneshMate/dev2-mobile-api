/**
 * batch class looping through Upgrade_Suspended_Subscription__c object records
 * 1. find ones that have Cancelled__c = false
 * 2. find ones that have Conversion_Date__c = today
 * 3. resume the subscription and cancel immediately (zuora does not allow cancellation on suspended subs)
 */
global class SuspendedSubCancellationBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
	
	String query;
	String ussId;
	
	global SuspendedSubCancellationBatch() {
		query = 'SELECT Suspended_Subscription__r.Zuora__Zuora_Id__c, Suspended_Subscription__r.Zuora__CustomerAccount__c, Date_Suspended__c FROM Upgrade_Suspended_Subscription__c WHERE Cancelled__c = false AND Conversion_Date__c = TODAY';
	}

	/**
	 * this one is for testing
	 */
	global SuspendedSubCancellationBatch(Id ussId) {
		this.ussId = ussId;
		query = 'SELECT Suspended_Subscription__r.Zuora__Zuora_Id__c, Suspended_Subscription__r.Zuora__CustomerAccount__c, Date_Suspended__c FROM Upgrade_Suspended_Subscription__c WHERE Id = :ussId';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Upgrade_Suspended_Subscription__c> usslist) {
		for (Upgrade_Suspended_Subscription__c uss : usslist) {
			// resume
			String result = zuoraAPIHelper.resumeSubscription(uss.Suspended_Subscription__r.Zuora__Zuora_Id__c, (uss.Date_Suspended__c >= Date.today() ? uss.Date_Suspended__c.addDays(1) : Date.today()), false);
			// cancel
			zuoraAmendHelper.cancelSubscription(uss.Suspended_Subscription__r.Zuora__Zuora_Id__c, Date.today());

			// update
			uss.Cancelled__c = true;

			// ondemand sync
			zuoraAPIHelper.ondemandSync(uss.Suspended_Subscription__r.Zuora__CustomerAccount__c);
		}
		update usslist;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}