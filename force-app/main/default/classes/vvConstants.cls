public with sharing class vvConstants {
	
	// subscription status
	public static final String activeStatus = 'Active';
	public static final String inActiveStatus = 'Cancelled';

    // trial name
	public static final String VV_TRIAL_NAME = 'VectorVest Trial';

    // product names
    public static final String MAIN_PRODUCT_NAME = 'VectorVest';
	public static final String PLUGIN_PRODUCT_NAME = 'VectorVest Plug-Ins';

	// vv type
	public static final String TYPE_STANDARD = 'Standard';
	public static final String TYPE_EXPRESS = 'Express';
	public static final String TYPE_EXCHANGE = 'Exchange Fee';
	public static final String TYPE_SEMINAR = 'Seminar';
	public static final String TYPE_ADDITIONL = 'Additional';
	public static final String TYPE_TRIAL = 'Trial';

	// discount rate plan name
	public static final String DISCOUNT_FIXED = 'Fixed Discount';
	public static final String DISCOUNT_PERCENTAGE = 'Percentage Discount';

	// currency symbols
	public static final String USD = '$';
	public static final String EUR = '€';
	public static final String GBP = '£';

	// referral coupon code name
	public static final String REFERRAL_MONTHLY = 'REFERRAL_MONTHLY';
	public static final String REFERRAL_ANNUAL = 'REFERRAL_ANNUAL';
	public static final String REFERRAL_QUARTERLY = 'REFERRAL_QUARTERLY';

	// watchlist coupon code name
	public static final String WATCHLIST = 'WATCHLIST';

	// case types
	public static final String CASE_ELITE_PACKAGE = 'Mail Elite Package';
	public static final String CASE_FRAUD_ALERT = 'Fraud Alert';
	public static final String CASE_CREATE_SUB = 'Create New Subscription';
	public static final String CASE_OPTIONSPRO = 'OptionsPro account set-up';

	// case queue
	public static final String CASE_QUEUE_OPTIONSPRO = 'OptionsPro Upgrade';
	public static final String CASE_QUEUE_RTPRO = 'RT Pro';
	public static final String CASE_QUEUE_RTPROSIGNUP = 'RT Pro Sign up';
	public static final String CASE_QUEUE_ELITE = 'New Elite Customers';
	public static final String CASE_QUEUE_FRAUD = 'Fraud Alerts';
	public static final String CASE_START_TRIAL = 'CSR-Start Trials';
	public static final String CASE_BUNDLE_ISSUE = 'Bundle Issue';

	// profile name for the remote users
	public static final String PROFILE_REMOTE_EVENT = 'VV-Remote Event User';


	// product pre-wording on upgrade/downgrade lists
	public static final String PRE_MAIN = 'Primary Product:';
	public static final String PRE_ADDITIONAL = 'Additional Country:';
	public static final String PRE_PLUGIN = 'VV Plugins:';
		
	// robotrader monthly with derby
	public static final Decimal ROBO_WITH_DERBY_PRICE = 49;

	public static String convertFeedText(String abbr) {
		if (abbr == 'EOD') return 'End-of-Day';
		else if (abbr == 'ID') return 'Intraday';
		else if (abbr == 'RT') return 'RealTime';

		return 'End-Of-Day';
	}

}