// Developer : Sravan - Accelerize360
public class testSubAccounts {
    public static List<String> weekDays = new List<String>{'Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'};
        private static Test_Weekly_Customers__c createWC(Date d, Date weekStart, Date weekEnd, String weekDay, Test_Weekly_Customers__c wc, 
                                                         Account a, List<AccountHistory> aHs, SBQQ__Subscription__c sb){
            wc.Week_Start_Date__c = weekStart;
            wc.Week_End_Date__c = weekEnd;
            wc.Account__c = a.Id;
            wc.Subscription__c = sb.Id;
            wc.put(weekDay+'__c',d);
            //wc.put(weekDay+'_Subs__c',sb.Name);
            wc.Sub_Id__c = sb.Name;                                                
            wc.BillingStreet__c = a.BillingStreet;
            wc.BillingCity__c = a.BillingCity;
            wc.BillingState__c = a.BillingState;
            wc.BillingPostalCode__c = a.BillingPostalCode;
            wc.BillingCountry__c = a.BillingCountry;
            
            if(!aHs.isEmpty()){
                for(AccountHistory ah: aHs){
                    if(ah.Field.endsWith('__c'))
                        wc.put(ah.Field,ah.oldValue);
                    else
                        wc.put(ah.Field+'__c',ah.oldValue);
                }
            }
            return wc;
        } 
    
    
    public static void process (Date d){
        System.debug('d : ' +  d);
        Date dn = d.addDays(1);
        Datetime dt = DateTime.newInstance(d, Time.newInstance(0, 0, 0, 0));
        String weekDay = dt.format('EEEE');
        System.debug('Day:::  ' + weekDay);
        Date weekStart = d.addDays(-weekDays.indexOf(dt.format('EEEE')));
        System.debug('Start : ' +  weekStart);
        Date weekEnd = weekStart.addDays(6);
        System.debug('End : ' +  weekEnd);
        
        List<Account> accs = [SELECT CreatedDate, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, EnterpriseCustomerID__c, 
                              FirstName, LastName, MiddleName,
                              Type, Id, HasOptedOutOfMassEmail__c, PersonHasOptedOutOfEmail, HasOptedOutOfMassPostMail__c, 
                              PersonDoNotCall, Name, Vector_Vest_Customer_Id__c, YahooId__c, 
                              (SELECT AccountId, Field, OldValue, NewValue, Id, CreatedDate FROM Histories WHERE CreatedDate >= :dn AND Field != 'Created' 
                               ORDER BY CreatedDate DESC), 
                              (SELECT ID, Name FROM SBQQ__Subscriptions__r WHERE (SBQQ__TerminatedDate__c = null or SBQQ__TerminatedDate__c > :d) AND // Terminated date should not be populated
        ((SBQQ__Contract__r.ZSB__TermType__c = 'Evergreen' AND SBQQ__StartDate__c <= :d) // if Contract is EverGreen check Start Date it should be less that today
        OR (SBQQ__Contract__r.ZSB__TermType__c != 'Evergreen' AND SBQQ__StartDate__c <= :d AND SBQQ__EndDate__c >= :d))) FROM Account 
                              WHERE Id IN (SELECT SBQQ__Account__c FROM SBQQ__Subscription__c WHERE SBQQ__StartDate__c <= :d AND SBQQ__EndDate__c >= :d)];
        
        Map<Id, SBQQ__Subscription__c> subcsWithWeekInfo = new Map<Id, SBQQ__Subscription__c>(
            [SELECT ID, Name, SBQQ__Account__c , SBQQ__StartDate__c, SBQQ__EndDate__c, SBQQ__TerminatedDate__c, 
             SecondaryCampaignSource__c, SBQQ__ProductId__c , SBQQ__ProductName__c, Enterprise_Subscription_ID__c,
             (SELECT Id, Name, Saturday__c, Sunday__c, Monday__c, Tuesday__c, Wednesday__c, Thursday__c, Friday__c, 
              Customer_ID__c, Saturday_Subs__c, Sunday_Subs__c, Monday_Subs__c, Tuesday_Subs__c, Wednesday_Subs__c, 
              Thursday_Subs__c, Friday_Subs__c, Year_Week__c, Product__c, Source__c, Customer_Type__c, BillingStreet__c, BillingCity__c, 
              BillingState__c, BillingPostalCode__c, BillingCountry__c, 
               Week_Start_Date__c, Week_End_Date__c, Account__c, Subscription__c  
              FROM Test_Weekly_Customers__r WHERE Week_Start_Date__c = :weekStart AND Week_End_Date__c = :weekEnd 
              ORDER By CreatedDate)
             FROM SBQQ__Subscription__c WHERE SBQQ__Account__c IN :accs AND (SBQQ__TerminatedDate__c = null or SBQQ__TerminatedDate__c > :d) AND // Terminated date should not be populated
        ((SBQQ__Contract__r.ZSB__TermType__c = 'Evergreen' AND SBQQ__StartDate__c <= :d) // if Contract is EverGreen check Start Date it should be less that today
        OR (SBQQ__Contract__r.ZSB__TermType__c != 'Evergreen' AND SBQQ__StartDate__c <= :d AND SBQQ__EndDate__c >= :d))]
        );
        
        List<Test_Weekly_Customers__c> wsLst = new List<Test_Weekly_Customers__c>();
        
        for(Account a:accs){
            for(SBQQ__Subscription__c sb:a.SBQQ__Subscriptions__r){
                List<Test_Weekly_Customers__c> weekRecs = subcsWithWeekInfo.get(sb.Id).Test_Weekly_Customers__r;
                if(weekRecs.isEmpty()){ // If Week rec exists for a subscription
                    //if(a.Histories.isEmpty())
                    //	wsLst.add(createWC(d, weekStart, weekEnd, weekDay, new Test_Weekly_Customers__c(), a, a.Histories, sb));
                    //else
                        wsLst.add(createWC(d, weekStart, weekEnd, weekDay, new Test_Weekly_Customers__c(), a, a.Histories, sb));
                }else{
                    Test_Weekly_Customers__c wc = weekRecs[weekRecs.size()-1];
                    if(a.Histories[a.Histories.size()-1].CreatedDate == d)
                         wsLst.add(createWC(d, weekStart, weekEnd, weekDay, new Test_Weekly_Customers__c(), a, a.Histories, sb));
                    else
                         wsLst.add(createWC(d, weekStart, weekEnd, weekDay, wc, a, a.Histories, sb));

                }
            }
            system.debug(a.Histories);
        }
        upsert wsLst;
    }
}