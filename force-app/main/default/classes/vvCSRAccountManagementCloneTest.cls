@isTest(seeAllData=true)
private class vvCSRAccountManagementCloneTest {
    @isTest static void testController() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();

        Test.startTest();

        
        // test brand new account init
        vvCSRAccountManagementControllerClone ctrl = new vvCSRAccountManagementControllerClone();
        ctrl.salutation = 'Mr.';
        ctrl.lastname = 'testlast';
        ctrl.firstname = 'testfirst';
        ctrl.a.Phone = '2223334444';
        ctrl.a.PersonEmail = 'test2123412@test.com';
       
        ctrl.tempSaveBool();
        ctrl.a.BillingStreet = '112 test';
        ctrl.a.BillingCity = 'Atlanta';
        ctrl.a.BillingPostalCode = '22933';
        ctrl.a.BillingState = 'Georgia';
        ctrl.a.BillingCountry = 'USA';

        ctrl.confirmEmail = 'sadfasdf'; // test confirm email error
        ctrl.tempSaveBool();
        ctrl.confirmEmail = ctrl.a.PersonEmail;

        Account a1 = [SELECT PersonEmail FROM Account WHERE PersonEmail <> null LIMIT 1];
        ctrl.referral_code = 'asfasdfasfdwetqw';

        ctrl.payment_method.Payment_Type__c = 'Credit Card';
        ctrl.payment_method.Credit_Card_Number__c = '2222333344445555';
        ctrl.payment_method.Credit_Card_Holder_Name__c = 'test test';
        ctrl.payment_method.Credit_Card_Type__c = 'Visa';
        ctrl.payment_method.Credit_Card_Expiration_Month__c = '03';
        ctrl.payment_method.Credit_Card_Expiration_Year__c = '2016';
        ctrl.payment_method.Credit_Card_CVV_Code__c = '222';
        ctrl.payment_method.Enc_Value__c = '12341324125135231512';
        ctrl.tempSaveBool();

        ctrl.referral_code = a1.PersonEmail;
        ctrl.tempSaveBool();

        ctrl.selectedMarket = '';
        ctrl.sameasbilling=true;
        ctrl.isCalledFrom ='Section1';
        ctrl.saveAndSubmit();
        ctrl.selectedMarket = 'US';

        ctrl.selectedProduct = '';
        ctrl.saveAndSubmit();
        ctrl.selectedProduct = 'EOD';

        ctrl.saveAndSubmit();
    //Test.stopTest();
    //Test.StartTest();
        // test account update
        Account a = [SELECT Id FROM Account WHERE PersonContactId = :userObj.ContactId];
        ApexPages.currentPage().getParameters().put('id',a.Id);
        ctrl = new vvCSRAccountManagementControllerClone();
        ctrl.sameAsBilling = true;

        ctrl.tempSave();

        ctrl.coupon_code = '123456';
        ctrl.fetchCoupon();
        ctrl.coupon_code = '12345';
        ctrl.fetchCoupon();

        ctrl.payment_method.Payment_Type__c = 'Credit Card';
        ctrl.payment_method.Credit_Card_Number__c = '2222333344445555';
        ctrl.payment_method.Credit_Card_Holder_Name__c = 'test test';
        ctrl.payment_method.Credit_Card_Type__c = 'Visa';
        ctrl.payment_method.Credit_Card_Expiration_Month__c = '03';
        ctrl.payment_method.Credit_Card_Expiration_Year__c = '2016';
        ctrl.payment_method.Credit_Card_CVV_Code__c = '222';
        ctrl.payment_method.Enc_Value__c = '12341324125135231512';

        ctrl.isCalledFrom ='Section4';
        //ctrl.saveAndSubmit();


        ctrl.payment_method.Credit_Card_Number__c = null;
        //ctrl.saveAndSubmit();
        ctrl.firstname = null;
        //ctrl.saveAndSubmit();
        ctrl.selectedProduct = '';
       // ctrl.saveAndSubmit();
        ctrl.selectedMarket = '';
       // ctrl.saveAndSubmit();

        ctrl.selectedEdition = 'Professional';
        ctrl.selectedProduct = 'RT';
        ctrl.determineFees();

        // test extension
        ctrl.selectedMarket = 'EU';
        vvAccountManagementExtension ame = new vvAccountManagementExtension(ctrl);
        ame.populateCountries();
        ame.isShowCurrencySelection();
        ame.getStates();
        ame.getShippingStates();
        ame.getSalutations();
        ame.getMarketSelectList();
        //ame.getBonusMarketSelectList();
        ame.getMarketStringList();
        ame.getEditionTypes();
        ame.getFeedTypes();
        ame.getExpirationYears();
        ame.getHowHear();
        ame.refreshState();
        ame.refreshShippingState();

        Test.stopTest();
    }
    @isTest static void testController2() {
        User userObj = CommunitiesUserCentralControllerTest.setupAccountUserSub();

        Test.startTest();

        
        // test brand new account init
        vvCSRAccountManagementControllerClone ctrl = new vvCSRAccountManagementControllerClone();
        ctrl.salutation = 'Mr.';
        ctrl.lastname = 'testlast';
        ctrl.firstname = 'testfirst';
        ctrl.a.Phone = '2223334444';
        ctrl.a.PersonEmail = 'test2123412@test.com';
       
        ctrl.a.BillingStreet = '112 test';
        ctrl.a.BillingCity = 'Atlanta';
        ctrl.a.BillingPostalCode = '22933';
        ctrl.a.BillingState = 'Georgia';
        ctrl.a.BillingCountry = 'USA';

        
        ctrl.confirmEmail = ctrl.a.PersonEmail;

        Account a1 = [SELECT PersonEmail FROM Account WHERE PersonEmail <> null LIMIT 1];
        ctrl.referral_code = 'asfasdfasfdwetqw';
        ctrl.isCalledFrom='Section1';
        ctrl.sameasbilling=true;
        ctrl.saveAndSubmit();
        
        ctrl.payment_method.Payment_Type__c = 'Credit Card';
        ctrl.payment_method.Credit_Card_Number__c = '2222333344445555';
        ctrl.payment_method.Credit_Card_Holder_Name__c = 'test test';
        ctrl.payment_method.Credit_Card_Type__c = 'Visa';
        ctrl.payment_method.Credit_Card_Expiration_Month__c = '03';
        ctrl.payment_method.Credit_Card_Expiration_Year__c = '2016';
        ctrl.payment_method.Credit_Card_CVV_Code__c = '222';
        ctrl.payment_method.Enc_Value__c = '12341324125135231512';
        
        ctrl.selectedMarket = 'US';
        ctrl.sameasbilling =true;
        ctrl.isCalledFrom ='Section4';
        ctrl.saveAndSubmit();
        
    //Test.stoptest();
    //Test.starttest();
        // test account update
        Account a = [SELECT Id FROM Account WHERE PersonContactId = :userObj.ContactId];
        ApexPages.currentPage().getParameters().put('id',a.Id);
        ctrl = new vvCSRAccountManagementControllerClone();
        ctrl.sameAsBilling = true;

        ctrl.tempSave();
        ctrl.saveAndSubmit();
        
        ctrl.payment_method.Payment_Type__c = 'Credit Card';
        ctrl.payment_method.Credit_Card_Number__c = '2222333344445555';
        ctrl.payment_method.Credit_Card_Holder_Name__c = 'test test';
        ctrl.payment_method.Credit_Card_Type__c = 'Visa';
        ctrl.payment_method.Credit_Card_Expiration_Month__c = '03';
        ctrl.payment_method.Credit_Card_Expiration_Year__c = '2016';
        ctrl.payment_method.Credit_Card_CVV_Code__c = '222';
        ctrl.payment_method.Enc_Value__c = '12341324125135231512';
        
        ctrl.selectedMarket = 'US';
        ctrl.isCalledFrom ='Section4';
        ctrl.saveAndSubmit();

        

       

        Test.stopTest();
    }
}