@isTest(seeAllData=true)
private class ReferralCreditTest {
    @isTest static void testMain() {
        // referral account
        //Account a = new Account(FirstName='testfirst',LastName='testlast',PersonEmail='test@test.com',Conversion_Date__c = Date.today().addDays(-10),Subscription_Status__c='Active');
        //insert a;
        Account a = new Account();
            a.firstname='test acc1';
            a.lastName = 'test acc';
            a.billingCity ='Test City';
            a.billingStreet = 'TestStreet';
            a.billingstate ='TestState';
            a.billingPostalcode ='23451';
            a.billingcountry ='United States';
            Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            a.recordtypeID=objAccrec.id;
           a.PersonMailingCity='Test City';
            a.PersonMailingStreet = 'TestStreet';
            a.PersonMailingState ='TestState';
            a.PersonMailingPostalCode ='23451';
            a.PersonMailingCountry ='United States';
            a.phone='090123456987';
            a.Conversion_Date__c = Date.today().addDays(-10);
            a.Subscription_Status__c='Active';
            //acc.ispersonAccount = true;
            insert a;
        // referred account
        //Account a2 = new Account(FirstName='referraltestingaccount',LastName='testlast1',PersonEmail='test@test.com',Referred_By__c=a.Id, Conversion_Date__c = Date.today(),Subscription_Status__c='Active');
        //insert a2;
        Account a2 = new Account();
            a2.firstname='test acc1';
            a2.lastName = 'test acc';
            a2.billingCity ='Test City';
            a2.billingStreet = 'TestStreet';
            a2.billingstate ='TestState';
            a2.billingPostalcode ='23451';
            a2.billingcountry ='United States';
            //Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            a2.recordtypeID=objAccrec.id;
           a2.PersonMailingCity='Test City';
            a2.PersonMailingStreet = 'TestStreet';
            a2.PersonMailingState ='TestState';
            a2.PersonMailingPostalCode ='23451';
            a2.PersonMailingCountry ='United States';
            a2.phone='090123456987';
            a2.Conversion_Date__c = Date.today();
            a2.Referred_By__c=a.Id;
            a2.Subscription_Status__c='Active';
            //acc.ispersonAccount = true;
            insert a2;

        Zuora__CustomerAccount__c zca = new Zuora__CustomerAccount__c(Zuora__Status__c='Active',Name='test billing account',Zuora__Account__c=a.Id,Zuora__Zuora_Id__c='12345');
        insert zca;

        Zuora__Subscription__c zs = new Zuora__Subscription__c(Name='Primary: a0001',Zuora__SubscriptionStartDate__c = Date.today().addMonths(-12), Zuora__SubscriptionEndDate__c = Date.today().addYears(2), Zuora__InitialTerm__c='12 months',Zuora__RenewalTerm__c='12 months',Zuora__TermStartDate__c=Date.today().addMonths(-3),
            Zuora__TermEndDate__c = Date.today().addMonths(20),Zuora__AutoRenew__c=true, Zuora__Zuora_Id__c='12345', Zuora__ContractEffectiveDate__c=Date.today().addMonths(-3), Zuora__ServiceActivationDate__c=Date.today().addMonths(-3), 
            Zuora__TCV__c=12,Zuora__Account__c=a.Id,Zuora__CustomerAccount__c=zca.Id,Zuora__Status__c='Active',Zuora__TermSettingType__c='EVERGREEN');
        insert zs;


        Test.startTest();
        System.schedule('Referral Credit Scheduler','0 30 23 * * ?', new ReferralCreditScheduler());
        Test.stopTest();
    }
}