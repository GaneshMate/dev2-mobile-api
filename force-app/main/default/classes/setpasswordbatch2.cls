global class setpasswordbatch2 implements Database.Batchable<sObject>,database.stateful {
    global id userid;
    global string passwordstr; 
    global list<User> lstuser = new list<user>();  
    global setpasswordbatch2(id useridtemp, string pwdtemp){
        userid = useridtemp;
        passwordstr= pwdtemp;
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
     return Database.getQueryLocator('select id,isactive from user where id=\''+userid+'\'');
        
    }
   
    global void execute(Database.BatchableContext BC, List<user> scope) {
         
         For(user objuser:scope){
             
             system.setpassword(objuser.id,passwordstr);
             lstuser.add(objuser);
         }
    }   
    
    global void finish(Database.BatchableContext BC) {
        For(user objuser:lstuser){ 
            //database.executebatch(new setpasswordbatch2(objuser.id,passwordstr));            
             system.setpassword(objuser.id,passwordstr);
         }
    }
}