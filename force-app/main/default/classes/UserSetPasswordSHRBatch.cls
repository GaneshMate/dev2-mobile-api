global class UserSetPasswordSHRBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    String query;
    String errors;
    
    global UserSetPasswordSHRBatch() {
        query = 'select id, username, isactive, contact.account.password__c from user where username like \'%@vectorvest-salesforce.com.partial\' and contact.account.password__c <> \'\'';
        errors = '';
    }
    global UserSetPasswordSHRBatch(String query) {
        this.query = query;
        errors = '';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<User> userlist) {
        //List<Account> acc_to_update = new List<Account>();

        for (User u : userlist) {
            try {
                System.setPassword(u.Id, u.Contact.Account.Password__c);
                //acc_to_update.add(new Account(Id=u.AccountId,Password__c=null));
            } catch (Exception e) {
                //acc_to_update.add(new Account(Id=u.AccountId, Invalid_Password__c=e.getMessage()));
                errors += 'Unable to set password for User: ' + u.Username + '|' + e.getMessage() + '\n';
            }
            

        }
        //if (!acc_to_update.isEmpty()) update acc_to_update;
    
    }
    
    global void finish(Database.BatchableContext BC) {
        if (errors != '' || test.isrunningtest()) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

            String[] toAddresses = new String[] {'sayed.ramadan@cloudwareconnections.com'};
            mail.setToAddresses(toAddresses);
            mail.setSubject('set user password errors:');
            mail.setPlainTextBody(errors);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
    
}