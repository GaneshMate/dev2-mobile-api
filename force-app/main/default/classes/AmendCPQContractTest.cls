@isTest
private class AmendCPQContractTest{
    @isTest static void testMethod1(){
         Account acc = new Account();
            acc.firstname='test acc1';
            acc.lastName = 'test acc';
            acc.billingCity ='Test City';
            acc.billingStreet = 'TestStreet';
            acc.billingstate ='TestState';
            acc.billingPostalcode ='23451';
            acc.billingcountry ='United States';
            acc.recordtypeID='0121100000014fO';
           acc.PersonMailingCity='Test City';
            acc.PersonMailingStreet = 'TestStreet';
            acc.PersonMailingState ='TestState';
            acc.PersonMailingPostalCode ='23451';
            acc.PersonMailingCountry ='United States';
            //acc.ispersonAccount = true;
            insert acc;
            
            Opportunity opp = new Opportunity(Name=acc.Name + ' Opportunity ' ,
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=acc.Id);
                                       
            Product2 prod = new Product2(Name = 'VectorVest 7 EOD US Monthly', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_MONTHLY' , Family = 'Books',isActive=true,SBQQ__BillingFrequency__c='Monthly');
            insert prod;
            product2 optprod = new product2(Name = 'VECTORVEST_MOBILE_MONTHLY',productcode='VECTORVEST_MOBILE_MONTHLY', isactive=True);
            insert optprod;
            SBQQ__ProductOption__c objprodoption = new SBQQ__ProductOption__c(SBQQ__Number__c=1,SBQQ__ConfiguredSKU__c=prod.id,SBQQ__OptionalSKU__c=optprod.id);
            insert objprodoption;
            Product2 prod2 = new Product2(Name = 'VectorVest 7 EOD US Annual', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_ANNUAL' , Family = 'Books',isActive=true,SBQQ__BillingFrequency__c='Annual');
            insert prod2;
            
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            
            CPQCommunity__c cpc = new CPQCommunity__c();
            cpc.Name = 'test2';
            cpc.PriceBookId__c = pricebookId ;
            insert cpc ;
            
            //insert master contract
            Contract objMContract = new Contract();
             //objContract.SBQQ__Quote__c = quoteRec.id;
             objMContract .SBQQ__Opportunity__c = opp.id;
             objMContract .Accountid=acc.id;
             objMContract .startdate=date.today();
             //objcontract.enddate=date.today().adddays(30);
             insert objMContract ;
            //insert aster contract
            
            SBQQ__Quote__c quoteRec = new SBQQ__Quote__c();
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SiteUser_Primary_Check__c = true;
            quoteRec.SBQQ__Primary__c = false;
            quoterec.SBQQ__MasterContract__c=objMContract.id;
            quoteRec.SBQQ__Opportunity2__c = opp.Id;
            //quoteRec.ZSB__BillToContact__c = c.Id;
           // quoteRec.ZSB__SoldToContact__c = c.Id;
            quoteRec.ZSB__BillCycleDay__c =  '1';
            quoteRec.ZSB__BillingBatch__c = 'Batch1';
            quoterec.SBQQ__StartDate__c=date.today();
            quoterec.SBQQ__EndDate__c = date.today().adddays(30);
            insert quoteRec ;
            SBQQ__QuoteLine__c ojQuoteline = new SBQQ__QuoteLine__c();
            ojQuoteline.SBQQ__Quote__c = quoteRec .id;
            ojQuoteline.SBQQ__Product__c = prod.id;
            insert ojQuoteline;
            SBQQ__QuoteLine__c ojQuoteline1 = new SBQQ__QuoteLine__c();
            ojQuoteline1.SBQQ__Quote__c = quoteRec .id;
            ojQuoteline1.SBQQ__Product__c = optprod.id;
            ojQuoteline1.SBQQ__RequiredBy__c =ojQuoteline.id;
            insert ojQuoteline1;
             Contract objContract = new Contract();
             objContract.SBQQ__Quote__c = quoteRec.id;
             objContract.SBQQ__Opportunity__c = opp.id;
             objcontract.Accountid=acc.id;
             objcontract.startdate=date.today();
             //objcontract.enddate=date.today().adddays(30);
             insert objContract;
             Zuora__CustomerAccount__c objZCustAccount = new Zuora__CustomerAccount__c();
             objZCustAccount .Zuora__Account__c = acc.id;
             objZCustAccount .Zuora__Status__c ='Active';
             objZCustAccount .Zuora__BillToName__c ='test';
             objZCustAccount.Zuora__Currency__c ='USD';
             //objZCustAccount .Zuora__CreditCard_Number__c = '4444333322221111';
             //objZCustAccount .Zuora__CreditCard_Expiration__c = '10/2019';
             insert objZCustAccount ;
             Zuora__PaymentMethod__c objZpaymentmethod = new Zuora__PaymentMethod__c();
             objZpaymentmethod .Zuora__CreditCardCountry__c='us';
             objZpaymentmethod .Zuora__CreditCardExpirationMonth__c='10';
             objZpaymentmethod.Zuora__CreditCardExpirationYear__c='2019';
             objZpaymentmethod.Zuora__CreditCardHolderName__c='test';
             objZpaymentmethod.Zuora__CreditCardMaskNumber__c='123';
             objZpaymentmethod.Zuora__DefaultPaymentMethod__c  = true;
             //objZpaymentmethod .
             //objZpaymentmethod .
             
             objZpaymentmethod.Zuora__BillingAccount__c = objZCustAccount .id;
             insert objZpaymentmethod;
             contact con =[select id from contact where accountid=:acc.id];
             user objuser = new user(id=userinfo.getuserid(), contactid=con.id);
             update objuser;
             user objuser1 = [select id, accountid from user where id=:objuser.id];
             system.debug('objuser###'+objuser1.accountid);
             SBQQ__Subscription__c objSub = new SBQQ__Subscription__c();
             objsub.SBQQ__Contract__c = objContract.id;
             objsub.SBQQ__Product__c =prod.id;
             objsub.SBQQ__QuoteLine__c = ojQuoteline.id;
             objsub.SBQQ__Account__c = acc.id;
             objsub.SBQQ__Quantity__c = 1;
             insert objsub;
             test.starttest();
             AmendCPQContract objAmendCPQContract = new AmendCPQContract();
             objAmendCPQContract .createAmendQuote(objContract.id);
             objAmendCPQContract.SwitchBilling(string.valueof(quoteRec.id),string.valueof(objsub.id),string.valueof(objcontract.id),string.valueof(prod.id));
             //objAmendCPQContract.makeQuotePrimary(quoterec.id);
             test.stoptest();
    }
    
    }