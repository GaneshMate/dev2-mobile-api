global class SuspendedSubCancellationScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		Database.executeBatch(new SuspendedSubCancellationBatch());
	}

	/*
	runs daily at 23:30
	
	System.schedule('Suspended Sub Cancellation Batch','0 30 23 * * ?', new SubscriptionResumeScheduler());
	 */
}