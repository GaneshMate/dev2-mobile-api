public class ActiveContractAndSubController {
    public string ErrorMessages{get;set;}
    public Zuora__Subscription__c suspendsub{get;set;}
    public boolean showdateselection{get;set;}
    public Boolean isRecordExist{get;set;}
    public List <SBQQ__Subscription__c> subscriptionList{get; set;}
    public String accountId {get;set;}
    public Boolean allBool {get;set;}
    public Id  idSelected {get; set;}
    public static Map<String,List<SBQQ__Subscription__c>> contractVsSubscriptions{get; set;}
    public static Map<String,Contract> contractMap{get; set;}
    List <Id> accountIds =new List<Id>();
    List <Id> contactIds =new List<Id>();
    List <Id> caseIds =new List<Id>();
    public String accName{get;set;}
    public List<String> ErrorList{get; set;}
    public List<String> Succ_List{get; set;}
    public List<SubscriptionWrapper> subscriptionWrapperList {get;set;}
    public ActiveContractAndSubController(){
        showdateselection=false;
        suspendsub = new Zuora__Subscription__c();
        contractVsSubscriptions= new Map<String,List<SBQQ__Subscription__c>>();
        subscriptionWrapperList = new List<SubscriptionWrapper>();
        contractMap= new Map<String,Contract>();
        accountId='';
        accountId = apexpages.currentpage().getparameters().get('accountId');
        System.debug('accountId------'+accountId);
        
        for(Account acc :[Select Name from Account where id =: accountId]){
            
            accName = acc.Name; 
            System.debug(accName);
        }
        for(SBQQ__Subscription__c sub : [select Id,SBQQ__Account__r.Name,SBQQ__Account__c,SBQQ__Contract__r.SuspendDate__c, SBQQ__Contract__r.resumedate__c,SBQQ__Contract__r.Name,SBQQ__Contract__r.TerminatedDate__c,SBQQ__Contract__r.AccountId,SBQQ__Contract__r.EndDate,SBQQ__Contract__r.Startdate,SBQQ__Contract__r.ZSB__SubscriptionId__c,SBQQ__Contract__r.ZSB__SubscriptionNumber__c,SBQQ__Contract__r.ContractNumber,SBQQ__Contract__r.Status,SBQQ__Contract__r.ZSB__TermType__c,name,SBQQ__ProductName__c from SBQQ__Subscription__c where SBQQ__Account__c=:accountId 
                                         and SBQQ__Contract__r.Status='Activated']){   
                                             
                                             if(sub.SBQQ__Contract__r.ZSB__SubscriptionId__c != null && sub.SBQQ__Contract__r.TerminatedDate__c == null){
                                                 //Commeted  Showing Termed Contracts
                                                 /*if(sub.SBQQ__Contract__r.ZSB__TermType__c == 'Termed' && sub.SBQQ__Contract__r.EndDate >= date.today()){
                                                if(sub.SBQQ__Contract__c!=null){
                                                contractMap.put(sub.SBQQ__Contract__c,sub.SBQQ__Contract__r);
                                                }
                                                if(contractVsSubscriptions.containsKey(sub.SBQQ__Contract__c)){
                                                contractVsSubscriptions.get(sub.SBQQ__Contract__c).add(sub);
                                                
                                                }else{
                                                if(sub.SBQQ__Contract__c!=null){
                                                contractVsSubscriptions.put(sub.SBQQ__Contract__c,new list<SBQQ__Subscription__c>{sub});
                                                }
                                                }
                                                }else*/ if(sub.SBQQ__Contract__r.ZSB__TermType__c == 'Evergreen'){
                                                    if(sub.SBQQ__Contract__c!=null){
                                                        contractMap.put(sub.SBQQ__Contract__c,sub.SBQQ__Contract__r);
                                                    }
                                                    if(contractVsSubscriptions.containsKey(sub.SBQQ__Contract__c)){
                                                        contractVsSubscriptions.get(sub.SBQQ__Contract__c).add(sub);
                                                        
                                                    }else{
                                                        if(sub.SBQQ__Contract__c!=null){
                                                            contractVsSubscriptions.put(sub.SBQQ__Contract__c,new list<SBQQ__Subscription__c>{sub});
                                                        }
                                                    }
                                                }
                                             }
                                             //listWrapper.add(new WrapperClass(contractVsSubscriptions,contractMap,false));
                                         }
        for(String contractId :contractVsSubscriptions.keySet()) {
            SubscriptionWrapper subscriptionWrapper = new SubscriptionWrapper();
            subscriptionWrapper.contract = contractMap.get(contractId);
            //subscriptionWrapper.account = contractMap.get(contractId).Name;
            subscriptionWrapper.checked = false;
            subscriptionWrapper.subscriptionsList = new List<SBQQ__Subscription__c>();
            subscriptionWrapper.subscriptionsList.addAll(contractVsSubscriptions.get(contractId));
            System.debug('Subscription Wrapper :' +subscriptionWrapper);
            subscriptionWrapperList.add(subscriptionWrapper);
        }
        isRecordExist = true;
        if(subscriptionWrapperList.size()==0){
            isRecordExist = false;
        }
        System.debug('contractVsSubscriptions--------'+contractVsSubscriptions);
        System.debug('contractMap--------'+contractMap);
        System.debug('subscriptionWrapperList--------'+subscriptionWrapperList);
    }
    
    public PageReference cancelAll(){
        System.debug('accountId------'+accountId);
        System.debug('subToDel------'+subscriptionWrapperList);
        Set<String> insertSet = new Set<String>();
        //List<String> subToDel = new List<String>(); 
        //List<SBQQ__Subscription__c> updateSubscription = new List<SBQQ__Subscription__c>(); 
        Date canceldate = Date.today().adddays(1);
        
        String result='';
        //CustomerInfoService.CustomerInfo cs = null;
        boolean ischeck=false;
        // Set<String> zs_zuora_ids = new Set<String>();   // subscriptions to cancel
       // Set<Id> canceled_subs = new Set<Id>(); 
        //List<SBQQ__Subscription__c> allSubscription;
        //List<SBQQ__Subscription__c> subscriptionList = new List<SBQQ__Subscription__c>();
        //List<SBQQ__Subscription__c> subscriptionCloneList = new List<SBQQ__Subscription__c>();
        
        //Set<Contract> contractSet = new Set<Contract>();
        //List<Contract> contractList = new List<Contract>();
        List<SubscriptionWrapper> newList = new List<SubscriptionWrapper>();
        List<String> ErrorList = new List<String>();
        List<String> Succ_List = new List<String>();
        String all_errs ='';
        String all_succ ='';
        integer index = 0;
        for(SubscriptionWrapper wrap : subscriptionWrapperList){ 
            //System.debug('wrap------'+wrap);
            if(wrap.checked){
                //if (null != wrap.Contract.ContractNumber) cs = CustomerInfoService.getCustomerInfoByContractId(wrap.Contract.ContractNumber);
                /*System.debug('***wrap.Contract.AccountId**'+wrap.Contract.AccountId);
                if (wrap.Contract.AccountId != null) {
                result = CPQIntegrationAPI.cancelSubscription(new Account(Id = wrap.Contract.AccountId), true, wrap.Contract.ContractNumber);
                //System.debug('******result******'+result);
                }*/
                
                result = zuoraAmendHelper.cancelSubscription(wrap.Contract.ZSB__SubscriptionId__c, canceldate);
                
                ischeck=true;
                if(result.equalsIgnoreCase('Successfully Cancelled')){
                    Succ_List.add('Contract Number:'+wrap.Contract.ContractNumber+' '+result);//Ganesh :: 22-2-2019 taking all success messages in a list
                    insertSet.add(wrap.Contract.ContractNumber);
                    
                    
                }else{  
                    if(result != null){
                        // Ganesh :: 20-2-2019 taking all error in list
                        //System.debug('Result :'+result);
                        
                         result = ('Contract Number:'+wrap.Contract.ContractNumber+' '+result.split(',').get(3)).removeEnd('}');
                         System.debug('Result :'+result);
                         ErrorList.add(result);
                         System.debug('****ErrorList*****'+ErrorList);
                         System.debug('ErrorList.size :' +ErrorList.size());
                    }
                    newList.add(wrap);
                }
                result='';
            }else{
                newList.add(wrap);
                
            }
        }
        subscriptionWrapperList=new list<SubscriptionWrapper>();
            subscriptionWrapperList.addall(newList);           
        
        
        if(!insertSet.isEmpty()){
            System.debug('Inside subscriptionClone List if');
           // Database.SaveResult[] results  = Database.insert(subscriptionCloneList);
            System.debug('Inside subscriptionClone List if after');
            insertsub(insertSet);
        }

        System.debug('ischeck------'+ischeck);
        if(!ischeck){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please select contract'));
            return null;
        }
        // Ganesh :: 20-2-2019 checking ErrorList and showing on vf page
        else if(ErrorList.size()>0)
        {   
            if(Succ_List.size()>0) // checking Success List and Showing on vf page
            {
                for(String s :Succ_List)
                {
                    all_succ +=s+'<br>';
                }
            }
           for(String r : ErrorList)
            {              
                all_errs +=r+'<br>';
            }
           System.debug('All Errors'+all_errs);
           ApexPages.Message err = new ApexPages.Message(ApexPages.Severity.ERROR,all_errs);  
           ApexPages.addMessage(err);
           /*ApexPages.Message succ = new ApexPages.Message(ApexPages.Severity.CONFIRM,all_succ);
           ApexPages.addMessage(succ);*/
            
           return null; 
            
        }
        else{
            accountId = apexpages.currentpage().getparameters().get('accountId');
            PageReference nextPage = new PageReference('/' + accountId);
            ischeck=false;
            return nextPage;
        }
}
    
    // ganesh :: 19-02-2016
    public PageReference backMethod() {
            accountId = apexpages.currentpage().getparameters().get('accountId');
            PageReference nextPage = new PageReference('/' + accountId);
            return nextPage;
    }
    
    public void showdateselectionmethod(){
        if(showdateselection == false){
        showdateselection=true;
        }
        else{
            suspendAll();
        }
        /*integer cnt =0;
        for(SubscriptionWrapper wrap : subscriptionWrapperList){            
            
            if(wrap.checked){
                cnt= cnt+1;
            }
        }
        if(cnt == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Select Atleast one contract'));
            return;
        }
        else{
            showdateselection=true;
        }*/
    }
    public void suspendAll() {
        string result='';
        ErrorMessages='';
        if(suspendsub.SuspendDate__c == null || suspendsub.ResumeDate__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Enter SuspendDate and ResumeDate'));
            return ;
        }
         if (suspendsub.ResumeDate__c < suspendsub.SuspendDate__c) {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING, 'Resume date has to be greater than suspend date.'));
            return;
        }
        if (suspendsub.ResumeDate__c != null && suspendsub.SuspendDate__c.daysBetween(suspendsub.ResumeDate__c) > 180) {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING, 'You can not suspend a subscription for more than 180 days.'));
            return;
        }

        if (suspendsub.ResumeDate__c != null && suspendsub.ResumeDate__c < Date.today()) {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING, 'You can not set new resume day in the past'));
            return;
        }
        list<Contract> lstContract = new list<Contract>();
        Contract objContract;
        for(SubscriptionWrapper wrap : subscriptionWrapperList){          
            
            
            if(wrap.checked){
                Zuora__Subscription__c zs;
                if(!test.isrunningtest())
                 zs = [SELECT Zuora__Account__c,Zuora__SubscriptionNumber__c,SuspendDate__c,Name,ResumeDate__c, Zuora__TermStartDate__c, Zuora__Zuora_Id__c, Zuora__ContractEffectiveDate__c, Market__c, Feed__c, Category__c FROM Zuora__Subscription__c WHERE Name = :wrap.Contract.ZSB__SubscriptionNumber__c];
                else 
                 zs = [SELECT Zuora__Account__c,Zuora__SubscriptionNumber__c,SuspendDate__c,Name,ResumeDate__c, Zuora__TermStartDate__c, Zuora__Zuora_Id__c, Zuora__ContractEffectiveDate__c, Market__c, Feed__c, Category__c FROM Zuora__Subscription__c limit 1];
                Date OriginalSuspenddate =zs.SuspendDate__c;
                Date OriginalResumedate = zs.ResumeDate__c;
                zs.SuspendDate__c = suspendsub.SuspendDate__c;
                zs.ResumeDate__c = suspendsub.ResumeDate__c;
                zuoraAPIHelper.updateSubscriptionSuspendResumeDate(zs);
                if(zs.ResumeDate__c == null){
                    result = zuoraAPIHelper.suspendSubscription(zs.Zuora__Zuora_Id__c, zs.SuspendDate__c);
                }
                else{
                    result = zuoraAPIHelper.suspendSubscriptionWithResume(zs.Zuora__Zuora_Id__c, zs.SuspendDate__c,zs.ResumeDate__c);
                }
                if(result.containsIgnoreCase('error')){
                    zs.SuspendDate__c = OriginalSuspenddate;
                    zs.ResumeDate__c = OriginalResumedate;
                    zuoraAPIHelper.updateSubscriptionSuspendResumeDate(zs);
                    ErrorMessages=ErrorMessages+'' +wrap.Contract.ContractNumber+': '+result+'<br/>';
                }
                else{                    
                    //zuoraAPIHelper.updateSubscriptionSuspendResumeDate(zs);
                    ErrorMessages=ErrorMessages+'' +wrap.Contract.ContractNumber+': '+'Successfully suspended the subscription <br/>';
                    objContract = new Contract(id=wrap.contract.id, suspenddate__c =zs.SuspendDate__c, resumedate__c=zs.resumedate__c );
                    lstContract.add(objcontract);
                }
            }
            
           
        }
        if(lstContract.size()>0){
            update lstcontract;
        }
        if(ErrorMessages == ''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Select Atleast one contract'));
            return;
        }
        
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, ErrorMessages));
            return ;
            
        
    }
    
    /*public class WrapperClass {
    public Boolean checked {get;set;}
    //public List<sObject> sub {get;set;}
    Map<String,List<SBQQ__Subscription__c>> contractVsSubscriptions {get;set;}
    Map<String,Contract> contractMap {get;set;}
    public WrapperClass(Map<String,List<SBQQ__Subscription__c>> contractVsSubscriptions,Map<String,Contract> contractMap,Boolean checked) {
    this.contractVsSubscriptions = contractVsSubscriptions;
    this.contractMap = contractMap;
    this.checked = checked;
    }
    }*/
    //AK
    public class SubscriptionWrapper {
        public Boolean checked {get;set;}
        public Contract contract {get;set;}
       // public String account {get;set;}
        public List<SBQQ__Subscription__c> subscriptionsList {get;set;} 
        
        public SubscriptionWrapper(){}
        public SubscriptionWrapper(Contract contract, List<SBQQ__Subscription__c> subscriptionsList, Boolean checked){
            this.subscriptionsList = subscriptionsList;
            this.contract = contract;
            this.checked = checked;
        }
    }
    /*private static List<SBQQ__Subscription__c> getContractSubscription(Set<String> contractNo){
        List<SBQQ__Subscription__c> allSubscription = new List<SBQQ__Subscription__c>();
        allSubscription = [SELECT Id, 
                           SBQQ__StartDate__c,
                           SBQQ__EndDate__c,
                           SBQQ__Quantity__c,
                           SBQQ__TerminatedDate__c,
                           SBQQ__RenewalQuantity__c,
                           SBQQ__NetPrice__c,
                           SBQQ__ListPrice__c,
                           SBQQ__CustomerPrice__c,
                           SBQQ__RegularPrice__c,
                           SBQQ__RenewalPrice__c,
                           SBQQ__SpecialPrice__c ,
                           SBQQ__Account__c,
                           SBQQ__Contract__c,
                           SBQQ__Contract__r.ContractNumber,
                           SBQQ__Contract__r.ZSB__SubscriptionId__c,
                           SBQQ__Contract__r.Status,
                           SBQQ__Product__c,
                           SBQQ__ProductName__c,
                           Contract_Term__c,
                           SBQQ__Contract__r.ContractTerm,
                           SBQQ__Contract__r.ZSB__TermType__c
                           FROM SBQQ__Subscription__c WHERE SBQQ__Contract__r.ContractNumber IN :contractNo AND SBQQ__TerminatedDate__c = NULL Order by LastModifiedDate desc];
        
        return allSubscription;
    }*/
    @future
    public static void insertsub(set<string> contractNo){
    list<SBQQ__Subscription__c> subscriptionCloneList = new list<SBQQ__Subscription__c>();
    list<SBQQ__Subscription__c> allSubscription =[SELECT Id, 
                           SBQQ__StartDate__c,
                           SBQQ__EndDate__c,
                           SBQQ__Quantity__c,
                           SBQQ__TerminatedDate__c,
                           SBQQ__RenewalQuantity__c,
                           SBQQ__NetPrice__c,
                           SBQQ__ListPrice__c,
                           SBQQ__CustomerPrice__c,
                           SBQQ__RegularPrice__c,
                           SBQQ__RenewalPrice__c,
                           SBQQ__SpecialPrice__c ,
                           SBQQ__Account__c,
                           SBQQ__Contract__c,
                           SBQQ__Contract__r.ContractNumber,
                           SBQQ__Contract__r.ZSB__SubscriptionId__c,
                           SBQQ__Contract__r.Status,
                           SBQQ__Product__c,
                           SBQQ__ProductName__c,
                           Contract_Term__c,
                           SBQQ__Contract__r.ContractTerm,
                           SBQQ__Contract__r.ZSB__TermType__c
                           FROM SBQQ__Subscription__c WHERE  SBQQ__TerminatedDate__c = NULL and SBQQ__Contract__r.ContractNumber IN :contractNo Order by LastModifiedDate desc];
        
        for(SBQQ__Subscription__c sub: allSubscription){
            sub.SBQQ__TerminatedDate__c = date.today().adddays(1);
            
            SBQQ__Subscription__c sub1 = sub.clone(false,false);
            
            sub1.SBQQ__RenewalQuantity__c = -1;
            sub1.SBQQ__Quantity__c = -1;
            sub1.SBQQ__NetPrice__c = 0;
            sub1.SBQQ__ListPrice__c = 0;
            sub1.SBQQ__CustomerPrice__c = 0;
            sub1.SBQQ__RegularPrice__c = 0;
            sub1.SBQQ__RenewalPrice__c = 0;
            sub1.SBQQ__SpecialPrice__c = 0;
            
            //sub1.SBQQ__RevisedSubscription__c =subscriptionList[0].Id;
            sub1.SBQQ__RevisedSubscription__c = sub.Id;  //Ganesh :: 05_03_2019
            
            subscriptionCloneList.add(sub1);
              
        }
        if(subscriptionCloneList.size()>0)
        insert subscriptionCloneList;
    }
}