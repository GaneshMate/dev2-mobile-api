global class batchEAR implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) {
       // String query = 'select id,accountid From Contract where startdate=today or SuspendDate__c=today or ResumeDate__c=today';
        String query='';
        if(!test.isRunningTest()){
        
         query = 'select id,accountid From Contract where startdate=today or TerminatedDate__c = today or(enddate=Yesterday and ZSB__TermType__c != \'Evergreen\' ) or SuspendDate__c=today or SuspendDate__c=Yesterday or ResumeDate__c=today';        
        //query = 'select id,accountid From Contract where id=\'80011000000n7xF\'';
        }
        else {
           query = 'select id,accountid From Contract where startdate=today or (enddate<=today and ZSB__TermType__c != \'Evergreen\' ) or SuspendDate__c=today or ResumeDate__c=today limit 1';         
        }
        //query = 'select id,accountid From Contract where id=\'80011000000mzvq\'';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Contract> scope) {
        set<id> setaccountids = new set<id>();
        for(Contract objcontract : scope){
            setaccountids.add(objcontract.accountid);
        }
        List<SBQQ__Subscription__c> allSubscriptionWithAccount = [select id,ResumeDate__c,Suspenddate__C,Subscription_Status__c,SBQQ__Account__c,SBQQ__StartDate__c,
        SBQQ__EndDate__c,SBQQ__Contract__r.ZSB__TermType__c,SBQQ__Product__r.Name,SBQQ__Product__r.ProductCode,SBQQ__Product__r.AddOn_Type__c,SBQQ__Product__r.Feed__c , SBQQ__Product__c ,
        SBQQ__Product__r.Market__c,SBQQ__Product__r.IsTrial__c,SBQQ__Product__r.Is_Secondary_Market__c,ZSB__RatePlanChargeId__c,
        SBQQ__Product__r.Professional__c,SBQQ__Product__r.SBQQ__BillingFrequency__c,SBQQ__TerminatedDate__c from SBQQ__Subscription__c where 
          SBQQ__Account__c in:setaccountids];
         SubscriptionTriggerHandlerWithEAR.populateEnterpriseAccessRight(allSubscriptionWithAccount);
         
    }   
    
    global void finish(Database.BatchableContext BC) {
    }
}