public with sharing class CPQIntegrationAPI {
    /************* Method to Create Contract ****************/
    
    private static Contract createContract(Account acc, String zuora_Id, String StartDate,Integer ContractTerm,String Status,String subscriptionStartDate,String termType){
   
            if (acc == null ||String.isBlank(zuora_Id)) {
               return null;
            }
        
            Contract contract = new Contract();
            contract.AccountId = acc.Id;
            contract.StartDate =Date.valueOf(StartDate);
            contract.Status = Status;
            contract.CurrencyIsoCode = acc.Account_Currency__c;
            contract.ContractTerm = ContractTerm;    
            contract.SBQQ__RenewalTerm__c = contract.ContractTerm;
            contract.VV_SubscriptionStartDate__c =Date.valueOf(subscriptionStartDate);
            contract.ZSB__TermType__c = termType;
        	contract.IsMobile__c = true;
            
            insert contract;
        
        return contract;
    }
    
    /********** Method to Create Subscription ********************/
    private static SBQQ__Subscription__c createSubscription(Id contractId,String prodName,Integer Quantity,Decimal NetPrice,String AppStore,Boolean IsMobileAutoConvert){
            String vvPricebook=Label.vvPricebook;
            PriceBookIds__c stdPriceBookId = PriceBookIds__c.getValues(vvPricebook);
             System.debug('****stdPriceBookId******'+stdPriceBookId);
            if (stdPriceBookId == null) {
                return null;
            }
            string strPricebookId = stdPriceBookId.Price_Book_Id__c;
            List<Product2> prodList=[select Id, Name,(SELECT ID,UnitPrice, ZSB__PRPChargeId__c, ZSB__PRPlanId__c FROM PricebookEntries where Pricebook2Id = :strPricebookId) 
                                  from product2 where Name =:prodName]; 
            
            if (prodList == null || prodList.size() == 0) {
               return null;
            }
            
            List<Contract> contractList =[select Account.id, Account.Name from Contract where Id =:contractId];
            
            SBQQ__Subscription__c subscribe = new SBQQ__Subscription__c();
            subscribe.SBQQ__Contract__c = contractId != null ? contractId : null;
            subscribe.SBQQ__Product__c = prodList != null ? prodList[0].Id :null;
            subscribe.SBQQ__Quantity__c = Quantity;
            subscribe.SBQQ__NetPrice__c =NetPrice;
            subscribe.SBQQ__SpecialPrice__c = NetPrice;
            subscribe.SBQQ__ListPrice__c = NetPrice;
            subscribe.SBQQ__CustomerPrice__c = NetPrice;
            subscribe.SBQQ__RegularPrice__c = NetPrice;
            subscribe.SBQQ__Account__c =contractList != null ? contractList[0].Account.Id : null;
            subscribe.ZSB__PRPChargeId__c = prodList[0].PricebookEntries != null ? prodList[0].PricebookEntries[0].ZSB__PRPChargeId__c : null;
            subscribe.ZSB__PRPlanId__c = prodList[0].PricebookEntries != null ? prodList[0].PricebookEntries[0].ZSB__PRPlanId__c : null;
        	subscribe.App_Store__c=AppStore;
        	subscribe.IsMobileAutoConvert__c=IsMobileAutoConvert;
            
            insert subscribe; 
        
        return subscribe;
    }
    public static SBQQ__Subscription__c createContractAndSubscription(Account acc,String zuora_Id,String prodName,String StartDate,String Status,Integer ContractTerm,Integer Quantity,Decimal NetPrice,String subscriptionStartDate,String termType,String AppStore,Boolean IsMobileAutoConvert){
        if(acc ==null){
            return null;
        }
        
       Contract contract = createContract(acc, zuora_Id,StartDate,ContractTerm,Status,subscriptionStartDate,termType);
       SBQQ__Subscription__c subscription = createSubscription(contract.Id,prodName,Quantity,NetPrice,AppStore,IsMobileAutoConvert);
        System.debug('****subscription******'+subscription);
       List<SBQQ__Subscription__c> subscriptionList = new List<SBQQ__Subscription__c>();
       
       if (subscription != null && subscription.Id != null) {
             subscriptionList = [SELECT SBQQ__ContractNumber__c, 
                                                        SBQQ__Contract__c,
                                                        SBQQ__Product__c,
                                                        SBQQ__Quantity__c,
                                                        SBQQ__NetPrice__c,
                                                        SBQQ__SpecialPrice__c,
                                                        SBQQ__ListPrice__c,SBQQ__CustomerPrice__c,
                                                        SBQQ__RegularPrice__c,
                                                        SBQQ__Account__c,
                                                        ZSB__PRPChargeId__c,
                                                        ZSB__PRPlanId__c,
                                                        SBQQ__ProductName__c,
                                                        SBQQ__StartDate__c,
                                                        SBQQ__EndDate__c,
                                                        SBQQ__Contract__r.Status,
                                                        Contract_Term__c,
                                                        SBQQ__Contract__r.ContractTerm,
                                                        SBQQ__Contract__r.ZSB__TermType__c
                                                        //SBQQ__Contract__r.StartDate
                                                        //SBQQ__Contract__r.EndDate
                                                        FROM SBQQ__Subscription__c WHERE Id =:subscription.Id];    
        }
        if(subscriptionList.size()>0)
          return subscriptionList[0];
          return null;
    }
    
     public static String cancelSubscription(Account a, Boolean is_allSubscription, String contractNo) {
         List<Zuora__CustomerAccount__c> zcalist = new List<Zuora__CustomerAccount__c> ();
         if(!test.isRunningTest()){
             zcalist = [SELECT Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Zuora__Account__c = :a.Id AND Zuora__Status__c = 'Active' ORDER BY CreatedDate DESC LIMIT 1];
             System.debug('****zcalist*****'+zcalist);
             if (zcalist.isEmpty()) return 'Error: Invalid zuora account.';   
         }
        

        Date canceldate = Date.today();
        String result = null;
        Set<String> zs_zuora_ids = new Set<String>();   // subscriptions to cancel
        Set<Id> canceled_subs = new Set<Id>(); 
         List<SBQQ__Subscription__c> allSubscription;
         Contract contract =[SELECT Id, ZSB__SubscriptionId__c FROM Contract WHERE ContractNumber =:contractNo];
         if(contract.ZSB__SubscriptionId__c ==null){
             System.debug('***In contractv query******');
             return 'Contract is not yet synced with Zuora. Please try after sometime!';    
         }
         if(is_allSubscription ==true){
            allSubscription = getContractSubscription(contractNo);
             System.debug('****allSubscription*****'+allSubscription);
         }
         
         if(allSubscription ==null ||allSubscription.isEmpty()){
             return 'There are no active subscriptions!';
         }
         
         List<SBQQ__Subscription__c>subscriptionList = new List<SBQQ__Subscription__c>();
         List<SBQQ__Subscription__c>subscriptionCloneList = new List<SBQQ__Subscription__c>();
         for(SBQQ__Subscription__c sub:allSubscription){
            sub.SBQQ__TerminatedDate__c = canceldate;
            subscriptionList.add(sub);
            subscriptionCloneList.add(sub.clone(false,false));
            zs_zuora_ids.add(sub.SBQQ__Contract__r.ZSB__SubscriptionId__c);
         }
        System.debug('******zs_zuora_ids******'+zs_zuora_ids);  
         for(SBQQ__Subscription__c sub:subscriptionCloneList){
            sub.SBQQ__RenewalQuantity__c = -1;
            sub.SBQQ__Quantity__c = -1;
            sub.SBQQ__NetPrice__c = 0;
            sub.SBQQ__ListPrice__c = 0;
            sub.SBQQ__CustomerPrice__c = 0;
            sub.SBQQ__RegularPrice__c = 0;
            sub.SBQQ__RenewalPrice__c = 0;
            sub.SBQQ__SpecialPrice__c = 0;
            sub.SBQQ__RevisedSubscription__c = subscriptionList[0].Id;
             
         }
        
         for (String s : zs_zuora_ids) {
             result = zuoraAmendHelper.cancelSubscription(s, Date.today());
             System.debug('******result*******'+result);
             if (!result.containsIgnoreCase('success')) return result;
         }
       
         if(!test.isRunningTest()){
             // ondemand sync
             result = zuoraAPIHelper.ondemandSync(zcalist[0].Id); 
         }
         //update subscriptionList[0].SBQQ__Contract__r;
         List<Contract> contractList = new List<Contract>();
         for(SBQQ__Subscription__c sub:subscriptionList){
            contractList.add(sub.SBQQ__Contract__r);    
         }
         
         update contractList;
         
         if(!subscriptionList.isEmpty())update subscriptionList;
         if(!subscriptionCloneList.isEmpty())insert subscriptionCloneList;
         
        return 'Success: ' + result; //zuoraAmendHelper.cancelSubscription(zs.Zuora__Zuora_Id__c, canceldate);  
    }
    
    /*get all subscriptions on the basis of account id*/
    private static List<SBQQ__Subscription__c> getContractSubscription(String contractNo){
        List<SBQQ__Subscription__c> allSubscription = new List<SBQQ__Subscription__c>();
        allSubscription = [SELECT Id, 
                           SBQQ__StartDate__c,
                           SBQQ__EndDate__c,
                           SBQQ__Quantity__c,
                           SBQQ__TerminatedDate__c,
                           SBQQ__RenewalQuantity__c,
                           SBQQ__NetPrice__c,
                           SBQQ__ListPrice__c,
                           SBQQ__CustomerPrice__c,
                           SBQQ__RegularPrice__c,
                           SBQQ__RenewalPrice__c,
                           SBQQ__SpecialPrice__c ,
                           SBQQ__Account__c,
                           SBQQ__Contract__c,
                           SBQQ__Contract__r.ContractNumber,
                           SBQQ__Contract__r.ZSB__SubscriptionId__c,
                           SBQQ__Contract__r.Status,
                           SBQQ__Product__c,
                           SBQQ__ProductName__c,
                           Contract_Term__c,
                           SBQQ__Contract__r.ContractTerm,
                           SBQQ__Contract__r.ZSB__TermType__c
                           FROM SBQQ__Subscription__c WHERE SBQQ__Contract__r.ContractNumber =:contractNo AND SBQQ__TerminatedDate__c = NULL Order by LastModifiedDate desc];

        return allSubscription;
    }
    
    /*get all subscriptions on the basis of account id*/
    /*private static List<SBQQ__Subscription__c> getAllSubscription(String accountId){
        List<SBQQ__Subscription__c> allSubscription = new List<SBQQ__Subscription__c>();
        allSubscription = [SELECT Id, 
                                   SBQQ__StartDate__c,
                                   SBQQ__EndDate__c,
                                   SBQQ__Quantity__c,
                                   SBQQ__TerminatedDate__c,
                                   SBQQ__RenewalQuantity__c,
                                   SBQQ__NetPrice__c,
                                   SBQQ__ListPrice__c,
                                   SBQQ__CustomerPrice__c,
                                   SBQQ__RegularPrice__c,
                                   SBQQ__RenewalPrice__c,
                                   SBQQ__SpecialPrice__c ,
                                   SBQQ__Account__c,
                                   SBQQ__Contract__c,
                                   SBQQ__Contract__r.ZSB__SubscriptionId__c,
                                   SBQQ__Contract__r.Status,
                                   SBQQ__Product__c,
                                   SBQQ__ProductName__c,
                                   Contract_Term__c,
                                   SBQQ__Contract__r.ContractTerm,
                                   SBQQ__Contract__r.ZSB__TermType__c
                           FROM SBQQ__Subscription__c 
                          WHERE SBQQ__Account__c =:accountId 
                            AND SBQQ__TerminatedDate__c = NULL 
                       ORDER BY LastModifiedDate desc];

        return allSubscription;
    }*/
}