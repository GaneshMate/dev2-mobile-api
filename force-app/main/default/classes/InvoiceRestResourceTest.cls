@istest
public class InvoiceRestResourceTest {
   
    private static List<Account> createAccounts(){
    	Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
		System.debug('RecordTypeIdAccount*******'+RecordTypeIdAccount);
		List<Account> accounts = new List<Account>();
        accounts.add(new Account(FirstName = 'testFirst',
                                 LastName = 'TestLast',
                                 Phone='7894561230',
                                 PersonEmail='Test@test.com', 
                                 recordtypeid=RecordTypeIdAccount,
                                 BillingStreet = 'wakad-hinjewadi road',
                BillingCity = 'pune',
                BillingState = 'maharashtra',
                BillingPostalCode = '411033',
                BillingCountry = 'India',
                ShippingStreet ='wakad',
                ShippingCity = 'pune',
                ShippingState = 'maharashtra',
                ShippingPostalCode = '411033',
                ShippingCountry = 'India',
                PersonMailingCity = 'pune',
                PersonMailingCountry='India',
                PersonMailingPostalCode='411033',
                PersonMailingState='maharashtra',
                PersonMailingStreet='wakad' ));
        Database.insert(accounts);
        List<Account> accountsReturns = [SELECT FirstName,LastName,Phone,PersonEmail,ApplicationUserID__c,EnterpriseCustomerID__c FROM Account Limit 50];
        return accountsReturns;
    }
    private static List<Zuora__ZInvoice__c> createZuoraInvoices(String accId){
    	List<Zuora__ZInvoice__c> zuoraAccounts = new List<Zuora__ZInvoice__c>();
        zuoraAccounts.add(new Zuora__ZInvoice__c(Zuora__Account__c=accId));
        Database.insert(zuoraAccounts);
        List<Zuora__ZInvoice__c> zuoraAccountsReturns = [select Id,Name from Zuora__ZInvoice__c where Zuora__Account__c =:accId Limit 50];
        System.debug('zuoraAccounts====>'+zuoraAccounts); 
        return zuoraAccountsReturns;
    }
    @isTest static void testGetUnpaidInvoicesByAppUserId() {
        
        Test.startTest();
        List<Account> account_list = createAccounts();
        List<Zuora__ZInvoice__c> zuoraAccounts = createZuoraInvoices(account_list[0].Id);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/invoice';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'GET';
        req.addParameter('aid', account_list[0].ApplicationUserID__c);
        
        InvoiceRestResource.getUnpaidInvoices();
        
        //System.assertEquals(200, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
        Test.stopTest();
    }
     @isTest static void testGetUnpaidInvoicesByEId() {
        
        Test.startTest();
       
        List<Account> account_list = createAccounts();
        List<Zuora__ZInvoice__c> zuoraAccounts = createZuoraInvoices(account_list[0].Id);
        
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/invoice';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'GET';
        req.addParameter('eid', account_list[0].EnterpriseCustomerID__c);
        
        InvoiceRestResource.getUnpaidInvoices();
        
        //System.assertEquals(200, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
        Test.stopTest();
    }
    
     @isTest static void testGetUnpaidInvoicesByEmail() {
        
        Test.startTest();
       
         List<Account> account_list = createAccounts();
         List<Zuora__ZInvoice__c> zuoraAccounts = createZuoraInvoices(account_list[0].Id);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/invoice';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'GET';
        req.addParameter('eid', account_list[0].EnterpriseCustomerID__c);
        req.addParameter('email', account_list[0].PersonEmail);
        
        InvoiceRestResource.getUnpaidInvoices();
        
        //System.assertEquals(200, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
        Test.stopTest();
    }
    //multiple parameter retruns 400
    @isTest static void testGetUnpaidInvoicesNegative() {
        
        Test.startTest();
       
        List<Account> account_list = createAccounts();
        List<Zuora__ZInvoice__c> zuoraAccounts = createZuoraInvoices(account_list[0].Id);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/invoice';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'GET';
        req.addParameter('email', account_list[0].PersonEmail);
        
        InvoiceRestResource.getUnpaidInvoices();
        
        //System.assertEquals(200, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
        Test.stopTest();
    }
    
    //no parameter retruns 400
    @isTest static void testGetUnpaidInvoicesNegative1() {
        
        Test.startTest();
       
        List<Account> account_list = createAccounts();
        List<Zuora__ZInvoice__c> zuoraAccounts = createZuoraInvoices(account_list[0].Id);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/invoice';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'GET';
        InvoiceRestResource.getUnpaidInvoices();
        
        //System.assertEquals(200, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
        Test.stopTest();
    }
    
     //no parameter retruns 400
    @isTest static void testGetUnpaidInvoicesNegative2() {
        
        Test.startTest();
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
		System.debug('RecordTypeIdAccount*******'+RecordTypeIdAccount);
		List<Account> accounts = new List<Account>();
        accounts.add(new Account(FirstName = 'testFirst1',LastName = 'TestLast1',Phone='7894561230',PersonEmail='Test1@test.com', recordtypeid=RecordTypeIdAccount,BillingStreet = 'wakad-hinjewadi road',
                BillingCity = 'pune',
                BillingState = 'maharashtra',
                BillingPostalCode = '411033',
                BillingCountry = 'India',
                ShippingStreet ='wakad',
                ShippingCity = 'pune',
                ShippingState = 'maharashtra',
                ShippingPostalCode = '411033',
                ShippingCountry = 'India',
                PersonMailingCity = 'pune',
                PersonMailingCountry='India',
                PersonMailingPostalCode='411033',
                PersonMailingState='maharashtra',
                PersonMailingStreet='wakad'                 ));
        Database.insert(accounts);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/invoice';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'GET';
        req.addParameter('email', accounts[0].PersonEmail);
        
        InvoiceRestResource.getUnpaidInvoices();
        
        //System.assertEquals(200, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
        Test.stopTest();
    }
    @isTest(SeeAllData=true) static void testCreatePaymentAgainstInvoice(){
     	Test.startTest();
        List<Account> account_list = createAccounts();
        
        Zuora__CustomerAccount__c zuoraCust = new Zuora__CustomerAccount__c();
        zuoraCust.Zuora__Account__c = account_list[0].Id;
        insert zuoraCust;
        
        List<Zuora__ZInvoice__c> zuoraInvoices = createZuoraInvoices(account_list[0].Id);
       
        String Json = '{"InvoiceNumber":'+'"'+ zuoraInvoices[0].Name +'"'+',"AccountId":'+'"'+zuoraCust.Id+'"'+',"EffectiveDate": "2018-01-11","AppliedInvoiceAmount": 5.00,"AppliedCreditBalanceAmount": 0,"Amount": 5.00,"Type": "External","Status": "Processed","PaymentMethodId": "2c92c0f9528db6aa015290834d504e3e","Comment": "some comment, if required","ReferenceId": "some ref id value, if required"}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/invoice';  
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(Json);
        
        InvoiceRestResource.createPaymentAgainstInvoice();
        
        System.assertEquals(200, res.statusCode);
        System.debug('*****res.statusCode*******'+res.statusCode);
        Test.stopTest(); 
    }
}