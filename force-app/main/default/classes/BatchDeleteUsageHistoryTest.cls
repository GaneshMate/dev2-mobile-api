@isTest
private class BatchDeleteUsageHistoryTest {
    @isTest static void testMain() {
         Account a = new Account(PersonMailingCity='Test City',PersonMailingStreet = 'TestStreet',PersonMailingState='TestState',PersonMailingPostalCode ='23451',personMailingCountry ='United States',Primary_Phone_Type__c='Mobile',FirstName='testfirst',LastName='testlast',PersonEmail='testveryunique@test.vvtest.com',Phone='2223334444',VV_Market__c='US',VV_Second_Market__c='CA',VV_Feed__c='ID',VV_Edition__c='Non-Professional',Subscription_Plan__c='Annual',Is_Elite__c=false,Is_Elite_Legacy__c=false,Is_Apple_App_Store_Customer__c=false,BillingStreet='2234 test',BillingCity='Test',BillingState='GA',BillingCountry='USA',BillingPostalCode='22344');
        insert a;
        
        Usage_History__c ae = new Usage_History__c(Account__c = a.Id, Login_Date_time__C = Datetime.now().addDays(-7));
        insert ae;
        Test.startTest();
        System.schedule('Usage History Cleanup Scheduler','0 5 0 * * ?', new BatchDeleteUsageHistoryScheduler());
        Test.stopTest();
    }
}