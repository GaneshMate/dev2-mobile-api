global class LightningLoginFormController {

    public LightningLoginFormController() {
        
    }

    @AuraEnabled
    public static String login(String username, String password, String startUrl) {
        try{
            ApexPages.PageReference lgn = Site.login(username, password, startUrl);
            aura.redirect(lgn);
            return null;
        }
        catch (Exception ex) {
            return ex.getMessage();            
        }
    }
    
    @AuraEnabled
    public static Boolean getIsUsernamePasswordEnabled() {  
        return true;
    }

    @AuraEnabled
    public static Boolean getIsSelfRegistrationEnabled() {
        return true;
    }

    @AuraEnabled
    public static String getSelfRegistrationUrl() {
        return null;
    }

    @AuraEnabled
    public static String getForgotPasswordUrl() {
        return null;
    }
    
    public static Auth.AuthConfiguration getAuthConfig(){
        return null;
    }
}