global class weeklySubsAccountsBatchScheduler implements Schedulable{
    
    
    global weeklySubsAccountsBatchScheduler() {
        system.debug('Constructure');
           
    }
    global void execute(SchedulableContext sc) {
        
        Date d;
        Datetime dt;
        Date priorWeekStart;
        Date startDateVal;
        Date endDateVal;
        List<String> weekDays = new List<String>{'Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'};
        
        dt = DateTime.newInstance(Date.Today().addDays(-1), Time.newInstance(0, 0, 0, 0));
        d = dt.date();
        priorWeekStart = d.addDays(-7-weekDays.indexOf(dt.format('EEEE')));
        
        system.debug('Execute Method');
        startDateVal = priorWeekStart;
        endDateVal = Date.Today().addDays(-1);
        weeklySubsAccountsBatchStartAndEndDate_1 Batch = new weeklySubsAccountsBatchStartAndEndDate_1 (startDateVal, endDateVal);
        Id BatchProcessId = Database.ExecuteBatch(Batch);
        
    }
}