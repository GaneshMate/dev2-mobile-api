public class TestCurrentTotalSubscribersUpdater{
    public static void UpdateTotalSubscriber(TestTotalSubscribers__c newTotalSubscriberRow, Integer monthNumber){       
        /* get the date from the new row */
        Date newRowDate= newTotalSubscriberRow.TestTotalSubscriberDate__c;
        Integer monthsBack = -1 * monthNumber;
        
        /* calculate the previous date from it */
        Date newDate = newRowDate.addMonths(monthsBack);
        
        /* now that we have the date we are looking for, then find the total row for that date, extract info from that row */
        
        TestTotalSubscribers__c previousRow;
        
        if(monthsBack==0){
            //if it's 0, we can use the original new subscriber row
            previousRow = newTotalSubscriberRow;
        }
        else{
            previousRow = [SELECT 
                           	Id,Name,TestTotalSubscriberDate__c,TestTotalSubscriberTotal__c 
                           FROM 
                           	TestTotalSubscribers__c 
                           WHERE 
                           	TestTotalSubscriberDate__c <=:newDate 
                           ORDER BY 
                           	TestTotalSubscriberDate__c 
                           DESC 
                           LIMIT 1];
        }
               
        if(previousRow!=NULL){
        	
        	TestTotalSubscribersCurrent__c currentRowToUpdate= [SELECT 
                                                                	Id,Name,StartDate__c,CurrentTotalSubscribers__c 
                                                                FROM 
                                                                	TestTotalSubscribersCurrent__c 
                                                                WHERE 
                                                                	Month__c=:monthNumber 
                                                                LIMIT 1];
            if(currentRowToUpdate!=NULL){
        		/* update the date on the current object */
        		currentRowToUpdate.StartDate__c= previousRow.TestTotalSubscriberDate__c;
        		currentRowToUpdate.CurrentTotalSubscribers__c= previousRow.TestTotalSubscriberTotal__c;        
        
        		/* run update the current object */
        		update currentRowToUpdate;   
                }                                       
        }
    }
    
    public static void UpdateRetentionReportRowWithCurrentTotalRows(TestRetentionReport__c retentionReportRow){
        List<Integer> monthNumbers = new List<Integer> {1,3};
            for(Integer monthNumber:monthNumbers){
       			TestTotalSubscribersCurrent__c currentRow= [SELECT 
                                                                	Id,Name,StartDate__c,CurrentTotalSubscribers__c
                                                                FROM 
                                                                	TestTotalSubscribersCurrent__c 
                                                                WHERE 
                                                                	Month__c=:monthNumber 
                                                                LIMIT 1]; 
                if(monthNumber==1){
                    retentionReportRow.TestCurrentTotalSubscriber1MonthLookup__c=currentRow.Id;
                }else{
                    if(monthNumber==3){
                     retentionReportRow.TestCurrentTotalSubscriber3MonthLookup__c=currentRow.Id;   
                    }
                }
            update retentionReportRow;
        }
    }
}