/**
 * batch process looping through all the subscription (zuora_subscription__c) records and update the account
 * 1. marketo fields
 * 2. feed and market fields
 * 3. populate enterprise access right fields
 */
global class SubscriptionAfterUpdateBatch implements Database.Batchable<sObject> {
    
    String query;
    Set<Id> aids;
    
    global SubscriptionAfterUpdateBatch() {
        query = 'SELECT Id FROM Account';
    }

    /**
     * called by trigger handler
     */
    global SubscriptionAfterUpdateBatch(Set<Id> aids) {
        this.aids = aids;
        query = 'SELECT Id FROM Account WHERE Zuora__Account__c IN :aids';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Account> lstAct) {

        List<Zuora__Subscription__c> complete_zs_list = [SELECT 
                        Zuora__Account__r.VV_Market__c, Zuora__Account__r.VV_Feed__c, Zuora__ContractEffectiveDate__c, 
                        Zuora__Status__c, Zuora__CancelledDate__c, Zuora__Account__c, SuspendDate__c, ResumeDate__c, 
                        Zuora__TermSettingType__c, Zuora__Account__r.Subscription_Status__c,Zuora__AutoRenew__c,
                        Zuora__TermStartDate__c, Zuora__TermEndDate__c,Category__c,Zuora__ServiceActivationDate__c,
                        (SELECT Name,zuora__Product__r.SF_Product__r.AddOn_Type__c, zuora__Product__r.SF_Product__r.ismobile__c,zuora__Product__r.SF_Product__r.IsTrial__c,zuora__Product__r.SF_Product__r.feed__c,zuora__Product__r.SF_Product__r.Market__c,zuora__Product__r.SF_Product__r.Name,zuora__Product__r.SF_Product__r.ProductCode,Zuora__Description__c, Zuora__EffectiveStartDate__c, Zuora__EffectiveEndDate__c, Zuora__Price__c, Zuora__Type__c, Zuora__SubscriptionRatePlan__r.Zuora__OriginalProductRatePlanId__c, Market__c, Feed__c FROM Zuora__Subscription_Product_Charges__r ORDER BY Zuora__EffectiveStartDate__c) FROM Zuora__Subscription__c WHERE Zuora__Account__c IN :lstAct and  Zuora__TermStartDate__c <= TODAY Order By Zuora__Status__c DESC];

        // populate enterprise access right
       // SubscriptionTriggerHandler.populateEnterpriseAccessRight(complete_zs_list);

        SubscriptionTriggerHandler.updateMarketoFields(complete_zs_list);

    }

    global void finish(Database.BatchableContext BC) {
        
    }


    
}