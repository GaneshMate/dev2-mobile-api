@isTest
private class AddPaymentTest {
    @isTest static void testMethod1(){
         Account acc = new Account();
            acc.firstname='test acc1';
            acc.lastName = 'test acc';
            acc.billingCity ='Test City';
            acc.billingStreet = 'TestStreet';
            acc.billingstate ='TestState';
            acc.billingPostalcode ='23451';
            acc.billingcountry ='United States';
             Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            acc.recordtypeID=objAccrec.id;
           acc.PersonMailingCity='Test City';
            acc.PersonMailingStreet = 'TestStreet';
            acc.PersonMailingState ='TestState';
            acc.PersonMailingPostalCode ='23451';
            acc.PersonMailingCountry ='United States';
            //acc.ispersonAccount = true;
            insert acc;
             
            Opportunity opp = new Opportunity(Name=acc.Name + ' Opportunity ' ,
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=acc.Id);
                                       
            Product2 prod = new Product2(Name = 'VectorVest 7 EOD US Monthly', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_MONTHLY' , Family = 'Books');
            insert prod;
            Product2 prod2 = new Product2(Name = 'VectorVest 7 EOD US Annual', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_ANNUAL' , Family = 'Books');
            insert prod2;
            
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            /*CPQCommunity__c cpc = new CPQCommunity__c();
            cpc.Name = 'test';
            cpc.PriceBookId__c = pricebookId ;
            insert cpc ;*/
            
            
            
            SBQQ__Quote__c quoteRec = new SBQQ__Quote__c();
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SiteUser_Primary_Check__c = true;
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SBQQ__Opportunity2__c = opp.Id;
            //quoteRec.ZSB__BillToContact__c = c.Id;
           // quoteRec.ZSB__SoldToContact__c = c.Id;
            quoteRec.ZSB__BillCycleDay__c =  '1';
            quoteRec.ZSB__BillingBatch__c = 'Batch1';
            quoteRec.SBQQ__Account__c =acc.id;
            insert quoteRec ;
            SBQQ__QuoteLine__c ojQuoteline = new SBQQ__QuoteLine__c();
            ojQuoteline.SBQQ__Quote__c = quoteRec .id;
            ojQuoteline.SBQQ__Product__c = prod.id;
            insert ojQuoteline;
             Contract objContract = new Contract();
             objContract.SBQQ__Quote__c = quoteRec.id;
             objContract.SBQQ__Opportunity__c = opp.id;
             objcontract.Accountid=acc.id;
             insert objContract;
             Zuora__CustomerAccount__c objZCustAccount = new Zuora__CustomerAccount__c();
             objZCustAccount .Zuora__Account__c = acc.id;
             objZCustAccount .Zuora__Status__c ='Active';
             objZCustAccount .Zuora__BillToName__c ='test';
             //objZCustAccount .Zuora__CreditCard_Number__c = '4444333322221111';
             //objZCustAccount .Zuora__CreditCard_Expiration__c = '10/2019';
             insert objZCustAccount ;
             test.starttest();
             Apexpages.currentpage().getparameters().put('id',quoteRec.id);
        AddPayment obj = new AddPayment();
        obj.getExpirationYears();
        payment_method_sf__c objpaymentmethod = new payment_method_sf__c();
        objpaymentmethod.Payment_Type__c ='Credit Card';
        objpaymentmethod.Credit_card_type__c='Visa';
        objpaymentmethod.Credit_Card_Number__c ='4444333322221111';
        objpaymentmethod.Credit_Card_Holder_Name__c ='test';
        objpaymentmethod.Credit_Card_Expiration_Month__c ='09';
        objpaymentmethod.Credit_Card_Expiration_Year__c ='2019';
        objpaymentmethod.Credit_Card_CVV_Code__c ='123';
        obj.payment_method=objpaymentmethod;
        obj.credit_card_num =objpaymentmethod.Credit_Card_Number__c;
        obj.credit_card_cvv = objpaymentmethod.Credit_Card_CVV_Code__c; 
        obj.addPaymentMethod();
        obj.getCurrencySymbol();
        test.stoptest();
    }
    @isTest static void testMethod2(){
         Account acc = new Account();
            acc.firstname='test acc1';
            acc.lastName = 'test acc';
            acc.billingCity ='Test City';
            acc.billingStreet = 'TestStreet';
            acc.billingstate ='TestState';
            acc.billingPostalcode ='23451';
            acc.billingcountry ='United States';
             Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            acc.recordtypeID=objAccrec.id;
           acc.PersonMailingCity='Test City';
            acc.PersonMailingStreet = 'TestStreet';
            acc.PersonMailingState ='TestState';
            acc.PersonMailingPostalCode ='23451';
            acc.PersonMailingCountry ='United States';
            //acc.ispersonAccount = true;
            insert acc;
             
            Opportunity opp = new Opportunity(Name=acc.Name + ' Opportunity ' ,
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=acc.Id);
                                       
            Product2 prod = new Product2(Name = 'VectorVest 7 EOD US Monthly', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_MONTHLY' , Family = 'Books');
            insert prod;
            Product2 prod2 = new Product2(Name = 'VectorVest 7 EOD US Annual', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_ANNUAL' , Family = 'Books');
            insert prod2;
            
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            /*CPQCommunity__c cpc = new CPQCommunity__c();
            cpc.Name = 'test';
            cpc.PriceBookId__c = pricebookId ;
            insert cpc ;*/
            
            
            
            SBQQ__Quote__c quoteRec = new SBQQ__Quote__c();
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SiteUser_Primary_Check__c = true;
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SBQQ__Opportunity2__c = opp.Id;
            //quoteRec.ZSB__BillToContact__c = c.Id;
           // quoteRec.ZSB__SoldToContact__c = c.Id;
            quoteRec.ZSB__BillCycleDay__c =  '1';
            quoteRec.ZSB__BillingBatch__c = 'Batch1';
            quoteRec.SBQQ__Account__c =acc.id;
            insert quoteRec ;
            SBQQ__QuoteLine__c ojQuoteline = new SBQQ__QuoteLine__c();
            ojQuoteline.SBQQ__Quote__c = quoteRec .id;
            ojQuoteline.SBQQ__Product__c = prod.id;
            insert ojQuoteline;
             Contract objContract = new Contract();
             objContract.SBQQ__Quote__c = quoteRec.id;
             objContract.SBQQ__Opportunity__c = opp.id;
             objcontract.Accountid=acc.id;
             insert objContract;
             Zuora__CustomerAccount__c objZCustAccount = new Zuora__CustomerAccount__c();
             objZCustAccount .Zuora__Account__c = acc.id;
             objZCustAccount .Zuora__Status__c ='Active';
             objZCustAccount .Zuora__BillToName__c ='test';
             //objZCustAccount .Zuora__CreditCard_Number__c = '4444333322221111';
             //objZCustAccount .Zuora__CreditCard_Expiration__c = '10/2019';
             insert objZCustAccount ;
             test.starttest();
             Apexpages.currentpage().getparameters().put('id',quoteRec.id);
        AddPayment obj = new AddPayment();
        obj.getExpirationYears();
        payment_method_sf__c objpaymentmethod = new payment_method_sf__c();
        objpaymentmethod.Payment_Type__c ='Credit Card';
        objpaymentmethod.Credit_card_type__c='Visa';
        //objpaymentmethod.Credit_Card_Number__c ='4444333322221111';
        objpaymentmethod.Credit_Card_Holder_Name__c ='test';
        objpaymentmethod.Credit_Card_Expiration_Month__c ='09';
        objpaymentmethod.Credit_Card_Expiration_Year__c ='2019';
        objpaymentmethod.Credit_Card_CVV_Code__c ='123';
        obj.payment_method=objpaymentmethod;
        obj.credit_card_num =objpaymentmethod.Credit_Card_Number__c;
        obj.credit_card_cvv = objpaymentmethod.Credit_Card_CVV_Code__c; 
        obj.addPaymentMethod();
        obj.getCurrencySymbol();
        test.stoptest();
    }
    @isTest static void testMethod3(){
         Account acc = new Account();
            acc.firstname='test acc1';
            acc.lastName = 'test acc';
            acc.billingCity ='Test City';
            acc.billingStreet = 'TestStreet';
            acc.billingstate ='TestState';
            acc.billingPostalcode ='23451';
            acc.billingcountry ='United States';
             Recordtype objAccrec =[select id from Recordtype where sobjecttype='Account' and name='person Account' limit 1];
            acc.recordtypeID=objAccrec.id;
           acc.PersonMailingCity='Test City';
            acc.PersonMailingStreet = 'TestStreet';
            acc.PersonMailingState ='TestState';
            acc.PersonMailingPostalCode ='23451';
            acc.PersonMailingCountry ='United States';
            //acc.ispersonAccount = true;
            insert acc;
             
            Opportunity opp = new Opportunity(Name=acc.Name + ' Opportunity ' ,
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=acc.Id);
                                       
            Product2 prod = new Product2(Name = 'VectorVest 7 EOD US Monthly', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_MONTHLY' , Family = 'Books');
            insert prod;
            Product2 prod2 = new Product2(Name = 'VectorVest 7 EOD US Annual', Feed__c = 'EOD',Market__c = 'US',
                             ProductCode = 'VV_7.0_EOD_US_ANNUAL' , Family = 'Books');
            insert prod2;
            
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            /*CPQCommunity__c cpc = new CPQCommunity__c();
            cpc.Name = 'test';
            cpc.PriceBookId__c = pricebookId ;
            insert cpc ;*/
            
            
            
            SBQQ__Quote__c quoteRec = new SBQQ__Quote__c();
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SiteUser_Primary_Check__c = true;
            quoteRec.SBQQ__Primary__c = false;
            quoteRec.SBQQ__Opportunity2__c = opp.Id;
            //quoteRec.ZSB__BillToContact__c = c.Id;
           // quoteRec.ZSB__SoldToContact__c = c.Id;
            quoteRec.ZSB__BillCycleDay__c =  '1';
            quoteRec.ZSB__BillingBatch__c = 'Batch1';
            quoteRec.SBQQ__Account__c =acc.id;
            insert quoteRec ;
            SBQQ__QuoteLine__c ojQuoteline = new SBQQ__QuoteLine__c();
            ojQuoteline.SBQQ__Quote__c = quoteRec .id;
            ojQuoteline.SBQQ__Product__c = prod.id;
            insert ojQuoteline;
             Contract objContract = new Contract();
             objContract.SBQQ__Quote__c = quoteRec.id;
             objContract.SBQQ__Opportunity__c = opp.id;
             objcontract.Accountid=acc.id;
             insert objContract;
             Zuora__CustomerAccount__c objZCustAccount = new Zuora__CustomerAccount__c();
             objZCustAccount .Zuora__Account__c = acc.id;
             objZCustAccount .Zuora__Status__c ='Active';
             objZCustAccount .Zuora__BillToName__c ='test';
             //objZCustAccount .Zuora__CreditCard_Number__c = '4444333322221111';
             //objZCustAccount .Zuora__CreditCard_Expiration__c = '10/2019';
             insert objZCustAccount ;
             test.starttest();
             Apexpages.currentpage().getparameters().put('id',quoteRec.id);
        AddPayment obj = new AddPayment();
        obj.getExpirationYears();
        payment_method_sf__c objpaymentmethod = new payment_method_sf__c();
        objpaymentmethod.Payment_Type__c ='ACH';
        /*objpaymentmethod.Credit_card_type__c='Visa';
        objpaymentmethod.Credit_Card_Number__c ='4444333322221111';
        objpaymentmethod.Credit_Card_Holder_Name__c ='test';
        objpaymentmethod.Credit_Card_Expiration_Month__c ='09';
        objpaymentmethod.Credit_Card_Expiration_Year__c ='2019';
        objpaymentmethod.Credit_Card_CVV_Code__c ='123';
        obj.payment_method=objpaymentmethod;
        obj.credit_card_num =objpaymentmethod.Credit_Card_Number__c;
        obj.credit_card_cvv = objpaymentmethod.Credit_Card_CVV_Code__c; */
        obj.addPaymentMethod();
        obj.getCurrencySymbol();
        test.stoptest();
    }
    @isTest static void testMethod4(){
        AddPayment obj = new AddPayment();
        obj.Tmethod1();
        obj.Tmethod2();
    }
    
}