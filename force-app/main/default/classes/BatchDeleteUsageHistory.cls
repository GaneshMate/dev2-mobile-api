global class BatchDeleteUsageHistory implements Database.Batchable<sObject>{
    global final String mQuery;
    
    global BatchDeleteUsageHistory(String query){
        mQuery=query;
    }

    global Database.QueryLocator Start(Database.BatchableContext batchableContext){
        return Database.getQueryLocator(mQuery);
    }

    global void execute(Database.BatchableContext batchableContext,
                        List<Usage_History__c> scope){
        delete scope;
    }

    global void finish(Database.BatchableContext batchableContext){}

}