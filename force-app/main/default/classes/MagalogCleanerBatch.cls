global class MagalogCleanerBatch implements Database.Batchable<sObject>,Database.Stateful{
    
    global final String Query;
    global integer Summary;
    global final String email= !String.isBlank(system.Label.Email_Id) ? system.Label.Email_Id :'test@email.com' ;
    
    global MagalogCleanerBatch(){
        if(test.isRunningTest()){
            this.Query='SELECT Id FROM Magalog_Finder__c';
        }else{
            this.Query='SELECT Id FROM Magalog_Finder__c WHERE CreatedDate != LAST_N_DAYS:180';
        }
        Summary = 0;
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Magalog_Finder__c> magalogFinderList){
        Database.DeleteResult[] drList = database.delete(magalogFinderList);
        // Iterate through each returned result
        integer i = 0;
        for(Database.DeleteResult dr : drList) {
            if (dr.isSuccess()) {
                i++;
                
            }
            
        }
        Summary = Summary + i;
    }
    
    global void finish(Database.BatchableContext BC){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        mail.setToAddresses(new String[] {email});
        mail.setReplyTo('batch@vieosolutions.com');
        mail.setSenderDisplayName('MagalogCleanerBatch Processing');
        mail.setSubject('MagalogCleanerBatch Process Completed');
        mail.setPlainTextBody(Summary+'  records are deleted successfully!');
        //It will send total number of records are deleted by batch
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        
    }
}