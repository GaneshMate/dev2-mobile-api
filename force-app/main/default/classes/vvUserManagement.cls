public class vvUserManagement {
    
    // Ganesh:: 22 NOV 2018 :: created a new boolean to identify a mobile user or not.
    public static Boolean isAppleCustomer = false;
    @future
    public static void activateUser(string userid){
        User objuser = new User(id=Userid,IsActive=true);
        update objuser;
    }
   
      /**
     * create community user called from community self registration or from CSR account creation page or Webservice
     */
    public static boolean isEmailNeedtoSend=true;
    // Ganesh:: 22 NOV 2018 :: created a new method to identify a mobile user.
     public static User createCommunityUser(Id accountId, Boolean isCommunity, Boolean isAppleCustomerFlag) {
         // Ganesh:: 22 NOV 2018 :: isAppleCustomerFlag assign to isAppleCustomer.
         isAppleCustomer = isAppleCustomerFlag;
        return createCommunityUser(accountId, isCommunity);
     }
    public static User createCommunityUser(Id accountId, Boolean isCommunity) {
        
        // determine profile id to use
        VVCommunitySettings__c cs = VVCommunitySettings__c.getOrgDefaults();
        String profileName;
        
        Account a = [select PersonContactId, Is_Apple_App_Store_Customer__c, Password__c, FirstName, LastName, Name, PersonEmail, IsCustomerPortal, Vector_Vest_Customer_Id__c from Account where Id = :accountId];
        // Ganesh:: 22 NOV 2018 :: check isAppleCustomer is true means it's a mobile user.
        if(isAppleCustomer == true){
            profileName = cs.Mobile_Customer_Profile__c;
        }
        else{
            profileName = cs.Customer_Profile__c <> null ? cs.Customer_Profile__c : 'VV-Customer Community Login User';
        }
        System.debug('*******a*****'+a);
         System.debug('*******cs*****'+cs);
        System.debug('*******cs Mobile_Customer_Profile__c*****'+cs.Mobile_Customer_Profile__c);
        System.debug('*******profileName*****'+profileName);
        Id profileId = [select Id from Profile WHERE Name = :profileName].Id;
        System.debug('*)))***********'+profileId);
        System.debug('*******a*****'+a);
        System.debug('*******a*****'+a.Password__c);
        if (a.IsCustomerPortal) {
            // already a portal user
            return null;
        }
        
        User userObj                = new User();
        String lang                 = 'en_US';
        String userPrefix           = a.PersonEmail.substring(0, a.PersonEmail.indexOf('@'));
        /*if(a.personemail != a.Vector_Vest_Customer_Id__c){
            a.Vector_Vest_Customer_Id__c =a.personemail;
        }*/
        if(!String.isBlank(String.valueOf(a.Vector_Vest_Customer_Id__c))){
            if(a.Vector_Vest_Customer_Id__c.contains('@'))
                userObj.Username        = a.Vector_Vest_Customer_Id__c;   
            else{
                userObj.Username        = a.Vector_Vest_Customer_Id__c+'@vectorvest-salesforce.com';
                system.debug('currentpage'+ApexPages.currentPage());
                //if (ApexPages.currentPage() != null && ApexPages.currentPage().getHeaders() != null && ApexPages.currentPage().getHeaders().get('Host')!= null && ApexPages.currentPage().getHeaders().get('Host').containsIgnoreCase('partial')) 
                   if(UserInfo.getOrganizationId() == '00D1100000Bv1hj' || UserInfo.getOrganizationId()=='00D1100000Bv1hjEAB')
                   userObj.Username += '.partial1';
            }
              
        }else{
            userObj.Username        = a.PersonEmail;
        }
        userObj.FirstName           = a.FirstName;
        userObj.LastName            = a.LastName;
        userObj.Email               = a.PersonEmail;
        userObj.contactId           = a.PersonContactId;
        userObj.CommunityNickname   = a.PersonEmail.abbreviate(19) + System.now().millisecond();
        userObj.LanguageLocaleKey   = lang;
        userObj.ProfileId           = profileId;
        userObj.emailencodingkey    = 'UTF-8';
        userObj.timezonesidkey      = 'America/New_York';
        userObj.localesidkey        = lang;
        userObj.Alias               = (userPrefix.length() > 8 ? userPrefix.substring(0, 8) : userPrefix);
        userObj.IsActive            = true;

        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.EmailHeader.triggerUserEmail = true;       
        dmo.EmailHeader.triggerOtherEmail = false;
        dmo.EmailHeader.triggerAutoResponseEmail = false;       
        dmo.optAllOrNone = false;
        userObj.setOptions(dmo);

            
        if (a.Password__c == null) { // from csr, don't send email
            insert userObj;
        } else {
            if (isCommunity) {
                Site.createExternalUser(userObj, accountId, a.Password__c);
            } else {
                
                if (a.Is_Apple_App_Store_Customer__c == true) {
                    // AK :: Changed 
                    System.debug('AK :: createExternalUser :: ' + userObj);
                    //Site.createPersonAccountPortalUser(userObj, '005360000013WoN', a.Password__c);
                    //Site.createExternalUser(userObj, accountId, a.Password__c);
                    insert userObj;
                    //System.setPassword(userObj.Id, a.Password__c);
                    //setPassword(userObj.Id, accountId,a.Password__c);
                } else {
                    insert userObj;
                    System.setPassword(userObj.Id, a.Password__c);
                    Account tempa = new Account(Id=accountId,Password__c=null);
                    System.debug('**userObj.id*****'+userObj.id);
                    update tempa;
                }
                
                
             System.debug('*******userObj*****'+userObj);
            }
            
        }

        return userObj;
    }
    //changed by atul: 23-feb-18
    /*@future      
    public static void setPassword(String userId, String accountId, string password ) {
        User users = [SELECT Id FROM User Where id  = :userId];
        Account account = [SELECT Id, Password__c FROM Account WHERE Id = :accountId];
        System.setPassword(users.Id, password);
        account.Password__c=null;
        System.debug('**userObj.id*****'+users.id);
        update account;
    }*/
}