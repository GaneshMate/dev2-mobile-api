/*Class to deep clone an existing clone*/
public without sharing class CloneQuote
{
    private String QuoteIdToClone{get;set;}
    private Map<string,Object> QuoteFieldUpdateBeforeInsert = new Map<string,Object>();
    private Boolean updateStartEnddates {get;set;}
    public CloneQuote(String qId)
    {
        QuoteIdToClone = qId;
        updateStartEnddates = false;
    }
    public CloneQuote(String qId,Map<string,Object> QuoteFieldValueMap)
    {
        QuoteIdToClone = qId;
        QuoteFieldUpdateBeforeInsert = QuoteFieldValueMap;
        QuoteFieldUpdateBeforeInsert.put('Custom_Billing_Account__c',null);
        updateStartEnddates = false;
    }
    public CloneQuote(String qId,Map<string,Object> QuoteFieldValueMap,Boolean changeDates)
    {
        system.debug('eneter in clone quote');
        QuoteIdToClone = qId;
        QuoteFieldUpdateBeforeInsert = QuoteFieldValueMap;
        QuoteFieldUpdateBeforeInsert.put('Custom_Billing_Account__c',null);
        updateStartEnddates = changeDates;
    }
    // Clone whole quote and insert
    public sObject doIt()
    {
       system.debug('enter in doit');
        sObject newQuote = cloneExistingQuote();
       newQuote  = updateSObjectField(newQuote ,QuoteFieldUpdateBeforeInsert );
       newQuote.put('SBQQ__Opportunity2__c',null);
       //newQuote.put('SBQQ__Opportunity2__c',QuoteFieldUpdateBeforeInsert.get('SBQQ__Opportunity2__c'));
       System.debug('NQuote------>>>'+newQuote);
       SBQQ__Quote__c objnewQuote = (SBQQ__Quote__c)newquote;
       system.debug('startdateeee'+objnewQuote.SBQQ__StartDate__c);
       if(objnewQuote.SBQQ__StartDate__c < Date.today()){
          newQuote.put('SBQQ__StartDate__c',Date.Today());
        } 
       
       
       insert newQuote;
       
       /*---Done this change as it was giving error on insert for customer portal user---*/
       
      newQuote.put('SBQQ__Opportunity2__c',QuoteFieldUpdateBeforeInsert.get('SBQQ__Opportunity2__c'));
      
      update newQuote;
       
       //commnted as part of new comm
      System.debug('NQuote 2------>>>'+newQuote.id);
       system.debug('newquotee'+newQuote.get('SBQQ__StartDate__c'));
      system.debug('newquotee'+objnewQuote.SBQQ__StartDate__c);
       Date qStartDate = newQuote.get('SBQQ__StartDate__c')!=null?(Date)newQuote.get('SBQQ__StartDate__c'):null;
       List<sObject> allQuoteLines = updateStartEnddates && qStartDate!=null  ? cloneQuoteLines(newQuote.id,qStartDate ):cloneQuoteLines(newQuote.id);
       //adding code to Populate Quote line Startdate
           SBQQ__Quote__c objQuote =[select id, SBQQ__StartDate__c,Trial_End_Date__c from SBQQ__Quote__c where id=:newQuote.id];
           set<id> SetProductids = new set<id>();
           for(Sobject objQline : allQuoteLines){
               SetProductids.Add((String)objQline.get('SBQQ__Product__c'));
           }
           map<id, string> mapProductwithSubtype = new map<id,string>();
           Boolean trialexist= false;
           for(Product2 objproduct :[select id,Istrial__c, SBQQ__SubscriptionType__c from Product2 where id in:SetProductids]){
               mapProductwithSubtype.put(objproduct.id,objProduct.SBQQ__SubscriptionType__c );
               if(objproduct.Istrial__c ==true){
                   trialexist = true;
               }
           }
           if(trialexist ==true){
           for(Sobject objQuoteline : allQuoteLines){
               id prodid =(Id)ObjQuoteline.get('SBQQ__Product__c');
               if(mapProductwithSubtype.get(prodid)=='Renewable'){
                   objQuoteline.put('SBQQ__StartDate__c',objQuote.Trial_End_Date__c);
               }
           }
           }
       //adding end
       insert allQuoteLines ;
      // newQuote.put('QuoteCalFields__c','test');
       //update newQuote;
       populateRequiredByForQuoteLines(allQuoteLines);
       
       List<sObject> allQuoteGroups = cloneQuoteGroups(newQuote.id);
       insert allQuoteGroups ;
       
       List<sObject> allQuoteTerms = cloneQuoteTerms(newQuote.id);
       insert allQuoteTerms ;
       //end new comm
       
       return newQuote;
    }
    
    public void populateRequiredByForQuoteLines(List<sObject> allQuoteLines)
    {
        Map<string,string> sourceNewQLIdMap = new Map<string,string>();
        for(sObject sObj : allQuoteLines )
        {
            string cloneSrcId = sObj.getCloneSourceId();
            string sObjid = (String)sObj.get('id');
            if(!String.isEmpty(cloneSrcId)){
                sourceNewQLIdMap.put(cloneSrcId,sObjid);
            }            
        }
        List<sObject> updateQLs = new List<sObject>();
        for(sObject sObj : allQuoteLines )
        {
            if(sObj.get('SBQQ__RequiredBy__c')!=null)
            {
                string sObjRequiredById = sObj.get('SBQQ__RequiredBy__c')!=null?(String)sObj.get('SBQQ__RequiredBy__c'):'';
                if(sourceNewQLIdMap.containsKey(sObjRequiredById))
                {
                    sObj.put('SBQQ__RequiredBy__c',sourceNewQLIdMap.get(sObjRequiredById));
                    updateQLs.add(sObj);
                }
            }          
        }
        update updateQLs;    
    }
    // Update Object fields with predefined values
    public sObject updateSObjectField(sObject obj,Map<string,Object> objFieldsvalue)
    {
        for(string objField : objFieldsvalue.keySet())
        {
            obj.put(objField,objFieldsvalue.get(objField));
        }
        
        return obj;
    }
    public sObject cloneExistingQuote()
    {
        String quoteQueryStr = 'select '+String.Join(getAllCustomFields(SBQQ__Quote__c.sObjectType),',')+' FROM SBQQ__Quote__c where id = :QuoteIdToClone';
        List<SBQQ__Quote__c> allQuote = Database.query(quoteQueryStr);
        sObject q = new SBQQ__Quote__c();
        if(allQuote.size()==1)
        {
           q = allQuote[0].clone(false,true,false,false);           
        }       
        return q;
    }
    
    public List<sObject> cloneQuoteLines(String newQuoteId)
    {
        String quoteLineQueryStr = 'select '+String.Join(getAllCustomFields(SBQQ__QuoteLine__c.sObjectType),',')+' FROM SBQQ__QuoteLine__c where SBQQ__Quote__c = :QuoteIdToClone';
        List<SBQQ__QuoteLine__c> allQuoteLines = Database.query(quoteLineQueryStr);
                
        List<sObject> quoteLinesToClone = new List<sObject>();
        for(sObject qLine : allQuoteLines) {
            sObject newQuoteLine= new SBQQ__QuoteLine__c();
            newQuoteLine = qLine.clone(false,true,false,false);
            newQuoteLine.put('SBQQ__Quote__c',newQuoteId);
            quoteLinesToClone.add(newQuoteLine);
        }
        return quoteLinesToClone;
    }
    public List<sObject> cloneQuoteLines(String newQuoteId,Date startDate)
    {
        String quoteLineQueryStr = 'select '+String.Join(getAllCustomFields(SBQQ__QuoteLine__c.sObjectType),',')+' FROM SBQQ__QuoteLine__c where SBQQ__Quote__c = :QuoteIdToClone';
        List<SBQQ__QuoteLine__c> allQuoteLines = Database.query(quoteLineQueryStr);
                
        List<sObject> quoteLinesToClone = new List<sObject>();
        for(sObject qLine : allQuoteLines) {
            sObject newQuoteLine= new SBQQ__QuoteLine__c();
            newQuoteLine = qLine.clone(false,true,false,false);
            newQuoteLine.put('SBQQ__Quote__c',newQuoteId);
            if(startDate!=null && qLine.get('SBQQ__StartDate__c')!=null)
            {
                if(qLine.get('SBQQ__EndDate__c')!=null)
                {
                    Date sDate = (Date)qLine.get('SBQQ__StartDate__c');
                    Date eDate = (Date)qLine.get('SBQQ__EndDate__c');
                    Integer numDays = sDate.daysBetween(eDate);
                    qLine.put('SBQQ__StartDate__c',Date.today());
                    qLine.put('SBQQ__EndDate__c',Date.today().addDays(numDays));
                    //added below two lines
                    //newQuoteLine.put('SBQQ__StartDate__c',Date.today());
                    //added below line
                    system.debug('startdateeee not nul'+startdate);
                    newQuoteLine.put('SBQQ__StartDate__c',startDate);
                    newQuoteLine.put('SBQQ__EndDate__c',null);
                    //newQuoteLine.put('SBQQ__EndDate__c',Date.today().addDays(numDays));
                    
                }else
                {
                     system.debug('startdateeee  nul'+startdate);
                    qLine.put('SBQQ__StartDate__c',Date.today());
                    //added below line
                    //newQuoteLine.put('SBQQ__StartDate__c',Date.today());
                    newQuoteLine.put('SBQQ__StartDate__c',startDate);
                }
            }           
            quoteLinesToClone.add(newQuoteLine);
        }
        return quoteLinesToClone;
    }
    
    public List<sObject> cloneQuoteGroups(String newQuoteId)
    {
        String quoteGroupQueryStr = 'select '+String.Join(getAllCustomFields(SBQQ__QuoteLineGroup__c.sObjectType),',')+' FROM SBQQ__QuoteLineGroup__c where SBQQ__Quote__c = :QuoteIdToClone';
        List<SBQQ__QuoteLineGroup__c> allQuoteGroups = Database.query(quoteGroupQueryStr );
                
        List<sObject> quoteGroupsToClone = new List<sObject>();
        for(sObject qGroup : allQuoteGroups ) {
            sObject newQuoteGroup = qGroup .clone(false,true,false,false);
            newQuoteGroup.put('SBQQ__Quote__c',newQuoteId);
            quoteGroupsToClone .add(newQuoteGroup);
        }
        return quoteGroupsToClone ;        
    }
    
    public List<sObject> cloneQuoteTerms(String newQuoteId)
    {
        String quoteTermQueryStr = 'select '+String.Join(getAllCustomFields(SBQQ__QuoteTerm__c.sObjectType),',')+' FROM SBQQ__QuoteTerm__c where SBQQ__Quote__c = :QuoteIdToClone';
        List<SBQQ__QuoteTerm__c> allQuoteTerms = Database.query(quoteTermQueryStr );
                
        List<sObject> quoteTermsToClone = new List<sObject>();
        for(sObject qTerm : allQuoteTerms) {
            sObject newQuoteTerm = qTerm.clone(false,true,false,false);
            newQuoteTerm .put('SBQQ__Quote__c',newQuoteId);
            quoteTermsToClone.add(newQuoteTerm );
        }
        return quoteTermsToClone ;       
    }
    
    public List<string> getAllCustomFields(Schema.sObjectType objType)
    {
        List<string> listObjectFields = new List<String>();
        Schema.DescribeSObjectResult objDesc = objType.getDescribe();                
        Map<String, Schema.SObjectField> objFields = objDesc.fields.getMap();
             
        for(Schema.sObjectField eachField :objFields.values()){                      
            //if(eachField.getDescribe().isCustom()){
                listObjectFields.add(eachField.getDescribe().getName());
            //}                        
        }
        
        return listObjectFields;
    }
}