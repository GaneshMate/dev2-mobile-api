@RestResource(UrlMapping='/customPriceCalculation/*')
global class CustomPriceCalculation {

     private static string serialize(object o) {
        return JSON.serialize(o);
    }
    global class Error {
        global integer code;
        global string msg;
        global Error(integer code, string msg) {
            this.code = code;
            this.msg = msg;
        }
    }
    private static string Product_Name  = 'productName';
    @HttpGet
    global static void doGet(){
       
        /*string retBody = '';
        RestRequest req = RestContext.request;
        RestResponse res = Restcontext.response;
        retBody = serialize('1800');
        res.headers.put('Content-type', 'application/json'); 
        res.statusCode = 200;
        res.responseBody = Blob.valueOf(retBody);
        */
        
        integer retCode = 0;
        string retBody = '';
        RestRequest req = RestContext.request;
        RestResponse res = Restcontext.response;
       
        String ProductN =req.params.get(Product_Name);
        System.debug('*****ProductN***'+ProductN);
        if(!String.isBlank(ProductN)){
      		List<product2> productList = getProduct(ProductN);
            if(productList !=null && !productList.isEmpty()){
                retCode = 200;
                retBody = serialize(productList);    
            }else{
                retCode = 404;
                retBody = serialize(new Error(retCode, 'There are no products!'));
            }
        }else{
            retCode = 400;
           
            retBody = serialize(new Error(retCode, 'Product name is required!'));
            System.debug('*****in else***'+retCode);
        }
        res.headers.put('Content-type', 'application/json'); 
        res.statusCode = retCode;
        res.responseBody = Blob.valueOf(retBody);
    }
 private static List<product2> getProduct(String ProductName){
        
        List<product2> product = new List<product2>();
        if(!String.isBlank(ProductName)){
            product =[select (SELECT UnitPrice FROM PricebookEntries)  from product2 where name=:ProductName];
        }

         return product;  
    }
}