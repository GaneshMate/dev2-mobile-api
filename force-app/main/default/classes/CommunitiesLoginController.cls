/**
* An apex page controller that exposes the site login functionality
*
*/
global with sharing class CommunitiesLoginController {
    global String username {get; set;}
    global String password {get; set;}
    global PageReference forgotpasswordcall(){
        PageReference pr = Page.ForgotPassword;
            pr.setRedirect(true);
        return pr;
    }
    global PageReference login() {
        String promocode = ApexPages.currentPage().getParameters().get('promo');
        String startUrl = '/CommunitiesUserCentral' + (String.isBlank(promocode) ? '' : '?promo=' + promocode);
        
       
        if (username != null && !username.contains('@')) {
            System.debug('*****username****'+username);
            
            username += '@vectorvest-salesforce.com';
            if (ApexPages.currentPage().getHeaders().get('Host').containsIgnoreCase('partial')) username += '.partial1';
        }
        
        //added by atul:22-march-2018
        List<User> userIdlist = new List<User>();
        if(!String.isBlank(username)){
            userIdlist = [SELECT Id FROM user WHERE Username =:username];
            System.debug('*****userIdlist****'+userIdlist);
        }
        // Find the license record to SBCPQ package
        PackageLicense pl = [SELECT Id FROM PackageLicense WHERE NamespacePrefix = 'SBQQ'];
        System.debug('***pl******'+pl);
        if(userIdlist != null && !userIdlist.isEmpty() && pl != null){
            //Assign salesforce CPQ license 
            List<UserPackageLicense> usrPckgLicenseList = [SELECT PackageLicenseId, UserId FROM UserPackageLicense WHERE PackageLicenseId = :pl.Id];
            System.debug('*****usrPckgLicenseList****'+usrPckgLicenseList);
            if(usrPckgLicenseList != null && !usrPckgLicenseList.isEmpty()){
                list<UserPackageLicense> lstUserPackageLicense  = new list<UserPackageLicense>();
                lstUserPackageLicense =[select id from UserPackageLicense where PackageLicenseId =: usrPckgLicenseList[0].PackageLicenseId and UserId =: userIdlist[0].Id];
                if(lstUserPackageLicense.size()==0){
                UserPackageLicense oUserPackageLicense = new UserPackageLicense(); 
                oUserPackageLicense.PackageLicenseId = usrPckgLicenseList[0].PackageLicenseId;
                oUserPackageLicense.UserId = userIdlist[0].Id;
                insert oUserPackageLicense;
                System.debug('*****oUserPackageLicense****'+oUserPackageLicense); 
                }   
                   
            }
            
            //Assign permission set to user
            List<PermissionSet> ps1 = [SELECT Id FROM PermissionSet WHERE NamespacePrefix = 'SBQQ' AND Name = 'SteelBrickCPQCustomerUser'];
            list<PermissionSetAssignment> lstpermissionset = new list<PermissionSetAssignment>();
            
            
            
            System.debug('**ps1***'+ps1);
            if(ps1 != null && !ps1.isEmpty() ){
                lstpermissionset =[select id,AssigneeId,PermissionSetId from PermissionSetAssignment where AssigneeId =: userIdlist[0].Id and PermissionSetId =: ps1[0].Id];
                if(lstpermissionset.size()==0){
                    PermissionSetAssignment psas = new PermissionSetAssignment();
                    psas.AssigneeId = userIdlist[0].Id;
                    psas.PermissionSetId = ps1[0].Id;
                    insert psas;
                    System.debug('*****psas****'+psas);  
                } 
            }
            
        }
        
        
        system.debug('Username##'+username);
        system.debug('password##'+password);
        string UsernameCopy = username;
        if(username.contains('@vectorvest-salesforce.com')){
            username = username.split('@')[0];
        }
        return Site.login(UsernameCopy, password, startUrl);
    }
    
    global CommunitiesLoginController () {}
}