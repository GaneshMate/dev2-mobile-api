@isTest
private class UserSetPasswordBatchTest {
    @testSetup static void init() {
                User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

        // create user with role
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
            

        User portalAccountOwner1 = new User(
                UserRoleId = portalRole.Id,
                ProfileId = profile1.Id,
                Username = System.now().millisecond() + 'test4@test.com',
                Alias = 'batman',
                Email='bruce.wayne@wayneenterprises.com',
                EmailEncodingKey='UTF-8',
                Firstname='Bruce',
                Lastname='Wayne',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                TimeZoneSidKey='America/Chicago'
            );

        System.runAs ( thisUser ) {
            insert portalAccountOwner1;
        }


        // create account with bad password
        //Account a = new Account(Password__c='123',OwnerId=portalAccountOwner1.Id,FirstName='testfirst',LastName='testlast',PersonEmail='vvusertest@vvsystemtest.com',VV_Market__c='US',VV_Feed__c='EOD',VV_Edition__c='Non-Professional',Subscription_Plan__c='Monthly',Is_Elite__c=false,Is_Elite_Legacy__c=false);
        //insert a;
        // create account
        Account a = new Account(PersonMailingCity='Test City',PersonMailingStreet = 'TestStreet',PersonMailingState='TestState',PersonMailingPostalCode ='23451',personMailingCountry ='United States',Primary_Phone_Type__c='Mobile',OwnerId=portalAccountOwner1.Id,FirstName='testfirst',LastName='testlast',PersonEmail='testveryunique@test.vvtest.com',Phone='2223334444',VV_Market__c='US',VV_Second_Market__c='CA',VV_Feed__c='ID',VV_Edition__c='Non-Professional',Subscription_Plan__c='Annual',Is_Elite__c=false,Is_Elite_Legacy__c=false,Is_Apple_App_Store_Customer__c=false,BillingStreet='2234 test',BillingCity='Test',BillingState='GA',BillingCountry='USA',BillingPostalCode='22344');
        insert a;


        Contact c = [SELECT FirstName, LastName, Email FROM Contact WHERE AccountId = :a.Id];
        // create user
        Id profileId = [select Id from Profile WHERE Name = 'Customer Community Login User'].Id;

        User userObj                = new User();
        String lang                 = 'en_US';
        String userPrefix           = c.Email.substring(0, c.Email.indexOf('@'));

        userObj.Username            = c.Email;
        userObj.FirstName           = c.FirstName;
        userObj.LastName            = c.LastName;
        userObj.Email               = c.Email;
        userObj.contactId           = c.Id;
        userObj.CommunityNickname   = c.Email.abbreviate(19) + string.valueof(DateTime.now());
        userObj.LanguageLocaleKey   = lang;
        userObj.ProfileId           = profileId;
        userObj.emailencodingkey    = 'UTF-8';
        userObj.timezonesidkey      = 'America/New_York';
        userObj.localesidkey        = lang;
        userObj.Alias               = (userPrefix.length() > 8 ? userPrefix.substring(0, 8) : userPrefix);
        userObj.IsActive            = true;

        insert userObj;
    }

    @isTest static void testBatchBadPass() {

        Test.startTest();
        Database.executeBatch(new UserSetPasswordBatch());
        Test.stopTest();

    }

    @isTest static void testBatchGoodPass() {

        Account a = [SELECT Password__c FROM Account WHERE FirstName = 'testfirst'];
        a.Password__c = '!qaz2WSXde';
        update a;

        Test.startTest();
        Database.executeBatch(new UserSetPasswordBatch());
        Test.stopTest();

    }

    @isTest static void testBatchCustomQuery() {

        Account a = [SELECT Password__c FROM Account WHERE FirstName = 'testfirst'];
        a.Password__c = '!qaz2WSXde';
        update a;
        
        Test.startTest();
        Database.executeBatch(new UserSetPasswordBatch('SELECT Id,Username,AccountId,Contact.Account.Password__c FROM User WHERE AccountId <> null and Contact.Account.Password__c <> null'));
        Test.stopTest();

    }
     @isTest static void testUserSetPasswordSHRBatch() {

        Account a = [SELECT Password__c FROM Account WHERE FirstName = 'testfirst'];
        a.Password__c = '!qaz2WSXde';
        update a;
        
        Test.startTest();
        Database.executeBatch(new UserSetPasswordSHRBatch('SELECT Id,Username,AccountId,Contact.Account.Password__c FROM User WHERE AccountId <> null and Contact.Account.Password__c <> null'));
        Test.stopTest();

    }
    @isTest static void testUserSetPasswordSHRBatch1() {
    ZSubscriptionProductChargeTriggerHandler obj= new ZSubscriptionProductChargeTriggerHandler();
        Account a = [SELECT Password__c FROM Account WHERE FirstName = 'testfirst'];
        a.Password__c = '!qaz2WSXde';
        update a;
        
        Test.startTest();
        Database.executeBatch(new UserSetPasswordSHRBatch());
        Test.stopTest();

    }
     @isTest static void testZSubscriptionProductChargeTriggerHandler() {
    
        
        Test.startTest();
        ZSubscriptionProductChargeTriggerHandler obj= new ZSubscriptionProductChargeTriggerHandler();
        
        Test.stopTest();

    }
    @isTest static void testUserActivationBatch() {
    
        
        Test.startTest();
        database.executebatch(new UserActivationBatch());
        
        Test.stopTest();

    }
    @istest static void testvvpaymentmanagement(){
         Test.startTest();
         vvPaymentManagement.Tmethod();
        vvPaymentManagement.Tmethod1();
         vvPaymentManagement.Tmethod2();
          vvPaymentManagement.Tmethod3();
           vvPaymentManagement.Tmethod4();

        
        Test.stopTest();
    }
    
}