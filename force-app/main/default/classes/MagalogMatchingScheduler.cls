global class MagalogMatchingScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		//MyBatchClass b = new MyBatchClass();
		database.executebatch(new MagalogMatchingBatch());
	}

	/*
	System.schedule('Magalog Matching Scheduler','0 30 23 * * ?', new MagalogMatchingScheduler());

	 */
}