/**
* batch file go through all magalog finder records daily. match them against account, delete matched items, insert magalog related record into account magalog object
*/
global class MagalogMatchingBatch implements Database.Batchable<sObject> {
    
    String query;
    
    global MagalogMatchingBatch() {
        this.query = 'SELECT Magalog_Id__c, FirstName__c, LastName__c, Street__c,WeekStartDate__c, PostalCode__c,Key_Type__c,Sequence_Number__c,BillingPostalCode__c FROM Magalog_Finder__c WHERE CreatedDate = LAST_N_DAYS:180';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Magalog_Finder__c> mflist) {
        Set<String> postals = new Set<String>();
        Set<String> streets = new Set<String>();
        Set<String> fnames = new Set<String>();
        Set<String> lnames = new Set<String>();
        Set<String> keyset = new Set<String>();
        Set<String> sequenceno = new Set<String>();
        Set<String> billingPostalCode = new Set<String>();
        List<Date> magalogDateList = new List<Date>();
        String regExp = '[-,._ ]';
        if(mflist.isEmpty() || mflist==null){return;}
        
        for (Magalog_Finder__c mf : mflist) {
            postals.add(mf.PostalCode__c + '%');
            streets.add(mf.Street__c + '%');
            fnames.add(mf.FirstName__c + '%');
            lnames.add(mf.LastName__c + '%');
            keyset.add(mf.Key_Type__c);
            sequenceno.add(mf.Sequence_Number__c + '%');
            billingPostalCode.add(mf.BillingPostalCode__c + '%');
            String tempDtae = mf.Magalog_Id__c.substring(3, 5)+'20'+mf.Magalog_Id__c.substring(5, 7);
            date magDate = mf.WeekStartDate__c;
           	magalogDateList.add(magDate);
        }
        List<Magalog__c> magaloglist = new List<Magalog__c>();	// insert list
        List<Magalog_Finder__c> mflist_to_delete = new List<Magalog_Finder__c>();
        Set<String> mfSetId = new Set<String>();// delete list
        List<Magalog_Subscription__c> magalogSubscriptionList = new List<Magalog_Subscription__c>(); // insert list
        // sample date 09-15-2018
        List<Date> SubscriptionDateMatchsList = new List<Date>();
        Date SubscriptionDateMatch;
        Datetime AccountDateMatch;
        Datetime lowAccDate;
        Date lowSubDate;
        for(Date startdate : magalogDateList){
            SubscriptionDateMatch = startdate;
            SubscriptionDateMatchsList.add(SubscriptionDateMatch);
            AccountDateMatch = Datetime.newInstanceGMT(SubscriptionDateMatch.year(), SubscriptionDateMatch.month(),SubscriptionDateMatch.day(),0,0,0);
            if(lowAccDate == null) {
                lowAccDate = AccountDateMatch;
            }else {
                if(lowAccDate > AccountDateMatch) {
                    lowAccDate = AccountDateMatch;
                }
            }
            if(lowSubDate == null) {
                lowSubDate = SubscriptionDateMatch;
            }else {
                if(lowSubDate > SubscriptionDateMatch) {
                    lowSubDate = SubscriptionDateMatch;
                }
            }
        }
        
        Map<Id,Magalog_Finder__c> AccountIdVsMagalog = new  Map<Id,Magalog_Finder__c>();
        //Map<Id,SBQQ__Subscription__c> subscriptionMap = new  Map<Id,SBQQ__Subscription__c>();
        //Ganesh 14-06-2019 :: Search subscription  from zuora account
        Map<Id,Zuora__Subscription__c> subscriptionMap = new  Map<Id,Zuora__Subscription__c>();
        Map<Id,Magalog_Finder__c> subscriptionVsMagalog = new  Map<Id,Magalog_Finder__c>();
       	// do a general query
        for (Account a : [SELECT 	FirstName, 
                          			LastName, 
                          			zip_code__c, 
                          			BillingStreet,
                          			CreatedDate 
                          FROM Account 
                          where LastName LIKE :lnames AND zip_code__c LIKE :billingPostalCode and createdDate >=: lowAccDate]) {
            // go through finder list again, find the account that match everything
           	for (Magalog_Finder__c mf : mflist) {
                //tight key
               	DateTime AccountCreatedDate = mf.WeekStartDate__c;
                if(!String.isBlank(mf.Key_Type__c) && mf.Key_Type__c.equalsIgnoreCase('k1')){
                    String lastName = !String.isBlank(mf.LastName__c)? mf.LastName__c :'';     
                    String street =  !String.isBlank(mf.Street__c)? mf.Street__c :'';
                    String zipcode = !String.isBlank(mf.BillingPostalCode__c)? mf.BillingPostalCode__c :'';
                    String firstName = !String.isBlank(mf.FirstName__c)? mf.FirstName__c.substring(0,1) :'';
                    String accLastName = !String.isBlank(a.LastName)? a.LastName :''; 
                    String accStreet = !String.isBlank(a.BillingStreet)? a.BillingStreet :'';
                    String accZipCode = !String.isBlank(a.zip_code__c)? a.zip_code__c :'';
                    String accFirstName = !String.isBlank(a.FirstName)? a.FirstName.substring(0,1) :'';
                    if(!String.isBlank((mf.FirstName__c))){
                        if(accLastName.containsIgnoreCase(lastName) && accStreet.containsIgnoreCase(street) && accZipCode.containsIgnoreCase(zipcode) && accFirstName.equalsIgnoreCase(firstName) && a.CreatedDate >= AccountCreatedDate){
                            if(!AccountIdVsMagalog.containsKey(a.id)){
                                AccountIdVsMagalog.put(a.id,mf); 
                                
                            }
                        }    
                    }else if(accLastName.containsIgnoreCase(lastName) && accStreet.containsIgnoreCase(street) && accZipCode.containsIgnoreCase(zipcode) && a.CreatedDate >= AccountCreatedDate){
                    	if(!AccountIdVsMagalog.containsKey(a.id)){
                            AccountIdVsMagalog.put(a.id,mf); 
                          
                        }
                    }
                }else if(!String.isBlank(mf.Key_Type__c) && mf.Key_Type__c.equalsIgnoreCase('k2')){ 
                    String lastName = !String.isBlank(mf.LastName__c)? mf.LastName__c :'';
                    String zipcode = !String.isBlank(mf.BillingPostalCode__c)? mf.BillingPostalCode__c :'';
                    String accLastName = !String.isBlank(a.LastName)? a.LastName :''; 
                    String accZipCode = !String.isBlank(a.zip_code__c)? a.zip_code__c :'';
                    if(accLastName.containsIgnoreCase(lastName) && accZipCode.containsIgnoreCase(zipcode) && a.CreatedDate >= AccountCreatedDate){
                        if(!AccountIdVsMagalog.containsKey(a.id)){
                            AccountIdVsMagalog.put(a.id,mf);
                            
                        }
                    }
                }
            }
        }
          
        Set<Id> magExistAccounts = new  Set<Id>();
        for(Magalog__c magalogDataList : [SELECT 	Account__c,
                                          			Id,
                                          			Magalog_Date__c,
                                          			Magalog_Id__c,
                                          			Name,Key_Type__c,
                                          			Start_Date__c 
                                          FROM Magalog__c where Account__c IN: AccountIdVsMagalog.keyset()]){
            if(!AccountIdVsMagalog.isEmpty() && AccountIdVsMagalog.containsKey(magalogDataList.Account__c)){
                magExistAccounts.add(magalogDataList.Account__c);
            }
        }
       
        for(Id accId :AccountIdVsMagalog.keyset()){
            if(!magExistAccounts.contains(accId)){
                Magalog__c m = new Magalog__c(Account__c = accId, 
                                              Magalog_Id__c = AccountIdVsMagalog.get(accId).Magalog_Id__c,
                                              Key_Type__c = AccountIdVsMagalog.get(accId).Key_Type__c,
                                              Start_Date__c = AccountIdVsMagalog.get(accId).WeekStartDate__c);
                magaloglist.add(m);	
                mfSetId.add(AccountIdVsMagalog.get(accId).Sequence_Number__c);
            }	    
        }
       	/*for(SBQQ__Subscription__c subscription :[SELECT 	Id,
                                                     		SBQQ__Account__r.FirstName,
                                                     		SBQQ__Account__r.LastName,
                                                     		SBQQ__Account__r.zip_code__c,
                                                     		SBQQ__Account__r.BillingStreet,
                                                     		SBQQ__StartDate__c,
                                                     		SBQQ__EndDate__c,
                                                     		Contract_Term__c,
                                                     		Sub_Product_Code__c,
                                                     		SBQQ__ProductName__c 
                                                     FROM SBQQ__Subscription__c 
                                                     where SBQQ__Account__r.LastName 
                                                     LIKE :lnames AND SBQQ__Account__r.zip_code__c LIKE :billingPostalCode 
                                                     			  AND  SBQQ__StartDate__c >=: lowSubDate
                                                    ]){
                for (Magalog_Finder__c mf : mflist) { 
                   	DateTime subscriptionCreatedDate = mf.WeekStartDate__c;
                    //tight key
                    if(!String.isBlank(mf.Key_Type__c) && mf.Key_Type__c.equalsIgnoreCase('k1')){
                       	String lastName = !String.isBlank(mf.LastName__c)? mf.LastName__c :'';     
                        String street =  !String.isBlank(mf.Street__c)? mf.Street__c :'';
                        String zipcode = !String.isBlank(mf.BillingPostalCode__c)? mf.BillingPostalCode__c :'';
                        String firstName = !String.isBlank(mf.FirstName__c)? mf.FirstName__c.substring(0,1) :'';
                        String subLastName = !String.isBlank(subscription.SBQQ__Account__r.LastName)? subscription.SBQQ__Account__r.LastName :''; 
                        String subStreet = !String.isBlank(subscription.SBQQ__Account__r.BillingStreet)? subscription.SBQQ__Account__r.BillingStreet :'';
                        String subZipCode = !String.isBlank(subscription.SBQQ__Account__r.zip_code__c)? subscription.SBQQ__Account__r.zip_code__c :'';
                        String subFirstName = !String.isBlank(subscription.SBQQ__Account__r.FirstName)? subscription.SBQQ__Account__r.FirstName.substring(0,1) :'';   
                       	if(!String.isBlank((mf.FirstName__c))){
                           	if(subLastName.containsIgnoreCase(lastName) && subStreet.containsIgnoreCase(street) && subZipCode.containsIgnoreCase(zipcode) && subFirstName.equalsIgnoreCase(firstName) && subscription.SBQQ__StartDate__c >= subscriptionCreatedDate){
                                if(!subscriptionMap.containsKey(subscription.id)){
                                    subscriptionMap.put(subscription.id,subscription); 
                                    if(!subscriptionVsMagalog.containsKey(subscription.id))
                                        subscriptionVsMagalog.put(subscription.id,mf);
                                }
                            }    
                        }else if(subLastName.containsIgnoreCase(lastName) && subStreet.containsIgnoreCase(street) && subZipCode.containsIgnoreCase(zipcode) && subscription.SBQQ__StartDate__c >= subscriptionCreatedDate){
                           	if(!subscriptionMap.containsKey(subscription.id)){
                                subscriptionMap.put(subscription.id,subscription); 
                                if(!subscriptionVsMagalog.containsKey(subscription.id))
                                    subscriptionVsMagalog.put(subscription.id,mf);
                            }
                        }
                    }else if(!String.isBlank(mf.Key_Type__c) && mf.Key_Type__c.equalsIgnoreCase('k2')){
                        String lastName = !String.isBlank(mf.LastName__c)? mf.LastName__c :'';
                        String zipcode = !String.isBlank(mf.BillingPostalCode__c)? mf.BillingPostalCode__c :'';
                        String subLastName = !String.isBlank(subscription.SBQQ__Account__r.LastName)? subscription.SBQQ__Account__r.LastName :''; 
                        String subZipCode = !String.isBlank(subscription.SBQQ__Account__r.zip_code__c)? subscription.SBQQ__Account__r.zip_code__c :'';   
                        if(subLastName.containsIgnoreCase(lastName) && subZipCode.containsIgnoreCase(zipcode) && subscription.SBQQ__StartDate__c >= subscriptionCreatedDate){
                            if(!subscriptionMap.containsKey(subscription.id)){
                                subscriptionMap.put(subscription.id,subscription); 
                                if(!subscriptionVsMagalog.containsKey(subscription.id))
                                    subscriptionVsMagalog.put(subscription.id,mf);
                            }
                        }
                    }
                 }
            }*/
        
        // Ganesh 14-06-2019 :: Search subscription  from zuora account
        for(Zuora__Subscription__c subscription :[SELECT 	Id,
                                                            Zuora__Account__r.LastName,
                                                            Zuora__Account__r.FirstName,
                                                            Zuora__Account__r.zip_code__c,
                                                            Zuora__Account__r.BillingStreet,
                                                            Zuora__TermStartDate__c,
                                                            Zuora__TermEndDate__c
                                                            FROM Zuora__Subscription__c 
                                                     		where Zuora__Account__r.LastName 
                                                            LIKE :lnames AND Zuora__Account__r.zip_code__c LIKE :billingPostalCode 
                                                            AND  Zuora__TermStartDate__c >=: lowSubDate]){
                                                                //System.debug('subscription--------'+subscription);
                for (Magalog_Finder__c mf : mflist) { 
                   	DateTime subscriptionCreatedDate = mf.WeekStartDate__c;
                    //tight key
                    if(!String.isBlank(mf.Key_Type__c) && mf.Key_Type__c.equalsIgnoreCase('k1')){
                       	String lastName = !String.isBlank(mf.LastName__c)? mf.LastName__c :'';     
                        String street =  !String.isBlank(mf.Street__c)? mf.Street__c :'';
                        String zipcode = !String.isBlank(mf.BillingPostalCode__c)? mf.BillingPostalCode__c :'';
                        String firstName = !String.isBlank(mf.FirstName__c)? mf.FirstName__c.substring(0,1) :'';
                        String subLastName = !String.isBlank(subscription.Zuora__Account__r.LastName)? subscription.Zuora__Account__r.LastName :''; 
                        String subStreet = !String.isBlank(subscription.Zuora__Account__r.BillingStreet)? subscription.Zuora__Account__r.BillingStreet :'';
                        String subZipCode = !String.isBlank(subscription.Zuora__Account__r.zip_code__c)? subscription.Zuora__Account__r.zip_code__c :'';
                        String subFirstName = !String.isBlank(subscription.Zuora__Account__r.FirstName)? subscription.Zuora__Account__r.FirstName.substring(0,1) :'';   
                       	if(!String.isBlank((mf.FirstName__c))){
                           	if(subLastName.containsIgnoreCase(lastName) && subStreet.containsIgnoreCase(street) && subZipCode.containsIgnoreCase(zipcode) && subFirstName.equalsIgnoreCase(firstName) && subscription.Zuora__TermStartDate__c >= subscriptionCreatedDate){
                                if(!subscriptionMap.containsKey(subscription.id)){
                                    subscriptionMap.put(subscription.id,subscription); 
                                    if(!subscriptionVsMagalog.containsKey(subscription.id))
                                        subscriptionVsMagalog.put(subscription.id,mf);
                                }
                            }    
                        }else if(subLastName.containsIgnoreCase(lastName) && subStreet.containsIgnoreCase(street) && subZipCode.containsIgnoreCase(zipcode) && subscription.Zuora__TermStartDate__c >= subscriptionCreatedDate){
                           	if(!subscriptionMap.containsKey(subscription.id)){
                                subscriptionMap.put(subscription.id,subscription); 
                                if(!subscriptionVsMagalog.containsKey(subscription.id))
                                    subscriptionVsMagalog.put(subscription.id,mf);
                            }
                        }
                    }else if(!String.isBlank(mf.Key_Type__c) && mf.Key_Type__c.equalsIgnoreCase('k2')){
                        String lastName = !String.isBlank(mf.LastName__c)? mf.LastName__c :'';
                        String zipcode = !String.isBlank(mf.BillingPostalCode__c)? mf.BillingPostalCode__c :'';
                        String subLastName = !String.isBlank(subscription.Zuora__Account__r.LastName)? subscription.Zuora__Account__r.LastName :''; 
                        String subZipCode = !String.isBlank(subscription.Zuora__Account__r.zip_code__c)? subscription.Zuora__Account__r.zip_code__c :'';   
                        if(subLastName.containsIgnoreCase(lastName) && subZipCode.containsIgnoreCase(zipcode) && subscription.Zuora__TermStartDate__c >= subscriptionCreatedDate){
                            if(!subscriptionMap.containsKey(subscription.id)){
                                subscriptionMap.put(subscription.id,subscription); 
                                if(!subscriptionVsMagalog.containsKey(subscription.id))
                                    subscriptionVsMagalog.put(subscription.id,mf);
                            }
                        }
                    }
                 }
            }
        System.debug('subscriptionMap------'+subscriptionMap); 
    	Set<Id> magExistSubscription = new  Set<Id>();
        // Ganesh 14-06-2019 :: Search subscription  from zuora account
       /* for(Magalog_Subscription__c magalogSubDataList : [SELECT 	Account__c,
                                                          			Id,
                                                          			Contract_Term__c,
                                                          			Product_Name__c,
                                                          			Sub_Product_Code__c,
                                                          			Subscription__c,
                                                          			Subscription_EndDate__c,
                                                          			Subscription_StartDate__c,
                                                          			Name ,
                                                          			Key_Type__c,Start_Date__c
                                                          FROM Magalog_Subscription__c 
                                                          where Subscription__c IN: subscriptionMap.keyset()]){*/
        // Ganesh 14-06-2019 :: get subscription data from Magalog_Subscription__c
        for(Magalog_Subscription__c magalogSubDataList : [SELECT 	Account__c,
                                                          Id,
                                                          Contract_Term__c,
                                                          Product_Name__c,
                                                          Sub_Product_Code__c,
                                                          Subscription__c,
                                                          Subscription_EndDate__c,
                                                          Subscription_StartDate__c,
                                                          Name ,
                                                          Key_Type__c,Start_Date__c,zoura_Subscription__c
                                                          FROM Magalog_Subscription__c 
                                                          where zoura_Subscription__c IN: subscriptionMap.keyset()]){
                                                              System.debug('magalogSubDataList-------'+magalogSubDataList);                     
                                                              if(!subscriptionMap.isEmpty() && subscriptionMap.containsKey(magalogSubDataList.zoura_Subscription__c)){
                                                                  magExistSubscription.add(magalogSubDataList.zoura_Subscription__c);
                                                              }
                                                          }
      	for(Id subId :subscriptionMap.keyset()){
            /*if(!magExistSubscription.contains(subId)){
                Magalog_Subscription__c magalogSubscription = new Magalog_Subscription__c();
                magalogSubscription.Name = 'MagaLogSub'+system.now().format().remove(' ');
                magalogSubscription.Account__c =subscriptionMap.get(subId).SBQQ__Account__c;
                magalogSubscription.Subscription__c = subId; 
                magalogSubscription.Contract_Term__c= subscriptionMap.get(subId).Contract_Term__c;
                magalogSubscription.Subscription_StartDate__c = subscriptionMap.get(subId).SBQQ__StartDate__c;
                magalogSubscription.Subscription_EndDate__c = subscriptionMap.get(subId).SBQQ__EndDate__c;
                magalogSubscription.Sub_Product_Code__c = subscriptionMap.get(subId).Sub_Product_Code__c;
                magalogSubscription.Product_Name__c = subscriptionMap.get(subId).SBQQ__ProductName__c;
                magalogSubscription.Key_Type__c = subscriptionVsMagalog.get(subId).Key_Type__c;
                magalogSubscription.Start_Date__c = subscriptionVsMagalog.get(subId).WeekStartDate__c;
                magalogSubscription.Magalog_Id__c = subscriptionVsMagalog.get(subId).Magalog_Id__c;
                magalogSubscriptionList.add(magalogSubscription);
                mfSetId.add(subscriptionVsMagalog.get(subId).Sequence_Number__c);
                
            }*/	
            // Ganesh 14-06-2019 :: Search subscription  from zuora account
            if(!magExistSubscription.contains(subId)){
                Magalog_Subscription__c magalogSubscription = new Magalog_Subscription__c();
                magalogSubscription.Name = 'MagaLogSub'+system.now().format().remove(' ');
                magalogSubscription.Account__c =subscriptionMap.get(subId).Zuora__Account__c;
                //magalogSubscription.Subscription__c = subId;
                // Ganesh 14-06-2019 :: assign zuora subscription id to magalog subcription.
                magalogSubscription.zoura_Subscription__c = subId;
                //magalogSubscription.Contract_Term__c= subscriptionMap.get(subId).Contract_Term__c;
                magalogSubscription.Subscription_StartDate__c = subscriptionMap.get(subId).Zuora__TermStartDate__c;
                magalogSubscription.Subscription_EndDate__c = subscriptionMap.get(subId).Zuora__TermEndDate__c;
                //magalogSubscription.Sub_Product_Code__c = subscriptionMap.get(subId).Sub_Product_Code__c;
                //magalogSubscription.Product_Name__c = subscriptionMap.get(subId).SBQQ__ProductName__c;
                magalogSubscription.Key_Type__c = subscriptionVsMagalog.get(subId).Key_Type__c;
                magalogSubscription.Start_Date__c = subscriptionVsMagalog.get(subId).WeekStartDate__c;
                magalogSubscription.Magalog_Id__c = subscriptionVsMagalog.get(subId).Magalog_Id__c;
                magalogSubscriptionList.add(magalogSubscription);
                mfSetId.add(subscriptionVsMagalog.get(subId).Sequence_Number__c);
                 System.debug('magalogSubscriptionList-------'+magalogSubscriptionList);
            }	
        }
       System.debug('magalogSubscriptionList-------'+magalogSubscriptionList);
        // insert magalog and delete the finder records
        for(Magalog_Finder__c mf : mflist ){
            if(mfSetId.contains(mf.Sequence_Number__c)){
            	mflist_to_delete.add(mf);    
            }    
        }
       	if (!magaloglist.isEmpty()) insert magaloglist;
        if (!magalogSubscriptionList.isEmpty()) insert magalogSubscriptionList;
        if (!mflist_to_delete.isEmpty()) delete mflist_to_delete;
    } 
    
    global void finish(Database.BatchableContext BC) {
        MagalogCleanerBatch mcb = new MagalogCleanerBatch();  
        Database.ExecuteBatch(mcb);
    }
    
   
}