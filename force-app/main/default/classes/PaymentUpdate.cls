public class PaymentUpdate {
    
    public PaymentUpdate(){
        List<PaymentMethod__c> updateList = new List<PaymentMethod__c>();    
        List<PaymentMethod__c> PaymentList =[SELECT PaymentId__c,CreditCardType__c FROM PaymentMethod__c];   
        for(PaymentMethod__c payments : PaymentList){
            //payment.CreditCardType__c = 'MasterCard';
            //updateList.add(payment);
            system.debug('payments: '+payments);
            Zuora.zObject payment = new Zuora.zObject('PaymentMethod');
            Payment.setValue('Id', payments.PaymentId__c); 
            payment.setValue('CreditCardType', payments.CreditCardType__c);
            system.debug('payment: '+payment);
            
            String errors = '';
            Zuora.zApi.SaveResult[] results = zuoraAPIHelper.updateAPICall(payment);
            system.debug('Check : Update Results: '+results);
            if(results.size()>0){
            for (Zuora.zApi.SaveResult result : results) {
                if (result.success){
                    system.debug(' result.Id: '+ result.Id);
                }
                else{
                    errors += (errors != '' ? ',' : '') + zuoraAPIHelper.getErrorStr(result.errors);
                    system.debug(' errors '+ errors);
                }
            }
            }
            
            
        }
    }
}