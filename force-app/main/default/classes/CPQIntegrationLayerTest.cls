@isTest
public class CPQIntegrationLayerTest {
    Public  testmethod  static void testCPQIntegrationLayer(){
        Test.StartTest();
        CPQIntegrationLayer obj = new CPQIntegrationLayer();
        obj.getProductInfo('test');
        obj.getProductInfo(new List<String>(), 'test');
        obj.getAccountInfo('test');
        obj.createAccount();
        obj.createOpportunity();
        obj.createQuote();
        obj.createAccountOppQuote();
        obj.AddQuoteLineToQuote(new SBQQ__Quote__c(),'test', new List<Product2>());
        obj.CreateContractfromOpprotunity();
        Test.StopTest();
    }
}